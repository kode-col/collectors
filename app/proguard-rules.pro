# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/maximefimov/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Begin: Google Play services
-keep class com.google.** { *;}
-keep interface com.google.** { *;}
-dontwarn com.google.**
# End: Google Play services

# Begin: Flurry
-keep class com.flurry.** { *; }
-dontwarn com.flurry.**

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}
# End: Flurry

# Begin: Gson
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
# End: Gson

# Begin: Retrofit and Picasso
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn retrofit.appengine.UrlFetchClient
-dontwarn com.squareup.okhttp.**
-dontwarn rx.**
-dontwarn okio.**
-dontwarn retrofit.**
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}
-keepattributes EnclosingMethod
# End: Retrofit and Picasso

# Begin: EventBus
-keepclassmembers class ** {
    public void onEvent*(**);
}
# End: EventBus

# Begin: Crashlytics
-keepattributes SourceFile,LineNumberTable
# End: Crashlytics


# Begin: Card IO
-keep class io.card.**
-keepclassmembers class io.card.** {
*;
}
# End: Card IO

-keepclassmembers class ** {
    public void onOperationFinished*(**);
}

# Begin: OrmLite
-keep class com.j256.**
-keepclassmembers class com.j256.** { *; }
-keep enum com.j256.**
-keepclassmembers enum com.j256.** { *; }
-keep interface com.j256.**
-keepclassmembers interface com.j256.** { *; }

-keepclassmembers class * {
    public <init>(android.content.Context);
 }

-keep class com.lifecorp.collector.model.**
-keepclassmembers class com.lifecorp.collector.model.** { *; }
-keep class com.lifecorp.collector.network.**
-keepclassmembers class com.lifecorp.collector.network.** { *; }

# End: OrmLite
