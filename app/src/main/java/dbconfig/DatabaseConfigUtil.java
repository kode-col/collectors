package dbconfig;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.lifecorp.collector.model.database.objects.DBActionResultType;
import com.lifecorp.collector.model.database.objects.DBActionType;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBCoordinate;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.database.objects.DBImageUpload;
import com.lifecorp.collector.model.database.objects.DBMoney;
import com.lifecorp.collector.model.database.objects.DBPhoneNumber;
import com.lifecorp.collector.model.database.objects.DBQueue;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {

    private static final Class<?>[] classes = new Class[] {
            DBCoordinate.class,
            DBDebtorProfile.class,
            DBEvent.class,
            DBMoney.class,
            DBCollection.class,
            DBPhoneNumber.class,
            DBQueue.class,
            DBSection.class,
            DBSectionField.class,
            DBActionType.class,
            DBActionResultType.class,
            DBImageUpload.class,
            DBCollectionImageUpload.class,
            DBEventImageUpload.class
    };

    public static void main(String[] args) throws Exception {
        writeConfigFile("ormlite_config.txt", classes);
    }

}
