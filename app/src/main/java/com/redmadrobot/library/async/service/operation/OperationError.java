package com.redmadrobot.library.async.service.operation;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Ошибка, возникающая при выполнении операции.
 *
 * @author maximefimov
 */
public final class OperationError implements Serializable {

    private static final long serialVersionUID = -3042686055658047285L;

    /**
     * Краткое описание ошибки.
     */
    private final String mMessage;

    /**
     * Исключение, породившее ошибку.
     */
    private final Exception mException;

    /**
     * Конструктор. При передаче исключения, пытается взять из него описание ошибки.
     *
     * @param exception Исключение, породившее ошибку.
     */
    public OperationError(@NonNull Exception exception) {
        mException = exception;
        final String exceptionMessage = exception.getMessage();
        if (exceptionMessage == null || exceptionMessage.isEmpty()) {
            mMessage = "Unknown error";
        } else {
            mMessage = exceptionMessage;
        }
    }

    /**
     * Геттер описания ошибки.
     *
     * @return Краткое описание ошибки.
     */
    @NonNull
    public String getMessage() {
        return mMessage;
    }

    /**
     * Геттер породившего ошибку исключения.
     *
     * @return Исключение, породившее ошибку.
     */
    @NonNull
    public Exception getException() {
        return mException;
    }
}
