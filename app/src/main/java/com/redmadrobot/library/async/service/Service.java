package com.redmadrobot.library.async.service;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.library.async.service.cache.OperationCache;
import com.redmadrobot.library.async.service.operation.Operation;
import com.redmadrobot.library.async.service.operation.OperationError;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;

/**
 * Сервис, выполняющий асинхронные операции.
 * Синглтон.
 *
 * @author maximefimov
 */
final class Service {

    /**
     * Константа, определяющая, может ли класс быть использован без явного указания конфигурации.
     *
     * @see com.redmadrobot.library.async.service.ServiceConfiguration
     */
    private final static boolean IS_NO_CONFIG_ALLOWED = true;

    /**
     * ID последней запущенной операции. Static потому что ID должны быть уникальный в рамках
     * одного
     * запуска приложения. Atomic потому что создание может произойти в любом потоке.
     */
    private final static AtomicInteger mLastOperationId = new AtomicInteger(0);

    /**
     * Статический инстанс класса.
     */
    private static Service sInstance;

    /**
     * Шина событий, через которую класс доставляет результат.
     */
    private final EventBus mEventBus = EventBus.getDefault();

    /**
     * Сервис, создающий асинхронные потоки, в которых делается основная работа.
     */
    private final ExecutorService mExecutorService = Executors.newCachedThreadPool();

    /**
     * Кэш результатов операций. Хранит только успешные результаты.
     */
    private final OperationCache mOperationCache;


    /**
     * Приватный конструктор, чтобы не создавать лишних инстансов.
     *
     * @param configuration конфигурация создаваемого сервиса
     */
    private Service(@NonNull ServiceConfiguration configuration) {
        mOperationCache = configuration.getOperationCache();
    }

    /**
     * Выполнение операции в отдельном потоке.
     *
     * @param operation       операция, которую необходимо выполнить.
     * @param operationResult экземпляр результата операции, который будет наполнен фактическими
     *                        данными по завершении асинхронной работы.
     * @param <Input>         тип входного параметра операции.
     * @param <Output>        тип выходного параметра операции.
     */
    private <Input, Output> void doAsync(@NonNull final Operation<Input, Output> operation,
            @NonNull final OperationResult<Output> operationResult) {
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                final int id = operationResult.getId();
                final Output cachedOutput = getCachedOutput(operation);
                if (cachedOutput != null) {
                    operationResult.setOutput(cachedOutput);
                } else {
                    runForResult(operation, operationResult);
                }
                mEventBus.post(operationResult);
                RunningOperationStorage.getInstance().operationFinished(id);
            }
        });
    }

    /**
     * Синхронное выполнение операции.
     *
     * @param operation       операция, которую необходимо выполнить.
     * @param operationResult экземпляр результата операции, который будет наполнен фактическими
     *                        данными по завершении асинхронной работы.
     * @param <Input>         тип входного параметра операции.
     * @param <Output>        тип выходного параметра операции.
     */
    private <Input, Output> void doSync(@NonNull final Operation<Input, Output> operation,
            @NonNull final OperationResult<Output> operationResult) {
        final int id = operationResult.getId();
        final Output cachedOutput = getCachedOutput(operation);
        if (cachedOutput != null) {
            operationResult.setOutput(cachedOutput);
        } else {
            runForResult(operation, operationResult);
        }
        RunningOperationStorage.getInstance().operationFinished(id);
    }

    /**
     * Поиск и возвращение кешированого результата операции.
     *
     * @param operation операция, результат которой ищется в кеше.
     * @param <Input>   тип входного параметра операции.
     * @param <Output>  тип выходного параметра операции.
     * @return объект типа <b>Input</b>, если в кеше есть совпадение, <b>null</b> в противном
     * случае.
     * @see #mOperationCache
     * @see com.redmadrobot.library.async.service.cache.OperationCache
     */
    @Nullable
    private <Input, Output> Output getCachedOutput(
            @NonNull final Operation<Input, Output> operation) {
        if (operation.canReadFromCache()) {
            return mOperationCache.get(operation);
        } else {
            return null;
        }
    }

    /**
     * Прямое выполнение операции с целью получения результата.
     *
     * @param operation       операция, которую необходимо выполнить.
     * @param operationResult экземпляр результата операции, который будет наполнен фактическими
     *                        данными по завершении работы метода.
     * @param <Input>         тип входного параметра операции.
     * @param <Output>        тип выходного параметра операции.
     */
    private <Input, Output> void runForResult(@NonNull final Operation<Input, Output> operation,
            @NonNull final OperationResult<Output> operationResult) {
        try {
            final Output output = operation.run();
            operationResult.setOutput(output);
            if (operation.canWriteToCache()) {
                mOperationCache.put(operation, output);
            }
        } catch (Exception e) {
            operationResult.setOperationError(new OperationError(e));
        }
    }

    /**
     * Статический инициализатор, выполняющий настройку сервиса ДО начала работы.
     * Если <b>IS_NO_CONFIG_ALLOWED</b> установлен в <b>true</b>, то вызов этого метода должен
     * предшествовать первому вызову <b>getInstance()</b>.
     *
     * @param configuration сервис будет создан с этой конфигурацией
     * @see #IS_NO_CONFIG_ALLOWED
     * @see #getInstance()
     */
    static synchronized void init(@NonNull ServiceConfiguration configuration) {
        sInstance = new Service(configuration);
    }

    /**
     * Статический геттер единственного инстанса.
     * Если <b>IS_NO_CONFIG_ALLOWED</b> установлен в <b>true</b>, то перед вызовом этого метода
     * нужно вызвать <b>init(ServiceConfiguration)</b>
     *
     * @return единственный инстанс класса.
     * @see #IS_NO_CONFIG_ALLOWED
     * @see #init(ServiceConfiguration)
     * @see com.redmadrobot.library.async.service.ServiceConfiguration
     */
    @NonNull
    static synchronized Service getInstance() {
        if (sInstance == null) {
            if (IS_NO_CONFIG_ALLOWED) {
                ServiceConfiguration.setDefaultConfiguration();
            } else {
                throw new IllegalStateException(
                        "Call ServiceConfiguration.setDefaultConfiguration or ServiceConfiguration.setConfiguration before calling Service.getInstance");
            }
        }

        return sInstance;
    }

    /**
     * Повторение операции.
     *
     * @param operationResult результат операции, которую необходимо повторить.
     * @param <Output>        тип выходного параметра операции.
     * @return ID запуска операции повторения.
     * @see #run(com.redmadrobot.library.async.service.operation.Operation)
     */
    @SuppressWarnings({"unused"})
    final <Output> int repeatOperation(@NonNull OperationResult<Output> operationResult) {
        final Operation<?, Output> operation = operationResult.getOperation();
        if (operation != null) {
            return run(operation);
        } else {
            throw new IllegalArgumentException("operationResult has null operation field");
        }
    }

    /**
     * Запуск операции.
     *
     * @param operation операция, которую необходимо выполнить.
     * @param <Input>   тип входного параметра операции.
     * @param <Output>  тип выходного параметра операции.
     * @return ID запуска операции.
     * @see #repeatOperation(OperationResult)
     */
    final <Input, Output> int run(@NonNull final Operation<Input, Output> operation) {
        final int id = mLastOperationId.incrementAndGet();
        OperationResult<Output> operationResult;
        try {
            // here is a "unchecked" warning, but we are pretty sure, the instance is fine
            operationResult = operation.getResultClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(
                    "OperationResult implementations must have public constructor with no arguments");
        }
        operationResult.setId(id);
        operationResult.setOperation(operation);

        RunningOperationStorage.getInstance().operationStarted(id);
        doAsync(operation, operationResult);
        return id;
    }

    /**
     * Запуск синхронной операции.
     *
     * @param operation операция, которую необходимо выполнить.
     * @param <Input>   тип входного параметра операции.
     * @param <Output>  тип выходного параметра операции.
     * @return OperationResult<Output> результат операции.
     * @see #repeatOperation(OperationResult)
     */
    final <Input, Output> OperationResult<Output> runSync(
            @NonNull final Operation<Input, Output> operation) {
        final int id = mLastOperationId.incrementAndGet();
        OperationResult<Output> operationResult;
        try {
            // here is a "unchecked" warning, but we are pretty sure, the instance is fine
            operationResult = operation.getResultClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(
                    "OperationResult implementations must have public constructor with no arguments");
        }
        operationResult.setId(id);
        operationResult.setOperation(operation);

        RunningOperationStorage.getInstance().operationStarted(id);
        doSync(operation, operationResult);

        return operationResult;
    }
}
