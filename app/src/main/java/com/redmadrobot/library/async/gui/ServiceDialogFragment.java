package com.redmadrobot.library.async.gui;

import com.redmadrobot.library.async.gui.helpers.fragment.ServiceFragmentHelper;
import com.redmadrobot.library.async.service.operation.Operation;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Диалоговый фрагмент, имеющий связь с сервисом.
 *
 * @author maximefimov
 */
public abstract class ServiceDialogFragment extends DialogFragment {

    private final ServiceFragmentHelper mServiceFragmentHelper = new ServiceFragmentHelper();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceFragmentHelper.onCreate(this, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mServiceFragmentHelper.onResume();
    }

    @Override
    public void onPause() {
        mServiceFragmentHelper.onPause();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mServiceFragmentHelper.onSaveInstanceState(outState);
    }

    @SuppressWarnings("unused")
    protected final void runSingleInstanceOperation(
            @NonNull Operation operation,
            @NonNull String tag) {
        mServiceFragmentHelper.runOperation(operation, tag);
    }

    @SuppressWarnings("unused")
    protected final void runOperation(
            @NonNull Operation operation) {
        mServiceFragmentHelper.runOperation(operation);
    }
}
