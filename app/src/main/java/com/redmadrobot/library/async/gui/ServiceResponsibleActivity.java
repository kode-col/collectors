package com.redmadrobot.library.async.gui;

/**
 * Интерфейс, обозначающий что реализующй его объект(скорее всего - активити) будет отвечать
 * требованиям по предотвращению
 * утечек памяти при использовании сервиса выполнения асинхронных операций.
 *
 * @author maximefimov
 */
public interface ServiceResponsibleActivity {

    public int getActivityId();
}
