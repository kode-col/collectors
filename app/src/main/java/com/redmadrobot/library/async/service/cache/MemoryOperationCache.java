package com.redmadrobot.library.async.service.cache;

import com.redmadrobot.library.async.service.operation.Operation;

import android.support.annotation.NonNull;
import android.util.LruCache;


/**
 * Реализация кеша операций, сохраняющая данные в оперативной памяти.
 *
 * @author maximefimov
 */
public final class MemoryOperationCache extends SceletalOperationCache {

    private final static int MAX_SIZE = 4 * 1024 * 1024; // 4MiB

    private final LruCache<Operation<?, ?>, Object> mLruCache;

    public MemoryOperationCache() {
        mLruCache = new LruCache<>(MAX_SIZE);
    }

    @Override
    public <Output> void put(@NonNull Operation<?, Output> operation, @NonNull Output output) {
        mLruCache.put(operation, output);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <Output> Output get(@NonNull Operation<?, Output> operation) {
        // suppressing warning because it's user authority to make proper "equals implementations"
        // and if he does, it's impossible to put a wrong entry
        return (Output) mLruCache.get(operation);
    }

    @Override
    public void remove(@NonNull Operation<?, ?> operation) {
        mLruCache.remove(operation);
    }

    @Override
    public void clear() {
        mLruCache.evictAll();
    }
}
