package com.redmadrobot.library.async.gui.helpers.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.support.annotation.NonNull;

/**
 * Вспомогательный класс для установки связи Fragment с сервисом выполнения асинхронных операций.
 *
 * @author maximefimov
 */
public final class ServiceFragmentHelper extends ServiceGeneralFragmentHelper<Fragment> {

    @Override
    protected Activity getActivity(@NonNull Fragment fragment) {
        return fragment.getActivity();
    }
}
