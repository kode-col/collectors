package com.redmadrobot.library.async.service;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Класс, управляющий связью GUI с сервисом асинхронных операций посредствам промежуточных классов.
 * Синглтон.
 * <br><br>
 * Используйте его в активити, применяя методы<br>
 * {@link #activityCreated()}<br>
 * {@link #activityDestroyed(int)}<br>
 * чтобы обеспечить правильную работу с памятью.
 * <br><br>
 * В любом классе для выполнения асинхронных операций, используйте методы<br>
 * {@link #createConnector()}<br>
 * {@link #createConnector(int)}<br>
 *
 * для создания новых объектов, связывающих GUI и сервис асинхронных операций, а также методы<br>
 * {@link #getConnector(int)}<br>
 * {@link #getConnector(int, int)}<br>
 *
 * для полученее ранее созданных объектов.
 *
 * <br><br>
 * В качестве примера и/или базовых классов активити и фрагмента используйте:
 * <br><br>
 * {@link com.redmadrobot.library.async.gui.ServiceActivity}<br>
 * {@link com.redmadrobot.library.async.gui.ServiceSupportActivity}<br>
 * {@link com.redmadrobot.library.async.gui.ServiceFragment}<br>
 * {@link com.redmadrobot.library.async.gui.ServiceSupportFragment}<br>
 *
 * @author maximefimov
 */
public final class ServiceManager {

    /**
     * Включение/выключение дебажного лога.
     *
     * @see #logd(String)
     */
    private final static boolean LOG_ENABLED = false;

    /**
     * Таг системного лога.
     */
    private final static String LOG_TAG = "ServiceManager";

    /**
     * ID виртуальной активити для тех объектов, которые не передают ID своей активити при запросе
     * на создание {@link com.redmadrobot.library.async.service.ServiceConnector}.
     */
    private final static int NO_ACTIVITY_ID = -1;

    /**
     * Время, спустя которое, после вызова метода {@link #activityDestroyed(int)} удаляются все
     * {@link com.redmadrobot.library.async.service.ServiceConnector}, созданные с ID этой
     * активити.
     *
     * @see #startDeath(int)
     * @see #stopDeath(int)
     */
    private final static long ACTIVITY_DEATH_CONFIRM_MILLS = 3 * 60 * 1000;

    /**
     * Статический инстанс класса.
     */
    private final static ServiceManager sInstance = new ServiceManager();

    /**
     * Приватный конструктор, чтобы не создавать лишних инстансов.
     */
    private ServiceManager() {
        mConnectors = new HashMap<>();
        mDeathTimers = new HashMap<>();
    }

    /**
     * Статический геттер единственного инстанса.
     *
     * @return единственный инстанс класса.
     */
    @NonNull
    public static ServiceManager getInstance() {
        return sInstance;
    }

    /**
     * ID для следующего созданного {@link com.redmadrobot.library.async.service.ServiceConnector}.
     * Static потому что ID должны быть уникальный в рамках
     * одного
     * запуска приложения. Atomic потому что создание может произойти в любом потоке.
     *
     * @see #createConnector()
     * @see #createConnector(int)
     */
    private final AtomicInteger mNextConnectorId = new AtomicInteger(0);

    /**
     * ID для следующей активити, вызвавшей {@link #activityCreated()}.
     * Static потому что ID должны быть уникальный в рамках
     * одного
     * запуска приложения. Atomic потому что создание может произойти в любом потоке.
     */
    private final AtomicInteger mNextActivityId = new AtomicInteger(0);

    /**
     * Хранилище созданных {@link com.redmadrobot.library.async.service.ServiceConnector}, где
     * ключи
     * - ID, полученные объектами при создании.
     */
    private final Map<Integer, ServiceConnector> mConnectors;

    /**
     * Запущенные таймеры, уничтожиющие {@link com.redmadrobot.library.async.service.ServiceConnector}
     * из {@link #mConnectors} по истечении времени. Ключ - ID активити, смерть которой
     * инициировала
     * таймер. По завершении работы таймера удаляются только те объекты, которые были созданы с
     * соответствующим ID активити.
     *
     * @see #activityDestroyed(int)
     */
    private final Map<Integer, Timer> mDeathTimers;

    /**
     * Этот метод должна вызывать активити, будучи впервые созданной.
     *
     * @return ID активити в данном классе, нужен для привязке с ней {@link
     * com.redmadrobot.library.async.service.ServiceConnector} и дальнейшей праивльной обработки
     * памяти.
     * @see #activityDestroyed(int)
     * @see #createConnector(int)
     */
    public final int activityCreated() {
        return mNextActivityId.getAndIncrement();
    }

    /**
     * Этот метод должна вызвать активити перед своим уничтожением.
     *
     * @param activityId ID, выданный активити в методе {@link #activityCreated()}
     */
    public final synchronized void activityDestroyed(int activityId) {
        startDeath(activityId);
    }

    /**
     * Метод стартует таймер, по истечении которого все {@link com.redmadrobot.library.async.service.ServiceConnector}
     * созданные с передаваемым ID активити, будут уничтожены.
     * Метод отменяет все предыдущие запущенные таймеры сметри для данного ID активити.
     *
     * @param activityId ID, выданный активити в методе {@link #activityCreated()}
     * @see #mConnectors
     * @see #mDeathTimers
     */
    private void startDeath(final int activityId) {
        stopDeath(activityId);
        logd("start death " + activityId);

        final List<Integer> connectorsKeysToDelete = new LinkedList<>();
        for (Map.Entry<Integer, ServiceConnector> entry : mConnectors.entrySet()) {
            if (entry.getValue().getActivityId() == activityId) {
                connectorsKeysToDelete.add(entry.getKey());
            }
        }

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (Integer key : connectorsKeysToDelete) {
                    deleteConnector(key);
                }
                logd("end death " + activityId);
            }
        }, ACTIVITY_DEATH_CONFIRM_MILLS);

        mDeathTimers.put(activityId, timer);
    }

    /**
     * Метод отменяет уничтожение {@link com.redmadrobot.library.async.service.ServiceConnector}
     * созданных с передаваемым ID активити.
     *
     * @param activityId ID, выданный активити в методе {@link #activityCreated()}
     * @see #startDeath(int)
     * @see #mDeathTimers
     */
    private void stopDeath(int activityId) {
        logd("stop death " + activityId);
        final Timer timer = mDeathTimers.get(activityId);
        if (timer != null) {
            timer.cancel();
            mDeathTimers.remove(activityId);
        }
    }

    /**
     * Метод создает {@link com.redmadrobot.library.async.service.ServiceConnector} без привязки к
     * активити.
     *
     * @return созданный экземпляр {@link com.redmadrobot.library.async.service.ServiceConnector}.
     * @see #createConnector(int)
     * @see #getConnector(int)
     */
    @NonNull
    public final synchronized ServiceConnector createConnector() {
        return createConnector(NO_ACTIVITY_ID);
    }

    /**
     * Метод создает {@link com.redmadrobot.library.async.service.ServiceConnector} с привязкой к
     * активити по ID.
     *
     * @param activityId ID, выданный активити в методе {@link #activityCreated()}
     * @return созданный экземпляр {@link com.redmadrobot.library.async.service.ServiceConnector}.
     * Жизненный цикл этого объекта связан с жизненным циклом активити, к которой он привязан.
     * @see #createConnector()
     * @see #getConnector(int, int)
     * @see #activityCreated()
     * @see #activityDestroyed(int)
     */
    @NonNull
    public final synchronized ServiceConnector createConnector(int activityId) {
        stopDeath(activityId);
        final int connectorId = mNextConnectorId.getAndIncrement();
        final ServiceConnector result = new ServiceConnector(connectorId, activityId);

        mConnectors.put(connectorId, result);

        return result;
    }

    /**
     * Метод возвращает ранее созданный {@link com.redmadrobot.library.async.service.ServiceConnector},
     * не привязанный к активити.
     *
     * @param id ID полученный при вызове {@link #createConnector()}
     * @return объект {@link com.redmadrobot.library.async.service.ServiceConnector}, если объект с
     * таким ID был создан, <b>null</b> в противном случае.
     */
    @NonNull
    public final synchronized ServiceConnector getConnector(int id) {
        return getConnector(id, NO_ACTIVITY_ID);
    }


    /**
     * Метод возвращает ранее созданный {@link com.redmadrobot.library.async.service.ServiceConnector},
     * привязанный к активити по ее ID.
     *
     * @param id         ID полученный при вызове {@link #createConnector(int)}
     * @param activityId ID, выданный активити в методе {@link #activityCreated()}
     * @return объект {@link com.redmadrobot.library.async.service.ServiceConnector}. Если такой
     * объект существовал, возвращается он сам, иначе объект создается. Второй вариант возможен
     * если
     * активити давно умерла, а теперь восстанавливается из фона.
     */
    @NonNull
    public final synchronized ServiceConnector getConnector(int id, int activityId) {
        ServiceConnector serviceConnector = mConnectors.get(id);
        if (serviceConnector != null) {
            stopDeath(serviceConnector.getActivityId());
        } else {
            serviceConnector = new ServiceConnector(id, activityId);
            mConnectors.put(id, serviceConnector);
        }
        return serviceConnector;
    }

    /**
     * Метод уничтожает {@link com.redmadrobot.library.async.service.ServiceConnector} с заданным
     * ID. Предварительно высвобождаются все занятые удаляемым объектом ресурсы.
     *
     * @param id ID полученный при вызове {@link #createConnector()} либо {@link
     *           #createConnector(int)}
     * @return <b>true</b> если объект был удален, <b>false</b> если объекта с таким ID не
     * существует.
     */
    private synchronized boolean deleteConnector(int id) {
        final ServiceConnector serviceConnector = mConnectors.get(id);
        if (serviceConnector == null) {
            return false;
        }
        serviceConnector.releaseResources();
        mConnectors.remove(id);

        return true;
    }

    /**
     * Метод выводит отладочное сообщение в лог, если включена опция логирования отладки.
     *
     * @param message сообщение для вывода в лог.
     * @see #LOG_ENABLED
     */
    private void logd(@NonNull String message) {
        if (LOG_ENABLED) {
            Log.d(LOG_TAG, this.toString() + " " + message);
        }
    }
}
