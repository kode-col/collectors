package com.redmadrobot.library.async.service;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Класс, предаставляющий хранилище запущенных операций.
 * Используется для создания операций, и их возврата соответствующим клиентам.
 * Синглтон.
 *
 * @author maximefimov
 */
final class RunningOperationStorage {

    /**
     * Статический инстанс класса.
     */
    private final static RunningOperationStorage sInstance = new RunningOperationStorage();

    /**
     * Приватный конструктор, чтобы не создавать лишних инстансов.
     */
    private RunningOperationStorage() {
    }

    /**
     * Статический геттер единственного инстанса.
     *
     * @return единственный инстанс класса.
     */
    @NonNull
    public static RunningOperationStorage getInstance() {
        return sInstance;
    }

    /**
     * Список ID запущенных операций в момент времени.
     * Синхронизирован, потому что стартовать операция может из любого потока.
     */
    private final List<Integer> mPendingOperations = Collections
            .synchronizedList(new LinkedList<Integer>());

    /**
     * Регистрация операции, как только что запущенной.
     * @param id ID операции
     */
    public final void operationStarted(int id) {
        mPendingOperations.add(id);
    }

    /**
     * Регистрация операции как только что завершенной.
     * @param id ID операции
     */
    public final void operationFinished(int id) {
        mPendingOperations.remove(Integer.valueOf(id));
    }

    /**
     * Проверка - запущщена ли операция.
     *
     * @see #operationStarted(int)
     * @see #operationFinished(int)
     * @param id ID операции
     * @return <b>true</b> если запущена, <b>false</b> в противном случае
     */
    public final boolean isOperationRunning(int id) {
        return mPendingOperations.contains(Integer.valueOf(id));
    }

}
