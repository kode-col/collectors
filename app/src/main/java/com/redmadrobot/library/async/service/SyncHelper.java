package com.redmadrobot.library.async.service;

import com.redmadrobot.library.async.service.operation.Operation;

import android.support.annotation.NonNull;

/**
 * Вспомогательный класс для выполнение синхронных запросов.
 *
 * @author dz
 */
public class SyncHelper {

    public static <Input, Output> OperationResult<Output> run(
            @NonNull final Operation<Input, Output> operation) {
        return Service.getInstance().runSync(operation);
    }

}
