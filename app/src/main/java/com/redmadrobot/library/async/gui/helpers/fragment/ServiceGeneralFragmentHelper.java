package com.redmadrobot.library.async.gui.helpers.fragment;

import com.redmadrobot.library.async.gui.ServiceResponsibleActivity;
import com.redmadrobot.library.async.service.ServiceConnector;
import com.redmadrobot.library.async.service.ServiceManager;
import com.redmadrobot.library.async.service.operation.Operation;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Общий вспомогательный класс для установки связи фрагмента с сервисом выполнения асинхронных операций.
 *
 * @author maximefimov
 */
abstract class ServiceGeneralFragmentHelper<FragmentClass> {

    private final static String TAG = "ServiceFragmentHelper";

    private final static String KEY_SERVICE_CONNECTOR = "service_connector";

    private ServiceConnector mServiceConnector;

    private FragmentClass mFragment;

    public final void onCreate(@NonNull FragmentClass fragment, @Nullable Bundle savedInstanceState) {
        mFragment = fragment;
        final Activity activity = getActivity(fragment);
        if(activity == null){
            throw new IllegalStateException("onCreate called when no Activity is attached");
        }
        Integer activityId = null;
        if (activity instanceof ServiceResponsibleActivity) {
            activityId = ((ServiceResponsibleActivity) activity).getActivityId();
        } else {
            Log.w(TAG,
                    "Attached activity does not implement ServiceResponsibleActivity. Memory leaks may occur.");
        }

        if (savedInstanceState != null) {
            if (activityId != null) {
                mServiceConnector = ServiceManager.getInstance()
                        .getConnector(savedInstanceState.getInt(KEY_SERVICE_CONNECTOR), activityId);
            } else {
                mServiceConnector = ServiceManager.getInstance()
                        .getConnector(savedInstanceState.getInt(KEY_SERVICE_CONNECTOR));
            }
        } else {
            if (activityId != null) {
                mServiceConnector = ServiceManager.getInstance().createConnector(activityId);
            } else {
                mServiceConnector = ServiceManager.getInstance().createConnector();
            }
        }
    }

    @Nullable
    protected abstract Activity getActivity(@NonNull FragmentClass fragment);

    public final void onResume() {
        mServiceConnector.onResume(mFragment);
    }

    public final void onPause() {
        mServiceConnector.onPause();
    }

    public final void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_SERVICE_CONNECTOR, mServiceConnector.getId());
    }

    public final void runOperation(@NonNull Operation operation, @NonNull String tag) {
        mServiceConnector.invoke(operation, tag);
    }

    public final void runOperation(@NonNull Operation operation) {
        mServiceConnector.invoke(operation);
    }
}
