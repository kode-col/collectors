package com.redmadrobot.library.async.service;

import com.redmadrobot.library.async.service.cache.NoCache;
import com.redmadrobot.library.async.service.cache.OperationCache;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Конфигурация сервиса выполнения асинхронных операций.
 *
 * @author maximefimov
 */
public final class ServiceConfiguration {

    /**
     * Конфигурация сервиса по умолчанию.
     */
    private final static ServiceConfiguration DEFAULT;

    static {
        final Builder builder = new Builder();
        DEFAULT = builder.build();
    }

    /**
     * Установка конфигурации сервиса по умолчанию.
     *
     * Если {@link com.redmadrobot.library.async.service.Service#IS_NO_CONFIG_ALLOWED} установлена
     * в
     * <b>false</b>, то обязательно нужно вызывать этот метод или {@link
     * #setConfiguration(ServiceConfiguration)}
     * до использования сервиса.
     *
     * @see #setConfiguration(ServiceConfiguration)
     */
    public static void setDefaultConfiguration() {
        setConfiguration(DEFAULT);
    }

    /**
     * Установка конфигурации сервиса.
     *
     * Если {@link com.redmadrobot.library.async.service.Service#IS_NO_CONFIG_ALLOWED} установлена
     * в
     * <b>false</b>, то обязательно нужно вызывать этот метод или {@link
     * #setDefaultConfiguration()}
     * до использования сервиса.
     *
     * @param serviceConfiguration конфигурация сервиса, который будет выполнять все асинхронные
     *                             операции.
     * @see #setDefaultConfiguration()
     */
    public static void setConfiguration(@NonNull ServiceConfiguration serviceConfiguration) {
        Service.init(serviceConfiguration);
    }

    /**
     * Используемый кеш операций.
     */
    private final OperationCache mOperationCache;

    /**
     * Приватный конструктор, поскольку для создания инстанса используется паттерн <b>Builder</b>
     *
     * @param operationCache кеш операций
     */
    private ServiceConfiguration(@NonNull
    OperationCache operationCache) {
        mOperationCache = operationCache;
    }

    /**
     * Геттер кеша операций.
     *
     * @return выбранный кеш операций.
     */
    @NonNull
    public OperationCache getOperationCache() {
        return mOperationCache;
    }

    /**
     * Порождающий класс-строитель.
     *
     * Используется паттерн <b>Builder</b>
     */
    public final static class Builder {

        /**
         * Используемый кеш операций.
         */
        private OperationCache mOperationCache;

        /**
         * Сборка инстанса {@link com.redmadrobot.library.async.service.ServiceConfiguration}.
         * Использует инициализированные поля, остальные заполняет на свое усмотрение.
         *
         * @return соответствующий инициализированным параметрам инстанс {@link
         * com.redmadrobot.library.async.service.ServiceConfiguration}
         */
        @NonNull
        public ServiceConfiguration build() {
            final OperationCache operationCache = (mOperationCache == null) ? new NoCache()
                    : mOperationCache;
            return new ServiceConfiguration(operationCache);
        }

        /**
         * Геттер кеша операций.
         *
         * @return выбранный кеш операций.
         */
        @Nullable
        public OperationCache getOperationCache() {
            return mOperationCache;
        }

        /**
         * Сеттер кеша операций.
         *
         * @param operationCache кеш операций.
         */
        public void setOperationCache(@Nullable OperationCache operationCache) {
            mOperationCache = operationCache;
        }
    }
}
