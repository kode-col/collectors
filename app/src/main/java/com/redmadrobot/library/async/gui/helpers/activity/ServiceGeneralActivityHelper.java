package com.redmadrobot.library.async.gui.helpers.activity;

import com.redmadrobot.library.async.gui.ServiceResponsibleActivity;
import com.redmadrobot.library.async.service.ServiceConnector;
import com.redmadrobot.library.async.service.ServiceManager;
import com.redmadrobot.library.async.service.operation.Operation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Общий вспомогательный класс для установки связи активити с сервисом выполнения асинхронных операций.
 *
 * @author maximefimov
 */
abstract class ServiceGeneralActivityHelper<ActivityClass> {

    private final static String TAG = "ServiceActivityHelper";

    private final static String KEY_SERVICE_CONNECTOR = "service_connector";

    private final static String KEY_ACTIVITY_ID = "activity_id";

    private ServiceConnector mServiceConnector;

    private int mServiceActivityId;

    private ActivityClass mActivity;

    public final void onCreate(@NonNull ActivityClass activity, @Nullable Bundle savedInstanceState) {
        mActivity = activity;
        if (savedInstanceState != null) {
            mServiceActivityId = savedInstanceState.getInt(KEY_ACTIVITY_ID);
            mServiceConnector = ServiceManager.getInstance()
                    .getConnector(savedInstanceState.getInt(KEY_SERVICE_CONNECTOR), mServiceActivityId);
        } else {
            if (!(activity instanceof ServiceResponsibleActivity)) {
                Log.w(TAG,
                        "Activity does not implement ServiceResponsibleActivity. Memory leaks may occur.");
            }

            mServiceActivityId = ServiceManager.getInstance().activityCreated();
            mServiceConnector = ServiceManager.getInstance().createConnector(mServiceActivityId);
        }
    }

    public final void onResume() {
        mServiceConnector.onResume(mActivity);
    }

    public final void onPause() {
        mServiceConnector.onPause();
    }

    public final void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(KEY_SERVICE_CONNECTOR, mServiceConnector.getId());
        outState.putInt(KEY_ACTIVITY_ID, mServiceActivityId);
    }

    public final void onDestroy() {
        ServiceManager.getInstance().activityDestroyed(mServiceActivityId);
    }

    public final int getServiceActivityId() {
        return mServiceActivityId;
    }

    public final void runOperation(@NonNull Operation operation, @NonNull String tag) {
        mServiceConnector.invoke(operation, tag);
    }

    public final void runOperation(@NonNull Operation operation) {
        mServiceConnector.invoke(operation);
    }
}
