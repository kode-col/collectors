package com.redmadrobot.library.async.service;

import com.redmadrobot.library.async.service.operation.Operation;

import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Модуль связи GUI с сервисом выполнения асинхронных операций.
 *
 * @author maximefimov
 */
public final class ServiceConnector {

    /**
     * Включение/выключение дебажного лога.
     *
     * @see #logd(String)
     */
    private final static boolean LOG_ENABLED = false;

    /**
     * Таг системного лога.
     */
    private final static String LOG_TAG = "ServiceConnector";

    /**
     * Имя метода, который будет вызываться в классе-клиенте при доставлении результата.
     * В качестве параметра метода будет передан инстанс класса Output-параметра операции.
     *
     * @see #onResume(Object)
     * @see com.redmadrobot.library.async.service.operation.Operation
     */
    public final static String CALLBACK_METHOD_NAME = "onOperationFinished";

    /**
     * Внутреннее состояние объекта. В состоянии {@link com.redmadrobot.library.async.service.ServiceConnector.State#PAUSED}
     * данные будут сохраняться до смены состояния, в состоянии {@link
     * com.redmadrobot.library.async.service.ServiceConnector.State#RESUMED} -
     * доставляться клиенту.
     */
    private State mState = State.PAUSED;

    /**
     * Шина событий, через которые объект получает результаты операций от сервиса.
     *
     * @see com.redmadrobot.library.async.service.Service#mEventBus
     */
    private final EventBus mEventBus = EventBus.getDefault();

    /**
     * Уникальный ID объекта.
     *
     * @see com.redmadrobot.library.async.service.ServiceManager#createConnector()
     * @see com.redmadrobot.library.async.service.ServiceManager#createConnector(int)
     */
    private final int mId;


    /**
     * Уникальный ID активити, к циклу жизни которой привязан объект.
     *
     * @see ServiceManager#activityCreated()
     */
    private final int mActivityId;

    /**
     * Список ID запусков операций, созданных с ключем прекеширования.
     *
     * @see #invoke(com.redmadrobot.library.async.service.operation.Operation, String)
     * @see com.redmadrobot.library.async.service.Service#run(com.redmadrobot.library.async.service.operation.Operation)
     */
    private final Map<String, Integer> mTaggedRequests = new HashMap<>();

    /**
     * Список ID запусков операций, созданных без ключа прекеширования.
     *
     * @see #invoke(com.redmadrobot.library.async.service.operation.Operation)
     * @see com.redmadrobot.library.async.service.Service#run(com.redmadrobot.library.async.service.operation.Operation)
     */
    private final List<Integer> mUntaggedRequests = new LinkedList<>();

    /**
     * Список полученных результатов операций. Наполняется, пока объект находится в состоянии
     * {@link
     * com.redmadrobot.library.async.service.ServiceConnector.State#PAUSED}. Очищается, когда
     * объект
     * переходит в состояние {@link com.redmadrobot.library.async.service.ServiceConnector.State#RESUMED}
     * через доставку результатов клиенту.
     */
    private final List<OperationResult<?>> mStoredResults = new LinkedList<>();

    /**
     * Клиент объекта, который инициирует запуск операций и получает от объекта их результат, когда
     * находится в необходимом состоянии.
     */
    private Object mServiceListener;


    /**
     * Конструктор default видимости, чтобы не позволить пользователям создавать инстансы извне.
     *
     * @param id         уникальный ID инстанса. Назначается в методе {@link
     *                   com.redmadrobot.library.async.service.ServiceManager#createConnector(int)}
     * @param activityId уникальны ID активити, в жизненном цикле которой существует объект.
     *                   Получить этот ID можно с помощью метода {@link ServiceManager#activityCreated()}
     * @see com.redmadrobot.library.async.service.ServiceManager
     */
    ServiceConnector(int id, int activityId) {
        mId = id;
        mActivityId = activityId;
        mEventBus.register(this);
        if (LOG_ENABLED) {
            Log.d(LOG_TAG, "ServiceConnector with id=" + id + " was created");
        }
    }

    /**
     * В этом методе должны освобождаться все ресурсы, захваченные объектом. После выхова этого
     * метода, объект не будет переиспользован, его уждет удаление и вечное забвение.
     *
     * @see com.redmadrobot.library.async.service.ServiceManager#deleteConnector(int)
     */
    void releaseResources() {
        mEventBus.unregister(this);
        mServiceListener = null;
        mTaggedRequests.clear();
        mUntaggedRequests.clear();
        mStoredResults.clear();
        logd("released resources");
    }

    /**
     * Геттер ID.
     *
     * @return уникальный в рамках запуска приложения ID объекта.
     * @see ServiceManager#createConnector()
     * @see ServiceManager#createConnector(int)
     * @see ServiceManager#getConnector(int)
     * @see ServiceManager#getConnector(int, int)
     */
    public int getId() {
        return mId;
    }

    /**
     * Геттер ID активити, к коотрой привязан этот объект.
     *
     * @return уникальный в рамках запуска приложения ID активити.
     * @see ServiceManager#activityCreated()
     * @see com.redmadrobot.library.async.service.ServiceManager#activityDestroyed(int)
     */
    public int getActivityId() {
        return mActivityId;
    }

    /**
     * Этот метод необходимо вызвать, когда клиентский объект начинает быть способным обрабатывать
     * результаты операций.
     *
     * @param serviceListener Клиентский объект, которому будут перенаправлены результаты операций
     *                        в
     *                        UI потоке до вызова {@link #onPause()}
     */
    public void onResume(@NonNull Object serviceListener) {
        logd("onResume");
        mServiceListener = serviceListener;
        mState = State.RESUMED;

        if (!mStoredResults.isEmpty()) {
            logd("has undelivered results");
            final List<OperationResult<?>> oldResults = new ArrayList<>(mStoredResults);
            mStoredResults.clear();
            for (OperationResult<?> result : oldResults) {
                deliverResult(result);
            }
            logd("no more undelivered results");
        } else {
            logd("has no undelivered results");
        }
    }

    /**
     * Этот метод необходимо вызвать, когда клиент перестает быть способным обрабатывать результаты
     * операций. После вызова клиент перестает получать результаты, они сохраняются в объекте и
     * будут обработаны, когда клиент вызовет {@link #onResume(Object)}.
     * По умолчанию считается, что клиентом был вызван этот метод.
     */
    public void onPause() {
        logd("onPause");
        mState = State.PAUSED;
        mServiceListener = null;
    }

    /**
     * Этот метод служит для получения результатов запущенных операций от сервиса. <b>Пользователь
     * никогда не должен вызывать этот метод сам.</b> Видимость public обусловлена выбранной
     * технологией передачи сообщений.
     *
     * @param operationResult результат, полученный от сервиса.
     */
    @SuppressWarnings("unused")
    public final void onEventMainThread(OperationResult<?> operationResult) {
        final int operationId = operationResult.getId();

        boolean shouldNotice = false;

        if (mTaggedRequests.containsValue(operationId)) {
            shouldNotice = true;
        }

        if (mUntaggedRequests.contains(operationResult.getId())) {
            mUntaggedRequests.remove(Integer.valueOf(operationId));
            shouldNotice = true;
        }

        if (shouldNotice) {
            logd("noticing result " + operationResult);
            onOperationFinished(operationResult);
        }
    }

    /**
     * Запуск операции без прекеширования результата. Полученный результат будет либо доставлен
     * объекту-клиенту сразу же (если последним был вызван {@link #onResume(Object)}), либо
     * сохранен
     * до времени, когда клиент сможет его обработать (если последним был вызван {@link
     * #onPause()})
     *
     * @param operation операция, которую необходимо запустить.
     * @see #invoke(com.redmadrobot.library.async.service.operation.Operation, String)
     */
    public final void invoke(@NonNull Operation operation) {
        logd("invoking untagged operation");
        mUntaggedRequests.add(Service.getInstance().run(operation));
    }

    /**
     * Запуск операции с ключом прекеширования. Метод нужен для исключения одновременного вызова
     * нескольких однотипных операций. Полученный результат будет либо доставлен
     * объекту-клиенту сразу же (если последним был вызван {@link #onResume(Object)}), либо
     * сохранен
     * до времени, когда клиент сможет его обработать (если последним был вызван {@link
     * #onPause()})
     *
     * @param operation операция, которую необходимо запустить.
     * @param tag       ключ прекеширования. Одновременно может быть запущенна только одна операция
     *                  с данным ключем.
     * @see #invoke(com.redmadrobot.library.async.service.operation.Operation)
     */
    public final void invoke(@NonNull Operation operation, @NonNull String tag) {
        logd("invoking tagged operation, tag=" + tag);
        final Integer savedId = mTaggedRequests.get(tag);
        if (savedId != null && RunningOperationStorage.getInstance().isOperationRunning(savedId)) {
            //ничего не делаем, операция уже выполняется
            logd("operation with tag=" + tag + " is running, do nothing");
            return;
        }

        logd("operation with tag=" + tag + " is not running, start it");
        mTaggedRequests.put(tag, Service.getInstance().run(operation));
    }

    /**
     * Этот метод определяет, кто нужно сделать с результатом - доставить клиенту (последний вызван
     * {@link #onResume(Object)}), либо сохранить до
     * лучших времен (последний вызван {@link #onPause()}).
     *
     * @param operationResult результат, полученный от сервиса.
     */
    private void onOperationFinished(@NonNull OperationResult<?> operationResult) {
        logd("onOperationFinished " + operationResult);
        switch (mState) {
            case PAUSED:
                storeResult(operationResult);
                break;
            case RESUMED:
                deliverResult(operationResult);
                break;
            default:
                throw new IllegalStateException("Unknown state: " + mState);
        }
    }

    /**
     * Сохранение результата внутри объекта. Результат будет доставлен клиенту после вызова {@link
     * #onResume(Object)} с помощью метода {@link #deliverResult(OperationResult)}.
     *
     * @param operationResult результат, полученный от сервиса.
     * @see #onPause()
     */
    private void storeResult(@NonNull OperationResult<?> operationResult) {
        logd("storeResult " + operationResult);
        mStoredResults.add(operationResult);
    }

    /**
     * Доставка результата операции клиенту. После вызова этого метода будет произведен поиск
     * метода клиента, соответствующего сигнатуре
     * <br><b>public void {@link #CALLBACK_METHOD_NAME}(OperationResult.getClass())</b>.<br>
     * Если такой метод не будет найден, в лог будет выведено сообщение с описанием проблемы.
     * <br>Метод может и должен быть вызван только между вызовами {@link #onResume(Object)} и
     * {@link
     * #onPause()}.
     *
     * @param operationResult результат, полученный от сервиса, либо восстановленный из списка
     *                        сохзраненных результатов.
     */
    private void deliverResult(@NonNull OperationResult<?> operationResult) {
        logd("deliverResult " + operationResult);
        final Class listenerClass = mServiceListener.getClass();
        final Method[] listenerMethods = listenerClass.getMethods();

        Method callbackMethod = null;

        final Class resultClass = operationResult.getClass();
        for (Method method : listenerMethods) {
            if (isCallback(method, resultClass)) {
                callbackMethod = method;
                try {
                    callbackMethod.invoke(mServiceListener, operationResult);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    Log.w(LOG_TAG, Log.getStackTraceString(e));
                }
            }
        }

        if (callbackMethod == null) {
            Log.w(LOG_TAG,
                    "Operation result (id=" + operationResult.getId() + "; class=" + operationResult
                            .getClass().getName() + ") was obtained, but there is no method in "
                            + mServiceListener + " to get it"
            );
            Log.w(LOG_TAG, "Method should look like");
            Log.w(LOG_TAG,
                    "public void " + CALLBACK_METHOD_NAME + "(" + resultClass.getName()
                            + " result) {}"
            );
        }
    }

    /**
     * Метод проверяет, является ли передаваемый параметр-метод подходящим для возврата результата
     * операции.
     *
     * @param method      проверяемый метод.
     * @param resultClass класс-результат, для возврата которого ищется метод.
     * @return <b>true</b> если метод подходит, <b>false</b> в противном случае.
     */
    private static boolean isCallback(@NonNull Method method, @NonNull Class<?> resultClass) {
        if (method.getName().equals(CALLBACK_METHOD_NAME)) {
            if (method.getReturnType() == Void.TYPE) {
                final Class<?>[] parameters = method.getParameterTypes();
                if (parameters.length == 1 && parameters[0].isAssignableFrom(resultClass)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Метод выводит отладочное сообщение в лог, если включена опция логирования отладки.
     *
     * @param message сообщение для вывода в лог.
     * @see #LOG_ENABLED
     */
    private void logd(@NonNull String message) {
        if (LOG_ENABLED) {
            Log.d(LOG_TAG, this.toString() + " " + message);
        }
    }

    /**
     * Удобная строковая репрезентация объекта.
     *
     * @return строка с описанием объекта.
     */
    @Override
    public String toString() {
        return "ServiceConnector[id=" + getId() + "]";
    }

    /**
     * Внутреннее состояние объекта.
     *
     * @see #mState
     * @see #onResume(Object)
     * @see #onPause()
     */
    private enum State {
        PAUSED,
        RESUMED
    }
}
