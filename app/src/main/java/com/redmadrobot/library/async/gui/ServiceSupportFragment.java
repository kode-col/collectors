package com.redmadrobot.library.async.gui;

import com.redmadrobot.library.async.gui.helpers.fragment.ServiceSupportFragmentHelper;
import com.redmadrobot.library.async.service.operation.Operation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

/**
 * Created by maximefimov on 01.09.14.
 *
 * Фрагмент, имеющий связь с сервисом.
 */
public abstract class ServiceSupportFragment extends Fragment {

    private final ServiceSupportFragmentHelper mServiceSupportFragmentHelper
            = new ServiceSupportFragmentHelper();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceSupportFragmentHelper.onCreate(this, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mServiceSupportFragmentHelper.onResume();
    }

    @Override
    public void onPause() {
        mServiceSupportFragmentHelper.onPause();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mServiceSupportFragmentHelper.onSaveInstanceState(outState);
    }

    @SuppressWarnings("unused")
    protected final void runSingleInstanceOperation(
            @NonNull Operation operation,
            @NonNull String tag) {
        mServiceSupportFragmentHelper.runOperation(operation, tag);
    }

    @SuppressWarnings("unused")
    protected final void runOperation(
            @NonNull Operation operation) {
        mServiceSupportFragmentHelper.runOperation(operation);
    }
}
