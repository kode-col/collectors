package com.redmadrobot.library.async.gui.helpers.activity;

import android.app.Activity;

/**
 * Вспомогательный класс для установки связи Activity с сервисом выполнения асинхронных операций.
 *
 * @author maximefimov
 */
public final class ServiceActivityHelper extends ServiceGeneralActivityHelper<Activity> {

}
