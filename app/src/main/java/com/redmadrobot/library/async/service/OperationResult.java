package com.redmadrobot.library.async.service;

import android.support.annotation.Nullable;

import com.redmadrobot.library.async.service.operation.Operation;
import com.redmadrobot.library.async.service.operation.OperationError;

/**
 * Общий результат операции.
 * Может содержать как как полученный результат, так и ошибку, не позволившую получить его.<br>
 * <b>Подклассы должны иметь public конструктор без аргументов!</b>
 *
 * @param <ResultType> тип результата, созданного операцией.
 * @author maximefimov
 * @see com.redmadrobot.library.async.service.operation.Operation#getResultClass()
 */
public abstract class OperationResult<ResultType> {

    /**
     * ID операции, результатом которой является объект
     */
    private int mId;

    /**
     * Операция, результатом которой является этот объект.
     */
    private Operation<?, ResultType> mOperation;

    /**
     * Результат операции. Может быть <b>null</b>, если при выполнении операции вызвалось
     * исключение.
     */
    private ResultType mOutput;

    /**
     * Ошибка выполнения операции. Может быть <b>null</b>, если при выполнении операции не
     * вызвалось исключение.
     */
    private OperationError mOperationError;

    /**
     * Геттер ID операции.
     *
     * @return ID операции, результатом которой является объект.
     */
    public int getId() {
        return mId;
    }

    /**
     * <b>Служебный метод, клиент класса не должен вызывать или переопределять этот метод!</b><br>
     * Сеттер ID операции.
     *
     * @param id ID операции, результатом которой является объект.
     */
    void setId(int id) {
        mId = id;
    }


    /**
     * Геттер результата операции.
     *
     * @return Результат операции. Может быть <b>null</b>, если при выполнении операции вызвалось
     * исключение.
     */
    public ResultType getOutput() {
        return mOutput;
    }

    /**
     * <b>Служебный метод, клиент класса не должен вызывать или переопределять этот метод!</b><br>
     * Сеттер результата операции.
     *
     * @param output Результат операции.
     */
    void setOutput(@Nullable ResultType output) {
        mOutput = output;
    }

    /**
     * Геттер ошибки выполнения операции.
     *
     * @return Ошибка выполнения операции. Может быть <b>null</b>, если при выполнении операции не
     * вызвалось исключение.
     */
    public OperationError getOperationError() {
        return mOperationError;
    }

    /**
     * <b>Служебный метод, клиент класса не должен вызывать или переопределять этот метод!</b><br>
     * Сеттер ошибки выполнения операции.
     *
     * @param operationError Ошибка выполнения операции.
     */
    void setOperationError(@Nullable OperationError operationError) {
        mOperationError = operationError;
    }

    /**
     * Геттер операции, породившей этот результат. Может понадобиться для повторения операции.
     *
     * @return Операция, результатом которой является этот объект.
     */
    @Nullable
    public Operation<?, ResultType> getOperation() {
        return mOperation;
    }

    /**
     * <b>Служебный метод, клиент класса не должен вызывать или переопределять этот метод!</b><br>
     * Сеттер операции, породившей этот объект.
     *
     * @param operation Операция, результатом которой является этот объект.
     */
    void setOperation(@Nullable Operation<?, ResultType> operation) {
        mOperation = operation;
    }

    /**
     * Геттер описания ошибки выполнения операции. Может быть <b>null</b>, если при выполнении
     * операции не
     * вызвалось исключение.
     *
     * @return Текстовое описание ошибки.
     */
    @Nullable
    public String getErrorMessage() {
        return mOperationError == null ? null : mOperationError.getMessage();
    }

    /**
     * Метод говорит, успешно ли выполнилась операция.
     *
     * @return <b>true</b> если операция выполнилась успешно, <b>false</b> в противном случае.
     */
    public final boolean isSuccessful() {
        return mOperationError == null;
    }


}
