package com.redmadrobot.library.async.gui.helpers.fragment;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

/**
 * Вспомогательный класс для установки связи Fragment из support библиотеки с сервисом выполнения
 * асинхронных операций.
 *
 * @author maximefimov
 */
public final class ServiceSupportFragmentHelper extends ServiceGeneralFragmentHelper<Fragment> {

    @Override
    protected Activity getActivity(@NonNull Fragment fragment) {
        return fragment.getActivity();
    }
}
