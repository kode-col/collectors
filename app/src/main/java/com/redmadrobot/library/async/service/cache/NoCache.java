package com.redmadrobot.library.async.service.cache;

import com.redmadrobot.library.async.service.operation.Operation;

import android.support.annotation.NonNull;

/**
 * Реализация кеша операций, которая, фактически, является отсутствием кеша как такового.
 *
 * @author maximefimov
 */
public final class NoCache extends SceletalOperationCache {

    @Override
    public <Output> void put(@NonNull Operation<?, Output> operation, @NonNull Output output) {

    }

    @Override
    public <Output> Output get(@NonNull Operation<?, Output> operation) {
        return null;
    }

    @Override
    public void remove(@NonNull Operation<?, ?> operation) {

    }

    @Override
    public void clear() {

    }
}
