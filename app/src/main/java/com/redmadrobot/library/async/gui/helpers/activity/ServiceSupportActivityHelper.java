package com.redmadrobot.library.async.gui.helpers.activity;

import android.support.v4.app.FragmentActivity;

/**
 * Вспомогательный класс для установки связи FragmentActivity с сервисом выполнения асинхронных
 * операций.
 *
 * @author maximefimov
 */
public final class ServiceSupportActivityHelper
        extends ServiceGeneralActivityHelper<FragmentActivity> {

}
