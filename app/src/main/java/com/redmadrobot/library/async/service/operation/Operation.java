package com.redmadrobot.library.async.service.operation;


import android.support.annotation.NonNull;

import com.redmadrobot.library.async.service.OperationResult;

/**
 * Операция - функциональный объект, превращающий Input в Output.<br>
 * Используется через наследование и описание в конкретных классах-операциях.
 *
 * @param <Input>  Тип входящего параметра.
 * @param <Output> Тип результата операции.
 * @author maximefimov
 * @see com.redmadrobot.library.async.service.ServiceConnector#invoke(Operation)
 * @see com.redmadrobot.library.async.service.ServiceConnector#invoke(Operation, String)
 */
public abstract class Operation<Input, Output> {

    /**
     * Входящий параметр.
     */
    protected final Input mRequest;

    /**
     * Защищенный конструктор, необходимо вызвать в классах-наследниках.
     *
     * @param request значение входящего параметра.
     */
    protected Operation(@NonNull Input request) {
        mRequest = request;
    }

    /**
     * Защищенный конструктор, необходимо вызвать в классах-наследниках.
     */
    protected Operation(){
        mRequest = null;
    }

    /**
     * Метод выполняет основную работу операции. Может содержать выполнение методов, тормозящих
     * поток, в котором они выполняются.
     * Может выбрасывать любые исключения.
     *
     * @return результат выполнения операции.
     */
    public abstract Output run();

    /**
     * Метод возвращает объект-класс, инстансы которого будут возвращены клиентам {@link
     * com.redmadrobot.library.async.service.ServiceConnector} в методах обратного вызова при
     * завершении выполнения операции. Это нужно для того, чтобы клиент мог прописать метод,
     * обрабатывающий результат конкретной операции.
     *
     * @return класс объекта-результата операции.
     */
    @NonNull
    public abstract Class<? extends OperationResult<Output>> getResultClass();

    /**
     * Метод говорит о том, можно ли повторить операцию.<br>
     * По умолчанию <b>true</b>.
     *
     * @return <b>true</b>, если операция повторяемая, <b>false</b> в обратном случае.
     */
    public boolean isRepeatable() {
        return true;
    }

    /**
     * Метод говорит о том, можно ли кешировать результат операции в глобальном кеше
     * приложения.<br>
     * По умолчанию <b>false</b>.<br>
     * При переопределении метода с целью разрешить кеширование переопределяйте методы {@link
     * #hashCode()} и  {@link #equals(Object)}, поскольку они используюстя в качестве ключа для
     * кеша.
     *
     * @return <b>true</b>, если кешировать результат нужно, <b>false</b> в обратном случае.
     */
//    public boolean isCacheable() {
//        return false;
//    }

    public boolean canReadFromCache(){
        return false;
    }

    public boolean canWriteToCache(){
        return false;
    }
}