package com.redmadrobot.library.async.gui;

import com.redmadrobot.library.async.gui.helpers.activity.ServiceActivityHelper;
import com.redmadrobot.library.async.service.operation.Operation;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by maximefimov on 01.09.14.
 *
 * Активити, имеющая связь с сервисом.
 */
public abstract class ServiceActivity extends Activity
        implements ServiceResponsibleActivity {

    private final ServiceActivityHelper mServiceSupportActivityHelper
            = new ServiceActivityHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceSupportActivityHelper.onCreate(this, savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mServiceSupportActivityHelper.onResume();
    }

    @Override
    protected void onPause() {
        mServiceSupportActivityHelper.onPause();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mServiceSupportActivityHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        mServiceSupportActivityHelper.onDestroy();
        super.onDestroy();
    }

    @Override
    public int getActivityId() {
        return mServiceSupportActivityHelper.getServiceActivityId();
    }

    @SuppressWarnings("unused")
    protected final void runServiceRequest(
            @NonNull Operation operation,
            @NonNull String tag) {
        mServiceSupportActivityHelper.runOperation(operation, tag);
    }

    @SuppressWarnings("unused")
    protected final void runServiceRequest(
            @NonNull Operation operation) {
        mServiceSupportActivityHelper.runOperation(operation);
    }
}
