package com.redmadrobot.library.async.service.cache;

import com.redmadrobot.library.async.service.operation.Operation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Интерфес кэша операций.
 *
 * @author maximefimov
 */
public interface OperationCache {

    /**
     * Сохранить результат операции в кеше.
     *
     * @param operation операция, результат которой сохраняется.
     * @param output    сохраняемый результат.
     * @param <Output>  тип сохраняемого результата.
     */
    public <Output> void put(@NonNull Operation<?, Output> operation, @NonNull Output output);

    /**
     * Получить ранее сохраненный результат операции из кеша.
     *
     * @param operation операция, результат которой требуется найти.
     * @param <Output>  тип результата.
     * @return <b>объект класса Output</b> если совпадение в кеше найдено, <b>null</b> в противном
     * случае.
     */
    @Nullable
    public <Output> Output get(@NonNull Operation<?, Output> operation);

    /**
     * Проверка наличия результата операции в кеше.
     *
     * @param operation операция, результат которой требуется найти.
     * @return <b>true</b>, если в кеше есть результат операции, <b>false</b> в противном случае.
     */
    public boolean has(@NonNull Operation<?, ?> operation);

    /**
     * Удаление сохраненного результата операции.
     *
     * @param operation операция, результат которой требуется удалить.
     */
    public void remove(@NonNull Operation<?, ?> operation);

    /**
     * Полная очистка кеша от всех сохраненных данных.
     */
    public void clear();
}
