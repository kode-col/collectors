package com.redmadrobot.library.async.service.cache;

import com.redmadrobot.library.async.service.operation.Operation;

import android.support.annotation.NonNull;

/**
 * Скелетная реализация кеша операций.
 *
 * @author maximefimov
 */
public abstract class SceletalOperationCache implements OperationCache {

    @Override
    public boolean has(@NonNull Operation<?, ?> operation) {
        return get(operation) != null;
    }
}
