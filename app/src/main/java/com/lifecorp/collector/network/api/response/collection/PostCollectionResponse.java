package com.lifecorp.collector.network.api.response.collection;

import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.model.objects.collection.Collection;

import java.util.List;

/**
 * Created by Alexander Smirnov on 18.03.15.
 *
 * Респонс для создания коллекции
 */
public class PostCollectionResponse extends CollectionsListResponse {

    public PostCollectionResponse(List<Collection> mCollectionItems) {
        super(mCollectionItems);
    }

    @Override
    public String getResponseType() {
        return Const.TAG_POST_COLLECTION;
    }

}
