package com.lifecorp.collector.network.operations.api.collection;

import android.support.annotation.NonNull;

import com.lifecorp.collector.model.database.helpers.DBFront;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.requests.ImageUploadRequest;
import com.lifecorp.collector.network.api.response.LoadImageResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.lifecorp.collector.utils.ImageFileHelper;
import com.redmadrobot.library.async.service.OperationResult;

/**
 * Created by Alexander Smirnov on 24.04.15.
 *
 * Операция загрузки изображения на сервер
 */
public class LoadImageOperation extends OperationApi<ImageUploadRequest, LoadImageResponse> {

    public LoadImageOperation(@NonNull CollectorApi collectorApi,
                              @NonNull ImageUploadRequest request) {
        super(collectorApi, request);
    }

    @NonNull
    @Override
    protected LoadImageResponse runServerRequest() {
        LoadImageResponse response;

        if (mRequest.getDbCollectionImageUpload() != null) {
            response = collectorApi.loadCollectionImage(mRequest.getId(), mRequest.getAccessToken(),
                    mRequest.getLatitude(), mRequest.getLongitude(), mRequest.getFileData());
        } else if (mRequest.getDbEventImageUpload() != null) {
            response = collectorApi.loadEventImage(mRequest.getId(), mRequest.getAccessToken(),
                    mRequest.getLatitude(), mRequest.getLongitude(), mRequest.getFileData());
        } else {
            throw new IllegalStateException("Не установлен объект для загрузки!");
        }

        saveResponseIsValid(response);

        return response;
    }

    @Override
    protected void process(LoadImageResponse serverResponse) {
        if (mRequest.getDbCollectionImageUpload() != null) {
            DBCollectionImageUpload image = mRequest.getDbCollectionImageUpload();

            if (image.getDbImageUpload() != null) {
                ImageFileHelper.removeImageFile(image.getDbImageUpload().getFilePath());
                DBFront.getInstance().removeDBCollectionImageUpload(image);
            }
        } else if(mRequest.getDbEventImageUpload() != null) {
            DBEventImageUpload image = mRequest.getDbEventImageUpload();

            if (image.getDbImageUpload() != null) {
                ImageFileHelper.removeImageFile(image.getDbImageUpload().getFilePath());
                DBFront.getInstance().removeDBEventImageUpload(image);
            }
        }
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<LoadImageResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<LoadImageResponse> {

    }

}
