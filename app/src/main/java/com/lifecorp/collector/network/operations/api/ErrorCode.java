package com.lifecorp.collector.network.operations.api;

import android.support.annotation.NonNull;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;

public enum ErrorCode {

    UNKNOWN(0),//Неизвестная ошибка
    TIMEOUT(
            10),//Возникает, когда запрос выполняется слишком долго
    BAD_REQUEST(
            400),//Возникает, когда все параметры указаны, но результат не корректен. В error_message возвращается описание ошибки на русском языке для вывода пользователю.
    UNAUTHORIZED(
            401),//Возникает, когда не указан GET параметр access_token, где это необходимо, или указан, но некорректный. Данная ошибка является безусловной командой приложению осуществить переход на экран авторизации.
    FORBIDDEN(
            403),//Возникает, когда пользователь не имеет права просматривать запрашиваемый контент.
    NOT_FOUND(404),//Объект, запрашиваемый по ID, не был найден.
    METHOD_NOT_ALLOWED(
            405),//Возникает, когда происходит обращение к ресурсу с некорректным HTTP методом запроса, eg: GET вместо POST.
    DATA_NOT_ALLOWED(
            415),//Возникает, когда метод не поддерживает тип данных
    I_AM_A_TEAPOT(
            418),//Возникает, когда устройство скопрометировано. Данная ошибка является безусловной командой удалить все конфиденциальные данные (в том числе логин пользователя) с устройства и перейти к экрану регистрации.
    FAILED_DEPENDENCY(
            424),//Возникает, когда pin-пароль пользователя устарел и требуется его замена.
    UPGRADE_REQUIRED(
            426),//Возникает, когда для доступа к функционалу пользователю нужно обновить версию приложения.
    INTERNAL_SERVER_ERROR(
            500),//Возникает, когда введены не все необходимые параметры, или во всех случаях не отраженных выше. В error_message содержится техническое сообщение об ошибке.
    NOT_IMPLEMENTED(
            501),//Возникает, когда сервер не поддерживает возможностей, необходимых для обработки запроса
    BAD_GATEWAY(
            502),//Возникает, когда промежуточный сервер получил недействительное ответное сообщение
    SERVICE_UNAVAILABLE(
            503)//Возникает, когда на сервере произошла внутренняя ошибка (Internal server error).
    ;

    private final int mCode;

    ErrorCode(int code) {
        mCode = code;
    }

    public int getCode() {
        return mCode;
    }

    @NonNull
    public static ErrorCode fromCode(int code) {
        for (ErrorCode result : values()) {
            if (result.getCode() == code) {
                return result;
            }
        }
        return UNKNOWN;
    }

    public static String getMsgByError(int code) {
        ErrorCode errorCode = ErrorCode.fromCode(code);

        switch (errorCode) {
            case UNKNOWN:
                return App.getContext().getString(R.string.error_msg_unknown);
            case TIMEOUT:
                return App.getContext().getString(R.string.error_msg_timeout);
            case BAD_REQUEST:
                return App.getContext().getString(R.string.error_msg_wrong);
            case UNAUTHORIZED:
                return App.getContext().getString(R.string.error_msg_unauthorized);
            case FORBIDDEN:
                return App.getContext().getString(R.string.error_msg_not_found);
            case NOT_FOUND:
                return App.getContext().getString(R.string.error_msg_not_found);
            case METHOD_NOT_ALLOWED:
                return App.getContext().getString(R.string.error_msg_not_allowed);
            case DATA_NOT_ALLOWED:
                return App.getContext().getString(R.string.error_msg_not_supported);
            case I_AM_A_TEAPOT:
                return App.getContext().getString(R.string.error_msg_compromised);
            case UPGRADE_REQUIRED:
                return App.getContext().getString(R.string.error_msg_refresh_version);
            case INTERNAL_SERVER_ERROR:
                return App.getContext().getString(R.string.error_msg_server_internal);
            case NOT_IMPLEMENTED:
                return App.getContext().getString(R.string.error_msg_server_not_realised);
            case BAD_GATEWAY:
                return App.getContext().getString(R.string.error_msg_server_internal);
            case SERVICE_UNAVAILABLE:
                return App.getContext().getString(R.string.error_msg_server_down);
        }

        return "";
    }

}
