package com.lifecorp.collector.network.operations.api.debtor;

import android.support.annotation.NonNull;

import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.objects.debtor.Debtor;
import com.lifecorp.collector.model.objects.debtor.Section;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.requests.debtor.DebtorsListRequest;
import com.lifecorp.collector.network.api.response.debtor.DebtorsListResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.lifecorp.collector.utils.LocationHelper;
import com.redmadrobot.library.async.service.OperationResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 16.03.15.
 *
 * Операция, получающая список должников
 */
public class GetDebtors extends OperationApi<DebtorsListRequest, DebtorsListResponse> {

    public GetDebtors(@NonNull CollectorApi collectorApi,
                      @NonNull DebtorsListRequest debtorsListRequest) {
        super(collectorApi, debtorsListRequest);
    }

    @NonNull
    @Override
    protected DebtorsListResponse runServerRequest() {
        final DebtorsListResponse response = collectorApi.getDebtorsRequest(
                mRequest.getAccessToken(), mRequest.getExtendedBool());
        saveResponseIsValid(response);

        return response;
    }

    public List<DBSection> responseToDBList(Debtor debtor) {
        List<DBSection> list = new ArrayList<>();

        if (debtor != null) {
            final int debtors_id = debtor.getId();

            List<Section> arraySections = debtor.getQuestionnaire().getSections();
            for (Section section : arraySections) {
                list.add(new DBSection(section, debtors_id));
            }
        }

        return list;
    }

    @Override
    protected void process(DebtorsListResponse response) {
        //processing DEBTORS
        List<DBDebtorProfile> listDebs = new ArrayList<>();
        final List<Debtor> debtorList = response.getDebtors();
        for (Debtor debtor : debtorList) {
            listDebs.add(new DBDebtorProfile(debtor));
        }
        DBCachePoint.getInstance().setAllDebtors(listDebs);

        // processing SECTIONS
        List<DBSection> listSections = new ArrayList<>();
        for (Debtor debtor : debtorList) {
            listSections.addAll(responseToDBList(debtor));
        }
        DBCachePoint.getInstance().setAllSections(listSections);

        DBSaver.getInstance().saveDebtorsToDB(listDebs, listSections);
        if (LocationHelper.getInstance().wasFound()) {
            DBCachePoint.getInstance().refreshCacheFields();
        }
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<DebtorsListResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<DebtorsListResponse> {

    }

}
