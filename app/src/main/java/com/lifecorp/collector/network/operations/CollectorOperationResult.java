package com.lifecorp.collector.network.operations;

import android.support.annotation.Nullable;

import com.lifecorp.collector.network.operations.api.ApiOperationException;
import com.lifecorp.collector.network.operations.api.ErrorCode;
import com.redmadrobot.library.async.service.OperationResult;
import com.redmadrobot.library.async.service.operation.OperationError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 12.03.15.
 *
 * Общий результат операции по запросу/изменению данных на сервере.
 */
public class CollectorOperationResult<ResultType> extends OperationResult<ResultType> {

    private final static List<ErrorCode> REDIRECTION_CODES;

    static {
        REDIRECTION_CODES = new ArrayList<>();
        REDIRECTION_CODES.add(ErrorCode.UNAUTHORIZED);
        REDIRECTION_CODES.add(ErrorCode.I_AM_A_TEAPOT);
        REDIRECTION_CODES.add(ErrorCode.FAILED_DEPENDENCY);
    }

    public final boolean hasOnlyCommonError() {
        return getRedirectionCode() == null;
    }

    @Nullable
    public final ErrorCode getErrorCode() {
        if (!isSuccessful()) {
            final OperationError mOperationError = getOperationError();
            if (mOperationError == null) {
                throw new IllegalStateException("Operation is not successful, but has no error.");
            }

            @SuppressWarnings("ThrowableResultOfMethodCallIgnored") final Exception exception
                    = mOperationError.getException();
            if (exception instanceof ApiOperationException) {
                final ApiOperationException apiOperationException
                        = (ApiOperationException) exception;
                return apiOperationException.getErrorCode();
            }
        }

        return null;
    }

    @Nullable
    public final ErrorCode getRedirectionCode() {
        ErrorCode errorCode = getErrorCode();
        return errorCode != null && REDIRECTION_CODES.contains(errorCode) ? errorCode : null;
    }
}
