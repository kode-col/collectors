package com.lifecorp.collector.network.operations.api.event;

import android.support.annotation.NonNull;

import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.objects.PhotoContainer;
import com.lifecorp.collector.model.objects.event.Event;
import com.lifecorp.collector.model.objects.event.EventCompletion;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.requests.event.EventCompleteRequest;
import com.lifecorp.collector.network.api.response.event.EventCompleteResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.redmadrobot.library.async.service.OperationResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 17.03.15.
 *
 * Операция для выполнения события
 */
public class EventCompleteOperation extends OperationApi<EventCompleteRequest,
        EventCompleteResponse> {

    public EventCompleteOperation(@NonNull CollectorApi collectorApi,
                                  @NonNull EventCompleteRequest completeEventRequest) {
        super(collectorApi, completeEventRequest);
    }

    @NonNull
    @Override
    protected EventCompleteResponse runServerRequest() {
        final EventCompleteResponse response = collectorApi.completeEvents(
                mRequest.getAccessToken(), mRequest.getEventCompletion());
        saveResponseIsValid(response);

        return response;
    }


    @Override
    protected void process(EventCompleteResponse response) {
        final List<PhotoContainer> photoContainerList = new ArrayList<>();
        final List<DBEventImageUpload> eventImageUploads = new ArrayList<>();
        final List<EventCompletion> events = mRequest.getEventCompletion().getEvents();

        for (EventCompletion eventCompletion : events) {
            photoContainerList.addAll(eventCompletion.getPhotoContainerList());
        }

        if (response.getEvents() != null && response.getEvents().size() == 1) {
            Event event = response.getEvents().get(0);
            final int id = event.getId() != null ? Integer.parseInt(event.getId()) : -1;

            if (id != -1) {

                for (PhotoContainer item : photoContainerList) {
                    item.setExternalId(id);
                    eventImageUploads.add(new DBEventImageUpload(item));
                }
            }
        }

        DBSaver.getInstance().saveEventsImagesForUpload(eventImageUploads);
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<EventCompleteResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<EventCompleteResponse> {

    }

}
