package com.lifecorp.collector.network.operations.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.lifecorp.collector.App;

/**
*Created by maximefimov on 19.08.14.
*
* Исключение, генирируемое при выполнении операции.
*/
public final class ApiOperationException extends RuntimeException {

    private final ErrorCode mReturnCode;

    public ApiOperationException(int resStringId) {
        this(App.getContext().getString(resStringId), ErrorCode.UNKNOWN);
    }

    public ApiOperationException(@NonNull String message, int code) {
        this(message, ErrorCode.fromCode(code));
    }

    private ApiOperationException(@NonNull String message, @NonNull ErrorCode returnCode) {
        super(message);
        mReturnCode = returnCode;
    }

    public ApiOperationException(@NonNull String message) {
        super(message);
        mReturnCode = ErrorCode.UNKNOWN;
    }

    public ApiOperationException(@NonNull String message, @Nullable Throwable cause) {
        super(message, cause);
        mReturnCode = ErrorCode.UNKNOWN;
    }


    public ApiOperationException(@NonNull String message, int code, @Nullable Throwable cause) {
        super(message, cause);
        mReturnCode = ErrorCode.fromCode(code);
    }

    @NonNull
    public ErrorCode getErrorCode() {
        return mReturnCode;
    }

}