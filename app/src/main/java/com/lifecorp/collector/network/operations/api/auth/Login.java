package com.lifecorp.collector.network.operations.api.auth;

import android.support.annotation.NonNull;

import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.requests.auth.LoginRequest;
import com.lifecorp.collector.network.api.response.auth.LoginResponse;
import com.lifecorp.collector.model.objects.auth.AuthBody;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;
import com.redmadrobot.library.async.service.OperationResult;

/**
 * Created by Alexander Smirnov on 13.03.15.
 *
 * Операция, авторизирующая пользователя.
 */
public class Login extends OperationApi<LoginRequest, LoginResponse> {

    public Login(@NonNull CollectorApi collectorApi,
                 @NonNull LoginRequest actionTypesRequest) {
        super(collectorApi, actionTypesRequest);
    }

    @NonNull
    @Override
    protected LoginResponse runServerRequest() {
        final AuthBody authBody = new AuthBody(mRequest.getLogin(), mRequest.getPassword());
        final LoginResponse response = collectorApi.authRequest(authBody);

        if (response.isValid()) {
            PreferencesStorage.getInstance().saveToken(response.getAccessToken());
        }

        return response;
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<LoginResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<LoginResponse> {

    }
}
