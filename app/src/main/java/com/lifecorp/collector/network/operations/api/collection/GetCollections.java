package com.lifecorp.collector.network.operations.api.collection;

import android.support.annotation.NonNull;

import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.objects.collection.Collection;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.requests.EmptyRequest;
import com.lifecorp.collector.network.api.response.collection.CollectionsListResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.redmadrobot.library.async.service.OperationResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 17.03.15.
 *
 * Операция для получения списка коллекций
 */
public class GetCollections extends OperationApi<EmptyRequest, CollectionsListResponse> {

    public GetCollections(@NonNull CollectorApi collectorApi,
                          @NonNull EmptyRequest emptyRequest) {
        super(collectorApi, emptyRequest);
    }

    @NonNull
    @Override
    protected CollectionsListResponse runServerRequest() {
        final CollectionsListResponse response = collectorApi.getCollectionsRequest(
                mRequest.getAccessToken());
        saveResponseIsValid(response);

        return response;
    }

    @Override
    protected void process(CollectionsListResponse response) {
        List<DBCollection> listCollections = new ArrayList<>();

        for (Collection item : response.getCollections()) {
            listCollections.add(new DBCollection(item));
        }

        DBCachePoint.getInstance().setAllCollections(listCollections);
        DBSaver.getInstance().saveCollectionsToDB(listCollections);
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<CollectionsListResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<CollectionsListResponse> {

    }

}
