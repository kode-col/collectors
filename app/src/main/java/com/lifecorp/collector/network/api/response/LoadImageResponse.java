package com.lifecorp.collector.network.api.response;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.utils.Const;

import java.util.Collection;

/**
 * Created by Alexander Smirnov on 05.05.15.
 *
 * Ответ сервера на загрузку изображения
 */
public class LoadImageResponse extends ServerResponse {

    @SerializedName("id")
    private String mId;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {

    }

    public String getResponseType() {
        return Const.TAG_LOAD_IMAGE;
    }

}
