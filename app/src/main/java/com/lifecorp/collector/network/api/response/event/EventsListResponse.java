package com.lifecorp.collector.network.api.response.event;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.model.objects.event.Event;
import com.lifecorp.collector.utils.Const;

import java.util.Collection;
import java.util.List;

public class EventsListResponse extends ServerResponse {

    @SerializedName("events")
    List<Event> mEventArray;

    public List<Event> getEvents() {
        return mEventArray;
    }

    public Event getEventByNumber(int position) {
        return getEvents().get(position);
    }

    public int getSize() {
        return getEvents() != null ? getEvents().size() : 0;
    }

    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {
        for (final Event event : mEventArray) {
            event.fillErrors(errorCollection);
        }
    }

    @Override
    public String getResponseType() {
        return Const.TAG_EVENTS;
    }

}
