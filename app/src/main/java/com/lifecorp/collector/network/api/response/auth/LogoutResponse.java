package com.lifecorp.collector.network.api.response.auth;

import android.support.annotation.NonNull;

import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.utils.Const;

import java.util.Collection;

/**
 * Created by Alexander Smirnov on 18.03.15.
 *
 * Респонс для удаления токена авторизации
 */
public class LogoutResponse extends ServerResponse {

    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {

    }

    @Override
    public String getResponseType() {
        return Const.TAG_LOGOUT;
    }

}
