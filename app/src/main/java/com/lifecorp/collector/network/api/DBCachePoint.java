package com.lifecorp.collector.network.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.lifecorp.collector.model.database.helpers.DBFront;
import com.lifecorp.collector.model.database.objects.DBActionResultType;
import com.lifecorp.collector.model.database.objects.DBActionType;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBCoordinate;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.utils.LocationHelper;

import java.util.ArrayList;
import java.util.List;

public class DBCachePoint {

    private static class SingletonInstance {
        public static final DBCachePoint INSTANCE = new DBCachePoint();
    }

    private DBCachePoint() {
    }

    public static DBCachePoint getInstance() {
        return SingletonInstance.INSTANCE;
    }

    private boolean mIsSynchronized;
    private List<DBEvent> mAllEvents = new ArrayList<>();
    private List<DBSection> mAllSections = new ArrayList<>();
    private List<DBActionType> mActionTypes = new ArrayList<>();
    private List<DBDebtorProfile> mAllDebtors = new ArrayList<>();
    private List<DBCollection> mAllCollections = new ArrayList<>();
    private List<DBActionResultType> mActionResultTypes = new ArrayList<>();

    public void setAllDebtors(List<DBDebtorProfile> allDebtors) {
        if (allDebtors == null) {
            allDebtors = new ArrayList<>();
        }

        mAllDebtors = allDebtors;
    }

    public void setAllCollections(List<DBCollection> allCollections) {
        if (allCollections == null) {
            allCollections = new ArrayList<>();
        }

        mAllCollections = allCollections;
    }

    public void setAllSections(List<DBSection> allSections) {
        if (allSections == null) {
            allSections = new ArrayList<>();
        }

        mAllSections = allSections;
    }

    public void setAllEvents(List<DBEvent> allEvents) {
        if (allEvents == null) {
            allEvents = new ArrayList<>();
        }

        mAllEvents = allEvents;
    }

    public void setActionResultTypes(List<DBActionResultType> actionResultTypes) {
        if (actionResultTypes == null) {
            actionResultTypes = new ArrayList<>();
        }

        mActionResultTypes = actionResultTypes;
    }

    @NonNull
    public List<DBDebtorProfile> getAllDebtors() {
        return mAllDebtors;
    }

    @NonNull
    public List<DBCollection> getAllCollections() {
        return mAllCollections;
    }

    @NonNull
    public List<DBEvent> getAllEvents() {
        return mAllEvents;
    }

    @Nullable
    public DBDebtorProfile findDebtorById(int debtorId) {
        DBDebtorProfile result = null;

        for (DBDebtorProfile dbDebtorProfile : getAllDebtors()) {
            if (dbDebtorProfile.getId() == debtorId) {
                result = dbDebtorProfile;
                break;
            }
        }

        return result;
    }

    public List<DBSection> getSectionsByDebtorsId(int debtors_id) {
        List<DBSection> localSections = new ArrayList<>();
        for (DBSection dbs : mAllSections) {
            if (dbs.getDebtors_id() == debtors_id) {
                localSections.add(dbs);
            }
        }

        // если не найдено секции в кэше -> тянем из БД и закидываем в кэш
        if (localSections.size() == 0) {
            localSections.addAll(DBFront.getInstance().getSectionByDebId(debtors_id));
            mAllSections.addAll(localSections);
        }

        return localSections;
    }

    public void refreshCacheFields() {
        for (DBDebtorProfile debtor: getAllDebtors()) {
            if (debtor != null) {
                DBCoordinate coordinate = debtor.getCoordinatePrimary();
                if (coordinate != null) {
                    Float dist = LocationHelper.getInstance()
                            .getDistance(coordinate.getLatitude(), coordinate.getLongitude());
                    debtor.setDistance(dist);
                }
            }
        }
    }

    @NonNull
    public List<DBActionType> getActionTypes() {
        return mActionTypes;
    }

    public void setActionTypes(List<DBActionType> actionTypes) {
        if (actionTypes == null) {
            actionTypes = new ArrayList<>();
        }

        mActionTypes = actionTypes;
    }

    @NonNull
    public List<DBActionResultType> getActionResultTypes() {
        return mActionResultTypes;
    }

    public void clearAll() {
        mIsSynchronized = false;
        setAllCollections(null);
        setAllDebtors(null);
        setAllEvents(null);
        setActionTypes(null);
        setActionResultTypes(null);
    }

    public boolean isSynchronized() {
        return mIsSynchronized;
    }

    public void setIsSynchronized(boolean state) {
        mIsSynchronized = state;
    }

}
