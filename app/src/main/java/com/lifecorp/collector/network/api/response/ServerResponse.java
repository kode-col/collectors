package com.lifecorp.collector.network.api.response;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Объект, возвращенный сервером в ответ на какой-то запрос.
 *
 * @author maximefimov
 */
public abstract class ServerResponse implements Serializable {

    public final boolean isValid() {
        return getErrors().size() == 0;
    }

    @NonNull
    public final List<String> getErrors() {
        final List<String> errors = new LinkedList<>();
        fillErrors(errors);
        return errors;
    }

    protected abstract void fillErrors(@NonNull Collection<String> errorCollection);

    public String getResponseType() {
        return "";
    }

}
