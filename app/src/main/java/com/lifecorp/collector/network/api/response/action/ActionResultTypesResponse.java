package com.lifecorp.collector.network.api.response.action;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.model.objects.action.ResultType;

import java.util.Collection;
import java.util.List;

/**
 * Created by maximefimov on 28.08.14.
 *
 * Ответ сервера на запрос типов результатов действий
 */
public final class ActionResultTypesResponse extends ServerResponse {

    @SerializedName("result_types_for_action")
    // Массив типов результатов действий
    private List<ResultType> mData;

    public List<ResultType> getData() {
        return mData;
    }

    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {
        if (getData() == null) {
            errorCollection.add("List must not be null");
        }
    }
}
