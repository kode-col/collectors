package com.lifecorp.collector.network.operations.api.event;

import android.support.annotation.NonNull;

import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.requests.EmptyRequest;
import com.lifecorp.collector.network.api.response.event.EventsListResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.redmadrobot.library.async.service.OperationResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 17.03.15.
 *
 * Операция, получающая список событий
 */
public class GetEvents extends OperationApi<EmptyRequest, EventsListResponse> {

    public GetEvents(@NonNull CollectorApi collectorApi,
                     @NonNull EmptyRequest emptyRequest) {
        super(collectorApi, emptyRequest);
    }

    @NonNull
    @Override
    protected EventsListResponse runServerRequest() {
        final EventsListResponse response = collectorApi.getEventsRequest(
                mRequest.getAccessToken());
        saveResponseIsValid(response);

        return response;
    }

    @Override
    protected void process(EventsListResponse response) {
        List<DBEvent> listEvents = new ArrayList<>();
        for (int i = 0; i < response.getSize(); ++i) {
            listEvents.add(new DBEvent(response.getEventByNumber(i)));
        }

        DBCachePoint.getInstance().setAllEvents(listEvents);
        DBSaver.getInstance().saveEventsToDB(listEvents);
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<EventsListResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<EventsListResponse> {

    }
}
