package com.lifecorp.collector.network.deserializers.general;

import android.support.annotation.Nullable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by maximefimov on 20.08.14.
 *
 * Десериализатор булей. Основывается на предположении, что 0 - ложь, 1 - истина.
 * Кроме этого десериализует и нормальные true и false.
 */
public final class BooleanDeserializer implements JsonDeserializer<Boolean> {


    // 0 = false, 1 = true, null otherwise
    @Nullable
    public static Boolean getBooleanFromInt(int i) {
        switch (i) {
            case 0:
                return Boolean.FALSE;
            case 1:
                return Boolean.TRUE;
            default:
                return null;
        }
    }

    @Override
    public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        Boolean result;

        result = getBooleanFromInt(json.getAsInt());
        if (result == null) {
            result = json.getAsBoolean();
        }

        return result;
    }
}
