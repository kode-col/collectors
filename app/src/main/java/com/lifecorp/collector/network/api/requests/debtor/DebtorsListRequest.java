package com.lifecorp.collector.network.api.requests.debtor;

import com.lifecorp.collector.network.api.requests.ServerRequest;

/**
 * Created by Alexander Smirnov on 16.03.15.
 *
 * Реквест для получения списка должников
 */
public class DebtorsListRequest extends ServerRequest {

    private final boolean mExtendedBool;

    public DebtorsListRequest(boolean extendedBool) {
        mExtendedBool = extendedBool;
    }

    public boolean getExtendedBool() {
        return mExtendedBool;
    }

}
