package com.lifecorp.collector.network;

import com.lifecorp.collector.model.database.helpers.DBFront;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.ui.activities.BaseActivity;
import com.lifecorp.collector.utils.Logger;

import java.util.List;

/**
 * Created by Alexander Smirnov on 29.04.15.
 *
 * Менеджер очереди изображений
 */
public class ImageQueueManager {

    private static class SingletonInstance {
        public static final ImageQueueManager INSTANCE = new ImageQueueManager();
    }

    private ImageQueueManager() {
    }

    public static ImageQueueManager getInstance() {
        return SingletonInstance.INSTANCE;
    }

    public void launchLoading(BaseActivity baseActivity) {
        List<DBCollectionImageUpload> collectionImageUploadForSend =
                DBFront.getInstance().getCollectionImageUploadForSend();
        List<DBEventImageUpload> eventImageUploadForSend =
                DBFront.getInstance().getEventImageUploadForSend();

        if (collectionImageUploadForSend.size() > 0) {
            Logger.e("PhotoQueue's collections size = " + collectionImageUploadForSend.size());

            DBCollectionImageUpload item = collectionImageUploadForSend.get(0);
            baseActivity.loadImage(item);

            DBFront.getInstance().removeDBCollectionImageUpload(item);
        } else if (eventImageUploadForSend.size() > 0) {
            Logger.e("PhotoQueue's events size = " + eventImageUploadForSend.size());

            DBEventImageUpload item = eventImageUploadForSend.get(0);
            baseActivity.loadImage(item);

            DBFront.getInstance().removeDBEventImageUpload(item);
        } else {
            Logger.e("PhotoQueue is empty");
        }
    }

    public int collectionImageQuerySize() {
        return DBFront.getInstance().getCollectionImageUploadForSend().size();
    }

    public int eventImageQuerySize() {
        return DBFront.getInstance().getEventImageUploadForSend().size();
    }

    public int getImageQuerySize() {
        return eventImageQuerySize() + collectionImageQuerySize();
    }

}
