package com.lifecorp.collector.network.api.requests;

/**
 * Created by Alexander Smirnov on 12.03.15.
 *
 * Пустой запрос. Нужен для корректного создания операции.
 */
public class EmptyRequest extends ServerRequest {

}
