package com.lifecorp.collector.network;

import retrofit.RestAdapter;

/**
 * Created by Alexander Smirnov on 13.03.15.
 *
 * Класс для хранения констант, связанных с доступом к серверу банка.
 */
public class NetworkConstants {

    private static final String SERVER_ADDRESS_BANK = "https://www.mb-elife.ru/front-mobile";

    private static final String SERVER_ADDRESS_BANK_TEST = "https://test.mb-elife.ru/front-mobile";//"http://178.238.115.3/front-mobile";
    private static final String SERVER_ADDRESS_BANK_TEST_LOCAL = "http://collector.test/front-mobile";

    private static final String SERVER_ADDRESS_BANK_TEST_NEW =
            "https://test.mb-elife.ru/front-mobile";

    public static final String NETWORK_STATE_CHANGED = "com.lifecorp.collector.network.changed";

    public static final RestAdapter.LogLevel RETROFIT_LOG_LEVEL = false
            ? RestAdapter.LogLevel.FULL
            : RestAdapter.LogLevel.NONE;

    public static final String SERVER_PATTERN_DEBTOR_PHOTO =
            "%s/biz/collect/debtor/%s/photo?access_token=";

    public static final String SERVER_PATTERN_IMAGES_PHOTO =
            "%s/biz/collect/images/%s?thumb=true&thumb-height=384&thumb-width=512" +
                    "&access_token=%s";

    public final static String SERVER_ADDRESS = SERVER_ADDRESS_BANK_TEST;


    private NetworkConstants() {

    }

}
