package com.lifecorp.collector.network.api;

import com.lifecorp.collector.model.objects.auth.AuthBody;
import com.lifecorp.collector.model.objects.event.SingleEventCompletion;
import com.lifecorp.collector.network.api.response.LoadImageResponse;
import com.lifecorp.collector.network.api.response.action.ActionResultTypesResponse;
import com.lifecorp.collector.network.api.response.action.ActionTypesResponse;
import com.lifecorp.collector.network.api.response.auth.LoginResponse;
import com.lifecorp.collector.network.api.response.auth.LogoutResponse;
import com.lifecorp.collector.network.api.response.collection.CollectionsListResponse;
import com.lifecorp.collector.network.api.response.collection.PostCollectionResponse;
import com.lifecorp.collector.network.api.response.debtor.DebtorsListResponse;
import com.lifecorp.collector.network.api.response.event.EventCompleteResponse;
import com.lifecorp.collector.network.api.response.event.EventsListResponse;
import com.lifecorp.collector.utils.Const;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

public interface CollectorApi {

    @POST("/auth")
    public LoginResponse authRequest(@Body AuthBody authbody);

    @GET("/logout")
    public LogoutResponse logoutRequest(@Query(Const.ACCESS_TOKEN) String access_token);

    @GET("/biz/collect/debtor")
    public DebtorsListResponse getDebtorsRequest(
            @Query(Const.ACCESS_TOKEN) String access_token,
            @Query(Const.EXTENDED_DEBTORS) boolean extendedBool
    );

    @GET("/biz/collect/events")
    public EventsListResponse getEventsRequest(@Query(Const.ACCESS_TOKEN) String access_token);

    @POST("/biz/collect/events")
    public EventCompleteResponse completeEvents(
            @Query(Const.ACCESS_TOKEN) String access_token,
            @Body SingleEventCompletion eventCompletion
    );

    @GET("/biz/collect/collections")
    public CollectionsListResponse getCollectionsRequest(
            @Query(Const.ACCESS_TOKEN) String access_token
    );

    @POST("/biz/collect/collections")
    public PostCollectionResponse postCollection(
            @Query(Const.ACCESS_TOKEN) String access_token,
            @Body CollectionsListResponse arrayCollections
    );

    @GET("/biz/catalog/action_types")
    public ActionTypesResponse getActionTypes(@Query(Const.ACCESS_TOKEN) String access_token);

    @GET("/biz/catalog/result_types")
    public ActionResultTypesResponse getActionResultTypes(
            @Query(Const.ACCESS_TOKEN) String access_token
    );

    @Multipart
    @POST("/biz/collect/collections/{id}/image")
    public LoadImageResponse loadCollectionImage(
            @Path("id") String id,
            @Query(Const.ACCESS_TOKEN) String access_token,
            @Part("latitude") float latitude,
            @Part("longitude") float longitude,
            @Part("image") TypedFile image
    );


    @Multipart
    @POST("/biz/collect/events/{id}/image")
    public LoadImageResponse loadEventImage(
            @Path("id") String id,
            @Query(Const.ACCESS_TOKEN) String access_token,
            @Part("latitude") float latitude,
            @Part("longitude") float longitude,
            @Part("image") TypedFile image
    );

}