package com.lifecorp.collector.network.operations.api.action;

import android.support.annotation.NonNull;

import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBActionType;
import com.lifecorp.collector.model.objects.action.ActionType;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.requests.EmptyRequest;
import com.lifecorp.collector.network.api.response.action.ActionTypesResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.redmadrobot.library.async.service.OperationResult;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alexander Smirnov on 13.03.15.
 *
 * Операция загрузки типов действия
 */
public class GetActionTypes extends OperationApi<EmptyRequest, ActionTypesResponse> {

    public GetActionTypes(@NonNull CollectorApi collectorApi,
                          @NonNull EmptyRequest emptyRequest) {
        super(collectorApi, emptyRequest);
    }

    @NonNull
    @Override
    protected ActionTypesResponse runServerRequest() {
        final ActionTypesResponse response = collectorApi.getActionTypes(mRequest.getAccessToken());
        saveResponseIsValid(response);

        return response;
    }

    private List<DBActionType> responseToDBList(ActionTypesResponse actionTypesList) {
        int size = 1;

        if (actionTypesList != null && actionTypesList.getData() != null) {
            size = actionTypesList.getData().size();
        }

        final List<DBActionType> result = new ArrayList<>(size);

        if(actionTypesList != null && actionTypesList.getData() != null) {
            for (ActionType actionType : actionTypesList.getData()) {
                result.add(new DBActionType(actionType));
            }
        }

        return result;
    }

    @Override
    protected void process(ActionTypesResponse response) {
        final List<DBActionType> dbActionTypes = responseToDBList(response);
        DBCachePoint.getInstance().setActionTypes(dbActionTypes);

        DBSaver.getInstance().saveActionsToDB(dbActionTypes);
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<ActionTypesResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<ActionTypesResponse> {

    }

}
