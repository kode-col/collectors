package com.lifecorp.collector.network.operations;

import android.support.annotation.NonNull;

import com.lifecorp.collector.App;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.network.ApiFactory;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.requests.EmptyRequest;
import com.lifecorp.collector.network.api.requests.ImageUploadRequest;
import com.lifecorp.collector.network.api.requests.auth.LoginRequest;
import com.lifecorp.collector.network.api.requests.auth.LogoutRequest;
import com.lifecorp.collector.network.api.requests.collection.PostCollectionRequest;
import com.lifecorp.collector.network.api.requests.debtor.DebtorsListRequest;
import com.lifecorp.collector.network.api.requests.event.EventCompleteRequest;
import com.lifecorp.collector.network.api.response.collection.CollectionsListResponse;
import com.lifecorp.collector.model.objects.DeviceInfo;
import com.lifecorp.collector.model.objects.event.SingleEventCompletion;
import com.lifecorp.collector.network.operations.api.action.GetActionResultTypes;
import com.lifecorp.collector.network.operations.api.action.GetActionTypes;
import com.lifecorp.collector.network.operations.api.auth.Login;
import com.lifecorp.collector.network.operations.api.auth.Logout;
import com.lifecorp.collector.network.operations.api.collection.GetCollections;
import com.lifecorp.collector.network.operations.api.collection.LoadImageOperation;
import com.lifecorp.collector.network.operations.api.collection.PostCollection;
import com.lifecorp.collector.network.operations.api.debtor.GetDebtors;
import com.lifecorp.collector.network.operations.api.event.EventCompleteOperation;
import com.lifecorp.collector.network.operations.api.event.GetEvents;
import com.redmadrobot.library.async.service.operation.Operation;

/**
 * Created by Alexander Smirnov on 12.03.15.
 *
 * За этим классом спрятана логика оперирования данными.
 * Предоставляемый интерфейс - получение и изменение данных по параметрам.
 */
public class CollectorOperationFactory {

    private final static CollectorOperationFactory sInstance = new CollectorOperationFactory();

    private final CollectorApi mCollectorApi = ApiFactory.getAPI();

    public static void init() {
        // Пустой метод для принудительной инициализации апи
    }

    @NonNull
    public static CollectorOperationFactory getInstance() {
        return sInstance;
    }

    private CollectorOperationFactory() {

    }

    @NonNull
    public Operation<?, ?> login(@NonNull final String login, @NonNull final String password) {
        final DeviceInfo deviceInfo = DeviceInfo.create(App.getContext());
        return new Login(mCollectorApi, new LoginRequest(login, password, deviceInfo));
    }

    @NonNull
    public Operation<?, ?> logout() {
        return new Logout(mCollectorApi, new LogoutRequest());
    }

    @NonNull
    public Operation<?, ?> getDebtors(final boolean extendedBool) {
        return new GetDebtors(mCollectorApi, new DebtorsListRequest(extendedBool));
    }

    @NonNull
    public Operation<?, ?> getEvents() {
        return new GetEvents(mCollectorApi, new EmptyRequest());
    }

    @NonNull
    public Operation<?, ?> getCollections() {
        return new GetCollections(mCollectorApi, new EmptyRequest());
    }

    @NonNull
    public Operation<?, ?> getActionTypes() {
        return new GetActionTypes(mCollectorApi, new EmptyRequest());
    }

    @NonNull
    public Operation<?, ?> getActionResultTypes() {
        return new GetActionResultTypes(mCollectorApi, new EmptyRequest());
    }

    @NonNull
    public Operation<?, ?> postCollection(CollectionsListResponse collectionListResponse) {
        return new PostCollection(mCollectorApi, new PostCollectionRequest(collectionListResponse));
    }

    @NonNull
    public Operation<?, ?> completeEvent(SingleEventCompletion eventCompletion) {
        return new EventCompleteOperation(mCollectorApi, new EventCompleteRequest(eventCompletion));
    }

    @NonNull
    public Operation<?, ?> loadImage(DBEventImageUpload image) {
        return new LoadImageOperation(mCollectorApi, new ImageUploadRequest(image));
    }

    @NonNull
    public Operation<?, ?> loadImage(DBCollectionImageUpload image) {
        return new LoadImageOperation(mCollectorApi, new ImageUploadRequest(image));
    }

}
