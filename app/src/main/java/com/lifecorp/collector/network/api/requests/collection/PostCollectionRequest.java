package com.lifecorp.collector.network.api.requests.collection;

import com.lifecorp.collector.network.api.requests.ServerRequest;
import com.lifecorp.collector.network.api.response.collection.CollectionsListResponse;

/**
 * Created by Alexander Smirnov on 17.03.15.
 *
 * Реквест для создания коллекции
 */
public class PostCollectionRequest extends ServerRequest {

    private CollectionsListResponse mCollectionsListResponse;

    public PostCollectionRequest(CollectionsListResponse collectionsListResponse) {
        mCollectionsListResponse = collectionsListResponse;
    }

    public CollectionsListResponse getCollectionsListResponse() {
        return mCollectionsListResponse;
    }

}
