package com.lifecorp.collector.network.deserializers.general;

import android.support.annotation.Nullable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by maximefimov on 20.08.14.
 *
 * Десериализатор дат. Содержит ряд паттернов, пытается произвести десериализацию по каждому, пока не подберет нужный.
 */
public final class DateDeserializer implements JsonDeserializer<Date> {

    private final static List<SimpleDateFormat> DATE_FORMATTERS;

    static {
        DATE_FORMATTERS = new ArrayList<>();
        DATE_FORMATTERS.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        DATE_FORMATTERS.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
        DATE_FORMATTERS.add(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
    }

    @Nullable
    public static Date getDateFromString(String string) {
        if (string == null || string.isEmpty()) {
            return null;
        }
        Date date = null;
        for (SimpleDateFormat dateFormat : DATE_FORMATTERS) {
            try {
                date = dateFormat.parse(string);
                break;
            } catch (ParseException e) {
                // do nothing, this formatter can't help us
            }
        }
        return date;
    }

    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        final Date result = getDateFromString(json.getAsString());
        if (result != null) {
            return result;
        } else {
            throw new JsonParseException(json + " is not a Date");
        }
    }

}