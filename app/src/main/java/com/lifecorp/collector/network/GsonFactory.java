package com.lifecorp.collector.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lifecorp.collector.network.deserializers.general.BooleanDeserializer;
import com.lifecorp.collector.network.deserializers.general.DateDeserializer;

import java.lang.reflect.Modifier;
import java.util.Date;

/**
 * Created by Alexander Smirnov on 12.03.15.
 *
 * Фабрика JSON [де-]сериализаторов.
 */
public final class GsonFactory {

    public static Gson create() {
        final GsonBuilder gsonBuilder = new GsonBuilder();

        //general
        gsonBuilder.registerTypeAdapter(Boolean.class, new BooleanDeserializer());
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());

        //enums
        /*
        gsonBuilder.registerTypeAdapter(AccountType.class, new AccountTypeDeserializer());
        */

        //objects
        /*
        gsonBuilder.registerTypeAdapter(TransactionCategory.class,
                new TransactionCategoryDeserializer());
        */

        return gsonBuilder.excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
    }

    private GsonFactory() {

    }

}
