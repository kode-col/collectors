package com.lifecorp.collector.network.api.response.action;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.model.objects.action.ActionType;

import java.util.Collection;
import java.util.List;

/**
 * Created by maximefimov on 28.08.14.
 *
 * Ответ сервера на запрос типов действий
 */
public final class ActionTypesResponse extends ServerResponse {

    @SerializedName("action_types")
    // Массив типов действий, eg: выезд к клиенту, звонок поручителю
    private List<ActionType> mData;

    public List<ActionType> getData() {
        return mData;
    }

    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {
        if (getData() == null) {
            errorCollection.add("List must not be null");
        }
    }
}
