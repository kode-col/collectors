package com.lifecorp.collector.network.operations.api.auth;

import android.support.annotation.NonNull;

import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.requests.auth.LogoutRequest;
import com.lifecorp.collector.network.api.response.auth.LogoutResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.redmadrobot.library.async.service.OperationResult;

/**
 * Created by Alexander Smirnov on 16.03.15.
 *
 * Операция, выхода пользователя
 */
public class Logout extends OperationApi<LogoutRequest, LogoutResponse> {

    public Logout(@NonNull CollectorApi collectorApi,
                  @NonNull LogoutRequest actionTypesRequest) {
        super(collectorApi, actionTypesRequest);
    }

    @NonNull
    @Override
    protected LogoutResponse runServerRequest() {
        return collectorApi.logoutRequest(mRequest.getAccessToken());
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<LogoutResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<LogoutResponse> {

    }

}
