package com.lifecorp.collector.network.api.requests.auth;

import android.support.annotation.NonNull;

import com.lifecorp.collector.network.api.requests.ServerRequest;
import com.lifecorp.collector.model.objects.DeviceInfo;

/**
 * Created by Alexander Smirnov on 13.03.15.
 *
 * Реквест для получения токена авторизации
 */
public class LoginRequest extends ServerRequest {

    private String mLogin;
    private String mPassword;
    private DeviceInfo mDeviceInfo;

    public LoginRequest(@NonNull String login, @NonNull String password,
                              @NonNull DeviceInfo deviceInfo) {
        mLogin = login;
        mPassword = password;
        mDeviceInfo = deviceInfo;
    }

    @NonNull
    public String getLogin() {
        return mLogin;
    }

    @NonNull
    public String getPassword() {
        return mPassword;
    }

    @NonNull
    public DeviceInfo getDeviceInfo() {
        return mDeviceInfo;
    }

}
