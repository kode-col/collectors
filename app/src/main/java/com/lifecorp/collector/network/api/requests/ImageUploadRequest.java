package com.lifecorp.collector.network.api.requests;

import android.support.annotation.Nullable;

import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.database.objects.DBImageUpload;
import com.lifecorp.collector.utils.ImageFileHelper;

import java.io.File;

import retrofit.mime.TypedFile;

/**
 * Created by Alexander Smirnov on 29.04.15.
 *
 * Реквест для загрузки изображения
 */
public class ImageUploadRequest extends ServerRequest {

    private String mId;
    private TypedFile mFileData;
    private float mLatitude;
    private float mLongitude;

    private DBCollectionImageUpload mDbCollectionImageUpload;
    private DBEventImageUpload mDbEventImageUpload;

    public ImageUploadRequest() {

    }

    public ImageUploadRequest(DBCollectionImageUpload imageUpload) {
        setDbCollectionImageUpload(imageUpload);
    }

    public ImageUploadRequest(DBEventImageUpload imageUpload) {
        setDbEventImageUpload(imageUpload);
    }

    public float getLongitude() {
        return mLongitude;
    }

    public void setLongitude(float longitude) {
        mLongitude = longitude;
    }

    public float getLatitude() {
        return mLatitude;
    }

    public void setLatitude(float latitude) {
        mLatitude = latitude;
    }

    public String getId() {
        return mId;
    }

    public void setId(int id) {
        mId = String.valueOf(id);
    }

    @Nullable
    public DBCollectionImageUpload getDbCollectionImageUpload() {
        return mDbCollectionImageUpload;
    }

    public void setDbCollectionImageUpload(DBCollectionImageUpload imageUpload) {
        mDbCollectionImageUpload = imageUpload;

        if (imageUpload != null) {
            setData(imageUpload.getDbImageUpload());
        }
    }

    @Nullable
    public DBEventImageUpload getDbEventImageUpload() {
        return mDbEventImageUpload;
    }

    public void setDbEventImageUpload(DBEventImageUpload imageUpload) {
        mDbEventImageUpload = imageUpload;

        if (imageUpload != null) {
            setData(imageUpload.getDbImageUpload());
        }
    }

    public void setData(DBImageUpload dbImageUpload) {
        if (dbImageUpload != null) {
            setId(dbImageUpload.getExternalId());

            setLatitude(dbImageUpload.getLatitude());
            setLongitude(dbImageUpload.getLongitude());
            setFileData(dbImageUpload.getFilePath());
        }
    }

    public TypedFile getFileData() {
        return mFileData;
    }

    public void setFileData(String filePath) {
        File image = ImageFileHelper.openFile(filePath);
        if (image != null) {
            mFileData = new TypedFile("application/octet-stream", image);
        }
    }

}
