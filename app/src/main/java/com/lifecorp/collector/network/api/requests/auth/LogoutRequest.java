package com.lifecorp.collector.network.api.requests.auth;

import com.lifecorp.collector.model.database.helpers.DBFront;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.requests.ServerRequest;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;

/**
 * Created by Alexander Smirnov on 16.03.15.
 *
 * Реквест для удаления токена авторизации
 */
public class LogoutRequest extends ServerRequest {

    public LogoutRequest() {
        // очистить все сохранённые данные
        PreferencesStorage.getInstance().clearToken();
        PreferencesStorage.getInstance().clearLogin();
        PreferencesStorage.getInstance().clearPassword();
        DBFront.getInstance().clearAllTables();
        DBCachePoint.getInstance().clearAll();
    }

}
