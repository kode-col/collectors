package com.lifecorp.collector.network.operations.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.lifecorp.collector.R;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.GsonFactory;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.requests.ServerRequest;
import com.lifecorp.collector.utils.Logger;
import com.redmadrobot.library.async.service.operation.Operation;

import javax.net.ssl.SSLException;

import retrofit.RetrofitError;

/**
 * Created by maximefimov on 19.08.14.
 *
 * Общая операция запроса API сервера.
 */
public abstract class OperationApi<Input extends ServerRequest, Output extends ServerResponse>
        extends Operation<Input, Output> {

    private final static String TAG = OperationApi.class.getSimpleName();

    private final static Gson GSON = GsonFactory.create();

    protected final CollectorApi collectorApi;

    private final static int DEFAULT_SERVER_ERROR = R.string.error_msg_server_internal;

    protected OperationApi(@NonNull CollectorApi collectorApi, @NonNull Input input) {
        super(input);
        this.collectorApi = collectorApi;
    }

    protected OperationApi(@NonNull CollectorApi collectorApi) {
        super();
        this.collectorApi = collectorApi;
    }

    @Override
    public final Output run() {
        try {
            return doServerRequest();
        } catch (RetrofitError retrofitError) {
            final Throwable cause = retrofitError.getCause();
            Log.w(TAG, Log.getStackTraceString(retrofitError));

            int errorCode = getErrorCode(retrofitError);
            if (retrofitError.getResponse() != null) {
                errorCode = retrofitError.getResponse().getStatus();
            }
            throw new ApiOperationException("Ошибка подключения к серверу (#" + errorCode + ").",
                    errorCode, cause);
        }
    }

    @NonNull
    protected static String encodeJsonObject(@NonNull Object object) {
        return GSON.toJson(object);
    }

    @NonNull
    protected abstract Output runServerRequest();

    protected boolean isNullResponseAllowed() {
        return true;
    }

    private Output doServerRequest() {
        final Output result = runServerRequest();

        if (isOutputValid(result)) {
            return result;
        } else {
            throw new ApiOperationException(DEFAULT_SERVER_ERROR);
        }
    }

    private boolean isOutputValid(@Nullable final Output output) {
        if (output == null) {
            if (!isNullResponseAllowed()) {
                return false;
            }
        } else if (!output.isValid()) {
            return false;
        }
        return true;
    }

    protected void saveResponseIsValid(Output serverResponse) {
        if (serverResponse != null && serverResponse.isValid()) {
            long start = System.currentTimeMillis();

            try {
                process(serverResponse);
                long elapsedTime = System.currentTimeMillis() - start;
                Logger.e("convert + saveToCache + saveToDB = sec: " + elapsedTime / 1000F);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void process(Output serverResponse) {

    }

    private static int getErrorCode(@NonNull final RetrofitError retrofitError) {
        final Throwable cause = retrofitError.getCause();
        final RetrofitError.Kind kind = retrofitError.getKind();

        if (cause != null && cause instanceof SSLException) {
            return StatusErrorCode.SSL_CHECK_FAIL.getId();
        }

        final StatusErrorCode errorCode;
        switch (kind) {
            case NETWORK:
                errorCode = StatusErrorCode.NETWORK;
                break;
            case CONVERSION:
                errorCode = StatusErrorCode.CONVERSION;
                break;
            case HTTP:
                errorCode = StatusErrorCode.HTTP;
                break;
            case UNEXPECTED:
                errorCode = StatusErrorCode.UNKNOWN;
                break;
            default:
                errorCode = StatusErrorCode.UNKNOWN;
                break;
        }

        return errorCode.getId();
    }

    private enum StatusErrorCode {
        SSL_CHECK_FAIL(1),
        NETWORK(2),
        CONVERSION(3),
        HTTP(4),
        UNKNOWN(99);

        private final int mId;

        StatusErrorCode(int id) {
            mId = id;
        }

        public int getId() {
            return mId;
        }
    }
}
