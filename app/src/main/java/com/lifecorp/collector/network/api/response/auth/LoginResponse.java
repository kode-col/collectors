package com.lifecorp.collector.network.api.response.auth;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.utils.Const;

import java.util.Collection;

/**
 * Created by Alexander Smirnov on 13.03.15.
 *
 * Респонс для получения токена авторизации
 */
public class LoginResponse extends ServerResponse {

    @SerializedName("access_token")
    private String mAccessToken;

    // expire_after

    public String getAccessToken() {
        return mAccessToken;
    }

    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {
        if (getAccessToken() == null) {
            errorCollection.add("access_token must not be null");
        }
    }

    @Override
    public String getResponseType() {
        return Const.TAG_TYPE_AUTH;
    }
}
