package com.lifecorp.collector.network.api.requests.event;

import com.lifecorp.collector.network.api.requests.ServerRequest;
import com.lifecorp.collector.model.objects.event.SingleEventCompletion;

/**
 * Created by Alexander Smirnov on 17.03.15.
 *
 * Реквест для выполнения события
 */
public class EventCompleteRequest extends ServerRequest {

    private SingleEventCompletion mEventCompletion;

    public EventCompleteRequest(SingleEventCompletion eventCompletion) {
        mEventCompletion = eventCompletion;
    }

    public SingleEventCompletion getEventCompletion() {
        return mEventCompletion;
    }

}
