package com.lifecorp.collector.network.api.requests;

import android.support.annotation.NonNull;

import com.lifecorp.collector.utils.preferences.PreferencesStorage;

/**
 * Created by Alexander Smirnov on 12.03.15.
 *
 * Базовый класс для серверных запросов.
 */
public abstract class ServerRequest {

    private final String mAccessToken = PreferencesStorage.getInstance().getToken();

    @NonNull
    public String getAccessToken() {
        return mAccessToken;
    }

}
