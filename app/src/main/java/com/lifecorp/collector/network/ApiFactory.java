package com.lifecorp.collector.network;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.lifecorp.collector.network.api.CollectorApi;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Alexander Smirnov on 13.03.15.
 *
 * Фабрика рестовых клиентов. Нужно использовать этот класс вместо ручного создания инстанса
 * CollectorAPI.
 */
public final class ApiFactory {

    private final static int CONNECT_TIMEOUT_MILLIS = 60;
    private final static int WRITE_TIMEOUT_MILLIS = 60;
    private final static int READ_TIMEOUT_MILLIS = 120;

    @NonNull
    public static CollectorApi getAPI() {
        return getAPI(NetworkConstants.SERVER_ADDRESS, GsonFactory.create());
    }

    private static OkClient createHttpClient() {
        final OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.SECONDS);
        client.setWriteTimeout(WRITE_TIMEOUT_MILLIS, TimeUnit.SECONDS);
        client.setReadTimeout(READ_TIMEOUT_MILLIS, TimeUnit.SECONDS);

        return new OkClient(client);
    }

    @NonNull
    private static CollectorApi getAPI(@NonNull final String apiUrl, @NonNull final Gson gson) {


        final RestAdapter.Builder builder = new RestAdapter.Builder().setClient(createHttpClient())
                .setEndpoint(apiUrl)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(NetworkConstants.RETROFIT_LOG_LEVEL);
        return builder.build().create(CollectorApi.class);
    }

    private ApiFactory() {

    }

}
