package com.lifecorp.collector.network.operations.api.collection;

import android.support.annotation.NonNull;

import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.objects.PhotoContainer;
import com.lifecorp.collector.model.objects.collection.Collection;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.requests.collection.PostCollectionRequest;
import com.lifecorp.collector.network.api.response.collection.PostCollectionResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.redmadrobot.library.async.service.OperationResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 17.03.15.
 *
 * Операция создания коллекции
 */
public class PostCollection extends OperationApi<PostCollectionRequest, PostCollectionResponse> {

    public PostCollection(@NonNull CollectorApi collectorApi,
                          @NonNull PostCollectionRequest postCollectionRequest) {
        super(collectorApi, postCollectionRequest);
    }

    @NonNull
    @Override
    protected PostCollectionResponse runServerRequest() {
        final PostCollectionResponse response = collectorApi.postCollection(
                mRequest.getAccessToken(), mRequest.getCollectionsListResponse());
        saveResponseIsValid(response);

        return response;
    }

    @Override
    protected void process(PostCollectionResponse response) {
        List<DBCollection> listCollections = new ArrayList<>();
        List<PhotoContainer> photoContainerList = null;

        for (Collection collection : mRequest.getCollectionsListResponse().getCollections()) {
            photoContainerList = collection.getPhotoContainerList();
        }

        for (Collection item : response.getCollections()) {
            final int id = item.getId();
            if (photoContainerList != null) {
                for (PhotoContainer photoContainerItem : photoContainerList) {
                    photoContainerItem.setExternalId(id);
                }
                item.setPhotoContainerList(photoContainerList);
            }
            listCollections.add(new DBCollection(item));
        }

        DBCachePoint.getInstance().getAllCollections().addAll(listCollections);
        DBSaver.getInstance().saveCollectionsAddToDB(listCollections);
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<PostCollectionResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<PostCollectionResponse> {

    }

}
