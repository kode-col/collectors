package com.lifecorp.collector.network.operations.api.action;

import android.support.annotation.NonNull;

import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBActionResultType;
import com.lifecorp.collector.model.objects.action.ResultType;
import com.lifecorp.collector.network.api.CollectorApi;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.requests.EmptyRequest;
import com.lifecorp.collector.network.api.response.action.ActionResultTypesResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.OperationApi;
import com.redmadrobot.library.async.service.OperationResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 17.03.15.
 *
 * Операция загрузки типов результатов действия
 */
public class GetActionResultTypes extends OperationApi<EmptyRequest, ActionResultTypesResponse> {

    public GetActionResultTypes(@NonNull CollectorApi collectorApi,
                                @NonNull EmptyRequest emptyRequest) {
        super(collectorApi, emptyRequest);
    }


    @NonNull
    @Override
    protected ActionResultTypesResponse runServerRequest() {
        final ActionResultTypesResponse response = collectorApi.getActionResultTypes(
                mRequest.getAccessToken());
        saveResponseIsValid(response);

        return response;
    }

    private List<DBActionResultType> responseToDBList(ActionResultTypesResponse resultTypesList) {
        final List<DBActionResultType> result = new ArrayList<>(resultTypesList.getData().size());

        for (ResultType actionType : resultTypesList.getData()) {
            result.add(new DBActionResultType(actionType));
        }

        return result;
    }

    @Override
    protected void process(ActionResultTypesResponse response) {
        final List<DBActionResultType> dbActionResultTypes = responseToDBList(response);
        DBCachePoint.getInstance().setActionResultTypes(dbActionResultTypes);

        DBSaver.getInstance().saveActionResultsToDB(dbActionResultTypes);
    }

    @NonNull
    @Override
    public Class<? extends OperationResult<ActionResultTypesResponse>> getResultClass() {
        return Result.class;
    }

    public static final class Result extends CollectorOperationResult<ActionResultTypesResponse> {

    }

}
