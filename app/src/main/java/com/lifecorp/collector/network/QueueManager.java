package com.lifecorp.collector.network;

import com.lifecorp.collector.model.database.helpers.DBFront;
import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBEventCompletion;
import com.lifecorp.collector.model.database.objects.DBQueue;
import com.lifecorp.collector.model.objects.collection.Collection;
import com.lifecorp.collector.model.objects.event.EventCompletion;
import com.lifecorp.collector.network.api.response.collection.CollectionsListResponse;
import com.lifecorp.collector.ui.activities.BaseActivity;
import com.lifecorp.collector.utils.Logger;

import java.util.ArrayList;
import java.util.List;

public class QueueManager {

    public static final int QUEUE_POST_EVENT = 1;
    public static final int QUEUE_REFRESH_ALL = 2;
    public static final int QUEUE_POST_COLLECTION = 3;

    private static class SingletonInstance {
        public static final QueueManager INSTANCE = new QueueManager();
    }

    private QueueManager() {
    }

    public static QueueManager getInstance() {
        return SingletonInstance.INSTANCE;
    }

    public void launchLoading(BaseActivity baseActivity) {
        if (DBFront.getInstance().isQueueNotEmpty()) {
            int size = DBFront.getInstance().getQueueSize();
            Logger.e("Queue's size = " + size);

            DBQueue dbq = DBFront.getInstance().getLastQueue();
            if (dbq != null) {
                int request_id = dbq.getRequest_id();
                Logger.e("Queue's request_id = " + request_id);

                executeLoading(baseActivity, request_id, dbq);
            }

        } else {
            Logger.e("Queue is empty");
        }
    }

    private void executeLoading(BaseActivity baseActivity, int request_id, DBQueue dbq) {
        switch (request_id) {
            case QUEUE_POST_COLLECTION:
                baseActivity.postCollection(new Collection(dbq.getCollectionObj()));
                break;
            case QUEUE_POST_EVENT:
                DBEventCompletion dbEventCompletion = dbq.getEventCompletion();
                baseActivity.completeEvent(new EventCompletion(dbEventCompletion));
                DBSaver.getInstance().removeEventCompletion(dbEventCompletion);
                break;
            case QUEUE_REFRESH_ALL:
                baseActivity.refreshAll();
                break;
        }

        remove(dbq.getID());
    }

    public void addRefreshAll() {
        Logger.e("Try to add Refresh all to Queue !!");

        if (!DBFront.getInstance().isQueryExistInDB(QUEUE_REFRESH_ALL)) {
            Logger.e("Add Refresh all to Queue !!");

            DBQueue dbQueue = new DBQueue();
            dbQueue.setRequest_id(QUEUE_REFRESH_ALL);
            DBSaver.getInstance().saveQueueToDB(dbQueue);
        }
    }

    public void add(int requestId, CollectionsListResponse collections) {
        Logger.e("Add PostCollection to Queue !!");

        if (requestId == QUEUE_POST_COLLECTION && collections != null) {
            DBCollection dbc = new DBCollection(collections.getCollectionByNumber(0));

            List<DBCollection> dbCollectionList = new ArrayList<>();
            dbCollectionList.add(dbc);
            DBSaver.getInstance().saveCollectionsAddToDB(dbCollectionList);

            DBQueue dbq = new DBQueue();
            dbq.setRequest_id(requestId);
            dbq.setCollectionObj(dbc);
            DBSaver.getInstance().saveQueueToDB(dbq);
        }
    }

    public void add(int requestId, EventCompletion eventCompletion) {
        Logger.e("Add PostEvent to Queue !!");

        if (requestId == QUEUE_POST_EVENT && eventCompletion != null) {
            List<DBEventCompletion> dbEventCompletionList = new ArrayList<>();
            dbEventCompletionList.add(new DBEventCompletion(eventCompletion));

            DBSaver.getInstance().saveEventCompletion(dbEventCompletionList);

            DBQueue dbq = new DBQueue();
            dbq.setRequest_id(requestId);
            dbq.setEventCompletion(dbEventCompletionList.get(0));
            DBSaver.getInstance().saveQueueToDB(dbq);
        }
    }

    public void remove(int id) {
        DBFront.getInstance().removeQueueById(id);
    }

}
