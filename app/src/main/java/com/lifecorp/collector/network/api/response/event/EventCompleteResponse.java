package com.lifecorp.collector.network.api.response.event;

import com.lifecorp.collector.utils.Const;

/**
 * Created by Alexander Smirnov on 18.03.15.
 *
 * Респонс для выполнения события
 */
public class EventCompleteResponse extends EventsListResponse {

    @Override
    public String getResponseType() {
        return Const.TAG_COMPLETE_EVENT;
    }

}
