package com.lifecorp.collector.network.api.response.collection;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.model.objects.collection.Collection;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.utils.Const;

import java.io.Serializable;
import java.util.List;

public class CollectionsListResponse extends ServerResponse implements Serializable {

    @SerializedName("collectionItems")
    private List<Collection> mCollectionItems;

    public CollectionsListResponse(List<Collection> collectionItems) {
        setCollections(collectionItems);
    }

    public List<Collection> getCollections() {
        return mCollectionItems;
    }

    public void setCollections(List<Collection> collections) {
        mCollectionItems = collections;
    }

    public Collection getCollectionByNumber(int position) {
        return mCollectionItems.get(position);
    }

    public int getSize() {
        return getCollections() != null ? getCollections().size() : 0;
    }

    @Override
    protected void fillErrors(@NonNull java.util.Collection<String> errorCollection) {
        for (final Collection collection : mCollectionItems) {
            collection.fillErrors(errorCollection);
        }
    }

    @Override
    public String getResponseType() {
        return Const.TAG_COLLECTIONS;
    }

}
