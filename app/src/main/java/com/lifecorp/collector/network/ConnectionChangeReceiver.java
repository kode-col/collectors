package com.lifecorp.collector.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import com.lifecorp.collector.App;
import com.lifecorp.collector.utils.Logger;

public class ConnectionChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.d("Network connectivity change");

        Bundle bundle = intent.getExtras();

        Log.e("RMR", "-| onReceive");

        if (bundle != null) {
            ConnectivityManager cm = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                Logger.d("Network " + networkInfo.getTypeName() + " connected");
                App.IS_NETWORK_AVAILABLE = true;
                Log.e("RMR", "-| onReceive is available");

                Intent tempIntent = new Intent().setAction(NetworkConstants.NETWORK_STATE_CHANGED);
                context.sendBroadcast(tempIntent);
            }
        }

        if (bundle != null && bundle.getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
            Logger.e("There's no network connectivity");
            App.IS_NETWORK_AVAILABLE = false;
            Log.e("RMR", "-| onReceive isn't available");
        }
    }
}