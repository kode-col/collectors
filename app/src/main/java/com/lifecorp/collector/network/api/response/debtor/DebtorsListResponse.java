package com.lifecorp.collector.network.api.response.debtor;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.model.objects.debtor.Debtor;
import com.lifecorp.collector.utils.Const;

import java.util.Collection;
import java.util.List;

public class DebtorsListResponse extends ServerResponse {

    @SerializedName("debtors")
    private List<Debtor> mDebtors;

    public List<Debtor> getDebtors() {
        return mDebtors;
    }

    public int getSize() {
        return getDebtors() != null ? getDebtors().size() : 0;
    }

    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {
        for (final Debtor debtor : mDebtors) {
            debtor.fillErrors(errorCollection);
        }
    }

    @Override
    public String getResponseType() {
        return Const.TAG_DEBTORS;
    }

}