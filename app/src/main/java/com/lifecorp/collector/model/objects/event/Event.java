package com.lifecorp.collector.model.objects.event;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.model.objects.Money;
import com.lifecorp.collector.model.objects.debtor.Coordinate;
import com.lifecorp.collector.network.api.response.ServerResponse;

import java.util.Collection;
import java.util.List;

public class Event extends ServerResponse {

    public final static int TYPE_MEETING = 0;
    public final static int TYPE_CALL = 1;

    @SerializedName("id")
    private String mId;

    @SerializedName("completed")
    // Признак завершенности задачи
    private boolean mCompleted;

    @SerializedName("debtor_id")
    // ID заемщика
    private int mDebtorId;

    @SerializedName("type")
    // тип из справочника(0 - выезд, 1 - звонок)
    private int mType;

    @SerializedName("description")
    // Описание
    private String mDescription;

    @SerializedName("date")
    // Дата задачи
    private long mTime;

    @SerializedName("result_type")
    // тип результата
    private int mResultTypeId;

    @SerializedName("summary")
    // результат(0 - отрицательный, 1 - положительный)
    private String mSummary;

    @SerializedName("poi_coordinate")
    // Координата, где было закрыто мероприятие
    private Coordinate mPoiCoordinate;

    @SerializedName("promised_payment")
    // Сумма обещанного платежа
    private Money mPromisedPayment;

    // Дата обещанного платежа
    @SerializedName("promised_payment_date")
    private long mPromisedPaymentDate;

    @SerializedName("image_ids")
    private List<String> mImageIds;

    public String getId() {
        return mId;
    }

    public boolean isCompleted() {
        return mCompleted;
    }

    public int getDebtorId() {
        return mDebtorId;
    }

    public int getType() {
        return mType;
    }

    public String getDescription() {
        return mDescription;
    }

    public long getTime() {
        return mTime;
    }

    public int getResultTypeId() {
        return mResultTypeId;
    }

    public String getSummary() {
        return mSummary;
    }

    public Coordinate getPoiCoordinate() {
        return mPoiCoordinate;
    }

    public Money getPromisedPayment() {
        return mPromisedPayment;
    }

    public long getPromisedPaymentDate() {
        return mPromisedPaymentDate;
    }

    public List<String> getImageIds() {
        return mImageIds;
    }

    // @TODO заполнить?
    @Override
    public void fillErrors(@NonNull Collection<String> errorCollection) {

    }

}
