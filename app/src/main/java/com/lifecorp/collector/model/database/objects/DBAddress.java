package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "address")
public class DBAddress {

    @DatabaseField(generatedId = true)
    private int id_;

    @DatabaseField
    private String address;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, useGetSet = true)
    private DBCollection dbCollection;


    public DBAddress() {
    }

    public DBAddress(String address, DBCollection dbCollection) {
        setAddress(address);
        
        if (dbCollection != null) {
            setDbCollection(dbCollection);
        }
    }

    public DBCollection getDbCollection() {
        return dbCollection;
    }

    public void setDbCollection(DBCollection dbCollection) {
        this.dbCollection = dbCollection;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

}
