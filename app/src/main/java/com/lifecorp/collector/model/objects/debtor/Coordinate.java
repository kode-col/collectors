package com.lifecorp.collector.model.objects.debtor;


import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.model.database.objects.DBCoordinate;
import com.lifecorp.collector.network.api.response.ServerResponse;

import java.util.Collection;

public class Coordinate extends ServerResponse {

    @SerializedName("latitude")
    // Широта
    private float mLatitude;

    @SerializedName("longitude")
    // Долгота
    private float mLongitude;

    public Coordinate() {

    }

    public Coordinate(DBCoordinate dbCoordinate) {
        if (dbCoordinate != null) {
            setLatitude(dbCoordinate.getLatitude());
            setLongitude(dbCoordinate.getLongitude());
        }
    }

    public float getLatitude() {
        return mLatitude;
    }

    public void setLatitude(float latitude) {
        mLatitude = latitude;
    }

    public float getLongitude() {
        return mLongitude;
    }

    public void setLongitude(float longitude) {
        mLongitude = longitude;
    }

    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {

    }
}
