package com.lifecorp.collector.model.objects;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.model.database.objects.DBMoney;

import java.io.Serializable;

public class Money implements Serializable {

    @SerializedName("amount")
    //Сумма
    private float mAmount;

    @SerializedName("currency")
    //Валюта в ISO 4217 letter notation eg: RUR.
    private String mCurrency;

    public Money() {

    }

    public Money(DBMoney dbMoney) {
        if (dbMoney != null) {
            setAmount(dbMoney.getAmount());
            setCurrency(dbMoney.getCurrency());
        }
    }

    public float getAmount() {
        return mAmount;
    }

    public void setAmount(float amount) {
        mAmount = amount;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

}
