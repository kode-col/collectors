package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBDebtorLink;

import java.sql.SQLException;

/**
 * Created by Alexander Smirnov on 14.05.15.
 *
 * Dao для DBDebtorLink
 */
@SuppressWarnings("unused")
public class DebtorLinkDao extends BaseDaoImpl<DBDebtorLink, Integer> {

    public DebtorLinkDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBDebtorLink.class);
    }

}
