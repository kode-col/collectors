package com.lifecorp.collector.model.objects.debtor;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.model.objects.Money;

import java.util.Collection;

public class Debtor extends ServerResponse {

    @SerializedName("id")
    private int mId;

    @SerializedName("first_name")
    private String mFirstName; // Имя

    @SerializedName("middle_name")
    private String mMiddleName; // Отчество

    @SerializedName("last_name")
    private String mLastName; // Фамилия

    @SerializedName("primary_phone")
    private PhoneNumber mPrimaryPhone; // Основной номер для связи

    @SerializedName("total_debt_amount")
    private Money mTotalDebtAmount; // Общая сумма долга

    @SerializedName("total_debt_delay")
    private int mTotalDebtDelay; // Общая количество дней просрочки

    @SerializedName("photo_url")
    private String mPhotoUrl; // Ссылка на картинку профиля

    @SerializedName("questionnaire")
    private Questionnaire mQuestionnaire; // Анкета заемщика

    @SerializedName("check_payment")
    private int mCheckPromisedPayment; // Признак обещанного платежа (1 – обещал заплатить, 0 – нет)

    @SerializedName("promised_payment")
    private Money mPromisedPaymentSum; // Сумма обещанного платежа

    @SerializedName("promised_payment_date")
    private long mPromisedPaymentDate; // Дата обещанного платежа


    public Questionnaire getQuestionnaire() {
        return mQuestionnaire;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getMiddleName() {
        return mMiddleName;
    }

    public String getLastName() {
        return mLastName;
    }

    public PhoneNumber getPrimaryPhone() {
        return mPrimaryPhone;
    }

    public Money getTotalDebtAmount() {
        return mTotalDebtAmount;
    }

    public int getTotalDebtDelay() {
        return mTotalDebtDelay;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public int getCheckPromisedPayment() {
        return mCheckPromisedPayment;
    }

    public Money getPromisedPaymentSum() {
        return mPromisedPaymentSum;
    }

    public long getPromisedPaymentDate() {
        return mPromisedPaymentDate;
    }

    // @TODO возможно дополнить
    @Override
    public void fillErrors(@NonNull Collection<String> errorCollection) {
        if (getFirstName() == null) {
            errorCollection.add("first_name must not be null");
        }

        if (getMiddleName() == null) {
            errorCollection.add("middle_name must not be null");
        }

        if (getLastName() == null) {
            errorCollection.add("last_name must not be null");
        }

        if (getQuestionnaire() == null) {
            errorCollection.add("questionnaire must not be null");
        } else {
            getQuestionnaire().fillErrors(errorCollection);
        }
    }

}
