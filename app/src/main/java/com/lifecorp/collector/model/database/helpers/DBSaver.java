package com.lifecorp.collector.model.database.helpers;

import com.lifecorp.collector.model.database.dao.ActionResultTypeDao;
import com.lifecorp.collector.model.database.dao.ActionTypeDao;
import com.lifecorp.collector.model.database.dao.AddressDao;
import com.lifecorp.collector.model.database.dao.CollectionDao;
import com.lifecorp.collector.model.database.dao.CollectionImageIdDao;
import com.lifecorp.collector.model.database.dao.CollectionImageUploadDao;
import com.lifecorp.collector.model.database.dao.DebtorLinkDao;
import com.lifecorp.collector.model.database.dao.DebtorProfileDao;
import com.lifecorp.collector.model.database.dao.EventCompletionDao;
import com.lifecorp.collector.model.database.dao.EventDao;
import com.lifecorp.collector.model.database.dao.EventImageIdDao;
import com.lifecorp.collector.model.database.dao.EventImageUploadDao;
import com.lifecorp.collector.model.database.dao.SectionDao;
import com.lifecorp.collector.model.database.dao.SectionFieldDao;
import com.lifecorp.collector.model.database.objects.DBActionResultType;
import com.lifecorp.collector.model.database.objects.DBActionType;
import com.lifecorp.collector.model.database.objects.DBAddress;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBCollectionImageId;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBDebtorLink;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.database.objects.DBEventCompletion;
import com.lifecorp.collector.model.database.objects.DBEventImageId;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.database.objects.DBQueue;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.utils.DateUtils;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

public class DBSaver extends DBAbstractHelper {

    private static class SingletonHolder {
        public static final DBSaver INSTANCE = new DBSaver();
    }

    public static DBSaver getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void saveQueueToDB(DBQueue queue) {
        try {
            setHelper();
            helper.getQueueDao().create(queue);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    public void saveDebtorsToDB(final List<DBDebtorProfile> debtorList,
                                final List<DBSection> sectionList) {
        try {
            setHelper();
            helper.clearDebtorProfileTable();
            final DebtorProfileDao localDebDao = helper.getDebtorProfileDao();
            localDebDao.callBatchTasks(new Callable<DBDebtorProfile>() {
                @Override
                public DBDebtorProfile call() throws Exception {
                    for (DBDebtorProfile tempDebtor : debtorList) {
                        localDebDao.create(tempDebtor);
                    }
                    return null;
                }

            });

            helper.clearSectionTable();
            helper.clearSectionFieldTable();
            final SectionDao localSectDao = helper.getSectionDao();
            final SectionFieldDao localSectFieldDao = helper.getSectionFieldDao();
            final DebtorLinkDao localDebtorLinkDao = helper.getDebtorLinkDao();
            localSectDao.callBatchTasks(new Callable<DBDebtorProfile>() {
                @Override
                public DBDebtorProfile call() throws Exception {
                    for (DBSection tempSection : sectionList) {
                        localSectDao.create(tempSection);

                        //--------- для ПОЛЕЙ в секциях
                        Collection<DBSectionField> first = tempSection.getFields();
                        if (first != null) {
                            for (DBSectionField secField : tempSection.getFields()) {
                                secField.setDbSection(tempSection);
                                localSectFieldDao.create(secField);

                                Collection<DBDebtorLink> second = secField.getDebtorLinks();
                                if (second != null) {
                                    for (DBDebtorLink debtorLink : second) {
                                        localDebtorLinkDao.create(debtorLink);
                                    }
                                }
                            }
                        }
                        //---------
                    }

                    return null;
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    public void saveEventsImagesForUpload(final List<DBEventImageUpload> dbEventImageUploadList) {
        try {
            setHelper();
            final EventImageUploadDao localEventDao = helper.getEventImageUploadDao();
            localEventDao.callBatchTasks(new Callable<DBEventImageUpload>() {

                @Override
                public DBEventImageUpload call() throws Exception {
                    for (DBEventImageUpload item : dbEventImageUploadList) {
                        localEventDao.create(item);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    // Сохраняет мероприятия в бд с предварительной очисткой таблицы мероприятий
    public void saveEventsToDB(final List<DBEvent> arg0) {
        try {
            setHelper();
            helper.clearEventTable();
            final EventDao localEventDao = helper.getEventDao();
            final EventImageIdDao localImageIdDao = helper.getEventImageIdDao();
            localEventDao.callBatchTasks(new Callable<DBEvent>() {
                @Override
                public DBEvent call() throws Exception {
                    for (DBEvent tempEvent: arg0) {
                        tempEvent.setTime(DateUtils.fixDateIfNeed(tempEvent.getTime()));
                        localEventDao.create(tempEvent);

                        Collection<DBEventImageId> second = tempEvent.getImagesExternal();
                        if (second != null) {
                            for (DBEventImageId item : second) {
                                localImageIdDao.create(item);
                            }
                        }
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    // Сохраняет коллекции в бд, при необходимости предварительно очищает таблицу коллекций
    private void saveCollectionsToDBPrivate(final List<DBCollection> arg0, boolean isNeedClear) {
        try {
            setHelper();
            if (isNeedClear) {
                helper.clearCollectionTable();
            }

            final CollectionDao localColDao = helper.getCollectionDao();
            final AddressDao localAddressDao = helper.getAddressDao();
            final CollectionImageIdDao localIdDao = helper.getCollectionImageIdDao();
            final CollectionImageUploadDao localUploadDao = helper.getCollectionImageUploadDao();

            localColDao.callBatchTasks(new Callable<DBCollection>() {
                @Override
                public DBCollection call() throws Exception {
                    for (DBCollection tempCollection : arg0) {
                        localColDao.create(tempCollection);

                        Collection<DBCollectionImageUpload> first = tempCollection.getImagesLocal();
                        if (first != null) {
                            for (DBCollectionImageUpload item : first) {
                                localUploadDao.create(item);
                            }
                        }

                        Collection<DBCollectionImageId> second = tempCollection.getImagesExternal();
                        if (second != null) {
                            for (DBCollectionImageId item : second) {
                                localIdDao.create(item);
                            }
                        }

                        Collection<DBAddress> addressCollection = tempCollection.getAddress();
                        if (addressCollection != null) {
                            for (DBAddress item : addressCollection) {
                                localAddressDao.create(item);
                            }
                        }
                    }

                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    // Сохраняет коллекции в бд, при необходимости предварительно очищает таблицу коллекций
    public void saveEventCompletion(final List<DBEventCompletion> arg0) {
        try {
            setHelper();

            final EventCompletionDao localEventComDao = helper.getEventCompletionDao();
            final EventImageUploadDao localUploadDao = helper.getEventImageUploadDao();

            localEventComDao.callBatchTasks(new Callable<DBEventCompletion>() {
                @Override
                public DBEventCompletion call() throws Exception {
                    for (DBEventCompletion event : arg0) {
                        localEventComDao.create(event);

                        Collection<DBEventImageUpload> images = event.getImagesLocal();
                        if (images != null) {
                            for (DBEventImageUpload image : images) {
                                localUploadDao.create(image);
                            }
                        }

                    }

                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    // Сохраняет изображение коллекции предназначенное для загрузки
    public void saveCollectionImageUpload(DBCollectionImageUpload image) {
        try {
            setHelper();

            final CollectionImageUploadDao localImageDao = helper.getCollectionImageUploadDao();
            localImageDao.create(image);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }


    // Сохраняет изображение мероприятия предназначенное для загрузки
    public void saveEventImageUpload(DBEventImageUpload image) {
        try {
            setHelper();

            final EventImageUploadDao localImageDao = helper.getEventImageUploadDao();
            localImageDao.create(image);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    public void removeEventCompletion(final DBEventCompletion item) {
        try {
            setHelper();

            Collection<DBEventImageUpload> images = item.getImagesLocal();
            if (images != null) {
                helper.getEventImageUploadDao().delete(images);
            }

            helper.getEventCompletionDao().delete(item);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    // Сохраняет коллекции в бд, с предварительной очистки таблицы коллекций
    public void saveCollectionsToDB(List<DBCollection> arg0) {
        saveCollectionsToDBPrivate(arg0, true);
    }

    // Сохраняет коллекции в бд, без предварительной очистки таблицы коллекций
    public void saveCollectionsAddToDB(List<DBCollection> arg0) {
        saveCollectionsToDBPrivate(arg0, false);
    }

    public void saveActionsToDB(final List<DBActionType> actionTypes) {
        try {
            setHelper();
            helper.clearActionTypeTable();
            final ActionTypeDao actionTypeDao = helper.getActionTypeDao();
            actionTypeDao.callBatchTasks(new Callable<DBActionType>() {
                @Override
                public DBActionType call() throws Exception {
                    for (DBActionType actionType: actionTypes) {
                        actionTypeDao.create(actionType);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    public void saveActionResultsToDB(final List<DBActionResultType> actionResultTypes) {
        try {
            setHelper();
            helper.clearActionResultTypeTable();
            final ActionResultTypeDao actionResultTypeDao = helper.getActionResultTypeDao();
            actionResultTypeDao.callBatchTasks(new Callable<DBActionResultType>() {
                @Override
                public DBActionResultType call() throws Exception {
                    for (DBActionResultType actionResultType: actionResultTypes) {
                        actionResultTypeDao.create(actionResultType);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

}
