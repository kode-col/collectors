package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBSection;

import java.sql.SQLException;

public class SectionDao extends BaseDaoImpl<DBSection, Integer> {

    public SectionDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBSection.class);
    }

}
