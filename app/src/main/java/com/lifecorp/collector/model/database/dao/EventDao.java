package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBEvent;

import java.sql.SQLException;

public class EventDao extends BaseDaoImpl<DBEvent, Integer> {

    public EventDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBEvent.class);
    }

}
