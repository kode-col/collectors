package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBCoordinate;

import java.sql.SQLException;

public class CoordinateDao extends BaseDaoImpl<DBCoordinate, Integer> {

    public CoordinateDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBCoordinate.class);
    }

}
