package com.lifecorp.collector.model.objects;

import com.lifecorp.collector.ui.enums.RobotoFont;

/**
 * Created by Alexander Smirnov on 14.04.15.
 *
 * Фильтер типа
 */
public class FilterType {
    private int mIconRes;
    private int mColorRes;
    private int mTitleRes;
    private RobotoFont mFont;

    public FilterType(int titleRes, int iconRes, int colorRes, RobotoFont font) {
        setIconRes(iconRes);
        setColorRes(colorRes);
        setTitleRes(titleRes);
        setFont(font);
    }

    public int getIconRes() {
        return mIconRes;
    }

    public void setIconRes(int iconRes) {
        mIconRes = iconRes;
    }

    public int getColorRes() {
        return mColorRes;
    }

    public void setColorRes(int colorRes) {
        mColorRes = colorRes;
    }

    public int getTitleRes() {
        return mTitleRes;
    }

    public void setTitleRes(int titleRes) {
        mTitleRes = titleRes;
    }

    public RobotoFont getFont() {
        return mFont;
    }

    public void setFont(RobotoFont font) {
        mFont = font;
    }

}
