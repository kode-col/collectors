package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Alexander Smirnov on 05.05.15.
 *
 * Таблица содержит ид изображений на сервере для мероприятий
 */
@DatabaseTable(tableName = "event_image_id")
public class DBEventImageId implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, useGetSet = true, foreignAutoCreate = true)
    private DBImageId dbImageId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, useGetSet = true)
    private DBEvent dbEvent;

    public DBEventImageId() {

    }

    public DBEventImageId(String id, DBEvent event) {
        setDbEvent(event);

        if (id != null) {
            setDbImageId(new DBImageId(id));
        }
    }

    public DBEvent getDbEvent() {
        return dbEvent;
    }

    public void setDbEvent(DBEvent event) {
        dbEvent = event;
    }

    public DBImageId getDbImageId() {
        return dbImageId;
    }

    public void setDbImageId(DBImageId imageId) {
        dbImageId = imageId;
    }

}
