package com.lifecorp.collector.model.objects.debtor;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;

import java.util.Collection;

public class PhoneNumber extends ServerResponse {

    @SerializedName("display_string")
    // Номер телефона для отображения в интерфейсе, eg: +7 (900) 123-45-67
    private String mDisplayString;

    @SerializedName("dial_number")
    // Номер телефона для набора, eg: +79001234567
    private String mDialNumber;

    @SerializedName("comment")
    // Комментарий (eg. Жена клиента)
    private String mComment;

    public String getDisplayString() {
        return mDisplayString;
    }

    public String getDialNumber() {
        return mDialNumber;
    }

    public String getComment() {
        return mComment;
    }

    @Override
    public void fillErrors(@NonNull Collection<String> errorCollection) {

    }

}
