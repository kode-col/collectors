package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBImageId;

import java.sql.SQLException;

/**
 * Created by Alexander Smirnov on 24.04.15.
 *
 * DAO для DBImageId
 */
public class ImageIdDao extends BaseDaoImpl<DBImageId, Integer> {

    public ImageIdDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBImageId.class);
    }

}
