package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.debtor.DebtorLink;

/**
 * Created by Alexander Smirnov on 07.05.15.
 *
 * Модель хранени списков ссылок
 */
@DatabaseTable(tableName = "debtor_link")
public class DBDebtorLink {

    @DatabaseField(generatedId = true, useGetSet = true)
    private int id;

    @DatabaseField(useGetSet = true)
    private String type;

    @DatabaseField(useGetSet = true)
    private String link;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, useGetSet = true)
    private DBSectionField dbSectionField;

    public DBDebtorLink() {

    }

    public DBDebtorLink(DebtorLink debtorLink, DBSectionField sectionField) {
        if (debtorLink != null) {
            setType(debtorLink.getType());
            setLink(debtorLink.getLink());

            if (sectionField != null) {
                setDbSectionField(sectionField);
            }
        }
    }

    public DBSectionField getDbSectionField() {
        return dbSectionField;
    }

    public void setDbSectionField(DBSectionField sectionField) {
        dbSectionField = sectionField;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
