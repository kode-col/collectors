package com.lifecorp.collector.model.objects.address;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 08.04.15.
 *
 * Контейнер для хранения структуры адресов сгрупированных по заголовку
 */
public class AddressGroupContainer {

    private String mTitle;
    private List<AddressContainer> mAddress = new ArrayList<>();

    public AddressGroupContainer() {
    }

    public AddressGroupContainer(String title) {
        mTitle = title;
    }

    @NonNull
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        if (title == null) {
            title = "";
        }

        mTitle = title;
    }

    public void addAddress(AddressContainer addressContainer) {
        if (addressContainer != null) {
            mAddress.add(addressContainer);
        }
    }

    public void addAddress(List<AddressContainer> addressContainerList) {
        if (addressContainerList != null) {
            mAddress.addAll(addressContainerList);
        }
    }

    @NonNull
    public List<AddressContainer> getAddress() {
        return mAddress;
    }

}
