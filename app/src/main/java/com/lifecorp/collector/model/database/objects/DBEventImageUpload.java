package com.lifecorp.collector.model.database.objects;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.PhotoContainer;

import java.io.Serializable;

/**
 * Created by Alexander Smirnov on 24.04.15.
 *
 * Изображение коллекции для загрузки
 */
@DatabaseTable(tableName = "event_image_upload")
public class DBEventImageUpload implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, useGetSet = true,
            foreignAutoCreate = true)
    private DBImageUpload dbImageUpload;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, useGetSet = true)
    private DBEventCompletion dbEventCompletion;

    public DBEventImageUpload() {

    }

    public DBEventImageUpload(PhotoContainer photoContainer) {
        if (photoContainer != null) {
            setDbImageUpload(new DBImageUpload(photoContainer));
        }
    }

    public DBEventImageUpload(PhotoContainer photoContainer, DBEventCompletion event) {
        setDbEventCompletion(event);

        if (photoContainer != null) {
            setDbImageUpload(new DBImageUpload(photoContainer));
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DBEventCompletion getDbEventCompletion() {
        return dbEventCompletion;
    }

    public void setDbEventCompletion(DBEventCompletion event) {
        dbEventCompletion = event;
    }

    public DBImageUpload getDbImageUpload() {
        return dbImageUpload;
    }

    public void setDbImageUpload(DBImageUpload imageUpload) {
        dbImageUpload = imageUpload;
    }

}
