package com.lifecorp.collector.model.objects.event;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.model.database.objects.DBEventCompletion;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.objects.Money;
import com.lifecorp.collector.model.objects.PhotoContainer;
import com.lifecorp.collector.model.objects.debtor.Coordinate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 06.05.15.
 *
 * Модульная сущность для отправки данных на сервер
 */
public class EventCompletion implements Serializable {

    @SerializedName("id")
    private final String mId;

    @SerializedName("completed")
    // Закрыто ли мероприятие, всегда true
    private final boolean mCompleted;

    @SerializedName("description")
    // Комментарий
    private final String mComment;

    @SerializedName("type")
    // Тип
    private final int mType;

    @SerializedName("summary")
    private final int mSummary;

    @SerializedName("result_type")
    private final int mResultType;

    @SerializedName("poi_coordinate")
    // Координата, где было закрыто мероприятие
    private Coordinate mPoiCoordinate;

    @SerializedName("promised_payment")
    // Сумма обещанного платежа
    private final Money mPromisedPayment; // TODO проверить нужны ли поля

    // Дата обещанного платежа
    @SerializedName("promised_payment_date")
    private final long mPromisedPaymentDate; // TODO проверить нужны ли поля

    private transient List<PhotoContainer> mPhotoContainerList;

    public EventCompletion(DBEventCompletion eventCompletion) {
        mId = eventCompletion.getId();
        mType = eventCompletion.getType();
        mCompleted = eventCompletion.isCompleted();
        mComment = eventCompletion.getComment();
        mSummary = eventCompletion.getSummary();
        mResultType = eventCompletion.getResultType();
        mPromisedPaymentDate = eventCompletion.getPromisedPaymentDate();
        mPoiCoordinate = new Coordinate(eventCompletion.getPoiCoordinate());
        mPromisedPayment = new Money(eventCompletion.getPromisedPaymentSum());

        mPhotoContainerList = convertImages(eventCompletion.getImagesLocal());
    }

    public EventCompletion(String id, String comment, int type, int summary, int resultType,
                           Coordinate poiCoordinate, Money promisedPayment,
                           long promisedPaymentDate, List<PhotoContainer> photoContainerList) {
        mId = id;
        mType = type;
        mCompleted = true;
        mComment = comment;
        mSummary = summary;
        mResultType = resultType;
        mPoiCoordinate = poiCoordinate;
        mPromisedPayment = promisedPayment;
        mPromisedPaymentDate = promisedPaymentDate;
        mPhotoContainerList = photoContainerList;
    }

    @NonNull
    private List<PhotoContainer> convertImages(java.util.Collection<DBEventImageUpload> data) {
        List<PhotoContainer> photoContainerList = new ArrayList<>();

        if (data != null) {
            for (DBEventImageUpload item : data) {
                photoContainerList.add(new PhotoContainer(item));
            }
        }

        return photoContainerList;
    }


    public String getId() {
        return mId;
    }

    public boolean isCompleted() {
        return mCompleted;
    }

    public String getComment() {
        return mComment;
    }

    public int getType() {
        return mType;
    }

    public int getSummary() {
        return mSummary;
    }

    public int getResultType() {
        return mResultType;
    }

    public Coordinate getPoiCoordinate() {
        return mPoiCoordinate;
    }

    public void setPoiCoordinate(Coordinate poiCoordinate) {
        mPoiCoordinate = poiCoordinate;
    }

    public Money getPromisedPayment() {
        return mPromisedPayment;
    }

    public long getPromisedPaymentDate() {
        return mPromisedPaymentDate;
    }

    public List<PhotoContainer> getPhotoContainerList() {
        return mPhotoContainerList;
    }

}
