package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.debtor.Section;
import com.lifecorp.collector.model.objects.debtor.SectionField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DatabaseTable(tableName = "section")
public class DBSection implements Serializable {

    public static final int TYPE_ADDRESS = 1;
    public static final int TYPE_CREDIT_INFO = 2;

    @DatabaseField(generatedId = true, useGetSet = true)
    private int id_;

    @DatabaseField(useGetSet = true)
    private String id;

    @DatabaseField(useGetSet = true)
    private int debtors_id;

    @DatabaseField(useGetSet = true)
    private int type;

    @DatabaseField(useGetSet = true)
    private String title;

    @ForeignCollectionField(eager = true, maxEagerForeignCollectionLevel = 2)
    private Collection<DBSectionField> fields;

    @DatabaseField(useGetSet = true)
    private int sections_credit_size;

    public DBSection() {

    }

    public DBSection(Section section, int debtorsId) {
        if (section != null) {
            setId(section.getId());
            setType(section.getType());
            setTitle(section.getTitle());
            setDebtors_id(debtorsId);
            setSectionIndexHack(section);
        }
    }

    // Ugly hack for saving several sections as one
    private void setSectionIndexHack(Section section) {
        if (section == null) {
            return;
        }

        List<SectionField> allInOneList = new ArrayList<>();
        allInOneList.addAll(setIndexForSectionFieldList(section.getFields(), 0));

        if (section.getSections() != null) {
            int i = 1;
            for (Section item : section.getSections()) {
                if (item != null) {
                    List<SectionField> newSectionFields = new ArrayList<>();
                    if (item.getFields() != null) {
                        newSectionFields.addAll(item.getFields());
                    }
                    if (item.getSections() != null) {
                        for (Section sectionItem : item.getSections()) {
                            if (sectionItem.getFields() != null) {
                                allInOneList.addAll(
                                        setIndexForSectionFieldList(sectionItem.getFields(), i));
                                i++;
                            }
                        }

                    } else {
                        allInOneList.addAll(setIndexForSectionFieldList(newSectionFields, i));
                        i++;
                    }
                }
            }
        }

        setFields(convertSectionFieldsList(allInOneList, this));
        setSections_credit_size(allInOneList.size());
    }

    private List<SectionField> setIndexForSectionFieldList(List<SectionField> list, int index) {
        List<SectionField> resultList = new ArrayList<>();

        if (list != null) {
            for (SectionField item : list) {
                item.setCreditSectionI(index);
                resultList.add(item);
            }
        }

        return resultList;
    }

    private List<DBSectionField> convertSectionFieldsList(List<SectionField> arraySectionFields,
                                                          DBSection dbSection) {
        List<DBSectionField> list = new ArrayList<>();

        if (arraySectionFields != null) {
            for (SectionField sectionField : arraySectionFields) {
                list.add(new DBSectionField(sectionField, dbSection));
            }
        }

        return list;
    }


    public int getSections_credit_size() {
        return sections_credit_size;
    }

    public void setSections_credit_size(int sectionsCreditSize) {
        sections_credit_size = sectionsCreditSize;
    }

    public int getDebtors_id() {
        return debtors_id;
    }

    public void setDebtors_id(int debtorsId) {
        debtors_id = debtorsId;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public String getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public Collection<DBSectionField> getFields() {
        return fields;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFields(Collection<DBSectionField> fields) {
        this.fields = fields;
    }

}
