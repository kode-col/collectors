package com.lifecorp.collector.model.database.helpers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.lifecorp.collector.model.database.objects.DBActionResultType;
import com.lifecorp.collector.model.database.objects.DBActionType;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.database.objects.DBQueue;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;

import java.util.ArrayList;
import java.util.List;

public class DBFront extends DBAbstractHelper {

    private static class SingletonHolder {
        public static final DBFront INSTANCE = new DBFront();
    }

    public static DBFront getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @NonNull
    public List<DBDebtorProfile> getAllDebtors() {
        List<DBDebtorProfile> debtors = null;

        try {
            setHelper();
            debtors = helper.getDebtorProfileDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return debtors != null ? debtors : new ArrayList<DBDebtorProfile>();
    }

    @NonNull
    public List<DBCollection> getAllCollections() {
        List<DBCollection> collections = null;

        try {
            setHelper();
            collections = helper.getCollectionDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return collections != null ? collections : new ArrayList<DBCollection>();
    }

    @NonNull
    public List<DBEvent> getAllEvents() {
        List<DBEvent> collections = null;

        try {
            setHelper();
            collections = helper.getEventDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return collections != null ? collections : new ArrayList<DBEvent>();
    }

    @NonNull
    public List<DBSection> getAllSections() {
        List<DBSection> sections = null;

        try {
            setHelper();
            sections = helper.getSectionDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return sections != null ? sections : new ArrayList<DBSection>();
    }

    @NonNull
    public List<DBSection> getSectionByDebId(int debtors_id) {
        List<DBSection> sections = null;

        try {
            setHelper();
            sections = helper.getSectionDao().queryBuilder().where().eq("debtors_id", debtors_id)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return sections != null ? sections : new ArrayList<DBSection>();
    }

    @Nullable
    public DBSectionField getSectionFieldById(int id) {
        List<DBSectionField> list = null;

        try {
            setHelper();
            list = helper.getSectionFieldDao().queryBuilder().where().eq("id_", id).query();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    @NonNull
    public List<DBActionType> getActionTypes() {
        List<DBActionType> dataList = null;

        try {
            setHelper();
            dataList = helper.getActionTypeDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return dataList != null ? dataList : new ArrayList<DBActionType>();
    }

    @NonNull
    public List<DBActionResultType> getActionResultTypes() {
        List<DBActionResultType> dataList = null;

        try {
            setHelper();
            dataList = helper.getActionResultTypeDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return dataList != null ? dataList : new ArrayList<DBActionResultType>();
    }

    @NonNull
    public List<DBCollectionImageUpload> getCollectionImageUpload() {
        List<DBCollectionImageUpload> dataList = null;

        try {
            setHelper();
            dataList = helper.getCollectionImageUploadDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return dataList != null ? dataList : new ArrayList<DBCollectionImageUpload>();
    }

    @NonNull
    public List<DBCollectionImageUpload> getCollectionImageUploadForSend() {
        List<DBCollectionImageUpload> dataList = getCollectionImageUpload();
        List<DBCollectionImageUpload> resultList = new ArrayList<>();

        for (DBCollectionImageUpload item : dataList) {
            if (item.getDbImageUpload() != null && item.getDbImageUpload().getExternalId() != -1) {
                resultList.add(item);
            }
        }

        return resultList;
    }

    @NonNull
    public List<DBEventImageUpload> getEventImageUpload() {
        List<DBEventImageUpload> dataList = null;

        try {
            setHelper();
            dataList = helper.getEventImageUploadDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return dataList != null ? dataList : new ArrayList<DBEventImageUpload>();
    }

    @NonNull
    public List<DBEventImageUpload> getEventImageUploadForSend() {
        List<DBEventImageUpload> dataList = getEventImageUpload();
        List<DBEventImageUpload> resultList = new ArrayList<>();

        for (DBEventImageUpload item : dataList) {
            if (item.getDbImageUpload() != null && item.getDbImageUpload().getExternalId() != -1) {
                resultList.add(item);
            }
        }

        return resultList;
    }

    public void clearAllTables() {
        try {
            setHelper();
            helper.clearAllTables();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    public void removeDBCollectionImageUpload(DBCollectionImageUpload dbCollectionImageUpload) {
        if (dbCollectionImageUpload != null) {
            try {
                setHelper();
                helper.getCollectionImageUploadDao().delete(dbCollectionImageUpload);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                releaseHelper(helper);
            }
        }
    }

    public void removeDBEventImageUpload(DBEventImageUpload dbEventImageUpload) {
        if (dbEventImageUpload != null) {
            try {
                setHelper();
                helper.getEventImageUploadDao().delete(dbEventImageUpload);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                releaseHelper(helper);
            }
        }
    }

    public boolean isQueueNotEmpty() {
        return getQueueSize() > 0;
    }

    public void removeQueueById(int id) {
        try {
            setHelper();
            helper.getQueueDao().deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }
    }

    public boolean isQueryExistInDB(int id) {
        List<DBQueue> dbQueues;
        boolean result = false;

        try {
            setHelper();
            dbQueues = helper.getQueueDao().queryBuilder().where().eq("request_id", id).query();

            if (dbQueues != null && dbQueues.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return result;
    }

    @Nullable
    public DBQueue getLastQueue() {
        List<DBQueue> dbQueues = new ArrayList<>();

        try {
            setHelper();
            dbQueues.addAll(helper.getQueueDao().queryForAll());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return dbQueues.size() > 0 ? dbQueues.get(0) : null;
    }

    public int getQueueSize() {
        List<DBQueue> dbQueues = new ArrayList<>();

        try {
            setHelper();
            dbQueues.addAll(helper.getQueueDao().queryForAll());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            releaseHelper(helper);
        }

        return dbQueues.size();
    }

}
