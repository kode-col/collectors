package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBMoney;

import java.sql.SQLException;

public class MoneyDao extends BaseDaoImpl<DBMoney, Integer> {

    public MoneyDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBMoney.class);
    }

}
