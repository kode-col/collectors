package com.lifecorp.collector.model.database.objects;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.event.Event;
import com.lifecorp.collector.network.api.DBCachePoint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DatabaseTable(tableName = "event")
public class DBEvent implements Serializable {

    @DatabaseField(generatedId = true)
    private int id_;

    @DatabaseField
    private String id;

    @DatabaseField
    private boolean mCompleted;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
    private DBDebtorProfile mDebtorProfile;

    @DatabaseField
    private int mType;

    @DatabaseField
    private String mDescription;

    @DatabaseField
    private long mTime;

    @DatabaseField
    private int mResultTypeId;

    @DatabaseField
    private String mSummary;

    @ForeignCollectionField(eager = true)
    private Collection<DBEventImageId> imagesExternal;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true)
    // Координата, где было закрыто мероприятие
    private DBCoordinate poiCoordinate;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true, columnName = "promised_payment")
    private DBMoney promisedPaymentSum; // Сумма обещанного платежа

    @DatabaseField(useGetSet = true, columnName = "promised_payment_date")
    private long promisedPaymentDate; // Дата обещанного платежа


    public DBEvent() {

    }

    public DBEvent(Event event) {
        if (event != null) {
            setId(event.getId());
            setCompleted(event.isCompleted());
            setDescription(event.getDescription());
            setTime(event.getTime());
            setType(event.getType());
            setResultTypeId(event.getResultTypeId());
            setSummary(event.getSummary());
            setDebtorProfile(DBCachePoint.getInstance().findDebtorById(event.getDebtorId()));
            setPromisedPaymentDate(event.getPromisedPaymentDate());

            if (event.getImageIds() != null) {
                setImagesExternal(convertImagesExternal(event.getImageIds()));
            }
            if (event.getPromisedPayment() != null) {
                setPromisedPaymentSum(new DBMoney(event.getPromisedPayment()));
            }
            if (event.getPoiCoordinate() != null) {
                setPoiCoordinate(new DBCoordinate(event.getPoiCoordinate()));
            }
        }
    }

    @NonNull
    private List<DBEventImageId> convertImagesExternal(List<String> imageIds) {
        List<DBEventImageId> imagesIdsList = new ArrayList<>();

        if (imageIds != null) {
            for (String id : imageIds) {
                imagesIdsList.add(new DBEventImageId(id, this));
            }
        }

        return imagesIdsList;
    }


    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isCompleted() {
        return mCompleted;
    }

    public void setCompleted(boolean completed) {
        mCompleted = completed;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        mTime = time;
    }

    public DBDebtorProfile getDebtorProfile() {
        return mDebtorProfile;
    }

    public void setDebtorProfile(DBDebtorProfile debtorProfile) {
        mDebtorProfile = debtorProfile;
    }

    public int getResultTypeId() {
        return mResultTypeId;
    }

    public void setResultTypeId(int resultTypeId) {
        mResultTypeId = resultTypeId;
    }

    public String getSummary() {
        return mSummary;
    }

    public void setSummary(String summary) {
        mSummary = summary;
    }

    public Collection<DBEventImageId> getImagesExternal() {
        return imagesExternal;
    }

    public void setImagesExternal(List<DBEventImageId> images) {
        imagesExternal = images;
    }

    public DBCoordinate getPoiCoordinate() {
        return poiCoordinate;
    }

    public void setPoiCoordinate(DBCoordinate coordinate) {
        poiCoordinate = coordinate;
    }

    public DBMoney getPromisedPaymentSum() {
        return promisedPaymentSum;
    }

    public void setPromisedPaymentSum(DBMoney sum) {
        promisedPaymentSum = sum;
    }

    public long getPromisedPaymentDate() {
        return promisedPaymentDate;
    }

    public void setPromisedPaymentDate(long date) {
        promisedPaymentDate = date;
    }

    @Override
    public String toString() {
        return getId() + " " + getType() + " " + getDebtorProfile().getLastName();
    }

}
