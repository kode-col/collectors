package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBSectionField;

import java.sql.SQLException;

public class SectionFieldDao extends BaseDaoImpl<DBSectionField, Integer> {

    public SectionFieldDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBSectionField.class);
    }

}
