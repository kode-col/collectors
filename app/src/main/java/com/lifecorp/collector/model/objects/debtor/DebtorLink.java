package com.lifecorp.collector.model.objects.debtor;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexander Smirnov on 07.05.15.
 *
 * Хранит данные со ссылкой на внешний источник
 */
public class DebtorLink {

    @SerializedName("type")
    private String mType;

    @SerializedName("link")
    private String mLink;

    public String getType() {
        return mType;
    }

    public String getLink() {
        return mLink;
    }

}
