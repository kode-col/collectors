package com.lifecorp.collector.model.database.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.dao.ActionResultTypeDao;
import com.lifecorp.collector.model.database.dao.ActionTypeDao;
import com.lifecorp.collector.model.database.dao.AddressDao;
import com.lifecorp.collector.model.database.dao.CollectionDao;
import com.lifecorp.collector.model.database.dao.CollectionImageIdDao;
import com.lifecorp.collector.model.database.dao.CollectionImageUploadDao;
import com.lifecorp.collector.model.database.dao.DebtorLinkDao;
import com.lifecorp.collector.model.database.dao.DebtorProfileDao;
import com.lifecorp.collector.model.database.dao.EventCompletionDao;
import com.lifecorp.collector.model.database.dao.EventDao;
import com.lifecorp.collector.model.database.dao.EventImageIdDao;
import com.lifecorp.collector.model.database.dao.EventImageUploadDao;
import com.lifecorp.collector.model.database.dao.QueueDao;
import com.lifecorp.collector.model.database.dao.SectionDao;
import com.lifecorp.collector.model.database.dao.SectionFieldDao;
import com.lifecorp.collector.model.database.objects.DBActionResultType;
import com.lifecorp.collector.model.database.objects.DBActionType;
import com.lifecorp.collector.model.database.objects.DBAddress;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBCollectionImageId;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBCoordinate;
import com.lifecorp.collector.model.database.objects.DBDebtorLink;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.database.objects.DBEventCompletion;
import com.lifecorp.collector.model.database.objects.DBEventImageId;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.database.objects.DBImageId;
import com.lifecorp.collector.model.database.objects.DBImageUpload;
import com.lifecorp.collector.model.database.objects.DBMoney;
import com.lifecorp.collector.model.database.objects.DBPhoneNumber;
import com.lifecorp.collector.model.database.objects.DBQueue;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.utils.Logger;

import java.sql.SQLException;


public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "LifeCollector.db";
    private static final int DATABASE_VERSION = 14;

    private EventDao mEventDao;
    private QueueDao mQueueDao;
    private AddressDao mAddressDao;
    private SectionDao mSectionDao;
    private ActionTypeDao mActionTypeDao;
    private CollectionDao mCollectionDao;
    private DebtorLinkDao mDebtorLinkDao;
    private EventImageIdDao mEventImageIdDao;
    private SectionFieldDao mSectionFieldDao;
    private DebtorProfileDao mDebtorProfileDao;
    private EventCompletionDao mEventCompletionDao;
    private ActionResultTypeDao mActionResultTypeDao;
    private EventImageUploadDao mEventImageUploadDao;
    private CollectionImageIdDao mCollectionImageIdDao;
    private CollectionImageUploadDao mCollectionImageUploadDao;

    private static DBHelper databaseHelper;
    private static int databaseUsers;
    private static Context context;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    public static synchronized DBHelper getHelper() {
        if (context == null) {
            throw new RuntimeException("DBHelper doesn't init");
        }

        databaseUsers++;

        if (databaseUsers == 1) {
            databaseHelper = OpenHelperManager.getHelper(context, DBHelper.class);
        }

        return databaseHelper;
    }

    public synchronized void realiseHelper() {
        if (databaseUsers > 0) databaseUsers--;

        if (databaseUsers == 0) {
            if (databaseHelper != null) {
                OpenHelperManager.releaseHelper();
                databaseHelper = null;
            }
        }
    }

    public static synchronized void init(Context context) {
        DBHelper.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        Logger.d("onCreate");
        try {
            TableUtils.createTableIfNotExists(connectionSource, DBCoordinate.class);
            TableUtils.createTableIfNotExists(connectionSource, DBEvent.class);
            TableUtils.createTableIfNotExists(connectionSource, DBMoney.class);
            TableUtils.createTableIfNotExists(connectionSource, DBCollection.class);
            TableUtils.createTableIfNotExists(connectionSource, DBPhoneNumber.class);
            TableUtils.createTableIfNotExists(connectionSource, DBSectionField.class);
            TableUtils.createTableIfNotExists(connectionSource, DBSection.class);
            TableUtils.createTableIfNotExists(connectionSource, DBDebtorProfile.class);
            TableUtils.createTableIfNotExists(connectionSource, DBAddress.class);
            TableUtils.createTableIfNotExists(connectionSource, DBQueue.class);
            TableUtils.createTableIfNotExists(connectionSource, DBActionType.class);
            TableUtils.createTableIfNotExists(connectionSource, DBActionResultType.class);
            TableUtils.createTableIfNotExists(connectionSource, DBImageUpload.class);
            TableUtils.createTableIfNotExists(connectionSource, DBEventImageUpload.class);
            TableUtils.createTableIfNotExists(connectionSource, DBCollectionImageUpload.class);
            TableUtils.createTableIfNotExists(connectionSource, DBImageId.class);
            TableUtils.createTableIfNotExists(connectionSource, DBEventImageId.class);
            TableUtils.createTableIfNotExists(connectionSource, DBCollectionImageId.class);
            TableUtils.createTableIfNotExists(connectionSource, DBEventCompletion.class);
            TableUtils.createTableIfNotExists(connectionSource, DBDebtorLink.class);
        } catch (SQLException e) {
            Logger.e("Can't create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion,
                          int newVersion) {
        Logger.d("onUpgrade");
        try {
            TableUtils.dropTable(connectionSource, DBCoordinate.class, true);
            TableUtils.dropTable(connectionSource, DBDebtorProfile.class, true);
            TableUtils.dropTable(connectionSource, DBEvent.class, true);
            TableUtils.dropTable(connectionSource, DBMoney.class, true);
            TableUtils.dropTable(connectionSource, DBCollection.class, true);
            TableUtils.dropTable(connectionSource, DBPhoneNumber.class, true);
            TableUtils.dropTable(connectionSource, DBSection.class, true);
            TableUtils.dropTable(connectionSource, DBSectionField.class, true);
            TableUtils.dropTable(connectionSource, DBAddress.class, true);
            TableUtils.dropTable(connectionSource, DBQueue.class, true);
            TableUtils.dropTable(connectionSource, DBActionType.class, true);
            TableUtils.dropTable(connectionSource, DBActionResultType.class, true);
            TableUtils.dropTable(connectionSource, DBImageUpload.class, true);
            TableUtils.dropTable(connectionSource, DBEventImageUpload.class, true);
            TableUtils.dropTable(connectionSource, DBCollectionImageUpload.class, true);
            TableUtils.dropTable(connectionSource, DBImageId.class, true);
            TableUtils.dropTable(connectionSource, DBEventImageId.class, true);
            TableUtils.dropTable(connectionSource, DBCollectionImageId.class, true);
            TableUtils.dropTable(connectionSource, DBEventCompletion.class, true);
            TableUtils.dropTable(connectionSource, DBDebtorLink.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Logger.e("Can't upgrade databases", e);
        }
    }

    public void clearDebtorProfileTable() throws SQLException {
        TableUtils.clearTable(this.connectionSource, DBDebtorProfile.class);
    }

    public void clearEventTable() throws SQLException {
        TableUtils.clearTable(this.connectionSource, DBEvent.class);
    }

    public void clearActionTypeTable() throws SQLException {
        TableUtils.clearTable(this.connectionSource, DBActionType.class);
    }

    public void clearActionResultTypeTable() throws SQLException {
        TableUtils.clearTable(this.connectionSource, DBActionResultType.class);
    }

    public void clearCollectionTable() throws SQLException {
        TableUtils.clearTable(this.connectionSource, DBCollection.class);
    }

    public void clearSectionTable() throws SQLException {
        TableUtils.clearTable(this.connectionSource, DBSection.class);
    }

    public void clearSectionFieldTable() throws SQLException {
        TableUtils.clearTable(this.connectionSource, DBSectionField.class);
    }

    public void clearAllTables() throws SQLException {
        clearEventTable();
        clearActionTypeTable();
        clearCollectionTable();
        clearDebtorProfileTable();
        clearActionResultTypeTable();
        clearSectionTable();
        clearSectionFieldTable();

        TableUtils.clearTable(this.connectionSource, DBCoordinate.class);
        TableUtils.clearTable(this.connectionSource, DBMoney.class);
        TableUtils.clearTable(this.connectionSource, DBPhoneNumber.class);
        TableUtils.clearTable(this.connectionSource, DBEventImageId.class);
        TableUtils.clearTable(this.connectionSource, DBCollectionImageId.class);
        TableUtils.clearTable(this.connectionSource, DBImageId.class);
        TableUtils.clearTable(this.connectionSource, DBDebtorLink.class);
    }

    @Override
    public void close() {
        super.close();

        mEventDao = null;
        mQueueDao = null;
        mAddressDao = null;
        mSectionDao = null;
        mActionTypeDao = null;
        mCollectionDao = null;
        mDebtorLinkDao = null;
        mEventImageIdDao = null;
        mSectionFieldDao = null;
        mDebtorProfileDao = null;
        mEventCompletionDao = null;
        mActionResultTypeDao = null;
        mEventImageUploadDao = null;
        mCollectionImageIdDao = null;
        mCollectionImageUploadDao = null;
    }

    public DebtorProfileDao getDebtorProfileDao() throws SQLException {
        if (mDebtorProfileDao == null) {
            mDebtorProfileDao = new DebtorProfileDao(getConnectionSource());
        }
        return mDebtorProfileDao;
    }

    public EventDao getEventDao() throws SQLException {
        if (mEventDao == null) {
            mEventDao = new EventDao(getConnectionSource());
        }
        return mEventDao;
    }

    public ActionTypeDao getActionTypeDao() throws SQLException {
        if (mActionTypeDao == null) {
            mActionTypeDao = new ActionTypeDao(getConnectionSource());
        }

        return mActionTypeDao;
    }

    public ActionResultTypeDao getActionResultTypeDao() throws SQLException {
        if (mActionResultTypeDao == null) {
            mActionResultTypeDao = new ActionResultTypeDao(getConnectionSource());
        }

        return mActionResultTypeDao;
    }

    public CollectionDao getCollectionDao() throws SQLException {
        if (mCollectionDao == null) {
            mCollectionDao = new CollectionDao(getConnectionSource());
        }
        return mCollectionDao;
    }

    public AddressDao getAddressDao() throws SQLException {
        if (mAddressDao == null) {
            mAddressDao = new AddressDao(getConnectionSource());
        }
        return mAddressDao;
    }

    public QueueDao getQueueDao() throws SQLException {
        if (mQueueDao == null) {
            mQueueDao = new QueueDao(getConnectionSource());
        }
        return mQueueDao;
    }

    public SectionDao getSectionDao() throws SQLException {
        if (mSectionDao == null) {
            mSectionDao = new SectionDao(getConnectionSource());
        }
        return mSectionDao;
    }

    public SectionFieldDao getSectionFieldDao() throws SQLException {
        if (mSectionFieldDao == null) {
            mSectionFieldDao = new SectionFieldDao(getConnectionSource());
        }
        return mSectionFieldDao;
    }


    public DebtorLinkDao getDebtorLinkDao() throws SQLException {
        if (mDebtorLinkDao == null) {
            mDebtorLinkDao = new DebtorLinkDao(getConnectionSource());
        }
        return mDebtorLinkDao;
    }

    public EventImageUploadDao getEventImageUploadDao() throws SQLException {
        if (mEventImageUploadDao == null) {
            mEventImageUploadDao = new EventImageUploadDao(getConnectionSource());
        }

        return mEventImageUploadDao;
    }

    public CollectionImageUploadDao getCollectionImageUploadDao() throws SQLException {
        if (mCollectionImageUploadDao == null) {
            mCollectionImageUploadDao = new CollectionImageUploadDao(getConnectionSource());
        }

        return mCollectionImageUploadDao;
    }

    public EventImageIdDao getEventImageIdDao() throws SQLException {
        if (mEventImageIdDao == null) {
            mEventImageIdDao = new EventImageIdDao(getConnectionSource());
        }

        return mEventImageIdDao;
    }

    public CollectionImageIdDao getCollectionImageIdDao() throws SQLException {
        if (mCollectionImageIdDao == null) {
            mCollectionImageIdDao = new CollectionImageIdDao(getConnectionSource());
        }

        return mCollectionImageIdDao;
    }

    public EventCompletionDao getEventCompletionDao() throws SQLException {
        if (mEventCompletionDao == null) {
            mEventCompletionDao = new EventCompletionDao(getConnectionSource());
        }

        return mEventCompletionDao;
    }

}
