package com.lifecorp.collector.model.database.helpers;

/**
 * Created by Alexander Smirnov on 25.03.15.
 */
public class DBAbstractHelper {

    protected DBHelper helper = null;

    protected DBAbstractHelper() {
    }

    protected static void releaseHelper(DBHelper dbHelper) {
        if (dbHelper != null) {
            dbHelper.realiseHelper();
        }
    }

    protected void setHelper() {
        helper = DBHelper.getHelper();
    }

}
