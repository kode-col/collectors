package com.lifecorp.collector.model.objects.debtor;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;

import java.util.Collection;
import java.util.List;

public class SectionField extends ServerResponse {

    @SerializedName("id")
    private String mId;

    @SerializedName("type")
    //Тип поля секции (0 - обычное поле, 1 - номер телефона, 2 - ссылка)
    private int mType;

    @SerializedName("title")
    //Заголовок поля
    private String mTitle;

    @SerializedName("value")
    //Текст(содержание) поля
    private String mValue;

    @SerializedName("phone_number")
    //Номер телефона для type == 1
    private PhoneNumber mPhoneNumber;

    @SerializedName("url_link")
    //Ссылка
    private List<DebtorLink> mUrlLink;

    @SerializedName("poi_coordinate")
    private Coordinate mPoiCoordinate;

    // Изначально был хак для нескольких версий кредитов, перешёл в хак для несколько секции
    // информации по кредиту.
    // credit_section_i (0 – Основная информация, 1 – История платежей, 2 – Поручители)
    private int mCreditSectionI; // credit_info_section_index

    public int getCreditSectionI() {
        return mCreditSectionI;
    }

    public void setCreditSectionI(int index) {
        mCreditSectionI = index;
    }

    public String getId() {
        return mId;
    }

    public int getType() {
        return mType;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getValue() {
        return mValue;
    }

    public PhoneNumber getPhoneNumber() {
        return mPhoneNumber;
    }

    public Coordinate getPoiCoordinate() {
        return mPoiCoordinate;
    }

    public List<DebtorLink> getUrlLink() {
        return mUrlLink;
    }

    // @TODO продолжить
    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {
        if (getTitle() == null) {
            errorCollection.add("title must not be null");
        }
    }

}
