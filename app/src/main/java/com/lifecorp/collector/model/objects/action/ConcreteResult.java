package com.lifecorp.collector.model.objects.action;

import com.google.gson.annotations.SerializedName;

/**
* Created by maximefimov on 28.08.14.
*/
public final class ConcreteResult {

    @SerializedName("name")
    // Название результата
    private String mName;

    @SerializedName("result_id")
    private int mId;

    public String getName() {
        return mName;
    }

    public int getId() {
        return mId;
    }
}
