package com.lifecorp.collector.model.objects.auth;


import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.model.objects.DeviceInfo;

public class AuthBody {

    @SerializedName("login")
    private String login;

    @SerializedName("password")
    private String password;

    @SerializedName("device_info")
    private DeviceInfo deviceInfo;

    public AuthBody() {

    }

    public AuthBody(String login, String password) {
        setLogin(login);
        setPassword(password);
        //setDeviceInfo(DeviceInfo.create(App.getContext()));
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}
