package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;

import java.sql.SQLException;

public class DebtorProfileDao extends BaseDaoImpl<DBDebtorProfile, String> {

    public DebtorProfileDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBDebtorProfile.class);
    }

}
