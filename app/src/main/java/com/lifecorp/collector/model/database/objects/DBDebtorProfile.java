package com.lifecorp.collector.model.database.objects;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.objects.debtor.Debtor;
import com.lifecorp.collector.model.objects.debtor.Section;
import com.lifecorp.collector.model.objects.debtor.SectionField;
import com.lifecorp.collector.network.NetworkConstants;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.utils.AddressGroupBuilder;
import com.lifecorp.collector.utils.DateUtils;
import com.lifecorp.collector.utils.MoneyFormatter;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@DatabaseTable(tableName = "debtor_profile")
public class DBDebtorProfile implements Serializable {

    @DatabaseField(generatedId = true, useGetSet = true)
    private int id_;

    @DatabaseField(useGetSet = true)
    private int id;

    @DatabaseField(useGetSet = true, columnName = "first_name")
    private String firstName;

    @DatabaseField(useGetSet = true, columnName = "middle_name")
    private String middleName;

    @DatabaseField(useGetSet = true, columnName = "last_name")
    private String lastName;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true, columnName = "primary_phone")
    private DBPhoneNumber primaryPhone;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true, columnName = "total_debt_amount")
    private DBMoney totalDebtAmount;

    @DatabaseField(useGetSet = true, columnName = "total_debt_delay")
    private int totalDebtDelay;

    @DatabaseField(useGetSet = true, columnName = "photo_url")
    private String photoUrl;

    @DatabaseField(useGetSet = true)
    private Float distance;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true)
    private DBCoordinate coordinatePrimary;

    @DatabaseField(useGetSet = true, columnName = "check_payment")
    private int checkPromisedPayment; // Признак обещанного платежа (1 – обещал заплатить, 0 – нет)

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true, columnName = "promised_payment")
    private DBMoney promisedPaymentSum; // Сумма обещанного платежа

    @DatabaseField(useGetSet = true, columnName = "promised_payment_date")
    private long promisedPaymentDate; // Дата обещанного платежа



    public DBDebtorProfile() {
    }

    public DBDebtorProfile(Debtor debtor) {
        if (debtor != null) {
            setId(debtor.getId());
            setFirstName(debtor.getFirstName());
            setMiddleName(debtor.getMiddleName());
            setLastName(debtor.getLastName());

            setTotalDebtDelay(debtor.getTotalDebtDelay());
            setPhotoUrl(buildPhotoUrl(debtor.getId()));
            setCoordinatePrimary(getPrimaryCoordinate(debtor));
            setCheckPromisedPayment(debtor.getCheckPromisedPayment());
            setPromisedPaymentDate(debtor.getPromisedPaymentDate());

            if (debtor.getPrimaryPhone() != null) {
                setPrimaryPhone(new DBPhoneNumber(debtor.getPrimaryPhone()));
            }
            if (debtor.getTotalDebtAmount() != null) {
                setTotalDebtAmount(new DBMoney(debtor.getTotalDebtAmount()));
            }
            if (debtor.getPromisedPaymentSum() != null) {
                setPromisedPaymentSum(new DBMoney(debtor.getPromisedPaymentSum()));
            }
        }
    }

    private String buildPhotoUrl(int debtorId) {
        return String.format(NetworkConstants.SERVER_PATTERN_DEBTOR_PHOTO,
                NetworkConstants.SERVER_ADDRESS, debtorId);
    }

    /**
     * дублирующая логика в {@link com.lifecorp.collector.utils.AddressGroupBuilder:getAddress()}
     **/
    private DBCoordinate getPrimaryCoordinate(Debtor debtor) {
        if (debtor != null) {
            List<Section> sections = new ArrayList<>(debtor.getQuestionnaire().getSections());
            if (sections.size() > 0) {

                for (Section section : sections) {
                    if (section.getType() != DBSection.TYPE_CREDIT_INFO
                            && section.getFields() != null) {

                        for (SectionField field : section.getFields()) {
                            if (AddressGroupBuilder.isAddressField(field.getId())) {
                                return new DBCoordinate(field.getPoiCoordinate());
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public DBCoordinate getCoordinatePrimary() {
        return coordinatePrimary;
    }

    public void setCoordinatePrimary(DBCoordinate coordinate) {
        coordinatePrimary = coordinate;
    }

    public int getCheckPromisedPayment() {
        return checkPromisedPayment;
    }

    public void setCheckPromisedPayment(int value) {
        checkPromisedPayment = value;
    }

    public DBMoney getPromisedPaymentSum() {
        return promisedPaymentSum;
    }

    public void setPromisedPaymentSum(DBMoney sum) {
        promisedPaymentSum = sum;
    }

    public long getPromisedPaymentDate() {
        return promisedPaymentDate;
    }

    public void setPromisedPaymentDate(long date) {
        promisedPaymentDate = date;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFio() {
        String fio = "";

        if (getLastName() != null) {
            fio += getLastName();
        }

        if (getFirstName() != null) {
            fio += " " + getFirstName();
        }

        if (getMiddleName() != null) {
            fio += " " + getMiddleName();
        }

        return fio.trim();
    }

    public String getFioShort() {
        String fio = "";


        if (getLastName() != null) {
            fio += getLastName();
        }

        if (getFirstName() != null) {
            fio += " " + getFirstName().substring(0, 1) + ".";
        }

        if (getMiddleName() != null) {
            fio += " " + getMiddleName().substring(0, 1) + ".";
        }

        return fio.trim();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public DBPhoneNumber getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(DBPhoneNumber primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public DBMoney getTotalDebtAmount() {
        return totalDebtAmount;
    }

    public void setTotalDebtAmount(DBMoney totalDebtAmount) {
        this.totalDebtAmount = totalDebtAmount;
    }

    public int getTotalDebtDelay() {
        return totalDebtDelay;
    }

    public void setTotalDebtDelay(int totalDebtDelay) {
        this.totalDebtDelay = totalDebtDelay;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String buildPhotoUrl() {
        return getPhotoUrl() + PreferencesStorage.getInstance().getToken()
                + "&thumb=true&thumb-height=128&thumb-width=128";
    }

    public void setPhotoUrl(String url) {
        photoUrl = url;
    }

    public List<DBSection> getSections() {
        return DBCachePoint.getInstance().getSectionsByDebtorsId(this.getId());
    }

    public boolean isPromised() {
        return getCheckPromisedPayment() == 1;
    }

    @NonNull
    public String getPromisedSum() {
        return getPromisedPaperMoney() + ", " + getPromisedCoins();
    }

    @NonNull
    public String getTotalDebtPaperMoney() {
        String[] sum = getTotalDebtExplode();
        String money = "";

        if (sum != null && sum.length == 2) {
            money = sum[0];
        }

        return money;
    }

    @NonNull
    public String getTotalDebtCoins() {
        String[] sum = getTotalDebtExplode();
        String coins = "";

        if (sum != null && sum.length == 2) {
            coins = sum[1];
        }

        return coins;
    }

    @NonNull
    public String getPromisedPaperMoney() {
        String[] sum = getPromisedPaymentSumExplode();
        String money = "";

        if (sum != null && sum.length == 2) {
            money = sum[0];
        }

        return money;
    }

    @NonNull
    public String getPromisedCoins() {
        String[] sum = getPromisedPaymentSumExplode();
        String coins = "";

        if (sum != null && sum.length == 2) {
            coins = sum[1];
        }

        return coins;
    }

    @Nullable
    private String[] getTotalDebtExplode() {
        return explodeMoney(getTotalDebtAmount());
    }

    private String[] explodeMoney(DBMoney moneyForExplode) {
        return moneyForExplode != null
                ? MoneyFormatter.format(moneyForExplode.getAmount())
                : null;
    }

    @Nullable
    private String[] getPromisedPaymentSumExplode() {
        return explodeMoney(getPromisedPaymentSum());
    }

    @NonNull
    public String getPromisedDate() {
        String result = "";

        if (getPromisedPaymentDate() != 0) {
            String pattern = App.getContext().getString(R.string.promised_payment_date_pattern);
            result =  String.format(pattern,
                    DateUtils.formatTimestamp(getPromisedPaymentDate()));
        }

        return result;
    }

}
