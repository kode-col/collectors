package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.PhotoContainer;

import java.io.Serializable;

/**
 * Created by Alexander Smirnov on 24.04.15.
 *
 * Изображение для загрузки
 */
@DatabaseTable(tableName = "image_upload")
public class DBImageUpload implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(useGetSet = true)
    private String filePath;

    @DatabaseField(useGetSet = true)
    private float latitude;

    @DatabaseField(useGetSet = true)
    private float longitude;

    @DatabaseField(useGetSet = true)
    private int externalId;

    public DBImageUpload() {

    }

    public DBImageUpload(PhotoContainer photoContainer) {
        if (photoContainer != null) {
            setExternalId(photoContainer.getExternalId());
            setFilePath(photoContainer.getFilePath());
            setLatitude(photoContainer.getLatitude());
            setLongitude(photoContainer.getLongitude());
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String path) {
        filePath = path;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public int getExternalId() {
        return externalId;
    }

    public void setExternalId(int id) {
        externalId = id;
    }

}
