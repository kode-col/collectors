package com.lifecorp.collector.model.objects.debtor;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;

import java.util.Collection;
import java.util.List;

public class Section extends ServerResponse {

    @SerializedName("id")
    private String mId;

    @SerializedName("type")
    private int mType;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("fields")
    private List<SectionField> mFields;

    @SerializedName("sections")
    private List<Section> mSections;

    public String getId() {
        return mId;
    }

    public int getType() {
        return mType;
    }

    public String getTitle() {
        return mTitle;
    }

    public List<SectionField> getFields() {
        return mFields;
    }

    public List<Section> getSections() {
        return mSections;
    }

    // @TODO дополнить
    @Override
    protected void fillErrors(@NonNull Collection<String> errorCollection) {
        if (getTitle() == null) {
            errorCollection.add("title must not be null");
        }
    }
}
