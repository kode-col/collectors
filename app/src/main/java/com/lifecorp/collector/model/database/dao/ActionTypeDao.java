package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBActionType;

import java.sql.SQLException;

public class ActionTypeDao extends BaseDaoImpl<DBActionType, Integer> {

    public ActionTypeDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBActionType.class);
    }

}
