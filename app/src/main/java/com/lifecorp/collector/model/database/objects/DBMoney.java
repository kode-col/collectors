package com.lifecorp.collector.model.database.objects;

import android.content.res.Resources;
import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.objects.Money;

import java.io.Serializable;

@DatabaseTable(tableName = "money")
public class DBMoney implements Serializable {

    @DatabaseField(generatedId = true)
    private int ID;

    @DatabaseField
    private float amount;

    @DatabaseField
    private String currency;


    public DBMoney() {
    }

    public DBMoney(Money money) {
        if (money != null) {
            setAmount(money.getAmount());
            setCurrency(money.getCurrency());
        }
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @NonNull
    public String getOutputCurrency() {
        Resources res = App.getContext().getResources();
        final String rublePatternForChange = res.getString(R.string.money_ru);
        final String rublePatternAfterChange = res.getString(R.string.money_ru_good);

        if (currency == null) {
            currency = "";
        }

        return rublePatternForChange.equals(currency) ? rublePatternAfterChange : currency;
    }

}
