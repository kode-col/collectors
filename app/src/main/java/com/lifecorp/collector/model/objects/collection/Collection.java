package com.lifecorp.collector.model.objects.collection;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.model.database.objects.DBAddress;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.objects.Money;
import com.lifecorp.collector.model.objects.PhotoContainer;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.model.objects.debtor.Coordinate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Collection extends ServerResponse implements Serializable {

    @SerializedName("id")
    private int mId;

    @SerializedName("debtor_id")
    // ID заемщика к кому относится запись
    private int mDebtorId;

    @SerializedName("contract_id")
    // ID номеров договора к которому относится запись
    private String mContractId;

    @SerializedName("type")
    // Тип действия (0 - выезд, 1 - звонок)
    private int mType;

    @SerializedName("date")
    // Дата создания задачи
    private long mDate;

    @SerializedName("address")
    // Адреса заемщика
    private List<String> mAddress;

    @SerializedName("status")
    // Статус (0 - открыт, 1 - выполнен)
    private int mStatus;

    @SerializedName("summary")
    // Результат (1 - положительный, 2 - отрицательный)
    private String mSummary;

    @SerializedName("result")
    // Результат
    private String mResult;

    @SerializedName("visit_date")
    // Дата планового визита
    private long mVisitDate;

    @SerializedName("pay_date")
    // Дата платежа
    private long mPayDate;

    @SerializedName("comment")
    // Комментарий
    private String mComment;

    @SerializedName("title")
    // Тема
    private String mTitle;

    @SerializedName("summ")
    // Сумма платежа
    private Money mSum;

    @SerializedName("completed")
    // Статус выполнения
    private boolean completed;

    @SerializedName("poi_coordinate")
    private Coordinate mPoiCoordinate;

    @SerializedName("image_ids")
    private List<String> mImageIds;

    private transient List<PhotoContainer> mPhotoContainerList;

    public Collection() {

    }

    public Collection(DBCollection collection) {
        if (collection != null) {
            setId(collection.getId());
            setDebtorId(collection.getDebtorId());
            setContractId(collection.getContractId());
            setType(collection.getType());
            setDate(collection.getDate());
            setStatus(collection.getStatus());
            setSummary(collection.getSummary());
            setResult(collection.getResult());
            setVisitDate(collection.getVisitDate());
            setPayDate(collection.getPayDate());
            setComment(collection.getComment());
            setTitle(collection.getTitle());
            setSum(new Money(collection.getSum()));
            setCompleted(collection.getCompleted());
            setAddress(convertAddressList(collection.getAddress()));
            setPhotoContainerList(convertImages(collection.getImagesLocal()));
        }
    }

    @NonNull
    private List<PhotoContainer> convertImages(java.util.Collection<DBCollectionImageUpload> data) {
        List<PhotoContainer> photoContainerList = new ArrayList<>();

        if (data != null) {
            for (DBCollectionImageUpload item : data) {
                photoContainerList.add(new PhotoContainer(item));
            }
        }

        return photoContainerList;
    }

    private List<String> convertAddressList(java.util.Collection<DBAddress> dbAddressCollection) {
        List<DBAddress> dbAddressList;
        List<String> addressList = new ArrayList<>();

        if (dbAddressCollection != null) {
            dbAddressList = dbAddressCollection instanceof List
                    ? (List<DBAddress>) dbAddressCollection
                    : new ArrayList<>(dbAddressCollection);

            for (DBAddress item : dbAddressList) {
                addressList.add(item.getAddress());
            }
        }

        return addressList;
    }

    public String getContractId() {
        return mContractId;
    }

    public void setContractId(String contractId) {
        mContractId = contractId;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getDebtorId() {
        return mDebtorId;
    }

    public void setDebtorId(int debtorId) {
        mDebtorId = debtorId;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }

    public List<String> getAddress() {
        return mAddress;
    }

    public void setAddress(List<String> address) {
        mAddress = address;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

    public String getSummary() {
        return mSummary;
    }

    public void setSummary(String summary) {
        mSummary = summary;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public long getVisitDate() {
        return mVisitDate;
    }

    public void setVisitDate(long visitDate) {
        mVisitDate = visitDate;
    }

    public long getPayDate() {
        return mPayDate;
    }

    public void setPayDate(long payDate) {
        mPayDate = payDate;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
       mTitle = title;
    }

    public Money getSum() {
        return mSum;
    }

    public void setSum(Money sum) {
        mSum = sum;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Coordinate getPoiCoordinate() {
        return mPoiCoordinate;
    }

    public void setPoiCoordinate(Coordinate poiCoordinate) {
        mPoiCoordinate = poiCoordinate;
    }

    public List<PhotoContainer> getPhotoContainerList() {
        return mPhotoContainerList;
    }

    public void setPhotoContainerList(List<PhotoContainer> photoContainerList) {
        mPhotoContainerList = photoContainerList;
    }

    public List<String> getImageIds() {
        return mImageIds;
    }

    @Override
    public void fillErrors(@NonNull java.util.Collection<String> errorCollection) {

    }

}
