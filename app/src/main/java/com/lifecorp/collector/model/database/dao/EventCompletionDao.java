package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBEventCompletion;

import java.sql.SQLException;

/**
 * Created by Alexander Smirnov on 06.05.15.
 * <p/>
 * Dao для DBEventComplation
 */
public class EventCompletionDao extends BaseDaoImpl<DBEventCompletion, Integer> {

    public EventCompletionDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBEventCompletion.class);
    }
}
