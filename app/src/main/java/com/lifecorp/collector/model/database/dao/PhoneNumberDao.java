package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBPhoneNumber;

import java.sql.SQLException;

public class PhoneNumberDao extends BaseDaoImpl<DBPhoneNumber, Integer> {

    public PhoneNumberDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBPhoneNumber.class);
    }

}
