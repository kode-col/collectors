package com.lifecorp.collector.model.objects.debtor;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.network.api.response.ServerResponse;

import java.util.Collection;
import java.util.List;

public class Questionnaire extends ServerResponse {

    @SerializedName("id")
    private String mId;

    @SerializedName("sections")
    // Секции анкеты заемщика
    private List<Section> mSections;

    public List<Section> getSections() {
        return mSections;
    }

    public String getId() {
        return mId;
    }

    @Override
    public void fillErrors(@NonNull Collection<String> errorCollection) {
        if (mSections == null) {
            errorCollection.add("sections must not be null");
        }
    }
}
