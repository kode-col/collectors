package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBCollection;

import java.sql.SQLException;

public class CollectionDao extends BaseDaoImpl<DBCollection, Integer> {

    public CollectionDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBCollection.class);
    }

}

