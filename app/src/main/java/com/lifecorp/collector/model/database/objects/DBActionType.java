package com.lifecorp.collector.model.database.objects;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.action.ActionType;

/**
 * Created by maximefimov on 28.08.14.
 *
 * Тип действия, например: выезд к клиенту, звонок поручителю и т.п.
 */
@DatabaseTable(tableName = "action_type")
public class DBActionType {

    @DatabaseField(generatedId = true)
    private int id_;

    @DatabaseField
    private int mId;

    @DatabaseField
    private String mName;

    public DBActionType() {
    }

    public DBActionType(ActionType actionType) {
        if (actionType != null) {
            setId(actionType.getId());
            setName(actionType.getName());
        }
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
