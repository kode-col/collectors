package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Alexander Smirnov on 05.05.15.
 *
 * Таблица содержит ид изображений на сервере
 */
@DatabaseTable(tableName = "image_id")
public class DBImageId implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(useGetSet = true)
    private String imageId;

    public DBImageId() {

    }

    public DBImageId(String imageId) {
        setImageId(imageId);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String id) {
        imageId = id;
    }

    @Override
    public String toString() {
        return imageId;
    }

}
