package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBActionResultType;

import java.sql.SQLException;

public class ActionResultTypeDao extends BaseDaoImpl<DBActionResultType, Integer> {

    public ActionResultTypeDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBActionResultType.class);
    }

}
