package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.debtor.DebtorLink;
import com.lifecorp.collector.model.objects.debtor.SectionField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DatabaseTable(tableName = "section_field")
public class DBSectionField implements Serializable {

    @DatabaseField(generatedId = true, useGetSet = true)
    private int id_;

    @DatabaseField(useGetSet = true)
    private String id;

    @DatabaseField(useGetSet = true)
    private int type;

    @DatabaseField(useGetSet = true)
    private String title;

    @DatabaseField(useGetSet = true)
    private String value;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true,
            useGetSet = true)
    private DBPhoneNumber phone_number;

    // credit_section_i (0 – Основная информация, 1 – История платежей, 2 – Поручители)
    @DatabaseField(useGetSet = true)
    private int credit_section_i;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, useGetSet = true)
    private DBSection dbSection;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true)
    private DBCoordinate poiCoordinate;

    @ForeignCollectionField(eager = true)
    private Collection<DBDebtorLink> debtorLinks;

    public DBSectionField() {

    }

    public DBSectionField(SectionField sectionField, DBSection dbSection) {
        if (sectionField != null) {
            setCredit_section_i(sectionField.getCreditSectionI());

            setDbSection(dbSection);
            setId(sectionField.getId());
            setType(sectionField.getType());
            setTitle(sectionField.getTitle());
            setValue(sectionField.getValue());

            if (sectionField.getPhoneNumber() != null) {
                setPhone_number(new DBPhoneNumber(sectionField.getPhoneNumber()));
            }
            if (sectionField.getPoiCoordinate() != null) {
                setPoiCoordinate(new DBCoordinate(sectionField.getPoiCoordinate()));
            }
            setDebtorLinks(convertLinks(sectionField.getUrlLink()));
        }
    }

    private List<DBDebtorLink> convertLinks(List<DebtorLink> debtorLinks) {
        List<DBDebtorLink> result = new ArrayList<>();

        if (debtorLinks != null) {
            for (DebtorLink item : debtorLinks) {
                result.add(new DBDebtorLink(item, this));
            }
        }

        return result;
    }

    public void setDebtorLinks(Collection<DBDebtorLink> dbDebtorLinks) {
        debtorLinks = dbDebtorLinks;
    }

    public Collection<DBDebtorLink> getDebtorLinks() {
        return debtorLinks;
    }

    public int getCredit_section_i() {
        return credit_section_i;
    }

    public void setCredit_section_i(int index) {
        credit_section_i = index;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public DBSection getDbSection() {
        return dbSection;
    }

    public void setDbSection(DBSection dbSection) {
        this.dbSection = dbSection;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public DBPhoneNumber getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(DBPhoneNumber phone_number) {
        this.phone_number = phone_number;
    }

    public DBCoordinate getPoiCoordinate() {
        return poiCoordinate;
    }

    public void setPoiCoordinate(DBCoordinate coordinate) {
        poiCoordinate = coordinate;
    }

}
