package com.lifecorp.collector.model.objects.action;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
* Created by maximefimov on 28.08.14.
*/
public final class StatusResult {

    @SerializedName("result_types")
    private List<ConcreteResult> mConcreteResults;

    @SerializedName("stat")
    private String mStatusName;

    @SerializedName("stat_id")
    // Было в документации
    private int mStatId;

    public List<ConcreteResult> getConcreteResults() {
        return mConcreteResults;
    }

    public String getStatusName() {
        return mStatusName;
    }

    public int getStatId() {
        return mStatId;
    }

}
