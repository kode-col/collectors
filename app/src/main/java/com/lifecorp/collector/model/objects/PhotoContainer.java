package com.lifecorp.collector.model.objects;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.database.objects.DBImageUpload;
import com.lifecorp.collector.utils.ImageFileHelper;

import java.io.Serializable;

/**
 * Created by Alexander Smirnov on 22.04.15.
 *
 * Контейнер имитирующий фото, необходим для отправки фото на сервер.
 * Содержит путь к файлу и координаты геолокации.
 */
public class PhotoContainer implements Serializable {

    private int mExternalId = -1;
    private Float mLatitude;
    private Float mLongitude;
    private String mFilePath;

    public PhotoContainer(DBCollectionImageUpload data) {
        if (data != null) {
            setData(data.getDbImageUpload());
        }
    }

    public PhotoContainer(DBEventImageUpload data) {
        if (data != null) {
            setData(data.getDbImageUpload());
        }
    }

    public PhotoContainer() {

    }

    private void setData(DBImageUpload dbImageUpload) {
        if (dbImageUpload != null) {
            setFilePath(dbImageUpload.getFilePath());
            setExternalId(dbImageUpload.getExternalId());
            setLatitude(dbImageUpload.getLatitude());
            setLongitude(dbImageUpload.getLongitude());
        }
    }

    public int getExternalId() {
        return mExternalId;
    }

    public void setExternalId(int id) {
        mExternalId = id;
    }

    @Nullable
    public Bitmap getBitmap() {
        return ImageFileHelper.loadBitmapFromFile(getFilePath());
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public Float getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Float latitude) {
        mLatitude = latitude;
    }

    public Float getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Float longitude) {
        mLongitude = longitude;
    }

    public void removePhoto() {
        ImageFileHelper.removeImageFile(getFilePath());

        setFilePath(null);
        setLatitude(null);
        setLongitude(null);
    }

    public boolean isEmpty() {
        return mFilePath == null || mFilePath.isEmpty();
    }

}
