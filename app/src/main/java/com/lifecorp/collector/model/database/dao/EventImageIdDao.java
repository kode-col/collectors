package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBEventImageId;

import java.sql.SQLException;

/**
 * Created by Alexander Smirnov on 24.04.15.
 *
 * DAO для DBEventImageId
 */
public class EventImageIdDao extends BaseDaoImpl<DBEventImageId, Integer> {

    public EventImageIdDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBEventImageId.class);
    }

}
