package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBQueue;

import java.sql.SQLException;

public class QueueDao extends BaseDaoImpl<DBQueue, Integer> {

    public QueueDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBQueue.class);
    }

}
