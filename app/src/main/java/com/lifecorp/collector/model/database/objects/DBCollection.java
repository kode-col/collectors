package com.lifecorp.collector.model.database.objects;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.PhotoContainer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DatabaseTable(tableName = "collection")
public class DBCollection implements Serializable {

    @DatabaseField(generatedId = true, useGetSet = true)
    private int id_;

    @DatabaseField(useGetSet = true)
    private int id;

    @DatabaseField(useGetSet = true, columnName = "debtor_id")
    private int debtorId;

    @DatabaseField(useGetSet = true, columnName = "contract_id")
    private String contractId;

    @DatabaseField(useGetSet = true)
    private int type;

    @DatabaseField(useGetSet = true)
    private long date;

    @ForeignCollectionField(eager = true)
    private Collection<DBAddress> address;

    @DatabaseField(useGetSet = true)
    private int status;

    @DatabaseField(useGetSet = true)
    private String summary;

    @DatabaseField(useGetSet = true)
    private String result;

    @DatabaseField(useGetSet = true, columnName = "visit_date")
    private long visitDate;

    @DatabaseField(useGetSet = true, columnName = "pay_date")
    private long payDate;

    @DatabaseField(useGetSet = true)
    private String comment;

    @DatabaseField(useGetSet = true)
    private String title;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true, columnName = "summ")
    private DBMoney sum;

    @DatabaseField(useGetSet = true)
    private boolean completed;

    @ForeignCollectionField(eager = true)
    private Collection<DBCollectionImageUpload> imagesLocal;

    @ForeignCollectionField(eager = true)
    private Collection<DBCollectionImageId> imagesExternal;

    public DBCollection() {
    }

    public DBCollection(com.lifecorp.collector.model.objects.collection.Collection collection) {
        if (collection != null) {
            setId(collection.getId());
            setDebtorId(collection.getDebtorId());
            setContractId(collection.getContractId());
            setType(collection.getType());
            setDate(collection.getDate());
            setStatus(collection.getStatus());
            setSummary(collection.getSummary());
            setResult(collection.getResult());
            setVisitDate(collection.getVisitDate());
            setPayDate(collection.getPayDate());
            setComment(collection.getComment());
            setTitle(collection.getTitle());
            setCompleted(collection.isCompleted());

            if (collection.getSum() != null) {
                setSum(new DBMoney(collection.getSum()));
            }

            setAddress(convertAddressList(collection.getAddress()));
            setImagesLocal(convertImages(collection.getPhotoContainerList()));
            setImagesExternal(convertImagesExternal(collection.getImageIds()));
        }
    }

    @NonNull
    private List<DBCollectionImageId> convertImagesExternal(List<String> imageIds) {
        List<DBCollectionImageId> imagesIdsList = new ArrayList<>();

        if (imageIds != null) {
            for (String id : imageIds) {
                imagesIdsList.add(new DBCollectionImageId(id, this));
            }
        }

        return imagesIdsList;
    }

    @NonNull
    private List<DBCollectionImageUpload> convertImages(List<PhotoContainer> photoContainerList) {
        List<DBCollectionImageUpload> list = new ArrayList<>();

        if (photoContainerList != null) {
            for (PhotoContainer item : photoContainerList) {
                list.add(new DBCollectionImageUpload(item, this));
            }
        }

        return list;
    }

    @NonNull
    private List<DBAddress> convertAddressList(List<String> arrayStr) {
        List<DBAddress> list = new ArrayList<>();

        if (arrayStr != null) {
            for (String str : arrayStr) {
                list.add(new DBAddress(str, this));
            }
        }

        return list;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(int debtorId) {
        this.debtorId = debtorId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Collection<DBAddress> getAddress() {
        return address;
    }

    public void setAddress(List<DBAddress> address) {
        this.address = address;
    }

    public Collection<DBCollectionImageUpload> getImagesLocal() {
        return imagesLocal;
    }

    public void setImagesLocal(List<DBCollectionImageUpload> images) {
        imagesLocal = images;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public long getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(long visitDate) {
        this.visitDate = visitDate;
    }

    public long getPayDate() {
        return payDate;
    }

    public void setPayDate(long payDate) {
        this.payDate = payDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DBMoney getSum() {
        return sum;
    }

    public void setSum(DBMoney sum) {
        this.sum = sum;
    }

    public boolean getCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Collection<DBCollectionImageId> getImagesExternal() {
        return imagesExternal;
    }

    public void setImagesExternal(Collection<DBCollectionImageId> images) {
        imagesExternal = images;
    }

}
