package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.debtor.PhoneNumber;

import java.io.Serializable;

@DatabaseTable(tableName = "phone_number")
public class DBPhoneNumber implements Serializable {

    @DatabaseField(generatedId = true)
    private int ID;

    @DatabaseField
    private String display_string;

    @DatabaseField
    private String dial_number;

    @DatabaseField
    private String comment;

    public DBPhoneNumber() {

    }

    public DBPhoneNumber(PhoneNumber phoneNumber) {
        if (phoneNumber != null) {
            setDisplayString(phoneNumber.getDisplayString());
            setDialNumber(phoneNumber.getDialNumber());
            setComment(phoneNumber.getComment());
        }
    }

    public String getDisplayString() {
        return display_string;
    }

    public void setDisplayString(String displayString) {
        display_string = displayString;
    }

    public String getDialNumber() {
        return dial_number;
    }

    public void setDialNumber(String dialNumber) {
        dial_number = dialNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
