package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;

import java.sql.SQLException;

/**
 * Created by Alexander Smirnov on 24.04.15.
 *
 * DAO для DBImageUpload
 */
public class CollectionImageUploadDao extends BaseDaoImpl<DBCollectionImageUpload, Integer> {

    public CollectionImageUploadDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBCollectionImageUpload.class);
    }

}
