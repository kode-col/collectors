package com.lifecorp.collector.model.database.objects;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.debtor.Coordinate;

import java.io.Serializable;

@DatabaseTable(tableName = "coordinate")
public class DBCoordinate implements Serializable {

    @DatabaseField(generatedId = true)
    private int ID;

    @DatabaseField 
    private float latitude;
    @DatabaseField 
    private float longitude;


    public DBCoordinate() {
    }

    public DBCoordinate(Coordinate coordinate) {
        if (coordinate != null) {
            setLatitude(coordinate.getLatitude());
            setLongitude(coordinate.getLongitude());
        }
    }

    public boolean equals(DBCoordinate item) {
        return item != null
                && getID() == item.getID()
                && getLatitude() == item.getLatitude()
                && getLongitude() == item.getLongitude();

    }

    public int getID() {
        return ID;
    }

    public void setID(int id) {
        ID = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

}
