package com.lifecorp.collector.model.objects.address;

import com.lifecorp.collector.model.database.objects.DBCoordinate;

/**
 * Created by Alexander Smirnov on 09.04.15.
 *
 * Контейнер содержащий адрес, координаты и геоданные
 */
public class AddressContainer {

    private int mSectionId = -1;

    private String mTitle;
    private String mAddress;
    private String mCreditInfo;
    private String mGuarantorFio;
    private DBCoordinate mCoordinate;

    public AddressContainer() {

    }

    public AddressContainer(String guarantorFio, String creditInfo, String title, String address,
                            DBCoordinate coordinate) {
        setTitle(title);
        setSectionId(-1);
        setAddress(address);
        setCoordinate(coordinate);
        setCreditInfo(creditInfo);
        setGuarantorFio(guarantorFio);
    }

    public AddressContainer(String guarantorFio, String creditInfo, String title, String address,
                            DBCoordinate coordinate, int sectionId) {
        setTitle(title);
        setAddress(address);
        setSectionId(sectionId);
        setCoordinate(coordinate);
        setCreditInfo(creditInfo);
        setGuarantorFio(guarantorFio);
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public DBCoordinate getCoordinate() {
        return mCoordinate;
    }

    public void setCoordinate(DBCoordinate coordinate) {
        mCoordinate = coordinate;
    }

    public String getGuarantorFio() {
        return mGuarantorFio;
    }

    public void setGuarantorFio(String guarantorFio) {
        mGuarantorFio = guarantorFio;
    }

    public String getCreditInfo() {
        return mCreditInfo;
    }

    public void setCreditInfo(String mCreditInfo) {
        this.mCreditInfo = mCreditInfo;
    }

    public int getSectionId() {
        return mSectionId;
    }

    public void setSectionId(int sectionId) {
        mSectionId = sectionId;
    }

    private boolean equalStrings(String first, String second) {
        if (first == null && second == null) {
            return true;
        } else if (first == null || second == null) {
            return false;
        } else {
            return first.equals(second);
        }
    }

    private boolean equalsCoordinate(DBCoordinate first, DBCoordinate second) {
        if (first == null && second == null) {
            return true;
        } else if (first == null || second == null) {
            return false;
        } else {
            return first.equals(second);
        }
    }

    public boolean isEqual(AddressContainer item) {
        return item != null && equalStrings(getTitle(), item.getTitle())
                && equalsCoordinate(getCoordinate(), item.getCoordinate())
                && equalStrings(getAddress(), item.getAddress())
                && equalStrings(getCreditInfo(), item.getCreditInfo())
                && equalStrings(getGuarantorFio(), item.getGuarantorFio());
    }

}
