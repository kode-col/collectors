package com.lifecorp.collector.model.objects.action;

import com.google.gson.annotations.SerializedName;

/**
 * Created by maximefimov on 28.08.14.
 *
 * Тип действия, например: выезд к клиенту, звонок поручителю и т.п.
 */
public final class ActionType {

    @SerializedName("type")
    private int mId;

    @SerializedName("name")
    // Название, eg: выезд к клиенту, звонок к поручителю
    private String mName;

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }
}
