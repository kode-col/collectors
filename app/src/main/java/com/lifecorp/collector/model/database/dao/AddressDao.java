package com.lifecorp.collector.model.database.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.lifecorp.collector.model.database.objects.DBAddress;

import java.sql.SQLException;

public class AddressDao extends BaseDaoImpl<DBAddress, Integer> {

    public AddressDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, DBAddress.class);
    }

}
