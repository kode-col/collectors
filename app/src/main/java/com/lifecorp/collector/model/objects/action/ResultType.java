package com.lifecorp.collector.model.objects.action;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by maximefimov on 28.08.14.
 *
 * Тип результата
 */
public final class ResultType {

    @SerializedName("action_type")
    // Тип действия
    private int mActionTypeId;

    @SerializedName("types_for_stats")
    // Результат по статусу
    private List<StatusResult> mResultsByStatus;

    public int getActionTypeId() {
        return mActionTypeId;
    }

    public List<StatusResult> getResultsByStatus() {
        return mResultsByStatus;
    }

}
