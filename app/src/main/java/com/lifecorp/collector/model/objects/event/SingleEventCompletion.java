package com.lifecorp.collector.model.objects.event;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SingleEventCompletion {

    @SerializedName("events")
    // массив закрываемых мероприятий, всегда 1
    private final List<EventCompletion> mEvents;

    public SingleEventCompletion(EventCompletion eventCompletion) {
        mEvents = new ArrayList<>(1);
        mEvents.add(eventCompletion);
    }

    public List<EventCompletion> getEvents() {
        return mEvents;
    }

}
