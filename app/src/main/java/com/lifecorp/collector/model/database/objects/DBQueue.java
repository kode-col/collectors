package com.lifecorp.collector.model.database.objects;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "queue")
public class DBQueue {

    @DatabaseField(generatedId = true, useGetSet = true)
    private int ID;

    @DatabaseField(useGetSet = true)
    private int request_id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            useGetSet = true)
    private DBCollection collectionObj;

    @DatabaseField(foreign = true, foreignAutoRefresh = true,
            useGetSet = true)
    private DBEventCompletion eventCompletion;

    public DBQueue() {

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int requestId) {
        request_id = requestId;
    }

    public DBCollection getCollectionObj() {
        return collectionObj;
    }

    public void setCollectionObj(DBCollection collection) {
        collectionObj = collection;
    }

    public DBEventCompletion getEventCompletion() {
        return eventCompletion;
    }

    public void setEventCompletion(DBEventCompletion item) {
        eventCompletion = item;
    }

}
