package com.lifecorp.collector.model.database.objects;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Alexander Smirnov on 05.05.15.
 *
 * Таблица содержит ид изображений на сервере для коллекций
 */
@DatabaseTable(tableName = "collection_image_id")
public class DBCollectionImageId implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true,  foreignAutoCreate = true,
            useGetSet = true)
    private DBImageId dbImageId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, useGetSet = true)
    private DBCollection dbCollection;

    public DBCollectionImageId() {

    }

    public DBCollectionImageId(String id, DBCollection collection) {
        setDbCollection(collection);

        if (id != null) {
            setDbImageId(new DBImageId(id));
        }
    }

    public DBCollection getDbCollection() {
        return dbCollection;
    }

    public void setDbCollection(DBCollection collection) {
        dbCollection = collection;
    }

    public DBImageId getDbImageId() {
        return dbImageId;
    }

    public void setDbImageId(DBImageId imageUpload) {
        dbImageId = imageUpload;
    }

    @Override
    public String toString() {
        String collectionId = dbCollection != null  ? String.valueOf(dbCollection.getId()) : "";
        String imageId = dbImageId != null ? dbImageId.toString() : "";

        return collectionId + " " + imageId;
    }

}
