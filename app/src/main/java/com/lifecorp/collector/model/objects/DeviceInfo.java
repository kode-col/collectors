package com.lifecorp.collector.model.objects;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.lifecorp.collector.App;
import com.lifecorp.collector.utils.Logger;

import java.io.Serializable;
import java.util.Locale;

public class DeviceInfo implements Serializable {

    private static final int OS_ANDROID_ID = 2;

    private final static String DEFAULT_LOCALE = "ru_RU";

    @SerializedName("device_id")
    //Идентификатор устройства
    private String mDeviceId;

    @SerializedName("device_locale")
    //Локаль устроства в формате IETF BCP 47, eg: "en_US", "ru_RU"
    private String mDeviceLocale;

    @SerializedName("device_os")
    private int mDeviceOs;

    @SerializedName("device_os_version")
    //Версия ОС, eg: 5.1
    private String mDeviceOsVersion;

    @SerializedName("app_version")
    //Версия приложения(внутренняя, не как в сторе), eg: 2.3(25)
    private String mAppVersion;

    @SerializedName("location_lon")
    //Местоположение устройства(долгота) или null
    private Float mLocationLon;

    @SerializedName("location_lat")
    //Местоположение устройства(широта) или null
    private Float mLocationLat;

    private DeviceInfo() {

    }

    public static DeviceInfo create(@NonNull final Context context) {
        final DeviceInfo deviceInfo = new DeviceInfo();

        String deviceId = Settings.Secure.getString(
                App.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        deviceInfo.setDeviceId(deviceId);
        deviceInfo.setDeviceLocale(getLocale());
        deviceInfo.setDeviceOs(OS_ANDROID_ID);
        deviceInfo.setDeviceOsVersion(Build.VERSION.RELEASE);

        try {
            final PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            deviceInfo.setAppVersion(packageInfo.versionName);
        } catch (final PackageManager.NameNotFoundException e) {
            Logger.wtf("Package info wasn't found");
        }

        final LocationManager locationManager = (LocationManager) context.getSystemService(
                Context.LOCATION_SERVICE);
        final String provider = locationManager.getBestProvider(new Criteria(), true);
        if (provider != null) {

            // TODO Разобраться почему не работает с включенной геолокацией
//            final Location location = locationManager.getLastKnownLocation(provider);
//            if (location != null) {
//                deviceInfo.setLocationLat(location.getLatitude());
//                deviceInfo.setLocationLon(location.getLongitude());
//            }

            deviceInfo.setLocationLat(null);
            deviceInfo.setLocationLon(null);
        }

        return deviceInfo;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public String getDeviceLocale() {
        return mDeviceLocale;
    }

    public void setDeviceLocale(String deviceLocale) {
        mDeviceLocale = deviceLocale;
    }

    public int getDeviceOs() {
        return mDeviceOs;
    }

    public void setDeviceOs(int deviceOs) {
        mDeviceOs = deviceOs;
    }

    public String getDeviceOsVersion() {
        return mDeviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        mDeviceOsVersion = deviceOsVersion;
    }

    public String getAppVersion() {
        return mAppVersion;
    }

    public void setAppVersion(String appVersion) {
        mAppVersion = appVersion;
    }

    public Float getLocationLon() {
        return mLocationLon;
    }

    public void setLocationLon(Float locationLon) {
        mLocationLon = locationLon;
    }

    public Float getLocationLat() {
        return mLocationLat;
    }

    public void setLocationLat(Float locationLat) {
        mLocationLat = locationLat;
    }

    private static String getLocale() {
        final Locale locale = Locale.getDefault();
        return locale != null ? locale.toString() : DEFAULT_LOCALE;
    }

}
