package com.lifecorp.collector.model.database.objects;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.action.ResultType;
import com.lifecorp.collector.model.objects.action.StatusResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximefimov on 28.08.14.
 *
 * Тип результата
 */
@DatabaseTable(tableName = "action_result_type")
public class DBActionResultType {

    private final static Gson GSON = new Gson(); // костыль чтобы не городить 100500 DAO

    @DatabaseField(generatedId = true)
    private int id_;

    @DatabaseField
    private int mActionTypeId;

    @DatabaseField
    private String mResultsJSONed;

    public DBActionResultType() {
    }

    public DBActionResultType(ResultType resultType) {
        if (resultType != null) {
            setActionTypeId(resultType.getActionTypeId());
            setResultsByStatus(new ArrayList<>(resultType.getResultsByStatus()));
        }
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public int getActionTypeId() {
        return mActionTypeId;
    }

    public void setActionTypeId(int actionTypeId) {
        mActionTypeId = actionTypeId;
    }

    @SuppressWarnings("unchecked")
    public List<StatusResult> getResultsByStatus() {
        return GSON.fromJson(mResultsJSONed, new TypeToken<List<StatusResult>>() {}.getType());
    }

    public void setResultsByStatus(List<StatusResult> resultsByStatus) {
        mResultsJSONed = GSON.toJson(resultsByStatus);
    }

}
