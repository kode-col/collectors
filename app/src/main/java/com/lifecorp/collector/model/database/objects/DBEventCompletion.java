package com.lifecorp.collector.model.database.objects;

import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.lifecorp.collector.model.objects.PhotoContainer;
import com.lifecorp.collector.model.objects.event.EventCompletion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Alexander Smirnov on 06.05.15.
 *
 * Таблица для данных закрития мероприятия
 */
@DatabaseTable(tableName = "event_completion")
public class DBEventCompletion implements Serializable {

    @DatabaseField(generatedId = true)
    private int id_;

    @DatabaseField
    private String id;

    @DatabaseField
    private boolean mCompleted;

    @DatabaseField
    private String mComment;

    @DatabaseField
    private int mType;

    @DatabaseField
    private int mSummary;

    @DatabaseField
    private int mResultType;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true)
    // Координата, где было закрыто мероприятие
    private DBCoordinate poiCoordinate;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            useGetSet = true, columnName = "promised_payment")
    private DBMoney promisedPaymentSum; // Сумма обещанного платежа

    @DatabaseField(useGetSet = true, columnName = "promised_payment_date")
    private long promisedPaymentDate; // Дата обещанного платежа

    @ForeignCollectionField(eager = true)
    private Collection<DBEventImageUpload> imagesLocal;


    public DBEventCompletion() {

    }

    public DBEventCompletion(EventCompletion eventCompletion) {
        if (eventCompletion != null) {
            setId(eventCompletion.getId());
            setCompleted(eventCompletion.isCompleted());
            setComment(eventCompletion.getComment());
            setType(eventCompletion.getType());
            setSummary(eventCompletion.getSummary());
            setResultType(eventCompletion.getResultType());
            setImagesLocal(convertImages(eventCompletion.getPhotoContainerList()));
            setPromisedPaymentDate(eventCompletion.getPromisedPaymentDate());

            if (eventCompletion.getPromisedPayment() != null) {
                setPromisedPaymentSum(new DBMoney(eventCompletion.getPromisedPayment()));
            }
            if (eventCompletion.getPoiCoordinate() != null) {
                setPoiCoordinate(new DBCoordinate(eventCompletion.getPoiCoordinate()));
            }
        }
    }

    @NonNull
    private List<DBEventImageUpload> convertImages(List<PhotoContainer> photoContainerList) {
        List<DBEventImageUpload> list = new ArrayList<>();

        if (photoContainerList != null) {
            for (PhotoContainer item : photoContainerList) {
                list.add(new DBEventImageUpload(item, this));
            }
        }

        return list;
    }

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isCompleted() {
        return mCompleted;
    }

    public void setCompleted(boolean completed) {
        mCompleted = completed;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public int getResultType() {
        return mResultType;
    }

    public void setResultType(int resultTypeId) {
        mResultType = resultTypeId;
    }

    public int getSummary() {
        return mSummary;
    }

    public void setSummary(int summary) {
        mSummary = summary;
    }

    public Collection<DBEventImageUpload> getImagesLocal() {
        return imagesLocal;
    }

    public void setImagesLocal(List<DBEventImageUpload> images) {
        imagesLocal = images;
    }

    public DBCoordinate getPoiCoordinate() {
        return poiCoordinate;
    }

    public void setPoiCoordinate(DBCoordinate coordinate) {
        poiCoordinate = coordinate;
    }

    public DBMoney getPromisedPaymentSum() {
        return promisedPaymentSum;
    }

    public void setPromisedPaymentSum(DBMoney sum) {
        promisedPaymentSum = sum;
    }

    public long getPromisedPaymentDate() {
        return promisedPaymentDate;
    }

    public void setPromisedPaymentDate(long date) {
        promisedPaymentDate = date;
    }

}
