package com.lifecorp.collector.model.objects;

import android.graphics.Bitmap;

import com.lifecorp.collector.utils.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 22.04.15.
 *
 * Контейнер который содержит массив путей к фотографиям, реализует логику добавления и удаления
 * елементов массива
 */
public class PhotoArrayContainer implements Serializable {

    private static final int MAX_PHOTO_COUNT = 8;

    private PhotoContainer[] mPhotoArray = new PhotoContainer[MAX_PHOTO_COUNT - 1];
    private PhotoContainer mFirstPhoto = new PhotoContainer();

    public PhotoArrayContainer() {
        for (int i = 0; i < mPhotoArray.length; i++) {
            mPhotoArray[i] = new PhotoContainer();
        }
    }

    public int getCount() {
        return MAX_PHOTO_COUNT;
    }

    public Bitmap getItem(int position) {
        if (position == 0) {
            return mFirstPhoto.getBitmap();
        } else {
            position -= 1;
            return position < getCount() ? mPhotoArray[position].getBitmap() : null;
        }
    }

    public int countOfPossibleAddPhoto() {
        int count = 0;

        for (int i = 0; i < getCount(); i++) {
            Bitmap item = getItem(i);
            if (item == null) {
                count++;
            }
        }

        return count;
    }

    public void addPhoto(String filePath, double latitude, double longitude) {
        int countOfPossibleAddPhoto = countOfPossibleAddPhoto();

        if (countOfPossibleAddPhoto > 0) {
            if (countOfPossibleAddPhoto == 1) {
                mFirstPhoto.setFilePath(filePath);
                mFirstPhoto.setLatitude((float) latitude);
                mFirstPhoto.setLongitude((float) longitude);
            } else {
                PhotoContainer photoContainer = mPhotoArray[getFirstEmptyPosition()];
                photoContainer.setFilePath(filePath);
                photoContainer.setLatitude((float) latitude);
                photoContainer.setLongitude((float) longitude);
            }
        } else {
            Logger.e("Ошибка добавления фотографии! Контейнер заполнен!");
        }
    }

    private int getFirstEmptyPosition() {
        int position = -1;

        for (int i = 0; i < mPhotoArray.length; i++) {
            if (mPhotoArray[i].getBitmap() == null) {
                position = i;
                break;
            }
        }

        return position;
    }

    public void removePhoto(int position) {
        if (position == 0) {
            mFirstPhoto.removePhoto();
        } else if (position < getCount()) {
            position -= 1;
            mPhotoArray[position].removePhoto();

            sortArray();
        }
    }

    // Сортирует массив для выкидывания пустот из середины
    private void sortArray() {
        PhotoContainer[] photoArray = new PhotoContainer[mPhotoArray.length];

        int arrayPositionNew = 0;
        int arrayPositionOld = 0;

        for (;arrayPositionOld < mPhotoArray.length; arrayPositionOld++) {
            PhotoContainer item = mPhotoArray[arrayPositionOld];
            if (item.getBitmap() != null) {
                photoArray[arrayPositionNew] = item;
                arrayPositionNew++;
            }
        }

        for (; arrayPositionNew < mPhotoArray.length; arrayPositionNew++) {
            photoArray[arrayPositionNew] = new PhotoContainer();
        }

        mPhotoArray = photoArray;
    }

    public void removeAllPhoto() {
        mFirstPhoto.removePhoto();

        for (PhotoContainer item : mPhotoArray) {
            item.removePhoto();
        }
    }

    public List<PhotoContainer> getDataList() {
        List<PhotoContainer> photoContainerList = new ArrayList<>();

        for (PhotoContainer photoContainer : mPhotoArray) {
            if (!photoContainer.isEmpty()) {
                photoContainerList.add(photoContainer);
            }
        }

        if (!mFirstPhoto.isEmpty()) {
            photoContainerList.add(mFirstPhoto);
        }

        return photoContainerList;
    }

    public boolean isEmpty() {
        return countOfPossibleAddPhoto() == getCount();
    }

}
