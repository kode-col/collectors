package com.lifecorp.collector.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;

public class Utils {

    private static final String AT = "@";
    private static final String HTTP = "http://";
    private static final String HTTPS = "https://";
    private static final String MAILTO = "mailto:";

    public static void closeKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static int getImageForType(int type) {
        return type == 1
                ? R.drawable.ico_history_calling_black
                : R.drawable.ico_history_meeting_black;
    }

    public static int getExpiredImageForType(int type) {
        return type == 1
                ? R.drawable.ico_history_calling_red
                : R.drawable.ico_history_meeting_red;
    }

    public static String formatIdForOutput(int id) {
        return formatIdForOutput(String.valueOf(id));
    }

    public static String formatIdForOutput(String id) {
        return String.format(App.getContext().getString(R.string.pattern_id), id);
    }

    public static boolean isIntentAvailable(Intent intent) {
        final PackageManager packageManager = App.getContext().getPackageManager();
        return intent != null && intent.resolveActivity(packageManager) != null;
    }

    public static void setFont(TextView textView, String fontName) {
        if (textView != null && fontName != null) {
            textView.setTypeface(TypefaceManager.getInstance().getTypeface(fontName));
        }
    }

    public static String getValidUrl(String link) {
        if (link != null) {
            if (link.contains(AT)) {
                if (!link.startsWith(MAILTO)) {
                    link = MAILTO + link;
                }
            } else {
                if (!link.startsWith(HTTP) && !link.startsWith(HTTPS)) {
                    link = HTTP + link;
                }
            }
        }

        return link;
    }

    public static void showLinkInBrowser(Context context, String link) {
        if (context != null && link != null) {


            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getValidUrl(link)));

            if (Utils.isIntentAvailable(browserIntent)) {
                context.startActivity(browserIntent);
            } else {
                ToastHelper.getInstance().makeToast(context, "Ошибка!");
            }
        }
    }

}
