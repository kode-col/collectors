package com.lifecorp.collector.utils.preferences;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Set;

/**
 * Нижнеуровневый класс для обращения к преференсам.
 *
 * @author maximefimov
 */
final class PrimitivePreferences {

    private final SharedPreferences mSharedPreferences;

    public PrimitivePreferences(@NonNull final SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    //GETTERS

    public final boolean getBoolean(@NonNull final Key key, @Nullable final String suffix,
                                    final boolean defaultValue) {
        return mSharedPreferences.getBoolean(createFullKey(key, suffix), defaultValue);
    }

    public final boolean getBoolean(@NonNull final Key key, final boolean defaultValue) {
        return getBoolean(key, null, defaultValue);
    }

    public final float getFloat(@NonNull final Key key, @Nullable final String suffix,
                                final float defaultValue) {
        return mSharedPreferences.getFloat(createFullKey(key, suffix), defaultValue);
    }

    public final float getFloat(@NonNull final Key key, final float defaultValue) {
        return getFloat(key, null, defaultValue);
    }

    public final int getInt(@NonNull final Key key, @Nullable final String suffix,
                            final int defaultValue) {
        return mSharedPreferences.getInt(createFullKey(key, suffix), defaultValue);
    }

    public final int getInt(@NonNull final Key key, final int defaultValue) {
        return getInt(key, null, defaultValue);
    }

    public final long getLong(@NonNull final Key key, @Nullable final String suffix,
                              final long defaultValue) {
        return mSharedPreferences.getLong(createFullKey(key, suffix), defaultValue);
    }

    public final long getLong(@NonNull final Key key, final long defaultValue) {
        return getLong(key, null, defaultValue);
    }

    @Nullable
    public final String getString(@NonNull final Key key, @Nullable final String suffix,
                                  final String defaultValue) {
        return mSharedPreferences.getString(createFullKey(key, suffix), defaultValue);
    }

    @Nullable
    public final String getString(@NonNull final Key key, @Nullable final String defaultValue) {
        return getString(key, null, defaultValue);
    }

    @Nullable
    public final Set<String> getStringSet(@NonNull final Key key, @Nullable final String suffix,
                                          final Set<String> defaultValue) {
        return mSharedPreferences.getStringSet(createFullKey(key, suffix), defaultValue);
    }

    @Nullable
    public final Set<String> getStringSet(@NonNull final Key key,
                                          @Nullable final Set<String> defaultValue) {
        return getStringSet(key, null, defaultValue);
    }

    //SETTERS

    public final void putBoolean(@NonNull final Key key, @Nullable final String suffix,
                                 final boolean value) {
        mSharedPreferences.edit().putBoolean(createFullKey(key, suffix), value).apply();
    }

    public final void putBoolean(@NonNull final Key key, final boolean value) {
        putBoolean(key, null, value);
    }

    public final void putFloat(@NonNull final Key key, @Nullable final String suffix,
                               final float value) {
        mSharedPreferences.edit().putFloat(createFullKey(key, suffix), value).apply();
    }

    public final void putFloat(@NonNull final Key key, final float value) {
        mSharedPreferences.edit().putFloat(key.toString(), value).apply();
    }

    public final void putInt(@NonNull final Key key, @Nullable final String suffix,
                             final int value) {
        mSharedPreferences.edit().putInt(createFullKey(key, suffix), value).apply();
    }

    public final void putInt(@NonNull final Key key, final int value) {
        mSharedPreferences.edit().putInt(key.toString(), value).apply();
    }

    public final void putLong(@NonNull final Key key, @Nullable final String suffix,
                              final long value) {
        mSharedPreferences.edit().putLong(createFullKey(key, suffix), value).apply();
    }

    public final void putLong(@NonNull final Key key, final long value) {
        mSharedPreferences.edit().putLong(key.toString(), value).apply();
    }

    public final void putString(@NonNull final Key key, @Nullable final String suffix,
                                @Nullable final String value) {
        mSharedPreferences.edit().putString(createFullKey(key, suffix), value).apply();
    }

    public final void putString(@NonNull final Key key, @Nullable final String value) {
        mSharedPreferences.edit().putString(key.toString(), value).apply();
    }

    public final void putStringSet(@NonNull final Key key, @Nullable final String suffix,
                                   @Nullable final Set<String> value) {
        mSharedPreferences.edit().putStringSet(createFullKey(key, suffix), value).apply();
    }

    public final void putStringSet(@NonNull final Key key, @Nullable final Set<String> value) {
        mSharedPreferences.edit().putStringSet(key.toString(), value).apply();
    }

    // OTHER
    public final boolean contains(@NonNull final Key key, @Nullable final String suffix) {
        return mSharedPreferences.contains(createFullKey(key, suffix));
    }

    public final boolean contains(@NonNull final Key key) {
        return contains(key, null);
    }

    public final void remove(@NonNull final Key key, @Nullable final String suffix) {
        mSharedPreferences.edit().remove(createFullKey(key, suffix)).apply();
    }

    public final void remove(@NonNull final Key key) {
        remove(key, null);
    }

    public final void clear() {
        mSharedPreferences.edit().clear().apply();
    }

    private static String createFullKey(@NonNull final Key key, @Nullable final String suffix) {
        return key.toString() + (suffix != null ? suffix : "");
    }
}
