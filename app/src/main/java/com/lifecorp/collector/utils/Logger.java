package com.lifecorp.collector.utils;

import android.util.Log;

import com.lifecorp.collector.BuildConfig;

/**
 * Created by Alexander Smirnov on 19.03.15.
 *
 * Логгер позволяет выводить логи только при включенном дебаге
 */
public class Logger {

    public static void wtf(String msg) {
        if (BuildConfig.DEBUG) {
            Log.wtf(Const.LOG_TAG, msg);
        }
    }

    public static void d(String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(Const.LOG_TAG, msg);
        }
    }

    public static void e(String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(Const.LOG_TAG, msg);
        }
    }

    public static void e(String msg, Throwable tr) {
        if (BuildConfig.DEBUG) {
            Log.e(Const.LOG_TAG, msg, tr);
        }
    }

}
