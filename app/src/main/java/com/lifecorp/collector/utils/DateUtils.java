package com.lifecorp.collector.utils;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Alexander Smirnov on 14.05.15.
 *
 * Утилита для работы с датами
 */
@SuppressWarnings("unused")
public class DateUtils {
    private static final DateFormat DATE_DEFAULT_FORMAT =
            new SimpleDateFormat("dd.MM.yyyy", new Locale("ru", "RU"));
    private static final DateFormat DATE_SHORT_FORMAT =
            new SimpleDateFormat("d MMMM", new Locale("ru", "RU"));
    private static final DateFormat DATE_LONG_FORMAT =
            new SimpleDateFormat("d MMMM yyyy года", new Locale("ru", "RU"));

    private static Boolean sIsNeedUpdate = null;
    private static int mUtcOffset = -1;


    public static long timestampStringToTimestampLong(String timeStamp) {
        return timeStamp != null ? parseTimestampDate(timeStamp).getTime() : 0;
    }

    @NonNull
    public static Date parseTimestampDate(String timeStamp) {
        Date result;

        try {
            result = DATE_DEFAULT_FORMAT.parse(timeStamp);
        } catch (Exception e) {
            e.printStackTrace();
            result = new Date();
        }

        return result;
    }

    @NonNull
    public static String formatTimestamp(long timeStamp) {
        return formatDate(new Date(timeStamp));
    }

    @NonNull
    public static String formatShortTimestamp(long timeStamp) {
        return formatDate(new Date(timeStamp), DATE_SHORT_FORMAT);
    }

    @NonNull
    public static String formatLongTimestamp(long timeStamp) {
        return formatDate(new Date(timeStamp), DATE_LONG_FORMAT);
    }

    @NonNull
    public static String formatDate(Date date) {
        return formatDate(date, DATE_DEFAULT_FORMAT);
    }

    @NonNull
    public static String formatDate(Date date, DateFormat format) {
        return date != null && format != null ? format.format(date) : "";
    }

    @NonNull
    public static String formatCalendar(Calendar calendar) {
        return calendar != null ? formatDate(calendar.getTime()) : "";
    }

    @NonNull
    public static String formatShortCalendar(Calendar calendar) {
        return calendar != null ? formatDate(calendar.getTime(), DATE_SHORT_FORMAT) : "";
    }

    @NonNull
    public static String formatLongCalendar(Calendar calendar) {
        return calendar != null ? formatDate(calendar.getTime(), DATE_LONG_FORMAT) : "";
    }

    @NonNull
    public static Calendar timestampToCalendar(long timeStamp) {
        return dateToCalendar(new Date(timeStamp));
    }

    @NonNull
    public static Calendar dateToCalendar(Date date) {
        Calendar result = Calendar.getInstance();
        if (date != null) {
            result.setTime(date);
        }

        return result;
    }

    @NonNull
    public static Calendar dateAsStringToCalendar(String timeStamp) {
        return dateToCalendar(parseTimestampDate(timeStamp));
    }

    @NonNull
    public static Calendar getFirstDayForDate(Calendar calendar) {
        Calendar result = getCopyOfCalendar(calendar);
        result.set(Calendar.DAY_OF_WEEK, result.getFirstDayOfWeek());

        return result;
    }

    @NonNull
    public static Calendar getLastDayForDate(Calendar calendar) {
        Calendar result = getCopyOfCalendar(calendar);
        result.set(Calendar.DAY_OF_WEEK, getLastDayOfWeek());

        return result;
    }

    public static int getLastDayOfWeek() {
        return Calendar.getInstance().getFirstDayOfWeek() == Calendar.MONDAY
                ? Calendar.SUNDAY
                : Calendar.SATURDAY;
    }


    public static Calendar getCopyOfCalendar(Calendar calendar) {
        Calendar result = Calendar.getInstance();
        if (calendar != null) {
            result.setTime(calendar.getTime());
        }

        return result;
    }

    public static String getWeekForDate(long timestamp) {
        Calendar calendar = timestampToCalendar(timestamp);
        Calendar firstWeekDay = getFirstDayForDate(calendar);
        Calendar lastWeekDay = getLastDayForDate(calendar);

        return formatShortCalendar(firstWeekDay) + " – " + formatLongCalendar(lastWeekDay);
    }

    public static boolean isSingleDate(Long dateFrom, Long dateTo) {
        return dateFrom != null && dateTo != null && dateFrom.equals(dateTo);
    }

    private static Calendar getGMTCalendar(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
        calendar.setTimeInMillis(timestamp);

        return calendar;
    }

    public static boolean isDateNeedFix(long timestamp) {
        if (sIsNeedUpdate == null && timestamp != 0) {
            Calendar c1 = getGMTCalendar(timestamp);
            Calendar c2 = getGMTCalendar(timestamp + getUtcOffset());

            sIsNeedUpdate = c1.get(Calendar.DAY_OF_YEAR) != c2.get(Calendar.DAY_OF_YEAR);
        }

        return sIsNeedUpdate != null ? sIsNeedUpdate : false;
    }

    private static void setUtcOffset() {

    }

    public static int getUtcOffset() {
        if (mUtcOffset == -1) {
            Calendar c = Calendar.getInstance();
            mUtcOffset = c.get(Calendar.ZONE_OFFSET) + c.get(Calendar.DST_OFFSET);
        }

        return mUtcOffset;
    }

    public static long fixDateIfNeed(long date) {
        return isDateNeedFix(date) ? date + getUtcOffset() : date;
    }

}
