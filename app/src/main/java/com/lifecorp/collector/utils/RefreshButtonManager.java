package com.lifecorp.collector.utils;

import android.os.Handler;
import android.view.MenuItem;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.helpers.DBFront;
import com.lifecorp.collector.network.ImageQueueManager;
import com.lifecorp.collector.network.api.DBCachePoint;

/**
 * Created by Alexander Smirnov on 25.05.15.
 *
 * Управляет отображением текущего состояния кнопки синхронизации
 */
public class RefreshButtonManager {

    private final static int REFRESH_ID_GREEN = R.drawable.ico_navbar_refresh_green;
    private final static int REFRESH_ID_DEFAULT = R.drawable.ico_navbar_refresh;
    private final static int REFRESH_ID_RED = R.drawable.ico_navbar_refresh_red;

    private Handler mHandler = new Handler();
    private MenuItem mMenuItem;

    private boolean mIsOnPause = false;

    private int getDelay() {
        return App.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
    }

    public void setIsOnPause(boolean status) {
        mIsOnPause = status;
    }

    public void setMenuItem(MenuItem menuItem) {
        mMenuItem = menuItem;
    }

    private void updateIcon() {
        if (mMenuItem != null && !mIsOnPause) {
            mMenuItem.setIcon(REFRESH_ID_DEFAULT);

            if (DBFront.getInstance().isQueueNotEmpty()
                    || ImageQueueManager.getInstance().getImageQuerySize() > 0) {
                mMenuItem.setIcon(REFRESH_ID_RED);
            } else if (DBCachePoint.getInstance().isSynchronized()) {
                mMenuItem.setIcon(REFRESH_ID_GREEN);
            }
        }
    }

    public void updateStatus() {
        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateIcon();
            }
        }, getDelay());
    }

}
