package com.lifecorp.collector.utils.spinner;

import android.widget.AdapterView;

/**
 * Created by Alexander Smirnov on 08.05.15.
 *
 * Абстрактный класс который по сути является интервейсом AdapterView.OnItemSelectedListener и
 * единственное что делает реализует пустой метод onNothingSelected что бы не дублировать его во
 * всех последующих реализациях.
 */
public abstract class SpinnerSelector implements AdapterView.OnItemSelectedListener {

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // do nothing
    }

}