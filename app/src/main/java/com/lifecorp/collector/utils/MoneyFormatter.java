package com.lifecorp.collector.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Alexander Smirnov on 01.04.15.
 *
 * Утилита для форматирования вывода суммы
 */
public class MoneyFormatter {

    private static final int MAXIMUM_FRACTION_DIGITS = 2;
    private static final int MINIMUM_FRACTION_DIGITS = 2;

    /*
    * Метод форматирует сумму на вывод в формате, возвращает массив из двух элементов
    * [банкноты, монеты]
    */
    public static String[] format(float sum) {
        NumberFormat formatter = NumberFormat.getNumberInstance(new Locale("ru", "RU"));
        formatter.setMaximumFractionDigits(MAXIMUM_FRACTION_DIGITS);
        formatter.setMinimumFractionDigits(MINIMUM_FRACTION_DIGITS);
        String money = formatter.format(sum);

        int index = money.indexOf(",");
        String coins = "";

        if (index != -1) {
            coins = money.substring(index).replace(",", "");
            money = money.substring(0, index);
        }

        return new String[] {money, coins};
    }

}
