package com.lifecorp.collector.utils;

import android.support.annotation.NonNull;

public enum TabPosition {
    DEBTORS(0), EVENTS(1), MAP(2);

    private final int mCode;

    TabPosition(int code) {
        mCode = code;
    }

    public int getCode() {
        return mCode;
    }

    @NonNull
    public static TabPosition fromCode(int code) {
        for (TabPosition result : values()) {
            if (result.getCode() == code) {
                return result;
            }
        }
        return DEBTORS;
    }
}
