package com.lifecorp.collector.utils;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.view.PagerSlidingTabStrip;

/**
 * Created by Alexander Smirnov on 14.05.15.
 *
 * Устанавливает адаптер для ViewPager и настраивает PagerSlidingTabStrip
 */
public class SetAdapterForViewPagerWithTitleTask extends AsyncTask<Void, Void, Void> {
    private final ViewPager mViewPager;
    private final PagerAdapter mPagerAdapter;
    private final PagerSlidingTabStrip mTitleTabSliding;

    public SetAdapterForViewPagerWithTitleTask(ViewPager viewPager, PagerAdapter pagerAdapter,
                                               PagerSlidingTabStrip pagerSlidingTabStrip) {
        mViewPager = viewPager;
        mPagerAdapter = pagerAdapter;
        mTitleTabSliding = pagerSlidingTabStrip;
    }

    protected Void doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if(mPagerAdapter != null && mViewPager != null && mTitleTabSliding != null) {
            Resources res = App.getContext().getResources();
            mViewPager.setAdapter(mPagerAdapter);
            mTitleTabSliding.setViewPager(mViewPager);
            mTitleTabSliding.setTextSize(20);
            mTitleTabSliding.setIndicatorColor(res.getColor(R.color.color_tab_line));
            mTitleTabSliding.setTextColorResource(R.color.color_tab_line);
        }
    }

}