package com.lifecorp.collector.utils;

import android.support.annotation.NonNull;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBCoordinate;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.model.objects.address.AddressContainer;
import com.lifecorp.collector.model.objects.address.AddressGroupContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 13.04.15.
 *
 * Класс для построения контейнеров для
 */
public class AddressGroupBuilder {

    private static final String ADDRESS_REG = "address_reg";
    private static final String ADDRESS_FACT = "address_fact";

    private static class SingletonHolder {
        public static final AddressGroupBuilder INSTANCE = new AddressGroupBuilder();
    }

    public static AddressGroupBuilder getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @NonNull
    public List<AddressGroupContainer> getAllAddressFromDebtor(DBDebtorProfile debtor) {
        // Метод достаёт все адреса должника и его поручителей
        final List<AddressGroupContainer> addressList = new ArrayList<>();

        String debtorAddress = App.getContext().getString(R.string.debtor_address);
        AddressGroupContainer debtorAddressContainer = new AddressGroupContainer(debtorAddress);
        debtorAddressContainer.addAddress(getAddressForDebtor(debtor));
        addressList.add(debtorAddressContainer);

        String guarantors = App.getContext().getString(R.string.debtor_guarantors);
        AddressGroupContainer guarantorsAddressContainer = new AddressGroupContainer(guarantors);
        guarantorsAddressContainer.addAddress(getAddressForDebtorGuarantors(debtor));
        addressList.add(guarantorsAddressContainer);

        return addressList;
    }


    private boolean checkForGuarantors(int sectionType) {
        return sectionType == DBSection.TYPE_CREDIT_INFO;
    }

    private boolean checkForDebtorAddress(int sectionType) {
        return sectionType != DBSection.TYPE_CREDIT_INFO;
    }

    public static boolean isAddressField(String id) {
        return id != null && (id.equals(ADDRESS_REG) || id.equals(ADDRESS_FACT));
    }

    /**
     * дублирующая логика в {@link DBDebtorProfile:getPrimaryCoordinate()}
     **/
    @NonNull
    private List<AddressContainer> getAddress(DBDebtorProfile debtor, boolean isForDebtor) {
        List<AddressContainer> addressContainerList = new ArrayList<>();

        if (debtor != null) {
            List<DBSection> sectionList = debtor.getSections();
            if (sectionList != null && sectionList.size() > 0) {

                for (DBSection section : sectionList) {
                    String fullName = null;
                    String creditName = section.getTitle();

                    int type = section.getType();
                    if (isForDebtor ? checkForDebtorAddress(type) : checkForGuarantors(type)
                            && section.getFields() != null) {
                        for (DBSectionField field : section.getFields()) {
                            if (isForDebtor) {
                                if (isAddressField(field.getId())) {
                                    int fieldId = field.getId_();
                                    String title = field.getTitle();
                                    String address = field.getValue();
                                    DBCoordinate coordinate = field.getPoiCoordinate();

                                    addressContainerList.add(createAddressContainer(null, null,
                                            title, address, coordinate, fieldId));
                                }
                            } else {
                                if (field.getId().equals("fullName")) {
                                    fullName = field.getValue();
                                } else if (isAddressField(field.getId())) {
                                    int fieldId = field.getId_();
                                    String title = field.getTitle();
                                    String address = field.getValue();
                                    DBCoordinate coordinate = field.getPoiCoordinate();

                                    addressContainerList.add(createAddressContainer(fullName,
                                            creditName, title, address, coordinate, fieldId));
                                }
                            }
                        }
                    }
                }
            }
        }

        return addressContainerList;
    }

    @NonNull
    private List<AddressContainer> getAddressForDebtorGuarantors(DBDebtorProfile debtor) {
        return getAddress(debtor, false);
    }

    // берём все доступные координаты должника (без координат поручителя)
    @NonNull
    public List<AddressContainer> getAddressForDebtor(DBDebtorProfile debtor) {
        return getAddress(debtor, true);
    }

    private AddressContainer createAddressContainer(String guarantorFio, String creditInfo,
                                                    String title, String address,
                                                    DBCoordinate coordinate, int fieldId) {
        return new AddressContainer(guarantorFio, creditInfo, title, address, coordinate, fieldId);
    }

}
