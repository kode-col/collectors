package com.lifecorp.collector.utils;

/**
 * Хранит константы в приложении
 *
 * Константы относящиеся к сети находятся в {@link com.lifecorp.collector.network.NetworkConstants}
 */
public class Const {

    public static final int DEFAULT_TIME = 300;

    /** debug consts */
    public static final String LOG_TAG = "RMR";

    /** api consts */
    public static final String ACCESS_TOKEN = "access_token";
    public static final String EXTENDED_DEBTORS = "ext";

    /** tag consts */
    public static final String TAG_LOGIN = "TAG_LOGIN";
    public static final String TAG_EVENTS = "TAG_EVENTS";
    public static final String TAG_LOGOUT = "TAG_LOGOUT";
    public static final String TAG_DEBTOR = "TAG_DEBTOR";
    public static final String TAG_DEBTORS = "TAG_DEBTORS";
    public static final String TAG_DEBTOR_ID = "TAG_DEBTOR_ID";
    public static final String TAG_CREDIT_ID = "TAG_CREDIT_ID";
    public static final String TAG_TYPE_AUTH = "TAG_TYPE_AUTH";
    public static final String TAG_LOAD_IMAGE = "TAG_LOAD_IMAGE";
    public static final String TAG_COLLECTIONS = "TAG_COLLECTIONS";
    public static final String TAG_ACTION_TYPES = "TAG_ACTION_TYPES";
    public static final String TAG_LIST_FRAGMENT = "TAG_LIST_FRAGMENT";
    public static final String TAG_COMPLETE_EVENT = "TAG_COMPLETE_EVENT";
    public static final String TAG_POST_COLLECTION = "TAG_POST_COLLECTION";
    public static final String TAG_MARKER_FOR_ADDRESS = "TAG_MARKER_FOR_ADDRESS";
    public static final String TAG_ACTION_RESULT_TYPES = "TAG_ACTION_RESULT_TYPES";

}
