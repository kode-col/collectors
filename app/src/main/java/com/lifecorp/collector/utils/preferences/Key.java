package com.lifecorp.collector.utils.preferences;

/**
 * Created by Alexander Smirnov on 13.03.15.
 *
 * Ключи для хранения данных в преференсах.
 */
enum Key {
    ACCESS_TOKEN,
    LOGIN,
    PASSWORD
}
