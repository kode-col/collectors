package com.lifecorp.collector.utils.spinner;

/**
 * Created by Alexander Smirnov on 08.05.15.
 *
 * Используется для вывода
 */
public final class SpinnerItem {

    private final int mId;
    private final String mName;

    public SpinnerItem(int id, String name) {
        mId = id;
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    @Override
    public String toString() {
        return mName;
    }

}
