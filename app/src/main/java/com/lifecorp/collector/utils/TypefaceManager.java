package com.lifecorp.collector.utils;

import android.graphics.Typeface;

import com.lifecorp.collector.App;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alexander Smirnov on 05.03.15.
 * Хранилище шрифтов, используется для предотвращения повторной загрузки шрифта
 */
public class TypefaceManager {
    private Map<String, Typeface> mTypefaceMap = new HashMap<>();

    private static class SingletonHolder {
        public static final TypefaceManager SINGLETON_INSTANCE = new TypefaceManager();
    }

    private TypefaceManager() {
    }

    public static TypefaceManager getInstance() {
        return SingletonHolder.SINGLETON_INSTANCE;
    }

    public Typeface getTypeface(String typefaceName) {
        if (!mTypefaceMap.containsKey(typefaceName)) {
            loadTypeface(typefaceName);
        }

        return mTypefaceMap.containsKey(typefaceName) ? mTypefaceMap.get(typefaceName) : null;
    }

    private void loadTypeface(String typefaceName) {
        try {
            Typeface myTypeface = Typeface.createFromAsset(App.getContext().getAssets(),
                    "fonts/" + typefaceName);

            addTypeface(typefaceName, myTypeface);
        } catch(RuntimeException e) {
            e.printStackTrace();
            Logger.e("Ошибка загрузки шрифта" + typefaceName);
        }
    }

    public void addTypeface(String typefaceName, Typeface typeface) {
        if (typefaceName != null && typeface != null) {
            mTypefaceMap.put(typefaceName, typeface);
        }
    }

}
