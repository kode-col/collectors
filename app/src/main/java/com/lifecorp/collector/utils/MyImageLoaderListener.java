package com.lifecorp.collector.utils;

import android.graphics.Bitmap;
import android.view.View;

import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.activities.MainActivity;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class MyImageLoaderListener implements ImageLoadingListener {

    private MainActivity mMainActivity;

    public MyImageLoaderListener(MainActivity mainActivity) {
        mMainActivity = mainActivity;
    }

    @Override
    public void onLoadingStarted(String s, View view) {}

    @Override
    public void onLoadingFailed(String s, View view, FailReason failReason) {
        if (failReason != null && failReason.getCause() != null) {
            String failMsg = failReason.getCause().getMessage();

            if (failMsg != null && failMsg.contains(mMainActivity.getString(R.string.no_auth))) {
                Logger.e("No authentication challenges found --> postAuth");
                mMainActivity.login();
            }
        }
    }

    @Override
    public void onLoadingComplete(String s, View view, Bitmap bitmap) {}

    @Override
    public void onLoadingCancelled(String s, View view) {}

}
