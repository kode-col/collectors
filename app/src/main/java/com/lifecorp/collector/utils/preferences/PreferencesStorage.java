package com.lifecorp.collector.utils.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.lifecorp.collector.App;

/**
 * Created by Alexander Smirnov on 13.03.15.
 *
 * Хранилище настроек
 */
public class PreferencesStorage {

    private static class SingletonHolder {
        public static final PreferencesStorage INSTANCE = new PreferencesStorage();
    }

    private final PrimitivePreferences mPreferences;

    @NonNull
    public static PreferencesStorage getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private PreferencesStorage() {
        final SharedPreferences mSharedPreferences = App.getContext()
                .getSharedPreferences("preferences", Context.MODE_PRIVATE);
        mPreferences = new PrimitivePreferences(mSharedPreferences);
    }

    public void saveToken(String token) {
        mPreferences.putString(Key.ACCESS_TOKEN, token);
    }

    public String getToken() {
        return mPreferences.getString(Key.ACCESS_TOKEN, "");
    }

    public void clearToken() {
        saveToken("");
    }

    public void saveLogin(final String login) {
        mPreferences.putString(Key.LOGIN, login);
    }

    public String getLogin() {
        return mPreferences.getString(Key.LOGIN, "");
    }

    public void clearLogin() {
        saveLogin("");
    }

    public void savePassword(final String password) {
        mPreferences.putString(Key.PASSWORD, password);
    }

    public String getPassword() {
        return mPreferences.getString(Key.PASSWORD, "");
    }

    public void clearPassword() {
        savePassword("");
    }
}
