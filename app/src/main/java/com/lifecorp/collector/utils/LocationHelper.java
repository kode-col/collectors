package com.lifecorp.collector.utils;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.lifecorp.collector.App;
import com.lifecorp.collector.model.objects.debtor.Coordinate;
import com.lifecorp.collector.network.api.DBCachePoint;

import java.util.List;

public class LocationHelper {

    private static final int LOCATION_UPDATE_TIME = 2 * 60 * 1000;

    public interface OnLocationChanged {
        void onLocationChanged();
    }

    private static class SingletonHolder {
        public static final LocationHelper INSTANCE = new LocationHelper();
    }
    private LocationNetworkProviderUpdateListener mNetworkListener;
    private LocationGPSProviderUpdateListener mGpsListener;
    private OnLocationChanged mOnLocationChanged;
    private LocationManager mLocManager;
    private Location mLastLocation;

    private LocationHelper() {
    }

    public static LocationHelper getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void initOnLocationChangedInterface(Activity activity) {
        if (mOnLocationChanged == null && activity instanceof OnLocationChanged) {
            mOnLocationChanged = (OnLocationChanged) activity;
        } else {
            mOnLocationChanged = null;
        }
    }

    public void initLocationUtils() {
        mLocManager = (LocationManager) App.getContext().getSystemService(Context.LOCATION_SERVICE);
        String provider = mLocManager.getBestProvider(new Criteria(), false);

        Logger.e("best_provider = " + provider);
        isLocationServicesEnabled();

        LocationProvider gpsProvider = mLocManager.getProvider(LocationManager.GPS_PROVIDER);
        if (gpsProvider != null) {
            provider = gpsProvider.getName();
            mGpsListener = new LocationGPSProviderUpdateListener(provider);
        }

        LocationProvider networkProv = mLocManager.getProvider(LocationManager.NETWORK_PROVIDER);
        if (networkProv != null) {
            provider = networkProv.getName();
            mNetworkListener = new LocationNetworkProviderUpdateListener(provider);
        }

        Location location = getLastLocation();
        if (location != null) {
            onLocationUpdated(location);
        } else {
            Logger.e("Location not available");
        }
    }

    public boolean isGpsEnabled() {
        return mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean isLocationServicesEnabled() {
        boolean isNetworkEnabled = mLocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGPSEnabled = isGpsEnabled();

        Logger.e("isGPSEnabled = " + isGPSEnabled + ", isNetworkEnabled = " + isNetworkEnabled);
        return isGPSEnabled || isNetworkEnabled;
    }

    public void requestLocationUpdates() {
        requestLocationUpdates(mNetworkListener);
        requestLocationUpdates(mGpsListener);
    }

    private void requestLocationUpdates(LocationNetworkProviderUpdateListener listener) {
        if (listener != null && mLocManager != null) {
            String name = listener.getProviderName();
            mLocManager.requestLocationUpdates(name, LOCATION_UPDATE_TIME, 10, listener);
        }
    }
    private void removeUpdates(LocationNetworkProviderUpdateListener listener) {
        if (listener != null && mLocManager != null) {
            mLocManager.removeUpdates(listener);
        }
    }

    public void removeLocationUpdates() {
        removeUpdates(mGpsListener);
        removeUpdates(mNetworkListener);
    }

    public Float getDistance(Float latDeb, Float lonDeb) {
        Location myLoc = new Location("myLoc");
        if (wasFound()) {
            myLoc = mLastLocation;
        }

        Location debLoc = new Location("debLoc");
        if (latDeb != null && lonDeb != null) {
            debLoc.setLatitude(latDeb);
            debLoc.setLongitude(lonDeb);
        }

        float dist = myLoc.distanceTo(debLoc) / 1000;

        return (Math.round(dist * 10) / 10.0f);
    }

    @NonNull
    public Coordinate getCoordinate() {
        Coordinate coordinate = new Coordinate();
        if (wasFound()) {
            coordinate.setLatitude((float) mLastLocation.getLatitude());
            coordinate.setLongitude((float) mLastLocation.getLongitude());
        }
        return coordinate;
    }

    @NonNull
    public LatLng getLatLng() {
        LatLng latLng = new LatLng(0, 0);
        if (wasFound()) {
            latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }

        return latLng;
    }

    @Nullable
    public Location getLastLocation() {
        Location bestResult = null;
        float bestAccuracy = Float.MAX_VALUE;
        long bestTime = Long.MIN_VALUE;

        if (!wasFound()) {
            LocationManager locationManager = (LocationManager) App.getContext()
                    .getSystemService(Context.LOCATION_SERVICE);
            List<String> matchingProviders = locationManager.getAllProviders();

            for (String provider : matchingProviders) {
                Location location = locationManager.getLastKnownLocation(provider);
                if (location != null) {
                    float accuracy = location.getAccuracy();
                    long time = location.getTime();

                    if (accuracy < bestAccuracy) {
                        bestResult = location;
                        bestAccuracy = accuracy;
                        bestTime = time;
                    } else if (bestAccuracy == Float.MAX_VALUE && time > bestTime) {
                        bestResult = location;
                        bestTime = time;
                    }
                }
            }
        } else {
            bestResult = mLastLocation;
        }

        return bestResult;
    }

    public void updateFromGoogleMap(Location location) {
        setLocation(location);
    }

    private void setLocation(Location location) {
        if (location != null) {
            mLastLocation = location;
        }
    }

    public boolean isNotOld() {
        long time30MinutesAgo = System.currentTimeMillis() / 1000 - 30 * 60 * 1000;
        return wasFound() && time30MinutesAgo < mLastLocation.getTime();
    }

    public boolean wasFound() {
        return mLastLocation != null;
    }

    private void onLocationUpdated(Location location) {
        if (location != null) {
            Logger.e("onLocationUpdated: "+ location.getLatitude() + " " + location.getLongitude());
            setLocation(location);

            DBCachePoint.getInstance().refreshCacheFields();

            if (mOnLocationChanged != null) {
                mOnLocationChanged.onLocationChanged();
            }
        } else {
            Logger.e("onLocationUpdated: location == null");
        }
    }

    private class LocationGPSProviderUpdateListener extends LocationNetworkProviderUpdateListener {

        public LocationGPSProviderUpdateListener(String provider) {
            super(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            super.onLocationChanged(location);

            if (mNetworkListener != null) {
                mLocManager.removeUpdates(mNetworkListener);
            }
        }
    }

    private class LocationNetworkProviderUpdateListener implements LocationListener {

        private String mProvider;

        public LocationNetworkProviderUpdateListener(String provider) {
            setProviderName(provider);
        }

        public void setProviderName(String provider) {
            mProvider = provider != null ? provider : "";
        }

        public String getProviderName() {
            return mProvider;
        }

        @Override
        public void onLocationChanged(Location location) {
            onLocationUpdated(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
            Location location = getLastLocation();
            if (location != null) {
                onLocationUpdated(location);
            }

            Logger.e("Provider " + mProvider + " enabled");
        }

        @Override
        public void onProviderDisabled(String s) {
            Logger.e("Provider " + mProvider + " disabled");
        }

    }

}
