package com.lifecorp.collector.utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import com.lifecorp.collector.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Alexander Smirnov on 23.04.15.
 *
 * Хелпер работает с фото, создание предварительных файлов, удаление и компресия фото
 */
public class ImageFileHelper {

    private static final int IMAGE_BIGGER_SIDE = 640;
    private static final int IMAGE_SMALLER_SIDE = 480;

    private final static int REQUEST_ADD_PHOTO_CODE = 333999333;
    private String mCurrentPhotoPath;

    @Nullable
    public static byte[] loadFileToByte(String path) {
        byte[] result = null;

        if (isFilePathValid(path)) {
            try {
                result = readFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @Nullable
    public static File openFile(String path) {
        File file = null;

        if (isFilePathValid(path)) {
            file = new File(path);
        }

        return file;
    }

    public static byte[] readFile(String file) throws IOException {
        return readFile(new File(file));
    }

    public static byte[] readFile(File file) throws IOException {
        // Open file
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            // Get and check length
            long longLength = f.length();
            int length = (int) longLength;

            if (length != longLength) {
                throw new IOException("File size >= 2 GB");
            }

            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);

            return data;
        } finally {
            f.close();
        }
    }

    public static boolean isFilePathValid(String path) {
        if (path == null || path.isEmpty()) {
            return false;
        }

        File file = new File(path);
        return file.exists() && file.isFile();
    }

    public static void removeImageFile(String path) {
        if (isFilePathValid(path)) {
            File file = new File(path);
            file.delete();
        }
    }

    @Nullable
    public static Bitmap loadBitmapFromFile(String path) {
        return isFilePathValid(path) ? BitmapFactory.decodeFile(path) : null;
    }

    public static void compressImageFile(String path) {
        if (!ImageFileHelper.isFilePathValid(path)) {
            return;
        }

        // Get the dimensions of the View
        int targetH = IMAGE_BIGGER_SIDE;
        int targetW = IMAGE_SMALLER_SIDE;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        if (photoH > IMAGE_BIGGER_SIDE || photoW > IMAGE_BIGGER_SIDE) {
            if (photoW > photoH) {
                targetW = IMAGE_BIGGER_SIDE;
                targetH = IMAGE_SMALLER_SIDE;
            }

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
            rewriteFile(path, bitmap);
        }
    }

    public static boolean copyFile(Context context, Uri uri, String toPath) {
        if (uri != null && isFilePathValid(toPath)) {
            Bitmap bitmap = null;
            InputStream is = null;
            try {
                is = context.getContentResolver().openInputStream(uri);
                bitmap = BitmapFactory.decodeStream(is);
                is.close();
            } catch (Exception e) {

            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (bitmap != null) {
                rewriteFile(toPath, bitmap);
                return true;
            }
        }

        return false;
    }

    public static boolean copyFile(String fromPath, String toPath) {
        if (isFilePathValid(fromPath) && isFilePathValid(toPath)) {
            Bitmap bitmap = BitmapFactory.decodeFile(fromPath);

            rewriteFile(toPath, bitmap);
            return true;
        }

        return false;
    }

    private static boolean rewriteFile(String path, Bitmap bitmap) {
        if (path != null && bitmap != null) {
            File file = new File(path);
            boolean isFileDeleted = file.delete();

            if (isFileDeleted) {
                FileOutputStream fOut = null;

                try {
                    boolean isFileCreated = file.createNewFile();

                    if (isFileCreated) {
                        fOut = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, fOut);
                        fOut.flush();
                        fOut.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Logger.e("Не могу создать файл!");
                } finally {
                    if (fOut != null) {
                        try {
                            fOut.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                return true;
            }
        }

        return false;
    }

    public String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public void setCurrentPhotoPath(String currentPhotoPath) {
        mCurrentPhotoPath = currentPhotoPath;
    }

    public boolean isResultTrue(int requestCode, int resultCode) {
        if (requestCode == REQUEST_ADD_PHOTO_CODE && resultCode == Activity.RESULT_OK) {
            compressImageFile(getCurrentPhotoPath());
            return true;
        } else {
            removeImageFile(getCurrentPhotoPath());
            return false;
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        // mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mCurrentPhotoPath = image.getAbsolutePath();
        return image.getAbsoluteFile();
    }

    public void makePhotoIntent(Fragment fragment) {
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Context context = fragment != null ? fragment.getActivity() : null;

        if (photoFile != null) {
            Intent intent = getImageIntent(context, Uri.fromFile(photoFile));

            if (intent != null && Utils.isIntentAvailable(intent)) {
                fragment.startActivityForResult(intent, REQUEST_ADD_PHOTO_CODE);
            } else {
                showToast(context, R.string.msg_no_add_photo);
            }
        } else {
            showToast(context, R.string.msg_error);
        }
    }

    public void showToast(Context context, int id) {
        ToastHelper.getInstance().makeToast(context, id);
    }

    @Nullable
    private Intent getImageIntent(Context context, Uri outputFileUri) {
        if (context == null || outputFileUri == null) {
            return null;
        }

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            ComponentName c = new ComponentName(res.activityInfo.packageName, res.activityInfo.name);
            intent.setComponent(c);
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        return chooserIntent;
    }

}
