package com.lifecorp.collector.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Alexander Smirnov on 18.05.15.
 *
 * Хелпер для работы с тостами
 */
public class ToastHelper {

    private static class SingletonHolder {
        public static final ToastHelper INSTANCE = new ToastHelper();
    }

    private Toast mToast;

    private ToastHelper() {
    }

    public static ToastHelper getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public void makeToast(Context context, int messageId) {
        if (context != null) {
            makeToast(context, context.getString(messageId));
        }
    }

    public void makeToastLong(Context context, int messageId) {
        if (context != null) {
            makeToastLong(context, context.getString(messageId));
        }
    }

    public void makeToast(Context context, String message) {
        showToast(context, message, Toast.LENGTH_SHORT);
    }

    public void makeToastLong(Context context, String message) {
        showToast(context, message, Toast.LENGTH_LONG);
    }

    private void showToast(Context ctx, String message, final int length) {
        if (ctx != null && message != null) {
            if (mToast == null) {
                mToast = Toast.makeText(ctx, message, length);
            }

            mToast.setText(message);
            mToast.setDuration(length);
            mToast.show();
        }
    }

}
