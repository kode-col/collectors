package com.lifecorp.collector.ui.adapters.debtor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.ui.adapters.ExpandableAdapter;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderTitle;
import com.lifecorp.collector.ui.interfaces.OnCreditCollectionWorkListener;
import com.lifecorp.collector.ui.view.CreditInfoView;
import com.lifecorp.collector.ui.view.FakeGridView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DebtorInfoAdapter extends ExpandableAdapter<DBSection> {

    private DBDebtorProfile mCurrentDebtor;
    private OnCreditCollectionWorkListener mListener;
    private List<DBCollection> mCollectionsAll = DBCachePoint.getInstance().getAllCollections();

    public DebtorInfoAdapter(DBDebtorProfile currentDebtor,
                             OnCreditCollectionWorkListener listener) {
        mListener = listener;
        mCurrentDebtor = currentDebtor;

        mData = mCurrentDebtor != null ? mCurrentDebtor.getSections() : new ArrayList<DBSection>();
    }

    public void updateCollections() {
        mCollectionsAll = DBCachePoint.getInstance().getAllCollections();
    }

    @Override
    protected View getTitleView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderTitle holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.listitem_expandable_title, parent, false);
            holder = new ViewHolderTitle();

            holder.title = (TextView) convertView.findViewById(R.id.tvExpandableTitle);
            holder.expIcon = convertView.findViewById(R.id.ivExpandableDots);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderTitle) convertView.getTag();
        }

        if (mCurrentDebtor.getSections() != null) {
            String title = "";

            DBSection item = getItem(position);
            if (item != null && item.getTitle() != null) {
                title = item.getTitle();
            }

            holder.title.setText(title);
        }

        if (isPositionOpen(position)) {
            rotateImmediately(holder.expIcon);
        }

        return convertView;
    }

    @Override
    protected View getContentView(int position, View view, ViewGroup parent) {
        DBSection item = getItem(position);
        if (item != null && item.getFields() != null) {
            List<DBSectionField> fieldList = new ArrayList<>(item.getFields());

            if (item.getType() == DBSection.TYPE_CREDIT_INFO) {
                // если кредитная информация - то специальный вид
                view = getCreditInfoView(parent.getContext(), item, fieldList);
            } else {
                // если НЕ кредитная информация - то фейковый грид вью
                view = getGridView(view, parent, fieldList);
            }
        }

        return view;
    }

    @NonNull
    private List<DBCollection> filterCollections(String contractId) {
        final int debId = mCurrentDebtor != null ? mCurrentDebtor.getId() : -1;

        List<DBCollection> collectionsTemp = new ArrayList<>();
        if (mCollectionsAll != null && contractId != null) {
            for (DBCollection dbc: mCollectionsAll) {
                if (debId == dbc.getDebtorId() && contractId.equals(dbc.getContractId())) {
                    collectionsTemp.add(dbc);
                }
            }
        }

        // Сортировать по дате
        Collections.sort(collectionsTemp, new Comparator<DBCollection>() {
            @Override
            public int compare(DBCollection col1, DBCollection col2) {
                return ((Long) col2.getDate()).compareTo(col1.getDate());
            }
        });

        return collectionsTemp;
    }

    private CreditInfoView getCreditInfoView(Context context, DBSection item,
                                             List<DBSectionField> fieldList) {
        List<DBCollection> creditCollectionList = filterCollections(item.getId());
        CreditInfoView view = new CreditInfoView(context, mCurrentDebtor.getFio(), item, fieldList,
                creditCollectionList, mListener);
        view.drawView();

        return view;
    }

    private FakeGridView getGridView(View view, ViewGroup parent, List<DBSectionField> fieldList) {
        if (view == null || !(view instanceof FakeGridView)) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_expandable_content, parent, false);
        }

        FakeGridView fakeGridView = (FakeGridView) view;

        BaseAdapter adapter = new DebtorSectionFieldAdapter(fieldList, mCurrentDebtor.getFio());
        fakeGridView.setAdapter(adapter);
        fakeGridView.drawView();

        return fakeGridView;
    }

}
