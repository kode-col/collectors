package com.lifecorp.collector.ui.fragments.map;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.helpers.DBFront;
import com.lifecorp.collector.model.database.objects.DBCoordinate;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.model.objects.address.AddressContainer;
import com.lifecorp.collector.model.objects.address.AddressGroupContainer;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.ui.activities.BaseActivity;
import com.lifecorp.collector.ui.enums.EventType;
import com.lifecorp.collector.ui.fragments.BaseWrapperFragment;
import com.lifecorp.collector.ui.fragments.debtor.DebtorAddressFragment;
import com.lifecorp.collector.ui.fragments.debtor.DebtorsListFragment;
import com.lifecorp.collector.ui.interfaces.OnAddressSelectedListener;
import com.lifecorp.collector.ui.interfaces.OnMapFilterChanged;
import com.lifecorp.collector.ui.interfaces.OnMapUpdatedListener;
import com.lifecorp.collector.ui.interfaces.OnSortByListener;
import com.lifecorp.collector.ui.interfaces.PressedDebtorManager;
import com.lifecorp.collector.utils.AddressGroupBuilder;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.LocationHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexander Smirnov on 02.04.15.
 *
 * Обёртка фрагмента карты, управляет взаимодействиями, содержит два состояния:
 *  - Отобразить список и карту всех клиентов/мероприятий
 *  - Отобразить список и карту всех адресов клиента
 */
public class MapWrapperFragment extends BaseWrapperFragment implements PressedDebtorManager,
        OnMapUpdatedListener, FragmentManager.OnBackStackChangedListener, OnSortByListener,
        OnAddressSelectedListener, OnMapFilterChanged, LocationHelper.OnLocationChanged {

    private static final String TAG_FILTER_SELECTED_EVENT_TYPE = "TAG_FILTER_SELECTED_EVENT_TYPE";
    private static final String TAG_LIST_FRAGMENT = "MAP_WRAPPER_FRAGMENT_TAG_LIST_FRAGMENT";
    private final static String TAG_IS_PROMISED_PAYMENT = "TAG_IS_PROMISED_PAYMENT";
    private final static String TAG_SAVE_SORT_POSITION = "TAG_SAVE_SORT_POSITION";
    private final static String TAG_ADDRESS_FRAGMENT = "TAG_ADDRESS_FRAGMENT";
    private final static String TAG_SECTION_FIELD_ID = "TAG_SECTION_FIELD_ID";
    private final static String TAG_MAP_FRAGMENT = "TAG_MAP_FRAGMENT";
    private static final String TAG_IS_FROM_MENU = "TAG_IS_FROM_MENU";

    private Menu mMenu;
    private int mDebtorId = -1;
    private boolean mIsFromMenu;
    private int mSaveSortPosition;
    private int mSectionFieldId = -1;
    private boolean mIsPromisedPayment;
    private DBDebtorProfile mPressedDeb;

    private boolean mIsNotAddToBackStack;
    private MapDebtorsFragment mMapFragment;
    private DebtorsListFragment mDebtorsListFragment;
    private List<DBDebtorProfile> mCurrentDebtorList;
    private Map<EventType, List<DBEvent>> mEventsByType;
    private DebtorAddressFragment mDebtorsAddressFragment;
    private Map<MarkerOptions, Object> mMarkersOptionsMap;
    private EventType mFilterSelectedEventType = EventType.EDGE; // Стандартно открывать "на грани"

    public static MapWrapperFragment newInstance(List<DBEvent> events) {
        MapWrapperFragment mapFragment = new MapWrapperFragment();
        mapFragment.setEvents(events);
        mapFragment.setFromMenu(true);

        return mapFragment;
    }

    public static MapWrapperFragment newInstance(List<DBEvent> events, int sectionFieldId) {
        MapWrapperFragment mapFragment = new MapWrapperFragment();
        mapFragment.setEvents(events);
        mapFragment.setFromMenu(false);
        mapFragment.setSectionFieldId(sectionFieldId);

        return mapFragment;
    }

    private void setSectionFieldId(int sectionFieldId) {
        mSectionFieldId = sectionFieldId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_map_wrapper, container, false);
        mCurrentDebtorList = new ArrayList<>();

        if (savedInstanceState != null) {
            mDebtorId = savedInstanceState.getInt(Const.TAG_DEBTOR_ID, -1);
            mSectionFieldId = savedInstanceState.getInt(TAG_SECTION_FIELD_ID);
            mSaveSortPosition = savedInstanceState.getInt(TAG_SAVE_SORT_POSITION);

            mIsFromMenu = savedInstanceState.getBoolean(TAG_IS_FROM_MENU);
            mIsPromisedPayment = savedInstanceState.getBoolean(TAG_IS_PROMISED_PAYMENT);

            mPressedDeb = (DBDebtorProfile) savedInstanceState.getSerializable(Const.TAG_DEBTOR);
            mFilterSelectedEventType = (EventType)
                    savedInstanceState.getSerializable(TAG_FILTER_SELECTED_EVENT_TYPE);

            Fragment fragment = getFragmentManager().findFragmentByTag(TAG_MAP_FRAGMENT);
            if (fragment != null && fragment instanceof MapDebtorsFragment) {
                mMapFragment = (MapDebtorsFragment) fragment;
                mMapFragment.setOnMapLoadedListener(this);
            }

            fragment = getFragmentManager().findFragmentByTag(TAG_LIST_FRAGMENT);
            if (fragment != null && fragment instanceof DebtorsListFragment) {
                mDebtorsListFragment = (DebtorsListFragment) fragment;
                mDebtorsListFragment.setPressedDebtorManager(this);
                mDebtorsListFragment.setOnSortByListener(this);
            }

            fragment = getFragmentManager().findFragmentByTag(TAG_ADDRESS_FRAGMENT);
            if (fragment != null && fragment instanceof DebtorAddressFragment) {
                mDebtorsAddressFragment = (DebtorAddressFragment) fragment;
                mDebtorsAddressFragment.setOnAddressSelectedListener(this);

                // Хак для поднятия фрагмента на верх, иногда неправильно востанавливается снизу
                if (getFragmentManager().getBackStackEntryCount() == 0) {
                    mDebtorsListFragment = null;
                }

                if (mDebtorsListFragment != null) {
                    mIsNotAddToBackStack = true;
                    FragmentTransaction trans = getFragmentManager().beginTransaction();
                    trans.remove(mDebtorsAddressFragment);
                    trans.commit();

                    showDebtorAddressFragment(mDebtorsAddressFragment);
                }
                // Конец хака
            }

            setEvents(DBCachePoint.getInstance().getAllEvents());
        } else {
            setMapFragment();

            if (mSectionFieldId == -1) {
                setDebtorsListFragment();
            } else {
                DBSectionField field = DBFront.getInstance().getSectionFieldById(mSectionFieldId);

                if (field != null) {
                    DBSection section = field.getDbSection();

                    if (section != null) {
                        mDebtorId = section.getDebtors_id();
                        setData();
                    } else {
                        makeToast(R.string.msg_error);
                    }
                } else {
                    makeToast(R.string.msg_error);
                }
            }
        }

        // Если сервисов карты нет то принудительно инициализировать списки
        if (!App.GOOGLE_SERVICE_IS_ENABLE) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setData();
                }
            }, Const.DEFAULT_TIME);
        }

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Const.TAG_DEBTOR_ID, mDebtorId);
        outState.putInt(TAG_SECTION_FIELD_ID, mSectionFieldId);
        outState.putInt(TAG_SAVE_SORT_POSITION, mSaveSortPosition);

        outState.putBoolean(TAG_IS_FROM_MENU, mIsFromMenu);
        outState.putBoolean(TAG_IS_PROMISED_PAYMENT, mIsPromisedPayment);

        outState.putSerializable(Const.TAG_DEBTOR, mPressedDeb);
        outState.putSerializable(TAG_FILTER_SELECTED_EVENT_TYPE, mFilterSelectedEventType);
    }

    @Override
    public void onResume() {
        super.onResume();

        getFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        getFragmentManager().removeOnBackStackChangedListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_map, menu);
        super.onCreateOptionsMenu(menu, inflater);

        mMenu = menu;
    }

    // добавить(установить) фрагмент отображающий карту с клиентами и мероприятиями
    private void setMapFragment() {
        mMapFragment = MapDebtorsFragment.newInstance(this);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_map_wrapper_map, mMapFragment, TAG_MAP_FRAGMENT)
                .commit();
    }

    // добавить(установить) фрагмент отображающий список клиентов
    private void setDebtorsListFragment() {
        mDebtorsListFragment = DebtorsListFragment.newInstance(this, this);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_map_wrapper_content, mDebtorsListFragment, TAG_LIST_FRAGMENT)
                .commit();
    }

    private void setDebtorAddressFragment(int debtorId) {
        mDebtorId = debtorId;
        setDebtorAddressFragment(DBCachePoint.getInstance().findDebtorById(debtorId));
    }

    // добавить(установить) фрагмент отображающий адреса клиента
    private void setDebtorAddressFragment(DBDebtorProfile debtorProfile) {
        if (debtorProfile != null) {
            List<AddressGroupContainer> dataList = mMapFragment.getMap() != null
                    ? AddressGroupBuilder.getInstance().getAllAddressFromDebtor(debtorProfile)
                    : null;
            setAddressData(debtorProfile, dataList);

            if (mDebtorsAddressFragment != null) {
                mDebtorsAddressFragment.setAllAddressFromDebtor(dataList);
                mDebtorsAddressFragment.setOnAddressSelectedListener(this);
                mDebtorsAddressFragment.notifyDataSetChanged();
            } else {
                showDebtorAddressFragment(DebtorAddressFragment.newInstance(debtorProfile,
                        mIsFromMenu, this, dataList));
            }

            if (dataList != null) {
                for (AddressGroupContainer container : dataList) {
                    for (AddressContainer address : container.getAddress()) {
                        if (address.getSectionId() == mSectionFieldId) {
                            Handler handler = new Handler();
                            final AddressContainer addressContainer = address;
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onInfoWindowOpened(addressContainer);
                                    onAddressSelected(addressContainer);
                                }
                            }, 500);

                            break;
                        }
                    }
                }
            }
        } else {
            makeToast(R.string.msg_error);
        }
    }

    private void showDebtorAddressFragment(DebtorAddressFragment fragment) {
        if (fragment != null) {
            mDebtorsAddressFragment = fragment;

            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_map_wrapper_content, mDebtorsAddressFragment,
                    TAG_ADDRESS_FRAGMENT);

            if (mDebtorsListFragment != null && !mIsNotAddToBackStack) {
                mIsNotAddToBackStack = false;
                fragmentTransaction.addToBackStack(TAG_ADDRESS_FRAGMENT);
            }

            fragmentTransaction.commit();
        }
    }

    // Создаёт отображение для карты и списка состоящее из адресов клиента(должника)
    private void setAddressData(DBDebtorProfile debtorProfile,
                                List<AddressGroupContainer> dataList) {
        if (debtorProfile != null && dataList != null && App.GOOGLE_SERVICE_IS_ENABLE) {
            mMarkersOptionsMap = new HashMap<>();

            final String debtorId = String.valueOf(debtorProfile.getId());
            final int drawableId = debtorProfile.isPromised()
                    ? EventType.getMarkerMoneyResource(EventType.ALL_CLIENTS)
                    : EventType.getMarkerResource(EventType.ALL_CLIENTS);

            LatLngBounds.Builder bc = new LatLngBounds.Builder();

            if (mMapFragment == null || mMapFragment.getMap() == null) {
                return;
            }

            for (AddressGroupContainer groupContainer : dataList) {
                for (AddressContainer addressContainer : groupContainer.getAddress()) {
                    DBCoordinate coordinate = addressContainer.getCoordinate();
                    LatLng latLng = new LatLng(coordinate.getLatitude(), coordinate.getLongitude());
                    bc.include(latLng);

                    final MarkerOptions markerOptions =
                            createMarker(debtorId, latLng, drawableId, mMarkersOptionsMap.size());

                    if (markerOptions != null) {
                        mMarkersOptionsMap.put(markerOptions, addressContainer);
                    }
                }
            }

            if (mMarkersOptionsMap != null && !mMarkersOptionsMap.isEmpty()) {
                mMapFragment.addMarkers(mMarkersOptionsMap);
                mMapFragment.setCamera(bc.build());
                mMarkersOptionsMap = null;
            } else {
                mMapFragment.setCameraOnFakeCenter();
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                getBaseActivity().createFilterDialog(mFilterSelectedEventType, mIsPromisedPayment,
                        this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        updateMenu(menu);
    }

    private void updateMenu(Menu menu) {
        if (menu != null) {
            MenuItem menuItem = menu.findItem(R.id.action_filter);
            if (menuItem != null) {
                switch (mFilterSelectedEventType) {
                    case ALL_EVENTS:
                    case ALL_CLIENTS:
                        menuItem.setIcon(R.drawable.ico_navbar_filter);
                        break;
                    case IN_WORK:
                        menuItem.setIcon(R.drawable.ico_navbar_filter_green);
                        break;
                    case EDGE:
                        menuItem.setIcon(R.drawable.ico_navbar_filter_orange);
                        break;
                    case EXPIRED:
                        menuItem.setIcon(R.drawable.ico_navbar_filter_red);
                        break;
                }
            }
        }
    }

    // Кэширует события отфильтровав их по типу
    private void setEvents(List<DBEvent> events) {
        mEventsByType = new EnumMap<>(EventType.class);
        mEventsByType.put(EventType.IN_WORK, new ArrayList<DBEvent>());
        mEventsByType.put(EventType.EDGE, new ArrayList<DBEvent>());
        mEventsByType.put(EventType.EXPIRED, new ArrayList<DBEvent>());

        final Calendar now = Calendar.getInstance();
        for (DBEvent event : events) {
            final EventType eventType = EventType.getEventType(event, now);
            mEventsByType.get(eventType).add(event);
        }
    }

    private void clearMapMarkers() {
        mMarkersOptionsMap = new HashMap<>();
        mCurrentDebtorList.clear();
    }

    // Обновить список и карту по последнему сохраненному фильтру
    private void refreshMarkers() {
        if (mDebtorId != -1) {
            setDebtorAddressFragment(mDebtorId);
        } else {
            refreshMarkers(mFilterSelectedEventType);
        }
    }

    // Обновтиь список и карту по типу
    private void refreshMarkers(EventType filterEventType) {
        if (mEventsByType != null && filterEventType != null) {
            mFilterSelectedEventType = filterEventType;
            clearMapMarkers();

            LatLngBounds.Builder bc = new LatLngBounds.Builder();
            int pointsCount = 0;

            if (filterEventType == EventType.ALL_CLIENTS) {
                pointsCount += showAllDebtors(bc);
            } else {
                if (filterEventType == EventType.ALL_EVENTS) {
                    for (EventType eventType : EventType.values()) {
                        pointsCount += createEventsMarkers(eventType, bc);
                    }
                } else {
                    pointsCount += createEventsMarkers(filterEventType, bc);
                }
            }

            if (mDebtorsListFragment != null) {
                setDebtorListToFragmentList();
                mDebtorsListFragment.setListVisible();

                mDebtorsListFragment.sortDebtors();
                mDebtorsListFragment.notifyDataSetChanged();
            }
            mMapFragment.addMarkers(mMarkersOptionsMap);

            if (pointsCount > 0) {
                mMapFragment.setCamera(bc.build());
            } else {
                mMapFragment.setCameraOnFakeCenter();
            }

            mMarkersOptionsMap = null;
        }
    }

    // Создаёт маркеры для событий предварительно фильтруя их по типу
    private int createEventsMarkers(EventType eventType, LatLngBounds.Builder bc) {
        List<DBEvent> events = mEventsByType.get(eventType);
        final int markerResId = EventType.getMarkerResource(eventType);
        final int markerMoneyResId = EventType.getMarkerMoneyResource(eventType);

        if (events == null) {
            events = new ArrayList<>();
        }

        for (DBEvent event : events) {
            final MarkerOptions markerOptions = createMarker(event, markerResId, markerMoneyResId);

            if (markerOptions != null) {
                mMarkersOptionsMap.put(markerOptions, event);
                bc.include(markerOptions.getPosition());
            }
        }

        return mMarkersOptionsMap.size();
    }

    // Создаёт список для отображения из всех клиентов (должников)
    private int showAllDebtors(LatLngBounds.Builder bc) {
        List<DBDebtorProfile> allDebtors = DBCachePoint.getInstance().getAllDebtors();
        final int markerResId = EventType.getMarkerResource(EventType.ALL_CLIENTS);
        final int markerMoneyResId = EventType.getMarkerMoneyResource(EventType.ALL_CLIENTS);

        for (DBDebtorProfile debtor : allDebtors) {
            if (debtor.getCoordinatePrimary() != null) {
                if (mIsPromisedPayment && !debtor.isPromised()) {
                    continue;
                }

                DBCoordinate debtorCoord = debtor.getCoordinatePrimary();
                bc.include(new LatLng(debtorCoord.getLatitude(), debtorCoord.getLongitude()));

                final MarkerOptions markerOptions = createMarker(debtor, debtor.isPromised()
                        ? markerMoneyResId : markerResId);

                if (markerOptions != null) {
                    mMarkersOptionsMap.put(markerOptions, debtor);
                }
            }
        }

        return mMarkersOptionsMap.size();
    }

    // Метод создания маркера для карты, создаёт метки для адресов клиента, при детальном просмотре
    public MarkerOptions createMarker(String debtorId, LatLng latLng, int drawableId, int size) {
        return new MarkerOptions()
                .title(debtorId)
                .snippet(Const.TAG_MARKER_FOR_ADDRESS + size)
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(drawableId));
    }

    // Создаёт маркер для клиентов
    public MarkerOptions createMarker(DBDebtorProfile debtor, int drawableId) {
        if (debtor != null) {
            addDebtorToMap(debtor);
            DBCoordinate debtorCoord = debtor.getCoordinatePrimary();

            if (!App.GOOGLE_SERVICE_IS_ENABLE) {
                return null;
            }

            return new MarkerOptions()
                    .title(debtor.getId() + "")
                    .position(new LatLng(debtorCoord.getLatitude(), debtorCoord.getLongitude()))
                    .icon(BitmapDescriptorFactory.fromResource(drawableId));
        }

        return null;
    }

    // Создаёт маркер для событий(мероприятий)
    public MarkerOptions createMarker(DBEvent event, int markerResId, int markerMoneyResId) {
        final DBDebtorProfile debtor = event.getDebtorProfile();

        if (debtor != null) {
            if (mIsPromisedPayment && !debtor.isPromised()) {
                return null;
            }

            final int drawableId = debtor.isPromised() ? markerMoneyResId : markerResId;

            addDebtorToMap(debtor);
            List<AddressContainer> addressContainerList =
                    AddressGroupBuilder.getInstance().getAddressForDebtor(debtor);

            if (addressContainerList.size() > 0) {
                AddressContainer item = addressContainerList.get(0);
                float latitude = item.getCoordinate().getLatitude();
                float longitude = item.getCoordinate().getLongitude();

                if (!App.GOOGLE_SERVICE_IS_ENABLE) {
                    return null;
                }

                return new MarkerOptions()
                        .snippet("" + item.getSectionId())
                        .title(debtor.getId() + "")
                        .position(new LatLng(latitude, longitude))
                        .icon(BitmapDescriptorFactory.fromResource(drawableId));
            }
        }

        return null;
    }

    private void setFromMenu(boolean fromMenu) {
        mIsFromMenu = fromMenu;
    }

    private void addDebtorToMap(DBDebtorProfile debtor) {
        if (debtor != null
                && mCurrentDebtorList != null
                && !isCurrentDebtorListIsContains(debtor)) {
            mCurrentDebtorList.add(debtor);
        }
    }

    private boolean isCurrentDebtorListIsContains(DBDebtorProfile debtor) {
        if (debtor == null || mCurrentDebtorList == null) {
            return false;
        }

        final int debtorId = debtor.getId();
        for (DBDebtorProfile item : mCurrentDebtorList) {
            if (debtorId == item.getId()) {
                return true;
            }
        }

        return false;
    }

    private void setDebtorListToFragmentList() {
        if (mDebtorsListFragment != null) {
            mDebtorsListFragment.setDebtorsList(mCurrentDebtorList);
        }
    }

    private void setData() {
        setDebtorListToFragmentList();

        List<DBEvent> eventList = DBCachePoint.getInstance().getAllEvents();
        if (eventList.size() > 0) {
            refreshMarkers();
        } else {
            mMapFragment.setCameraOnFakeCenter();
        }
    }

    @Override
    public void onOperationFinished(CollectorOperationResult<? extends ServerResponse> result) {
        if (result != null
                && result.isSuccessful()
                && Const.TAG_EVENTS.equals(result.getOutput().getResponseType())) {
            List<DBEvent> eventList = DBCachePoint.getInstance().getAllEvents();

            if (eventList.size() > 0) {
                setEvents(eventList);
                refreshMarkers();
            }
        }
    }

    @Override
    public void onDialogBtnAcceptClicked(String tag, Object object) {
        switch (tag) {
            case BaseActivity.TAG_MAP_FILTER_DIALOG:
                refreshMarkers(mFilterSelectedEventType);
                updateMenu(mMenu);
                break;
        }
    }

    @Override
    public void onDebtorChanged(DBDebtorProfile dbDebtorProfile) {
        mPressedDeb = dbDebtorProfile;
        if (mDebtorsListFragment != null) {
            mDebtorsListFragment.goToPressedPosition();
        }

        if (mMapFragment != null) {
            mMapFragment.showInfoWindowForDebtor(mPressedDeb);
        }
    }

    @Override
    public DBDebtorProfile getPressedDebtor() {
        return mPressedDeb;
    }

    @Override
    public void onMapLoaded() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setData();
            }
        }, Const.DEFAULT_TIME);
    }

    @Override
    public void onInfoWindowClicked(String debtorId) {
        if (mMapFragment != null) {
            mMapFragment.clearMarkers();
        }

        clearMapMarkers();

        int debId = Integer.parseInt(debtorId);
        setDebtorAddressFragment(debId);
    }

    @Override
    public void onInfoWindowOpened(DBDebtorProfile debtor) {
        if (mDebtorsListFragment != null && debtor != null) {
            mDebtorsListFragment.setPressedDebtor(debtor.getId());
            mDebtorsListFragment.goToPressedPosition();
        }
    }

    @Override
    public void onInfoWindowOpened(AddressContainer addressContainer) {
        if (mDebtorsAddressFragment != null && addressContainer != null) {
            mDebtorsAddressFragment.setPressedAddress(addressContainer);
            mDebtorsAddressFragment.goToPressedPosition();
        }
    }

    @Override
    public void onBackStackChanged() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            mDebtorId = -1;
            mIsNotAddToBackStack = false;
            mDebtorsAddressFragment = null;

            getMainActivity().setMapTitleAndIcon();
            refreshMarkers();
            setDebtorListToFragmentList();
        }
    }

    @Override
    public void setOnSortPositionChanged(int sortType) {
        mSaveSortPosition = sortType;
    }

    @Override
    public int getSaveSortPosition() {
        return mSaveSortPosition;
    }

    @Override
    public void onAddressSelected(AddressContainer addressContainer) {
        if (mMapFragment != null) {
            mMapFragment.showInfoWindowForAddress(addressContainer);
        }
    }

    @Override
    public void onFilterChanged(EventType eventType) {
        mFilterSelectedEventType = eventType;
    }

    @Override
    public void onPromisedPaymentChanged(boolean state) {
        mIsPromisedPayment = state;
    }

    @Override
    public void onLocationChanged() {
        if (mDebtorsListFragment != null) {
            mDebtorsListFragment.setCoordinates();
        }
    }

}
