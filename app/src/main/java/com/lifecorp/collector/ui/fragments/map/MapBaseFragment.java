package com.lifecorp.collector.ui.fragments.map;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.fragments.BaseFragment;

/**
 * Created by Alexander Smirnov on 23.04.15.
 *
 * Базовый класс для работы с картой
 */
public abstract class MapBaseFragment extends BaseFragment {

    private static final String TAG_MAP_INNER_FRAGMENT = "TAG_MAP_INNER_FRAGMENT";
    private static final String TAG_IS_FIRST_UPDATE = "TAG_IS_FIRST_UPDATE";

    private GoogleMap mMap;
    private MapFragment mMapFragment;
    private boolean mIsFirstUpdate = true;
    private final Handler mHandler = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getContentLayoutId(), container, false);
        if (savedInstanceState != null) {
            mMapFragment = (MapFragment)
                    getFragmentManager().findFragmentByTag(TAG_MAP_INNER_FRAGMENT);
            mMapFragment.onCreateView(inflater, container, savedInstanceState);
        }

        initButtons(view);
        initMap();

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(TAG_IS_FIRST_UPDATE, mIsFirstUpdate);
        if (mMapFragment != null) {
            mMapFragment.onSaveInstanceState(outState);
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (mMapFragment != null) {
            mMapFragment.onActivityCreated(savedInstanceState);
        }
    }

    private void initMap() {
        if (mMapFragment == null) {
            mMapFragment = MapFragment.newInstance();
            getFragmentManager()
                    .beginTransaction()
                    .replace(getMapReplaceId(), mMapFragment, TAG_MAP_INNER_FRAGMENT)
                    .commit();


            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    initMapAfterAddingFirst();
                }
            }, 100);
        } else {
            initMapAfterAddingFirst();
        }
    }

    private void initMapAfterAddingFirst() {
        mMap = mMapFragment.getMap();
        if (mMap == null) {
            makeToast(R.string.toast_map_sorry);
            return;
        }

        setCameraOnFakeCenter();
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                // set settings
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setRotateGesturesEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mMap.setMyLocationEnabled(true);
                initMapAfterAdding();
            }
        });
    }

    //  Фукнция устанавливает как цент карты координаты Москвы(Moscow) и зум уровня 8
    public void setCameraOnFakeCenter() {
        final float moscowLatitude = 55.75857F;
        final float moscowLongitude = 37.6177F;
        final float moscowDefaultZoom = 8f;

        updateCamera(moscowLatitude, moscowLongitude, moscowDefaultZoom);
    }

    private float getMapZoom() {
        float zoom = 0;
        if (getMap() != null) {
            CameraPosition cameraPosition = getMap().getCameraPosition();

            if (cameraPosition != null) {
                zoom = cameraPosition.zoom;
            }
        }

        return zoom;
    }

    protected void updateCamera(double latitude, double longitude) {
        updateCamera(latitude, longitude, getMapZoom());
    }

    protected void updateCamera(double latitude, double longitude, float zoom) {
        updateCamera(new LatLng(latitude, longitude), zoom);
    }

    protected void updateCamera(LatLng latLng) {
        updateCamera(latLng, getMapZoom());
    }

    protected void updateCamera(LatLng latLng, float zoom) {
        if (latLng != null && getMap() != null) {
            updateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        }
    }

    protected void updateCamera(final CameraUpdate cameraUpdate) {
        if (cameraUpdate != null && getMap() != null) {
            if (mIsFirstUpdate) {
                mIsFirstUpdate = false;
                getMap().moveCamera(cameraUpdate);
            } else {
                getMap().animateCamera(cameraUpdate);
            }
        }
    }

    public void setCamera(LatLngBounds bounds) {
        final int paddingInPixels = 150;
        updateCamera(CameraUpdateFactory.newLatLngBounds(bounds, paddingInPixels));
    }

    @Nullable
    public GoogleMap getMap() {
        return mMap;
    }

    protected abstract int getContentLayoutId();

    protected abstract int getMapReplaceId();

    protected abstract void initButtons(View rootView);

    protected abstract void initMapAfterAdding();

}
