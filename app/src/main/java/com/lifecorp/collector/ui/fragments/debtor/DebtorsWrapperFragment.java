package com.lifecorp.collector.ui.fragments.debtor;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.ui.activities.BaseActivity;
import com.lifecorp.collector.ui.fragments.BaseFragment;
import com.lifecorp.collector.ui.fragments.BaseWrapperFragment;
import com.lifecorp.collector.ui.fragments.StubFragment;
import com.lifecorp.collector.ui.fragments.collection.CollectionHistoryWrapperFragment;
import com.lifecorp.collector.ui.interfaces.PressedDebtorManager;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.LocationHelper;
import com.lifecorp.collector.utils.Utils;

/**
 * Created by Alexander Smirnov on 16.03.15.
 *
 * Фрагмент оборачивающий логику работы с должниками
 */
public class DebtorsWrapperFragment extends BaseWrapperFragment
        implements LocationHelper.OnLocationChanged, PressedDebtorManager {

    private static final String TAG_PRESSED_DEB = "TAG_PRESSED_DEB";
    private static final String TAG_STUB_FRAGMENT = "TAG_STUB_FRAGMENT";
    private static final String TAG_DEB_INFO_FRAGMENT = "TAG_DEB_INFO_FRAGMENT";
    private static final String TAG_SAVED_TEXT_SEARCH_PANEL = "TAG_SAVED_TEXT_SEARCH_PANEL";
    private static final String TAG_MAIN_COLLECTION_FRAGMENT = "TAG_MAIN_COLLECTION_FRAGMENT";

    private String mSavedTextSearchPanel = "";
    public DBDebtorProfile mPressedDeb = null;

    private SearchView mSearchView;
    private MenuItem mSearchMenuItem;

    private StubFragment mStubFragment;
    private DebtorInfoFragment mDebInfoFragment;
    private DebtorsListFragment mDebListFragment;
    private CollectionHistoryWrapperFragment mMainCollectionFragment;

    public static DebtorsWrapperFragment newInstance() {
        return new DebtorsWrapperFragment();
    }

    public static DebtorsWrapperFragment newInstance(String pressedDebId) {
        int debId = Integer.parseInt(pressedDebId);
        return newInstance(DBCachePoint.getInstance().findDebtorById(debId));
    }

    public static DebtorsWrapperFragment newInstance(DBDebtorProfile pressedDeb) {
        DebtorsWrapperFragment fragment = new DebtorsWrapperFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(TAG_PRESSED_DEB, pressedDeb);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle extras = getArguments();
        if (extras != null) {
            mPressedDeb = (DBDebtorProfile) extras.getSerializable(TAG_PRESSED_DEB);
        }

        if (savedInstanceState == null) {
            setDebtorsListFragment();
            setDebtorInfoFragment();
        } else {
            mDebInfoFragment = (DebtorInfoFragment) getFragmentManager()
                    .findFragmentByTag(TAG_DEB_INFO_FRAGMENT);
            mDebListFragment = (DebtorsListFragment) getFragmentManager()
                    .findFragmentByTag(Const.TAG_LIST_FRAGMENT);
            mDebListFragment.setPressedDebtorManager(this);
            mStubFragment = (StubFragment) getFragmentManager()
                    .findFragmentByTag(TAG_STUB_FRAGMENT);
            mMainCollectionFragment = (CollectionHistoryWrapperFragment) getFragmentManager()
                    .findFragmentByTag(TAG_MAIN_COLLECTION_FRAGMENT);

            mSavedTextSearchPanel = savedInstanceState.getString(TAG_SAVED_TEXT_SEARCH_PANEL);
            mPressedDeb = (DBDebtorProfile) savedInstanceState.getSerializable(TAG_PRESSED_DEB);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(TAG_PRESSED_DEB, mPressedDeb);
        outState.putString(TAG_SAVED_TEXT_SEARCH_PANEL, mSavedTextSearchPanel);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        return inflater.inflate(R.layout.fragment_debtors_wrapper, container, false);
    }

    private void showFragment(BaseFragment fragment, String fragmentTag, boolean isAddToBackStack) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        ft.setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out)
                .replace(R.id.fragment_debtors_wrapper_content, fragment, fragmentTag);

        if (isAddToBackStack) {
            ft.addToBackStack(null);
        }

        ft.commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_debtors, menu);

        mSearchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) mSearchMenuItem.getActionView();

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String arg0) {
                Utils.closeKeyboard(mSearchView);
                if (mDebListFragment != null) {
                    mDebListFragment.setSearchParams(arg0);
                }
                return false;
            }

            public boolean onQueryTextChange(String arg0) {
                if (mDebListFragment != null) {
                    mDebListFragment.setSearchParams(arg0);
                }
                mSavedTextSearchPanel = arg0;
                return false;
            }

        });

        mSearchMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                mSearchView.setIconified(true);
                if (mDebListFragment != null) {
                    mDebListFragment.setSearchParams("");
                }
                mSavedTextSearchPanel = "";
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void refreshAll() {
        if (mDebListFragment != null) {
            mDebListFragment.startRefreshAll();
        }
    }

    @Override
    public void onOperationFinished(CollectorOperationResult<? extends ServerResponse> result) {
        if (mDebListFragment != null) {
            mDebListFragment.onOperationFinished(result);
        }
        if (mDebInfoFragment != null) {
            mDebInfoFragment.onOperationFinished(result);
        }
        if (mMainCollectionFragment != null) {
            mMainCollectionFragment.onOperationFinished(result);
        }
    }

    public void setNoDebFragment() {
        getFragmentManager().popBackStack();
        mStubFragment = new StubFragment();
        showFragment(mStubFragment, TAG_STUB_FRAGMENT, false);
    }

    private void setDebtorInfoFragment() {
        getFragmentManager().popBackStack();
        if (mPressedDeb != null) {
            mDebInfoFragment = new DebtorInfoFragment();
            showFragment(mDebInfoFragment, TAG_DEB_INFO_FRAGMENT, false);
        } else {
            setNoDebFragment();
            mDebInfoFragment = null;
        }
    }

    public void setCollectionFragment(int debtorId, String creditId) {
        mMainCollectionFragment = CollectionHistoryWrapperFragment.newInstance(debtorId, creditId);
        showFragment(mMainCollectionFragment, TAG_MAIN_COLLECTION_FRAGMENT, true);
    }

    public void collapseSearch() {
        if (mSearchMenuItem != null) {
            mSearchView.setIconified(true);
            mSearchMenuItem.collapseActionView();
        }
        Utils.closeKeyboard(mSearchView);
        mSavedTextSearchPanel = "";
        if (mDebListFragment != null) {
            mDebListFragment.setSearchParams("");
        }
    }

    private void setDebtorsListFragment() {
        mDebListFragment = DebtorsListFragment.newInstance(this);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_debtors_wrapper_list, mDebListFragment,
                        Const.TAG_LIST_FRAGMENT)
                .commit();
    }

    public boolean isSearchActive() {
        return mSearchView != null && !mSearchView.isIconified();
    }

    @Override
    public void onLocationChanged() {
        if (mDebInfoFragment != null) {
            mDebInfoFragment.setDistanceToDebtor();
        }

        if (mDebListFragment != null) {
            mDebListFragment.setCoordinates();
        }
    }

    public String getSavedTextSearchPanel() {
        return mSavedTextSearchPanel;
    }

    @Override
    public void onDialogBtnAcceptClicked(String tag, Object object) {
        switch (tag) {
            case BaseActivity.TAG_CALL_DIALOG:
                getBaseActivity().callPhone(object.toString());
                break;
        }
    }

    @Override
    public void onDebtorChanged(DBDebtorProfile pressedDeb) {
        mPressedDeb = pressedDeb;
        setDebtorInfoFragment();
    }

    @Override
    public DBDebtorProfile getPressedDebtor() {
        return mPressedDeb;
    }

}
