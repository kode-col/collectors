package com.lifecorp.collector.ui.activities;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.auth.Login;
import com.lifecorp.collector.network.operations.api.auth.Logout;
import com.lifecorp.collector.network.operations.api.debtor.GetDebtors;
import com.lifecorp.collector.ui.dialogfragments.BaseDialog;
import com.lifecorp.collector.ui.fragments.BaseWrapperFragment;
import com.lifecorp.collector.ui.fragments.LeftMenuFragment;
import com.lifecorp.collector.ui.fragments.debtor.DebtorsWrapperFragment;
import com.lifecorp.collector.ui.fragments.event.EventsPlanFragment;
import com.lifecorp.collector.ui.fragments.map.MapWrapperFragment;
import com.lifecorp.collector.ui.interfaces.OnCreditCollectionWorkListener;
import com.lifecorp.collector.utils.LocationHelper;
import com.lifecorp.collector.utils.RefreshButtonManager;
import com.lifecorp.collector.utils.TabPosition;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;
import com.redmadrobot.library.async.gui.ServiceFragment;

/**
 * Основное активити приложения, содержит левое меню и контейнер для фрагментов
 */
public class MainActivity extends BaseActivity implements BaseDialog.OnDialogBtnAcceptClicked,
        BaseDialog.OnDialogBtnCancelClicked, FragmentManager.OnBackStackChangedListener,
        OnCreditCollectionWorkListener, LocationHelper.OnLocationChanged {

    private static final String TAG_TITLE_ID = "TAG_TITLE_ID";
    private static final String TAG_TITLE_OLD_ID = "TAG_TITLE_OLD_ID";
    private static final String TAG_TAB_POSITION = "TAG_TAB_POSITION";
    private static final String TAG_LEFT_FRAGMENT = "TAG_LEFT_FRAGMENT";
    private static final String TAG_CURRENT_FRAGMENT = "TAG_CURRENT_FRAGMENT";
    private static final String TAG_ACTION_BAR_ICON_ID = "TAG_ACTION_BAR_ICON_ID";
    private static final String TAG_DEBTOR_COUNT_TITLE = "TAG_DEBTOR_COUNT_TITLE";
    private static final String TAG_ACTION_BAR_ICON_OLD_ID = "TAG_ACTION_BAR_ICON_OLD_ID";
    private static final String TAG_DEBTOR_OLD_COUNT_TITLE = "TAG_DEBTOR_OLD_COUNT_TITLE";
    private static final String TAG_BACK_STACK_COUNT_WHEN_SAVE_TITLE =
            "TAG_BACK_STACK_COUNT_WHEN_SAVE_TITLE";
    private static final long BACK_PRESS_EXIT_INTERVAL = 2048; // in milliseconds

    private TextView mActivityTitle;
    private DrawerLayout mDrawerLayout;
    private TextView mActivityDebtorsCount;
    private LeftMenuFragment mLeftFragment;
    private LocationHelper mLocationManager;
    private ActionBarDrawerToggle mDrawerToggle;
    private EventsPlanFragment mEventsPlanFragment;
    private MapWrapperFragment mMapWrapperFragment;
    private DebtorsWrapperFragment mDebtorsWrapperFragment;

    private int mTabPosition;
    private long mLastPressTime;
    private boolean mSlideState;
    private int mBackStackCountWhenSaveTitle = 0;
    private RefreshButtonManager mRefreshButtonManager;
    private String mDebtorCountTitle, mDebtorCountOldTitle; // кеш тайтов и иконок
    private int mTitleId, mActionBarIconId, mTitleOldId = 0, mActionBarIconOldId = 0;
    // TODO сделать кэш не последнего заголовка, а всех предыдущих

    @Override
    public void onLocationChanged() {
        if (mDebtorsWrapperFragment != null) {
            mDebtorsWrapperFragment.onLocationChanged();
        } else if (mMapWrapperFragment != null) {
            mMapWrapperFragment.onLocationChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);

        mRefreshButtonManager = new RefreshButtonManager();

        if (!DBCachePoint.getInstance().isSynchronized()) {
            refreshAll(false);
        }

        initInterface(savedInstanceState);
    }

    private void initInterface(Bundle savedInstanceState) {
        setActionBar();

        getFragmentManager().addOnBackStackChangedListener(this);

        mLocationManager = LocationHelper.getInstance();
        mLocationManager.initOnLocationChangedInterface(this);
        mLocationManager.initLocationUtils();

        if (savedInstanceState == null) {
            mLeftFragment = new LeftMenuFragment();
            mLeftFragment.setCurrentItem(TabPosition.EVENTS);
            getFragmentManager().beginTransaction().add(R.id.left_drawer, mLeftFragment,
                    TAG_LEFT_FRAGMENT).commit();
            showEventsFragment();
        } else {
            mDebtorCountTitle = savedInstanceState.getString(TAG_DEBTOR_COUNT_TITLE, "");
            mDebtorCountOldTitle = savedInstanceState.getString(TAG_DEBTOR_COUNT_TITLE, "");

            mTitleId = savedInstanceState.getInt(TAG_TITLE_ID, 0);
            mTitleOldId = savedInstanceState.getInt(TAG_TITLE_OLD_ID, 0);
            mTabPosition = savedInstanceState.getInt(TAG_TAB_POSITION, 0);
            mActionBarIconId = savedInstanceState.getInt(TAG_ACTION_BAR_ICON_ID, 0);
            mActionBarIconOldId = savedInstanceState.getInt(TAG_ACTION_BAR_ICON_OLD_ID, 0);
            mBackStackCountWhenSaveTitle =
                    savedInstanceState.getInt(TAG_BACK_STACK_COUNT_WHEN_SAVE_TITLE, 0);

            Fragment leftFragment = getFragmentManager().findFragmentByTag(TAG_LEFT_FRAGMENT);
            if (leftFragment != null && leftFragment instanceof LeftMenuFragment) {
                mLeftFragment = (LeftMenuFragment) leftFragment;
            }

            Fragment fragment = getFragmentManager().findFragmentByTag(TAG_CURRENT_FRAGMENT);
            if (fragment != null) {
                setCurrentFragment((ServiceFragment) fragment);
            }

            setActionBarArrowDependingOnFragmentsBackStack();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(TAG_DEBTOR_COUNT_TITLE, mDebtorCountTitle);
        outState.putString(TAG_DEBTOR_OLD_COUNT_TITLE, mDebtorCountOldTitle);

        outState.putInt(TAG_TITLE_ID, mTitleId);
        outState.putInt(TAG_TITLE_OLD_ID, mTitleOldId);
        outState.putInt(TAG_TAB_POSITION, mTabPosition);
        outState.putInt(TAG_ACTION_BAR_ICON_ID, mActionBarIconId);
        outState.putInt(TAG_ACTION_BAR_ICON_OLD_ID, mActionBarIconOldId);
        outState.putInt(TAG_BACK_STACK_COUNT_WHEN_SAVE_TITLE, mBackStackCountWhenSaveTitle);
    }

    private void setActionBar() {
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View v = LayoutInflater.from(this).inflate(R.layout.view_activity_title, null, false);
            mActivityTitle = (TextView) v.findViewById(R.id.activity_title);
            mActivityDebtorsCount = (TextView) v.findViewById(R.id.activity_debtors_count);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleDrawer();
                }

            });

            actionBar.setCustomView(v);

            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer,
                    R.string.app_name, R.string.app_name) {

                public void onDrawerClosed(View view) {
                    mSlideState = false; // Закрыт
                    setActionBarArrowDependingOnFragmentsBackStack();
                }

                public void onDrawerOpened(View drawerView) {
                    mSlideState = true; // Открыт
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);
        }
    }

    private void toggleDrawer() {
        if (getBackStackCount() == 0) {
            if (mSlideState) {
                mDrawerLayout.closeDrawer(Gravity.START);
            } else {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        } else {
            onBackPressed();
        }
    }

    public void openFragment(int tabPositionId) {
        TabPosition tabPosition = TabPosition.fromCode(tabPositionId);

        switch (tabPosition) {
            case DEBTORS:
                showDebtorsFragment();
                break;
            case EVENTS:
                showEventsFragment();
                break;
            case MAP:
                showMapFragment();
                break;
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (title == null) {
            title = "";
        }

        if (mActivityTitle != null) {
            mActivityTitle.setText(title);
            mActivityDebtorsCount.setText("");
        }
    }

    public void setDebtorsCount(String countTitle) {
        if (countTitle == null || mTabPosition != TabPosition.DEBTORS.getCode()) {
            countTitle = "";
        }

        if (mActivityDebtorsCount != null) {
            mDebtorCountTitle = countTitle;
            mActivityDebtorsCount.setText(countTitle);
        }
    }

    public void setTitleAndIcon(int titleId, int iconId) {
        mTitleId = titleId;
        setTitle(titleId);

        if (getActionBar() != null) {
            mActionBarIconId = iconId;
            getActionBar().setIcon(iconId);
        }
    }

    public void setDebtorTitleAndIcon() {
        setTitleAndIcon(R.string.menu_debtors, R.drawable.ico_menu_client);
    }

    public void setEventsTitleAndIcon() {
        setTitleAndIcon(R.string.menu_events, R.drawable.ico_menu_plan);
    }

    public void setMapTitleAndIcon() {
        setTitleAndIcon(R.string.menu_map, R.drawable.ico_menu_map);
    }

    public void setDebtorAddressTitleAndIcon() {
        setTitleAndIcon(R.string.title_debtor_address, R.drawable.ico_menu_map);
    }

    public void setDebtorFormTitleAndIcon() {
        setTitleAndIcon(R.string.title_debtor_form, R.drawable.ico_menu_client);
    }

    private void setTitleAndIcon(TabPosition tabPosition) {
        switch (tabPosition) {
            case DEBTORS:
                setDebtorTitleAndIcon();
                break;
            case EVENTS:
                setEventsTitleAndIcon();
                break;
            case MAP:
                setMapTitleAndIcon();
                break;
        }
    }

    public void showDebtorsFragment() {
        showFragment(DebtorsWrapperFragment.newInstance(), TabPosition.DEBTORS);
    }

    public void showDebtorsFragment(String pressedDebId) {
        showFragment(DebtorsWrapperFragment.newInstance(pressedDebId), TabPosition.DEBTORS);
    }

    public void showEventsFragment() {
        showFragment(EventsPlanFragment.newInstance(), TabPosition.EVENTS);
    }

    public void showMapFragment() {
        showFragment(MapWrapperFragment.newInstance(DBCachePoint.getInstance().getAllEvents()),
                TabPosition.MAP);
    }


    public void showMapFragment(int id) {
        showFragment(MapWrapperFragment.newInstance(DBCachePoint.getInstance().getAllEvents(), id),
                TabPosition.MAP);
    }

    private void setCurrentFragment(ServiceFragment fragment) {
        if (fragment != null) {
            mDebtorsWrapperFragment = null;
            mEventsPlanFragment = null;
            mMapWrapperFragment = null;

            if (fragment instanceof DebtorsWrapperFragment) {
                mDebtorsWrapperFragment = (DebtorsWrapperFragment) fragment;
            } else if (fragment instanceof EventsPlanFragment) {
                mEventsPlanFragment = (EventsPlanFragment) fragment;
            } else if (fragment instanceof MapWrapperFragment) {
                mMapWrapperFragment = (MapWrapperFragment) fragment;
            } else {
                throw new IllegalStateException("Новый тип фрагмента?");
            }
        }
    }

    private void saveTitlesToCache() {
        mDebtorCountOldTitle = mDebtorCountTitle;
        mActionBarIconOldId = mActionBarIconId;
        mTitleOldId = mTitleId;
        mBackStackCountWhenSaveTitle = getBackStackCount();
    }

    private int getBackStackCount() {
        return getFragmentManager().getBackStackEntryCount();
    }

    @Override
    protected void showCompleteEventFragment(Fragment fragment) {
        saveTitlesToCache();
        setEventsTitleAndIcon();

        mDebtorCountTitle = "";

        addFragment(R.id.content_frame, fragment, TAG_COMPLETE_EVENT, false);
    }

    @Override
    protected void showCollectionAddFragment(Fragment fragment) {
        saveTitlesToCache();
        setDebtorFormTitleAndIcon();

        mDebtorCountTitle = "";

        addFragment(R.id.content_frame, fragment, TAG_ADD_DIALOG, false);
    }

    private void showFragment(ServiceFragment fragment, TabPosition tabPosition) {
        if (fragment != null && tabPosition != null && mTabPosition != tabPosition.getCode()) {
            tryLaunchOperationQueue();
            invalidateOptionsMenu();

            setTitleAndIcon(tabPosition);
            setCurrentFragment(fragment);
            mTabPosition = tabPosition.getCode();
            if (mLeftFragment != null) {
                mLeftFragment.setCurrentItem(tabPosition);
            }

            addFragment(R.id.content_frame, fragment, TAG_CURRENT_FRAGMENT, true);
        }
    }

    private void addFragment(int id, Fragment fragment, String tag, boolean isReplace) {
        if (fragment != null && tag != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();

            if (isReplace) {
                ft.replace(id, fragment, tag);
            } else {
                ft.add(id, fragment, tag).addToBackStack(tag);
            }

            ft.commit();
        }
    }

    @Nullable
    public DBDebtorProfile getPressedDeb() {
        DBDebtorProfile dbDebtorProfile = null;

        if (mDebtorsWrapperFragment != null) {
            dbDebtorProfile = mDebtorsWrapperFragment.getPressedDebtor();
        } else if (mMapWrapperFragment != null) {
            dbDebtorProfile = mMapWrapperFragment.getPressedDebtor();
        }

        return dbDebtorProfile;
    }

    @Nullable
    public String getSavedTextSearchPanel() {
        String savedTextSearchPanel = null;

        if (mDebtorsWrapperFragment != null) {
            savedTextSearchPanel = mDebtorsWrapperFragment.getSavedTextSearchPanel();
        }

        return savedTextSearchPanel;
    }

    public boolean isSearchActive() {
        boolean result = false;

        if (mDebtorsWrapperFragment != null) {
            result = mDebtorsWrapperFragment.isSearchActive();
        }

        return result;
    }

    public void collapseSearch() {
        if (mDebtorsWrapperFragment != null) {
            mDebtorsWrapperFragment.collapseSearch();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mLocationManager.requestLocationUpdates();
        setTitleAndIcon(mTitleId, mActionBarIconId);
        setDebtorsCount(mDebtorCountTitle);

        if (mRefreshButtonManager != null) {
            mRefreshButtonManager.setIsOnPause(false);
            mRefreshButtonManager.updateStatus();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mRefreshButtonManager != null) {
            mRefreshButtonManager.setIsOnPause(true);
        }
        mLocationManager.removeLocationUpdates();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        if (mRefreshButtonManager != null) {
            MenuItem menuItem = menu.findItem(R.id.action_refresh);
            mRefreshButtonManager.setMenuItem(menuItem);
            mRefreshButtonManager.updateStatus();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.isDrawerIndicatorEnabled()
                && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else {
            switch (item.getItemId()) {
                case R.id.action_refresh:
                    refreshAll();
                    return true;
                case android.R.id.home:
                    if (getFragmentManager().popBackStackImmediate()) {
                        return true;
                    }
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
    }

    private void setTitleFromCache(boolean needChange) {
        if (needChange && mTitleOldId != 0 && mActionBarIconOldId != 0) {
            setTitleAndIcon(mTitleOldId, mActionBarIconOldId);
            setDebtorsCount(mDebtorCountOldTitle);

            mDebtorCountOldTitle = null;
            mActionBarIconOldId = 0;
            mTitleOldId = 0;
            mBackStackCountWhenSaveTitle = 0;
        }
    }

    private void setActionBarArrowDependingOnFragmentsBackStack() {
        mDrawerToggle.setDrawerIndicatorEnabled(getBackStackCount() == 0);
        setTitleFromCache(getBackStackCount() == mBackStackCountWhenSaveTitle);
    }

    @Override
    protected void onDestroy() {
        getFragmentManager().removeOnBackStackChangedListener(this);
        super.onDestroy();
    }

    // for toggle
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    // for toggle
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void closeLeft() {
        mDrawerLayout.closeDrawer(Gravity.START);
    }

    @Override
    public void startActivity(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        super.startActivity(intent);
        overridePendingTransition(0, 0);
    }

    @Override
    public void setContentView(int layoutResID) {
        getLayoutInflater().inflate(layoutResID, (ViewGroup) findViewById(R.id.content_frame));
    }

    public void onOperationFinished(GetDebtors.Result result) {
        mIsLoadingDebtors = false;

        if (result != null && result.isSuccessful()) {
            loadEvents();
        }
    }

    public void onOperationFinished(Logout.Result result) {
        super.goToAuthorization();
    }

    @Override
    public void onOperationFinished(Login.Result result) {
        mIsTryLogin = false;

        if (result != null && result.isSuccessful()) {
            tryLaunchOperationQueue();
        } else {
            PreferencesStorage.getInstance().clearLogin();
            PreferencesStorage.getInstance().clearPassword();
            goToAuthorization();
        }
    }

    public void turnGPSOn() {
        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    @Override
    public void goToAuthorization() {
        if (!mIsTryLogin) {
            PreferencesStorage.getInstance().clearToken();

            if (!PreferencesStorage.getInstance().getLogin().isEmpty()
                    && !PreferencesStorage.getInstance().getPassword().isEmpty()) {
                login();
            } else {
                super.goToAuthorization();
            }
        }
    }

    @Override
    protected void sendOperationForFragment(
            CollectorOperationResult<? extends ServerResponse> result) {
        mRefreshButtonManager.updateStatus();

        if (getCurrentFragment() != null) {
            getCurrentFragment().onOperationFinished(result);
        }
    }

    @Override
    protected BaseWrapperFragment getCurrentFragment() {
        BaseWrapperFragment fragment = null;

        if (mDebtorsWrapperFragment != null) {
            fragment = mDebtorsWrapperFragment;
        } else if (mEventsPlanFragment != null) {
            fragment = mEventsPlanFragment;
        } else if (mMapWrapperFragment != null) {
            fragment = mMapWrapperFragment;
        }

        return fragment;
    }
    
    @Override
    public void onDialogBtnAcceptClicked(String tag, Object object) {
        if (getCurrentFragment() != null) {
            getCurrentFragment().onDialogBtnAcceptClicked(tag, object);
        }

        switch (tag) {
            case BaseActivity.TAG_MAP_DIALOG:
                turnGPSOn();
                break;
        }
    }

    @Override
    public void onDialogBtnCancelClicked(String tag, Object object) {
        // необходим как задел на будущее и для работы сброса фильтра в плане мероприятий
    }

    @Override
    public void onBackPressed() {
        if (mSlideState) {
            toggleDrawer();
        } else if (getBackStackCount() > 0) {
            super.onBackPressed();
        } else {
            long pressTime = System.currentTimeMillis();
            if (pressTime - mLastPressTime <= BACK_PRESS_EXIT_INTERVAL) {
               super.onBackPressed();
            } else {
                makeToast(R.string.message_back_once);
            }

            mLastPressTime = pressTime;
        }
    }

    @Override
    protected void addRefreshAllToQuery() {
        super.addRefreshAllToQuery();


    }

    @Override
    public void onBackStackChanged() {
        setActionBarArrowDependingOnFragmentsBackStack();
    }

    public boolean isLoadingDebtors() {
        return mIsLoadingDebtors;
    }

    @Override
    public void onViewCollectionHistory(int debtorId, String creditId) {
        if (mDebtorsWrapperFragment != null) {
            mDebtorsWrapperFragment.setCollectionFragment(debtorId, creditId);
        }
    }

    @Override
    public void onAddCollection(int debtorId, String creditId) {
        createAddCollectionFragment(getPressedDeb(), creditId); // создать запись в коллекцию
    }

}
