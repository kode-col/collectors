package com.lifecorp.collector.ui.fragments;

import android.app.Activity;

import com.lifecorp.collector.ui.activities.BaseActivity;
import com.lifecorp.collector.ui.activities.MainActivity;
import com.lifecorp.collector.utils.ToastHelper;
import com.redmadrobot.library.async.gui.ServiceFragment;

public abstract class BaseFragment extends ServiceFragment {

    protected void makeToast(int msgId) {
        ToastHelper.getInstance().makeToast(getActivity(), msgId);
    }

    protected void makeToastLong(int msgId) {
        ToastHelper.getInstance().makeToastLong(getActivity(), msgId);
    }

    protected BaseActivity getBaseActivity() {
        final Activity activity = getActivity();
        if (activity == null) {
            return null;
        }

        return (BaseActivity) activity;
    }

    protected MainActivity getMainActivity() {
        final Activity activity = getActivity();
        if (activity == null) {
            return null;
        }

        return (MainActivity) activity;
    }
    
}
