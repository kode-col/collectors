package com.lifecorp.collector.ui.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import com.lifecorp.collector.R;
import com.lifecorp.collector.utils.Const;

/**
 * Created by Alexander Smirnov on 15.04.15.
 *
 * Загрузочное активити
 */
public class SplashActivity extends Activity implements Animator.AnimatorListener {

    private static final int ANIMATION_DURATION = 1200;
    private View mLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        mLogo = findViewById(R.id.activity_splash_logo);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onAnimationEnd(null);
            }
        }, ANIMATION_DURATION);

        // startAnimation(mLogo);
    }

    private void startAnimation(View logo) {
        ObjectAnimator scaleUp = ObjectAnimator.ofPropertyValuesHolder(logo,
                PropertyValuesHolder.ofFloat("scaleX", 0f, 1f),
                PropertyValuesHolder.ofFloat("scaleY", 0f, 1f));
        scaleUp.setInterpolator(new OvershootInterpolator());
        scaleUp.setDuration(ANIMATION_DURATION);
        scaleUp.setStartDelay(Const.DEFAULT_TIME);
        scaleUp.addListener(this);
        scaleUp.start();
    }

    @Override
    public void onAnimationStart(Animator animation) {
        // ничего
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        startActivity(new Intent(this, AuthActivity.class));
        finish();
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        // ничего
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
        // ничего
    }

}
