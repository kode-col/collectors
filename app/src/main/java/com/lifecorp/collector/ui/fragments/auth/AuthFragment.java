package com.lifecorp.collector.ui.fragments.auth;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.network.operations.api.auth.Login;
import com.lifecorp.collector.ui.activities.AuthActivity;
import com.lifecorp.collector.ui.fragments.BaseFragment;
import com.lifecorp.collector.utils.Utils;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;

public class AuthFragment extends BaseFragment {

    private String LOGIN_EXTRA = "LOGIN_EXTRA";
    private String PASS_EXTRA = "PASS_EXTRA";

    private EditText mEditTextPassword;
    private EditText mEditTextLogin;
    private Button mButtonEnter;

    public boolean mOperationRunning = false;
    public boolean isNoTextL = true;
    public boolean isNoTextP = true;
    private boolean mIsUnauthorized;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);

        initInterface(view);

        if (savedInstanceState != null) {
            if (mEditTextLogin != null) {
                mEditTextLogin.setText(savedInstanceState.getString(LOGIN_EXTRA));
            }
            if (mEditTextPassword != null) {
                mEditTextPassword.setText(savedInstanceState.getString(PASS_EXTRA));
            }
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mEditTextLogin != null)  {
            outState.putString(LOGIN_EXTRA, getLogin());
        }
        if (mEditTextPassword != null) {
            outState.putString(PASS_EXTRA, getPassword());
        }
    }

    // Слушаем результат выполнения запроса логина
    @SuppressWarnings("UnusedDeclaration")
    public void onOperationFinished(final Login.Result result) {
        if (result != null) {
            mOperationRunning = false;
            toggleEnableMode(true);

            if (!getBaseActivity().checkoutError(result)) {
                if (!PreferencesStorage.getInstance().getToken().isEmpty()) {
                    ((AuthActivity) getActivity()).goToNextActivity();
                } else {
                    getBaseActivity().showHideProgress(false);
                    showErrorMessage(result);
                }
            } else {
                getBaseActivity().showHideProgress(false);
                showErrorMessage(result);
            }
        }
    }

    private void showErrorMessage(final Login.Result result) {
        int defaultResId = R.string.msg_error;

        if (result.getErrorCode() != null && result.getErrorCode().getCode() == 0) {
            // если код ошибки равен 0, значит отсутствует интернет подключение
            defaultResId = R.string.msg_no_internet_connection;
        }

        makeToast(mIsUnauthorized ? R.string.msg_enter_error : defaultResId);

        if (mIsUnauthorized) {
            mIsUnauthorized = false;
        }
    }

    public void setIsUnauthorized(boolean isUnauthorized) {
        mIsUnauthorized = isUnauthorized;
    }

    // true - in enable mode, false - in loading mode
    public void toggleEnableMode(boolean state) {
        mEditTextLogin.setEnabled(state);
        mEditTextLogin.setFocusableInTouchMode(state);

        mEditTextPassword.setEnabled(state);
        mEditTextPassword.setFocusableInTouchMode(state);

        mButtonEnter.setEnabled(state);
    }

    public String getLogin() {
        return mEditTextLogin.getText().toString();
    }

    public String getPassword() {
        return mEditTextPassword.getText().toString();
    }

    public void initInterface(View view) {
        mEditTextLogin = (EditText) view.findViewById(R.id.etAuthLogin);
        mEditTextPassword = (EditText) view.findViewById(R.id.etAuthPass);
        mButtonEnter = (Button) view.findViewById(R.id.btnAuthEnter);

        if (!getLogin().isEmpty()) {
            isNoTextL = false;
        }
        if (!getPassword().isEmpty()) {
            isNoTextP = false;
        }
        mButtonEnter.setEnabled(!isNoTextL && !isNoTextP);

        mButtonEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getLogin().isEmpty() || getPassword().isEmpty()) {
                    makeToast(R.string.msg_enter);
                } else if (!mOperationRunning) {
                    mOperationRunning = true;

                    toggleEnableMode(false);

                    PreferencesStorage.getInstance().saveLogin(getLogin());
                    PreferencesStorage.getInstance().savePassword(getPassword());

                    getBaseActivity().login();
                    getBaseActivity().showHideProgress(true);
                }
            }
        });

        mEditTextPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null) {
                    if (actionId == EditorInfo.IME_ACTION_SEND ||
                            (event.getAction() == KeyEvent.ACTION_DOWN &&
                                    event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                        Utils.closeKeyboard(mEditTextPassword);
                        mButtonEnter.performClick();
                        return true;
                    }
                }
                return false;
            }
        });

        mEditTextLogin.addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                isNoTextL = editable.toString().isEmpty();
                super.afterTextChanged(editable);
            }
        });

        mEditTextPassword.addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                isNoTextP = editable.toString().isEmpty();
                super.afterTextChanged(editable);
            }
        });

    }

    private class CustomTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!mOperationRunning) {
                mButtonEnter.setEnabled(!isNoTextL && !isNoTextP);
            }
        }
    }

}
