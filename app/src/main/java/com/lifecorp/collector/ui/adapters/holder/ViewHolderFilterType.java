package com.lifecorp.collector.ui.adapters.holder;

import android.view.View;
import android.widget.ImageView;

import com.lifecorp.collector.ui.view.CustomFontTextView;

/**
 * Created by Alexander Smirnov on 14.04.15.
 *
 * ViewHolder для {@link com.lifecorp.collector.ui.adapters.MapFilterTypeAdapter}
 */
public class ViewHolderFilterType {
    public View divider;
    public ImageView ivChecked;
    public CustomFontTextView title;
}
