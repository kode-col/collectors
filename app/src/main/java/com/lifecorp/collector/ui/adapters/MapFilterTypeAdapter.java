package com.lifecorp.collector.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.objects.FilterType;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderFilterType;
import com.lifecorp.collector.ui.enums.EventType;
import com.lifecorp.collector.ui.enums.RobotoFont;
import com.lifecorp.collector.ui.view.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 14.04.15.
 *
 * Адаптер для списка в диалоге {@link com.lifecorp.collector.ui.dialogfragments.MapFilterDialog}
 * отображает различные варианты фильтрации для
 * {@link com.lifecorp.collector.ui.fragments.map.MapWrapperFragment}
 */
public class MapFilterTypeAdapter extends BaseAdapter {

    private final List<FilterType> mFilterItemList = new ArrayList<>();
    private int mSelectedItem = -1;

    public MapFilterTypeAdapter(EventType selectedEventType) {
        fillData();
        eventTypeToPosition(selectedEventType);
    }

    private void fillData() {
        mFilterItemList.add(new FilterType(R.string.filter_all, R.drawable.ico_ok_black,
                R.color.black, RobotoFont.MEDIUM));
        mFilterItemList.add(new FilterType(R.string.filter_all_events, R.drawable.ico_ok_black,
                R.color.black_80, RobotoFont.MEDIUM));
        mFilterItemList.add(new FilterType(R.string.filter_in_work, R.drawable.ico_ok_green,
                R.color.green, RobotoFont.REGULAR));
        mFilterItemList.add(new FilterType(R.string.filter_edge, R.drawable.ico_ok_orange,
                R.color.orange, RobotoFont.REGULAR));
        mFilterItemList.add(new FilterType(R.string.filter_expired, R.drawable.ico_ok_red,
                R.color.red, RobotoFont.REGULAR));
    }

    private void eventTypeToPosition(EventType selectedEventType) {
        if (selectedEventType != null) {
            switch (selectedEventType) {
                case ALL_CLIENTS:
                    setSelectedItem(0);
                    break;
                case ALL_EVENTS:
                    setSelectedItem(1);
                    break;
                case IN_WORK:
                    setSelectedItem(2);
                    break;
                case EDGE:
                    setSelectedItem(3);
                    break;
                case EXPIRED:
                    setSelectedItem(4);
                    break;
            }
        }
    }

    public EventType positionToEventType(int position) {
        EventType eventType = EventType.ALL_CLIENTS;

        switch (position) {
            case 0:
                eventType = EventType.ALL_CLIENTS;
                break;
            case 1:
                eventType = EventType.ALL_EVENTS;
                break;
            case 2:
                eventType = EventType.IN_WORK;
                break;
            case 3:
                eventType = EventType.EDGE;
                break;
            case 4:
                eventType = EventType.EXPIRED;
                break;
        }

        return eventType;
    }

    public void setSelectedItem(int position) {
        mSelectedItem = position;
    }

    @Override
    public int getCount() {
        return mFilterItemList.size();
    }

    @Override
    public FilterType getItem(int position) {
        return position < getCount() ? mFilterItemList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderFilterType holder;

        if (convertView == null) {
            int layoutId = R.layout.view_dialog_filter_list_item;
            convertView = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);

            holder = new ViewHolderFilterType();
            holder.title = (CustomFontTextView)
                    convertView.findViewById(R.id.view_dialog_filter_list_item_title);
            holder.divider = convertView.findViewById(R.id.view_dialog_filter_list_item_divider);
            holder.ivChecked = (ImageView)
                    convertView.findViewById(R.id.view_dialog_filter_list_item_checked);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderFilterType) convertView.getTag();
        }

        final FilterType item = getItem(position);
        if (item != null) {
            holder.title.setText(item.getTitleRes());
            holder.title.setFont(RobotoFont.getFontName(item.getFont()));
            holder.title.setTextColor(App.getContext().getResources().getColor(item.getColorRes()));
            holder.ivChecked.setVisibility(position == mSelectedItem ? View.VISIBLE : View.GONE);

            // у каждой эдемента свой ресурс изображения
            holder.ivChecked.setImageResource(item.getIconRes());

            // показывать только для первого элемента
            holder.divider.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
        }

        return convertView;
    }

}
