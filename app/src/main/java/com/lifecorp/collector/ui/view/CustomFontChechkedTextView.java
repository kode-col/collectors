package com.lifecorp.collector.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.utils.Utils;

/**
 * Created by Alexander Smirnov on 08.05.15.
 *
 *
 */
public class CustomFontChechkedTextView extends CheckedTextView {

    public CustomFontChechkedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public CustomFontChechkedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomFontChechkedTextView(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomFont);
            Utils.setFont(this, a.getString(R.styleable.CustomFont_fontName));
            a.recycle();
        }
    }

}
