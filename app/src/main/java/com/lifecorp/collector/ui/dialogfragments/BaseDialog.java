package com.lifecorp.collector.ui.dialogfragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.lifecorp.collector.R;

public abstract class BaseDialog extends DialogFragment {

    protected OnDialogBtnAcceptClicked mOnDialogBtnAcceptClicked;
    protected OnDialogBtnCancelClicked mOnDialogBtnCancelClicked;

    public interface OnDialogBtnCancelClicked {
        void onDialogBtnCancelClicked(String tag, Object object);
    }

    public interface OnDialogBtnAcceptClicked {
        void onDialogBtnAcceptClicked(String tag, Object object);
    }

    public void close() {
        if (getFragmentManager() != null) {
            getFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
        }
    }

    @Override
    public void show(@NonNull FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
            try {
                manager.executePendingTransactions();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnDialogBtnCancelClicked) {
            mOnDialogBtnCancelClicked = (OnDialogBtnCancelClicked) activity;
        }
        if (activity instanceof OnDialogBtnAcceptClicked) {
            mOnDialogBtnAcceptClicked = (OnDialogBtnAcceptClicked) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnDialogBtnCancelClicked = null;
        mOnDialogBtnAcceptClicked = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }

    protected void initButtons(View rootView) {
        rootView.findViewById(R.id.btnDialogCancel).setOnClickListener(getOnCancelClickListener());
        rootView.findViewById(R.id.btnDialogAccept).setOnClickListener(getOnAcceptClickListener());
    }

    protected final View.OnClickListener getOnCancelClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCancelClick();
            }
        };
    }

    protected final View.OnClickListener getOnAcceptClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAcceptClick();
            }
        };
    }

    private void onCancelClick() {
        close();
        if (mOnDialogBtnCancelClicked != null) {
            onCancelApproveClick();
        }
    }

    private void onAcceptClick() {
        close();
        if (mOnDialogBtnAcceptClicked != null) {
            onAcceptApproveClick();
        }
    }

    protected abstract void onCancelApproveClick();

    protected abstract void onAcceptApproveClick();
}