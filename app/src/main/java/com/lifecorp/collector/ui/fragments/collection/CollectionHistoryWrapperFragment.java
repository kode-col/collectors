package com.lifecorp.collector.ui.fragments.collection;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.lifecorp.collector.R;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.ui.adapters.collection.CollectionHistoryPagerAdapter;
import com.lifecorp.collector.ui.fragments.BaseFragment;
import com.lifecorp.collector.ui.interfaces.OnCollectorFragmentChangeListener;
import com.lifecorp.collector.ui.view.PagerSlidingTabStrip;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.SetAdapterForViewPagerWithTitleTask;


public class CollectionHistoryWrapperFragment extends BaseFragment implements
        OnCollectorFragmentChangeListener {

    private int mDebtorId;
    private String mCreditId;
    private CollectionHistoryPagerAdapter mAdapter;
    private CollectionHistoryFragment mHistoryFragment;
    private CollectionHistoryExpiredFragment mExpiredFragment;

    public static CollectionHistoryWrapperFragment newInstance(int debtorId, String creditId) {
        CollectionHistoryWrapperFragment fragment = new CollectionHistoryWrapperFragment();
        fragment.setDebtorId(debtorId);
        fragment.setCreditId(creditId);
        return fragment;
    }

    private void setDebtorId(int debtorId) {
        mDebtorId = debtorId;
    }

    private void setCreditId(String creditId) {
        mCreditId = creditId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collection_main, container, false);

        if (savedInstanceState != null) {
            mDebtorId = savedInstanceState.getInt(Const.TAG_DEBTOR_ID, 0);
            mCreditId = savedInstanceState.getString(Const.TAG_CREDIT_ID, "");
        }

        initInterface(view);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Const.TAG_DEBTOR_ID, mDebtorId);
        outState.putString(Const.TAG_CREDIT_ID, mCreditId);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_collection, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                getBaseActivity().createAddCollectionFragment(getMainActivity().getPressedDeb(),
                        mCreditId);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onOperationFinished(CollectorOperationResult<? extends ServerResponse> result) {
        if (result != null && result.isSuccessful()) {
            if (result.getOutput() != null) {
                String tag = result.getOutput().getResponseType();

                switch (tag) {
                    case Const.TAG_COLLECTIONS:
                        if (mHistoryFragment != null) {
                            mHistoryFragment.onOperationFinished(result);
                        }
                        break;
                    case Const.TAG_POST_COLLECTION:
                        if (mHistoryFragment != null) {
                            mHistoryFragment.onOperationFinished(result);
                        }
                        if (mExpiredFragment != null) {
                            mExpiredFragment.onOperationFinished(result);
                        }
                        break;
                    case Const.TAG_COMPLETE_EVENT:
                        getBaseActivity().refreshAll();
                        if (mAdapter != null) {
                            mAdapter.notifyDataSetChanged();
                        }

                        if (mHistoryFragment != null) {
                            mHistoryFragment.onOperationFinished(result);
                        }
                        if (mExpiredFragment != null) {
                            mExpiredFragment.onOperationFinished(result);
                        }
                        break;
                    case Const.TAG_EVENTS:
                        if (mExpiredFragment != null) {
                            mExpiredFragment.onOperationFinished(result);
                        }
                        break;
                }
            }
        }
    }

    private void initInterface(View view) {
        mAdapter = new CollectionHistoryPagerAdapter(getFragmentManager(),
                mDebtorId, mCreditId, this);

        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.collectionPager);
        PagerSlidingTabStrip pagerTitleStrip =
                (PagerSlidingTabStrip) view.findViewById(R.id.collectionTabsStrip);

        new SetAdapterForViewPagerWithTitleTask(viewPager, mAdapter, pagerTitleStrip).execute();
    }

    @Override
    public void onCollectionHistoryFragmentChanged(CollectionHistoryFragment fragment) {
        mHistoryFragment = fragment;
    }

    @Override
    public void onCollectionExpiredFragmentChanged(CollectionHistoryExpiredFragment fragment) {
        mExpiredFragment = fragment;
    }

}
