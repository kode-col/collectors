package com.lifecorp.collector.ui.adapters.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Alexander Smirnov on 20.04.15.
 *
 * Базовый холдер заголовка выпадающего списка
 */
public class ViewHolderTitle {
    public View expIcon;
    public ImageView icon;
    public TextView title, date;
}