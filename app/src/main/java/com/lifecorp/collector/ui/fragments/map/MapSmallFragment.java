package com.lifecorp.collector.ui.fragments.map;

import android.location.Location;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.gms.maps.GoogleMap;
import com.lifecorp.collector.R;
import com.lifecorp.collector.utils.LocationHelper;

/**
 * Created by Alexander Smirnov on 21.04.15.
 *
 * Карта для фрагмента создания записи Collection
 */
public class MapSmallFragment extends MapBaseFragment implements View.OnClickListener,
        GoogleMap.OnMyLocationChangeListener {

    private static final int DEFAULT_LOCATION_SEARCH_DURATION = 20000;
    private static final int DEFAULT_SMALL_ZOOM = 12;
    private Animation mAnimationRotate;
    private View mClickableHolder;
    private View mCircle;

    @Override
    protected int getContentLayoutId() {
        return R.layout.fragment_map_small;
    }

    @Override
    protected int getMapReplaceId() {
        return R.id.fragment_map_new_small_map;
    }

    @Override
    protected void initButtons(View rootView) {
        if (rootView != null) {
            mCircle = rootView.findViewById(R.id.fragment_map_new_refresh_location_circle);
            mClickableHolder = rootView.findViewById(R.id.fragment_map_new_refresh_location);
            mClickableHolder.setOnClickListener(this);
        }
    }

    @Override
    protected void initMapAfterAdding() {
        if (getMap() == null || mCircle == null) {
            return;
        }

        mAnimationRotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_infinity);
        getMap().setOnMyLocationChangeListener(this);
        if (mClickableHolder != null) {
            mClickableHolder.callOnClick();
        }

        Location lastLoc = LocationHelper.getInstance().getLastLocation();
        if (lastLoc != null) {
            updateCamera(lastLoc.getLatitude(), lastLoc.getLongitude(), DEFAULT_SMALL_ZOOM);
        } else {
            setCameraOnFakeCenter();
        }
    }

    @Override
    public void onClick(View v) {
        if (mCircle != null && LocationHelper.getInstance().isLocationServicesEnabled()) {
            makeToast(R.string.msg_try_search_location);
            mCircle.startAnimation(mAnimationRotate);

            mCircle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkSearchLocationResult();
                }
            }, DEFAULT_LOCATION_SEARCH_DURATION);
        } else {
            getBaseActivity().createEnableLocationServiceDialog();
        }
    }

    private void checkSearchLocationResult() {
        if (mCircle != null) {
            mCircle.clearAnimation();
        }
        if (getBaseActivity() != null && (getMap() == null || getMap().getMyLocation() == null)) {
            getBaseActivity().createLocationSearchProblem();
        }
    }

    @Override
    public void onMyLocationChange(Location location) {
        LocationHelper.getInstance().updateFromGoogleMap(location);
        updateCamera(location.getLatitude(), location.getLongitude(), DEFAULT_SMALL_ZOOM);
        mCircle.clearAnimation();
    }

}
