package com.lifecorp.collector.ui.interfaces;

/**
 * Created by Alexander Smirnov on 17.04.15.
 *
 * Интерфейс для работы с коллекциями кредита
 */
public interface OnCreditCollectionWorkListener {

    void onViewCollectionHistory(int debtorId, String creditId);

    void onAddCollection(int debtorId, String creditId);

}
