package com.lifecorp.collector.ui.adapters.holder;

import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.ui.adapters.debtor.DebtorsListAdapter;

/**
 * Created by Alexander Smirnov on 24.03.15.
 *
 * ViewHolder для {@link DebtorsListAdapter}
 */
public class ViewHolderDebtor {
    public TextView name;
    public TextView money;
    public TextView coins;
    public TextView dateDist;

    public ImageView debtorIcon;
    public ImageView promisedPay;
}
