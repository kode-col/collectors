package com.lifecorp.collector.ui.interfaces;

import com.lifecorp.collector.ui.enums.EventFilterType;

/**
 * Created by Alexander Smirnov on 14.04.15.
 *
 * Интерфейс изменения состояния фильтра мероприятия
 */
public interface OnEventFilterChanged {

    void onEventFilterChanged(EventFilterType eventType, Long dateFrom, Long dateTo);

}
