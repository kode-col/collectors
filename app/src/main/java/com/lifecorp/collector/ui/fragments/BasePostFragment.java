package com.lifecorp.collector.ui.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBActionResultType;
import com.lifecorp.collector.model.database.objects.DBActionType;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.objects.PhotoArrayContainer;
import com.lifecorp.collector.model.objects.action.ConcreteResult;
import com.lifecorp.collector.model.objects.action.StatusResult;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.ui.adapters.collection.PhotoGridAdapter;
import com.lifecorp.collector.ui.fragments.map.MapSmallFragment;
import com.lifecorp.collector.ui.interfaces.OnPhotoManageListener;
import com.lifecorp.collector.utils.DateUtils;
import com.lifecorp.collector.utils.ImageFileHelper;
import com.lifecorp.collector.utils.LocationHelper;
import com.lifecorp.collector.utils.Utils;
import com.lifecorp.collector.utils.spinner.SpinnerItem;
import com.lifecorp.collector.utils.spinner.SpinnerSelector;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Alexander Smirnov on 08.05.15.
 *
 * Базовый фрагмент для постинга
 */
public abstract class BasePostFragment extends BaseFragment implements View.OnClickListener,
        OnPhotoManageListener {

    private static final String TAG_EVENT = "TAG_EVENT";
    private static final String TAG_COMMENT = "TAG_COMMENT";
    private static final String TAG_SMALL_MAP = "TAG_SMALL_MAP";
    private static final String TAG_PHOTO_CONTAINER = "TAG_PHOTO_CONTAINER";
    private static final String TAG_PHOTO_WAS_CREATED = "TAG_PHOTO_WAS_CREATED";
    private static final String TAG_IS_ADD_COLLECTION = "TAG_IS_ADD_COLLECTION";
    private static final String TAG_PHOTO_CURRENT_PATH = "TAG_PHOTO_CURRENT_PATH";
    private static final String TAG_SPINNER_SELECT_TYPE = "TAG_SPINNER_SELECT_TYPE";
    private static final String TAG_SPINNER_SELECT_GENERAL = "TAG_SPINNER_SELECT_GENERAL";
    private static final String TAG_SPINNER_SELECT_CONCRETE = "TAG_SPINNER_SELECT_CONCRETE";

    private boolean mIsOnSaveInstance = false;
    private int mSpinnerConcreteResultPos = -1;
    private int mSpinnerGeneralResultPos = -1;
    private int mSpinnerTypePos = -1;
    private String mCommentInput;

    private DBEvent mEvent;
    private int mDefaultPosition;
    private boolean mPhotoIsCreating;
    private String mPhotoCurrentPath;
    protected boolean mIsAddCollection;
    private List<DBActionType> mActionTypes;
    private ImageFileHelper mImageFileHelper;
    private List<DBActionResultType> mActionResultTypes;

    protected EditText mComment;
    protected EditText mPayDate;
    protected EditText mSumInput;
    protected Spinner mSpinnerType;
    protected PhotoGridAdapter mAdapter;
    protected Spinner mSpinnerGeneralResult;
    protected Spinner mSpinnerConcreteResult;
    protected PhotoArrayContainer mPhotoContainer = new PhotoArrayContainer();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mEvent = (DBEvent) savedInstanceState.getSerializable(TAG_EVENT);
            mPhotoContainer = (PhotoArrayContainer)
                    savedInstanceState.getSerializable(TAG_PHOTO_CONTAINER);
            mCommentInput = savedInstanceState.getString(TAG_COMMENT, null);

            mIsAddCollection = savedInstanceState.getBoolean(TAG_IS_ADD_COLLECTION);
            mPhotoIsCreating = savedInstanceState.getBoolean(TAG_PHOTO_WAS_CREATED);
            mPhotoCurrentPath = savedInstanceState.getString(TAG_PHOTO_CURRENT_PATH);

            mSpinnerTypePos = savedInstanceState.getInt(TAG_SPINNER_SELECT_TYPE);
            mSpinnerGeneralResultPos = savedInstanceState.getInt(TAG_SPINNER_SELECT_GENERAL, -1);
            mSpinnerConcreteResultPos = savedInstanceState.getInt(TAG_SPINNER_SELECT_CONCRETE, -1);
        }

        mActionTypes = DBCachePoint.getInstance().getActionTypes();
        mActionResultTypes = DBCachePoint.getInstance().getActionResultTypes();
    }

    @Override
    public void onResume() {
        super.onResume();

        setEditTextInput(mComment, mCommentInput);
        setSpinnerSelection(mSpinnerType, mSpinnerTypePos);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mSpinnerType != null) {
            mCommentInput = mComment.getText().toString();
            mSpinnerTypePos = mSpinnerType.getSelectedItemPosition();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mIsOnSaveInstance = true;

        outState.putSerializable(TAG_EVENT, mEvent);
        outState.putBoolean(TAG_PHOTO_WAS_CREATED, mPhotoIsCreating);
        outState.putBoolean(TAG_IS_ADD_COLLECTION, mIsAddCollection);
        outState.putSerializable(TAG_PHOTO_CONTAINER, mPhotoContainer);

        outState.putSerializable(TAG_SPINNER_SELECT_TYPE, mSpinnerType.getSelectedItemPosition());
        outState.putInt(TAG_SPINNER_SELECT_GENERAL,
                mSpinnerGeneralResult.getSelectedItemPosition());
        outState.putInt(TAG_SPINNER_SELECT_CONCRETE,
                mSpinnerConcreteResult.getSelectedItemPosition());
        outState.putString(TAG_COMMENT, mComment.getText().toString());

        if (mPhotoIsCreating &&  mImageFileHelper != null) {
            outState.putString(TAG_PHOTO_CURRENT_PATH, mImageFileHelper.getCurrentPhotoPath());
        }
    }

    protected void initMap() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.post_add_small_map, new MapSmallFragment(), TAG_SMALL_MAP)
                .commit();
    }

    protected void initComment(View view, int wrapperId) {
        View parent = view.findViewById(wrapperId);
        setTextForTextView(parent, R.id.add_input_title, R.string.tv_add_comment);
        mComment = (EditText) parent.findViewById(R.id.add_input_content);
        setHint(mComment, R.string.no_comment);
    }

    protected void initPhotoGrid(View view) {
        GridView gridView = (GridView) view.findViewById(R.id.post_add_photo_grid);
        mAdapter = new PhotoGridAdapter(this, mPhotoContainer);
        gridView.setAdapter(mAdapter);
    }

    protected void initButtons(View view) {
        view.findViewById(R.id.post_button_cancel).setOnClickListener(this);
        view.findViewById(R.id.post_button_accept).setOnClickListener(this);
    }

    protected void initPayDate(View view, int id) {
        View parent = view.findViewById(id);
        setTextForTextView(parent, R.id.add_date_title, R.string.tv_add_pay_date);

        View button = parent.findViewById(R.id.add_date_button);
        mPayDate = (EditText) parent.findViewById(R.id.add_date_content);
        mPayDate.setText(getTodayDate());
        setCalendarDate(mPayDate, button);
    }

    protected final void initType(View view, int wrapperId, int titleTextId) {
        View parent = view.findViewById(wrapperId);
        setTextForTextView(parent, R.id.add_spinner_title, titleTextId);

        mSpinnerType = (Spinner) parent.findViewById(R.id.add_spinner_spinner);
        SpinnerSelector selector = new SpinnerSelector() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateGeneralResultSpinner(getSpinnerValue(mSpinnerType));
            }
        };

        initSpinner(mSpinnerType, getTypes(), getDefaultPosition(), selector);
        mSpinnerTypePos = getDefaultPosition();
    }

    protected final void initGeneralResult(View view, int wrapperId, int titleTextId) {
        View parent = view.findViewById(wrapperId);
        setTextForTextView(parent, R.id.add_spinner_title, titleTextId);

        mSpinnerGeneralResult = (Spinner) parent.findViewById(R.id.add_spinner_spinner);
        SpinnerSelector selector = new SpinnerSelector() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateConcreteResultSpinner(getSpinnerValue(mSpinnerType),
                        mSpinnerGeneralResult.getSelectedItemPosition());
            }
        };

        initSpinner(mSpinnerGeneralResult, null, 0, selector);
    }

    protected final void initConcreteResult(View view, int wrapperId, int titleTextId) {
        View parent = view.findViewById(wrapperId);
        setTextForTextView(parent, R.id.add_spinner_title, titleTextId);

        mSpinnerConcreteResult = (Spinner) parent.findViewById(R.id.add_spinner_spinner);
        initSpinner(mSpinnerConcreteResult, null, 0, null);
    }

    protected void updateGeneralResultSpinner(int typeId) {
        List<SpinnerItem> spinnerItemList = getGeneralResultByTypeId(typeId);
        mSpinnerGeneralResult.setAdapter(getArrayAdapter(spinnerItemList));

        setSpinnerSelection(mSpinnerGeneralResult, mSpinnerGeneralResultPos);
    }

    protected void updateConcreteResultSpinner(int typeId, int resultId) {
        List<SpinnerItem> spinnerItemList =
                getConcreteResultByTypeAndResultIds(typeId, resultId);
        mSpinnerConcreteResult.setAdapter(getArrayAdapter(spinnerItemList));

        setSpinnerSelection(mSpinnerConcreteResult, mSpinnerConcreteResultPos);
    }

    protected void setSpinnerSelection(Spinner spinner, int position) {
        if (spinner != null && position != -1 && spinner.getCount() > position) {
            spinner.setSelection(position);
        }
    }

    protected void setEditTextInput(EditText editText, String input) {
        if (editText != null && input != null) {
            editText.setText(input);
        }
    }

    public void setEvent(DBEvent event) {
        mEvent = event;
    }

    public DBEvent getEvent() {
        return mEvent;
    }

    @NonNull
    public List<SpinnerItem> getTypes() {
        List<SpinnerItem> types = new ArrayList<>(mActionTypes.size());

        mDefaultPosition = 0;
        int i = 0;

        for (DBActionType actionType : mActionTypes) {
            types.add(new SpinnerItem(actionType.getId(), actionType.getName()));

            if (mEvent != null && mEvent.getType() == actionType.getId()) {
                mDefaultPosition = i;
            }

            i++;
        }

        if (mSpinnerTypePos != -1) {
            mDefaultPosition = mSpinnerTypePos;
        }

        return types;
    }

    public int getDefaultPosition() {
        return mDefaultPosition;
    }

    public DBActionResultType getResultForType(int typeId) {
        DBActionResultType actionResultType = null;

        for (DBActionResultType actionResultTypeIterator : mActionResultTypes) {

            if (actionResultTypeIterator.getActionTypeId() == typeId) {
                actionResultType = actionResultTypeIterator;
                break;
            }
        }

        return actionResultType;
    }

    private List<StatusResult> getStatusResultListByTypeId(int typeId) {
        return getResultForType(typeId).getResultsByStatus();
    }

    public List<SpinnerItem> getGeneralResultByTypeId(int typeId) {
        final List<StatusResult> statusResults = getStatusResultListByTypeId(typeId);

        final List<SpinnerItem> generalResults = new ArrayList<>(statusResults.size());
        for (int i = 0; i < statusResults.size(); i++) {
            StatusResult item = statusResults.get(i);
            generalResults.add(new SpinnerItem(item.getStatId(), item.getStatusName()));
        }

        return generalResults;
    }

    public List<SpinnerItem> getConcreteResultByTypeAndResultIds(int typeId, int resultId) {
        final List<ConcreteResult> concreteResults = getStatusResultListByTypeId(typeId)
                .get(resultId).getConcreteResults();
        final List<SpinnerItem> results = new ArrayList<>(concreteResults.size());
        for (ConcreteResult concreteResult : concreteResults) {
            results.add(new SpinnerItem(concreteResult.getId(), concreteResult.getName()));
        }

        return results;
    }

    protected void initSpinner(Spinner spinnerView, List<SpinnerItem> list, int defaultPosition,
                            SpinnerSelector spinnerSelector) {

        if (spinnerView != null) {
            if (list != null) {
                spinnerView.setAdapter(getArrayAdapter(list));
            }

            spinnerView.setSelection(defaultPosition);
            spinnerView.setOnItemSelectedListener(spinnerSelector);
        }
    }

    @Nullable
    protected EditText initEditText(View rootView, int idWrapper, int idTitle, int idHint) {
        EditText editText = null;

        if (rootView != null) {
            View layAddTitle = rootView.findViewById(idWrapper);
            setTextForTextView(layAddTitle, R.id.add_input_title, idTitle);

            editText = (EditText) layAddTitle.findViewById(R.id.add_input_content);
            setHint(editText, idHint);
        }

        return editText;
    }

    public void setTextForTextView(View view, int textViewId, int stringId) {
        setTextForTextView(view, textViewId, getString(stringId));
    }

    public void setTextForTextView(View view, int textViewId, String string) {
        if (view != null) {
            TextView textView = (TextView) view.findViewById(textViewId);

            if (textView != null) {
                textView.setText(string);
            }
        }
    }

    public ArrayAdapter<String> getArrayAdapter(String[] stringArray) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                R.layout.view_add_spinner_dropdown_item, stringArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }


    public ArrayAdapter<CharSequence> getArrayAdapter(int textArrayResId) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                textArrayResId, R.layout.view_add_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }

    public ArrayAdapter<SpinnerItem> getArrayAdapter(List<SpinnerItem> list) {
        final ArrayAdapter<SpinnerItem> adapter = new ArrayAdapter<>(getActivity(),
                R.layout.view_add_spinner_dropdown_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }

    protected void setDefaultSumHint(EditText editText) {
        setHint(editText, R.string.zero_sum_hint);
    }

    protected void setHint(EditText editText, int resId) {
        if (editText != null) {
            editText.setHint(resId);
        }
    }

    public String getTodayDate() {
        return DateUtils.formatDate(new Date());
    }

    public void setCalendarDate(final EditText editText, View button) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateSetListener =
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        editText.setText(DateUtils.formatDate(calendar.getTime()));
                    }
                };

        editText.setOnClickListener(getDataPickerOnClickListener(calendar, dateSetListener));
        button.setOnClickListener(getDataPickerOnClickListener(calendar, dateSetListener));
    }

    private View.OnClickListener getDataPickerOnClickListener(final Calendar calendar,
                                      final DatePickerDialog.OnDateSetListener dateSetListener) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = calendar;
                if (v instanceof EditText) {
                    cal = DateUtils.dateAsStringToCalendar(((EditText) v).getText().toString());
                }

                new DatePickerDialog(getActivity(), dateSetListener, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        };
    }

    protected final int getSpinnerValue(Spinner spinner) {
        return ((SpinnerItem) spinner.getSelectedItem()).getId();
    }

    private boolean checkInputData() {
        return isPhotoIsRequire() ? !mPhotoContainer.isEmpty() : true;
    }

    @Override
    public void onClick(View v) {
        Utils.closeKeyboard(v);

        switch (v.getId()) {
            case R.id.post_button_accept:
                if (checkInputData()) {
                    mIsAddCollection = true;
                    sendPost();
                    makeToast(R.string.toast_adding_collection);
                    getMainActivity().onBackPressed();
                } else {
                    makeToastLong(R.string.msg_no_photo);
                }
                break;
            case R.id.post_button_cancel:
                getMainActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        initFileHelperIfNeed();
        mPhotoIsCreating = false;

        if (mImageFileHelper.isResultTrue(requestCode, resultCode)) {
            final boolean isCamera;
            if (data == null) {
                isCamera = true;
            } else {
                final String action = data.getAction();
                if (action == null) {
                    isCamera = false;
                } else {
                    isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                }
            }

            final String currentPath = mImageFileHelper.getCurrentPhotoPath();

            if (!isCamera) {
                Uri selectedImageUri = data == null ? null : data.getData();
                if (!ImageFileHelper.copyFile(getActivity(), selectedImageUri, currentPath)) {
                    mImageFileHelper.removeImageFile(currentPath);
                }
            }

            LatLng latLng = LocationHelper.getInstance().getLatLng();
            mAdapter.addPhoto(currentPath, latLng.latitude, latLng.longitude);
        }

        mImageFileHelper = null;
    }

    @Override
    public void makePhoto() {
        if (LocationHelper.getInstance().wasFound()
                && LocationHelper.getInstance().isNotOld()) {
            if (!mPhotoIsCreating) {
                mPhotoIsCreating = true;
                initFileHelperIfNeed();
            }

            mImageFileHelper.makePhotoIntent(this);
        } else {
            makeToastLong(R.string.msg_no_location);
        }
    }

    @Override
    public void onDetach() {
        if (!mIsOnSaveInstance && !mIsAddCollection) {
            mPhotoContainer.removeAllPhoto();
        }

        super.onDetach();
    }

    private void initFileHelperIfNeed() {
        if (mImageFileHelper == null) {
            mImageFileHelper = new ImageFileHelper();

            if (mPhotoCurrentPath != null) {
                mImageFileHelper.setCurrentPhotoPath(mPhotoCurrentPath);
                mPhotoCurrentPath = null;
            }
        }
    }

    protected abstract void sendPost();

    protected boolean isPhotoIsRequire() {
        return false;
    }

}
