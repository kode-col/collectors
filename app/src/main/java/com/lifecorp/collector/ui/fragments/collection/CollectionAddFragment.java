package com.lifecorp.collector.ui.fragments.collection;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.model.objects.Money;
import com.lifecorp.collector.model.objects.collection.Collection;
import com.lifecorp.collector.ui.fragments.BasePostFragment;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.DateUtils;
import com.lifecorp.collector.utils.spinner.SpinnerItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Alexander Smirnov on 21.04.15.
 * <p/>
 * Фрагмент добавления новой Collection
 */
public class CollectionAddFragment extends BasePostFragment {

    private static final String TAG_SPINNER_SELECT_TERRITORY = "TAG_SPINNER_SELECT_TERRITORY";
    private static final String TAG_SPINNER_SELECT_CONTRACT = "TAG_SPINNER_SELECT_CONTRACT";
    private static final String TAG_SPINNER_SELECT_CURRENCY = "TAG_SPINNER_SELECT_CURRENCY";
    private static final String TAG_SPINNER_SELECT_ADDRESS = "TAG_SPINNER_SELECT_ADDRESS";
    private static final String TAG_SPINNER_SELECT_STATUS = "TAG_SPINNER_SELECT_STATUS";
    private static final String TAG_ADDRESS_OTHER = "TAG_ADDRESS_OTHER";
    private static final String TAG_THEME = "TAG_THEME";
    private static final String TAG_DATE = "TAG_DATE";

    private DBDebtorProfile mDBDebtorProfile;
    private String mAddressOtherInput;
    private String mContractId;
    private String mThemeInput;
    private String mDateInput;

    private int mStatusInput = -1;
    private int mAddressInput = -1;
    private int mContractInput = -1;
    private int mCurrencyInput = -1;
    private int mTerritoryInput = -1;

    private EditText mDate;
    private EditText mTheme;
    private EditText mAddressOther;

    private Spinner mSpinnerStatus;
    private Spinner mSpinnerAddress;
    private Spinner mSpinnerContract;
    private Spinner mSpinnerTerritory;
    private Spinner mSpinnerSumCurrency;

    public static CollectionAddFragment newInstance(DBDebtorProfile debtorProfile,
                                                    String contractId) {
        CollectionAddFragment fragment = new CollectionAddFragment();
        fragment.setDebtorProfile(debtorProfile);
        fragment.setContractId(contractId);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        if (bundle != null) {
            setDebtorProfile((DBDebtorProfile) bundle.getSerializable(Const.TAG_DEBTOR));
            setContractId(bundle.getString(Const.TAG_CREDIT_ID));

            mDateInput = bundle.getString(TAG_DATE);
            mThemeInput = bundle.getString(TAG_THEME);
            mAddressOtherInput = bundle.getString(TAG_ADDRESS_OTHER);

            mStatusInput = bundle.getInt(TAG_SPINNER_SELECT_STATUS, -1);
            mAddressInput = bundle.getInt(TAG_SPINNER_SELECT_ADDRESS, -1);
            mContractInput = bundle.getInt(TAG_SPINNER_SELECT_CONTRACT, -1);
            mCurrencyInput = bundle.getInt(TAG_SPINNER_SELECT_CURRENCY, -1);
            mTerritoryInput = bundle.getInt(TAG_SPINNER_SELECT_TERRITORY, -1);
        }

        View view = inflater.inflate(R.layout.fragment_collection_new, container, false);
        initInterface(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        setEditTextInput(mDate, mDateInput);
        setEditTextInput(mTheme, mThemeInput);
        setEditTextInput(mAddressOther, mAddressOtherInput);

        setSpinnerSelection(mSpinnerStatus, mStatusInput);
        setSpinnerSelection(mSpinnerAddress, mAddressInput);
        setSpinnerSelection(mSpinnerContract, mContractInput);
        setSpinnerSelection(mSpinnerTerritory, mTerritoryInput);
        setSpinnerSelection(mSpinnerSumCurrency, mCurrencyInput);
    }

    @Override
    public void onPause() {
        super.onPause();

        mDateInput = mDate.getText().toString();
        mThemeInput = mTheme.getText().toString();
        mAddressOtherInput = mAddressOther.getText().toString();

        mStatusInput = mSpinnerStatus.getSelectedItemPosition();
        mAddressInput = mSpinnerAddress.getSelectedItemPosition();
        mContractInput = mSpinnerContract.getSelectedItemPosition();
        mTerritoryInput = mSpinnerTerritory.getSelectedItemPosition();
        mCurrencyInput = mSpinnerSumCurrency.getSelectedItemPosition();
    }

    private void initInterface(View view) {
        initMap();
        initSum(view);
        initType(view);
        initDate(view);
        initTheme(view);
        initDebtor(view);
        initStatus(view);
        initGeneralResult(view);
        initAddress(view);
        initButtons(view);
        initComment(view);
        initPayDate(view);
        initContract(view);
        initPhotoGrid(view);
        initTerritory(view);
        initResultPayment(view);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(Const.TAG_DEBTOR, mDBDebtorProfile);
        outState.putString(Const.TAG_CREDIT_ID, mContractId);

        outState.putString(TAG_DATE, mDate.getText().toString());
        outState.putString(TAG_THEME, mTheme.getText().toString());
        outState.putString(TAG_ADDRESS_OTHER, mAddressOther.getText().toString());

        outState.putInt(TAG_SPINNER_SELECT_STATUS, mSpinnerStatus.getSelectedItemPosition());
        outState.putInt(TAG_SPINNER_SELECT_ADDRESS, mSpinnerAddress.getSelectedItemPosition());
        outState.putInt(TAG_SPINNER_SELECT_CONTRACT, mSpinnerContract.getSelectedItemPosition());
        outState.putInt(TAG_SPINNER_SELECT_TERRITORY, mSpinnerTerritory.getSelectedItemPosition());
        outState.putInt(TAG_SPINNER_SELECT_CURRENCY, mSpinnerSumCurrency.getSelectedItemPosition());
    }

    public void setDebtorProfile(DBDebtorProfile dbDebtorProfile) {
        mDBDebtorProfile = dbDebtorProfile;
    }

    public void setContractId(String contractId) {
        mContractId = contractId;
    }

    @NonNull
    @Override
    public List<SpinnerItem> getTypes() {
        final int count = 2;

        List<SpinnerItem> typesList = super.getTypes();
        List<SpinnerItem> resultList = new ArrayList<>(count);

        if (typesList.size() >= count) {
            for (int i = 0; i < count; i++) {
                resultList.add(typesList.get(i));
            }
        }

        return resultList;
    }

    private void initType(View view) {
        initType(view, R.id.layAddType, R.string.tv_add_type);
    }

    private void initDebtor(View view) {
        View layAddClient = view.findViewById(R.id.layAddClient);
        setTextForTextView(layAddClient, R.id.add_text_title, R.string.tv_add_client);

        if (mDBDebtorProfile != null) {
            setTextForTextView(layAddClient, R.id.add_text_content, mDBDebtorProfile.getFioShort());
        }
    }

    private void initDate(View view) {
        View layAddDate = view.findViewById(R.id.layAddDate);
        TextView title = (TextView) layAddDate.findViewById(R.id.add_date_title);
        title.setText(R.string.tv_add_date);

        View button = layAddDate.findViewById(R.id.add_date_button);
        mDate = (EditText) layAddDate.findViewById(R.id.add_date_content);
        mDate.setText(getTodayDate());
        mDate.setEnabled(false); // TODO добавить выключенный стиль
        // setCalendarDate(mDate, button);
    }

    private void initPayDate(View view) {
        initPayDate(view, R.id.layAddPayDate);
    }

    private void initTerritory(View view) {
        View layAddTerritory = view.findViewById(R.id.layAddTerritory);
        setTextForTextView(layAddTerritory, R.id.add_spinner_title, R.string.tv_add_territory);

        mSpinnerTerritory = (Spinner) layAddTerritory.findViewById(R.id.add_spinner_spinner);
        mSpinnerTerritory.setAdapter(getArrayAdapter(R.array.spinner_territory));

        mSpinnerTerritory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSpinnerAddress.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
                mAddressOther.setVisibility(position == 0 ? View.GONE : View.VISIBLE);

                if (position == 0) {
                    mSpinnerAddress.setAdapter(getArrayAdapter(getAddresses()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void initAddress(View view) {
        View layAddAddress = view.findViewById(R.id.layAddAddress);
        setTextForTextView(layAddAddress, R.id.add_address_title, R.string.tv_add_address);

        mAddressOther = (EditText) layAddAddress.findViewById(R.id.add_address_other);
        mSpinnerAddress = (Spinner) layAddAddress.findViewById(R.id.add_address_spinner);
    }

    private void initStatus(View view) {
        View layAddStatus = view.findViewById(R.id.layAddStatus);
        setTextForTextView(layAddStatus, R.id.add_spinner_title, R.string.tv_add_status);

        ArrayAdapter<CharSequence> adapterStatus = ArrayAdapter.createFromResource(getActivity(),
                R.array.spinner_status, android.R.layout.simple_spinner_item);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSpinnerStatus = (Spinner) layAddStatus.findViewById(R.id.add_spinner_spinner);
        mSpinnerStatus.setAdapter(adapterStatus);
    }

    private void initComment(View view) {
        initComment(view, R.id.complete_event_comment);
    }

    private void initGeneralResult(View view) {
        initGeneralResult(view, R.id.layAddResult, R.string.tv_add_result);
    }

    private void initResultPayment(View view) {
        initConcreteResult(view, R.id.layAddAction, R.string.tv_add_payment);
    }

    private void initSum(View view) {
        View layAddSum = view.findViewById(R.id.layAddSum);
        setTextForTextView(layAddSum, R.id.add_input_sum_title, R.string.tv_add_sum);

        mSumInput = (EditText) layAddSum.findViewById(R.id.add_input_sum_input);
        setDefaultSumHint(mSumInput);

        mSpinnerSumCurrency = (Spinner) layAddSum.findViewById(R.id.add_input_sum_currency);
        mSpinnerSumCurrency.setAdapter(getArrayAdapter(R.array.spinner_sum));
    }

    private void initContract(View view) {
        View layAddContract = view.findViewById(R.id.layAddContract);
        setTextForTextView(layAddContract, R.id.add_spinner_title, R.string.tv_add_contract);

        mSpinnerContract = (Spinner) layAddContract.findViewById(R.id.add_spinner_spinner);
        mSpinnerContract.setAdapter(getArrayAdapter(getContracts()));
    }

    private void initTheme(View view) {
        mTheme = initEditText(view, R.id.layAddTheme, R.string.tv_add_theme, R.string.no_desc);
    }

    private String[] getAddresses() {
        List<String> addressList = new ArrayList<>();

        if (mDBDebtorProfile != null && mDBDebtorProfile.getSections() != null) {
            for (DBSection section : mDBDebtorProfile.getSections()) {

                if (section.getType() == DBSection.TYPE_ADDRESS && section.getFields() != null) {
                    for (DBSectionField field : section.getFields()) {

                        if (field != null && field.getValue() != null) {
                            addressList.add(field.getValue());
                        }

                    }
                }
            }
        }

        return addressList.toArray(new String[addressList.size()]);
    }

    @NonNull
    private List<DBSectionField> getContractsDBSectionField() {
        Set<DBSectionField> contractsSection1 = new HashSet<>();
        Set<DBSectionField> contractsSection2 = new HashSet<>();

        if (mDBDebtorProfile != null && mDBDebtorProfile.getSections() != null) {
            for (DBSection section : mDBDebtorProfile.getSections()) {

                if (section != null
                        && section.getType() == DBSection.TYPE_CREDIT_INFO
                        && section.getFields() != null) {
                    for (DBSectionField field : section.getFields()) {

                        if (field != null && field.getId().equals("id")) {
                            if (mContractId != null && mContractId.equals(section.getId())) {
                                contractsSection1.add(field);
                            } else {
                                contractsSection2.add(field);
                            }
                        }

                    }
                }

            }
        }

        List<DBSectionField> contractList = new ArrayList<>();
        contractList.addAll(contractsSection1);
        contractList.addAll(contractsSection2);
        return contractList;
    }

    @NonNull
    private String[] getContracts() {
        List<DBSectionField> sectionFields = getContractsDBSectionField();
        List<String> contractList = new ArrayList<>();

        for (DBSectionField field : sectionFields) {
            if (field != null) {
                contractList.add(field.getValue());
            }
        }

        return contractList.toArray(new String[contractList.size()]);
    }

    @NonNull
    private String getContractId(int position) {
        final List<DBSectionField> fieldsList = getContractsDBSectionField();

        return fieldsList.size() > position ? fieldsList.get(position).getDbSection().getId() : "";
    }

    @NonNull
    private Collection getAddingCollection() {
        Collection tempCollection = new Collection();

        tempCollection.setType(getSpinnerValue(mSpinnerType));
        long date = DateUtils.timestampStringToTimestampLong(mDate.getText().toString());
        tempCollection.setDate(DateUtils.fixDateIfNeed(date));

        if (mDBDebtorProfile != null) {
            tempCollection.setDebtorId(mDBDebtorProfile.getId());
        }

        int territoryType = mSpinnerTerritory.getSelectedItemPosition();
        if (territoryType == 0) {
            String[] addressesStr = getAddresses();
            int pos = mSpinnerAddress.getSelectedItemPosition();
            tempCollection.setAddress(Arrays.asList(addressesStr[pos]));
        } else {
            String[] address = new String[]{mAddressOther.getText().toString()};
            tempCollection.setAddress(Arrays.asList(address));
        }

        String comment = mComment.getText().toString();
        tempCollection.setComment(comment.isEmpty() ? getString(R.string.no_comment) : comment);

        tempCollection.setStatus(mSpinnerStatus.getSelectedItemPosition());
        tempCollection.setSummary(mSpinnerGeneralResult.getSelectedItem().toString());
        tempCollection.setResult(mSpinnerConcreteResult.getSelectedItem().toString());

        String amountStr = mSumInput.getText().toString();
        if (amountStr.isEmpty()) {
            amountStr = getString(R.string.zero_sum_hint);
        }

        Money money = new Money();
        money.setAmount(Float.parseFloat(amountStr));
        money.setCurrency(mSpinnerSumCurrency.getSelectedItem().toString());
        tempCollection.setSum(money);

        long payDate = DateUtils.timestampStringToTimestampLong(mPayDate.getText().toString());
        tempCollection.setPayDate(payDate);
        tempCollection.setContractId(getContractId(mSpinnerContract.getSelectedItemPosition()));

        String thm = mTheme.getText().toString();
        tempCollection.setTitle(thm.isEmpty() ? mSpinnerType.getSelectedItem().toString() : thm);
        tempCollection.setPhotoContainerList(mPhotoContainer.getDataList());

        return tempCollection;
    }


    @Override
    protected void sendPost() {
        getMainActivity().postCollection(getAddingCollection());
    }

}
