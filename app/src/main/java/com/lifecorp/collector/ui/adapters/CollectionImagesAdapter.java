package com.lifecorp.collector.ui.adapters;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBCollectionImageId;
import com.lifecorp.collector.network.NetworkConstants;
import com.lifecorp.collector.ui.activities.MainActivity;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderPhotoSmall;
import com.lifecorp.collector.utils.MyImageLoaderListener;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Alexander Smirnov on 05.05.15.
 *
 * Адаптер для отображения изображений присоединённых к коллекции
 */
public class CollectionImagesAdapter extends BaseAdapter {

    private MainActivity mMainActivity;
    private List<DBCollectionImageId> mDataList;

    public CollectionImagesAdapter(MainActivity mainActivity,
                                   Collection<DBCollectionImageId> imagesList) {
        mMainActivity = mainActivity;
        mDataList = new ArrayList<>(imagesList);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Nullable
    @Override
    public DBCollectionImageId getItem(int position) {
        return getCount() > position ? mDataList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderPhotoSmall holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_small_photo_grid_item, parent, false);

            holder = new ViewHolderPhotoSmall();
            holder.photo = (ImageView) convertView;
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderPhotoSmall) convertView.getTag();
        }

        DBCollectionImageId item = getItem(position);

        if (item != null && item.getDbImageId() != null) {
            ImageLoader.getInstance().displayImage(buildPhotoUrl(item.getDbImageId().getImageId()),
                    holder.photo, new MyImageLoaderListener(mMainActivity));
        } else {
            holder.photo.setImageResource(R.drawable.ico_spisok_no_photo);
        }

        return convertView;
    }

    private String buildPhotoUrl(String id) {
        return String.format(NetworkConstants.SERVER_PATTERN_IMAGES_PHOTO,
                NetworkConstants.SERVER_ADDRESS, id, PreferencesStorage.getInstance().getToken());
    }

}
