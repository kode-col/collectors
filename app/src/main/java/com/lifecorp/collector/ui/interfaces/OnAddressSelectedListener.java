package com.lifecorp.collector.ui.interfaces;

import com.lifecorp.collector.model.objects.address.AddressContainer;

/**
 * Created by Alexander Smirnov on 09.04.15.
 *
 * Говорит что адрес был выбран
 */
public interface OnAddressSelectedListener {

    void onAddressSelected(AddressContainer addressContainer);

}
