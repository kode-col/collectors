package com.lifecorp.collector.ui.enums;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Alexander Smirnov on 14.04.15.
 *
 * Перечисление списка шрифтов доступных в приложении
 */
public enum RobotoFont {
    BLACK("roboto_black"),
    BLACK_ITALIC("roboto_black_italic"),
    BOLD("roboto_bold"),
    BOLD_ITALIC("roboto_bold_italic"),
    ITALIC("roboto_italic"),
    LIGHT("roboto_light"),
    LIGHT_ITALIC("roboto_light_italic"),
    MEDIUM("roboto_medium"),
    MEDIUM_ITALIC("roboto_medium_italic"),
    REGULAR("roboto_regular"),
    THIN("roboto_thin"),
    THIN_ITALIC("roboto_thin_italic");

    private String mFontName;

    private RobotoFont(@NonNull String fontName) {
        mFontName = fontName + ".ttf";
    }

    private String getFontName() {
        return mFontName;
    }

    @Nullable
    public static String getFontName(RobotoFont robotoFont) {
        return robotoFont != null ? robotoFont.getFontName() : null;
    }
}
