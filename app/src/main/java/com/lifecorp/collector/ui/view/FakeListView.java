package com.lifecorp.collector.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.lifecorp.collector.R;

/**
 * Created by Alexander Smirnov on 30.03.15.
 *
 * ScrollView содержащий LinearLayout заменяет ListView. Изначально был необходим для избежания
 * ситуации реализации List в List-t, после изменения требований т.к. подходил лучше имеющихся
 * реализаций ExpandableListView.
 */
public class FakeListView extends ScrollView {

    private BaseAdapter mAdapter;
    private LinearLayout mLinearLayout;

    public FakeListView(Context context) {
        super(context);
    }

    public FakeListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FakeListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setAdapter(BaseAdapter adapter) {
        mAdapter = adapter;
    }

    public void drawView() {
        removeAllViews();
        if (mLinearLayout == null) {
            mLinearLayout = createLinearLayout();
        }

        mLinearLayout.removeAllViews();
        addView(mLinearLayout);

        if (mAdapter != null) {
            final int size = mAdapter.getCount();

            for (int i = 0; i < size; i++) {
                if (mLinearLayout.getChildCount() != 0) {
                    mLinearLayout.addView(createHorizontalDivider());
                }

                mLinearLayout.addView(mAdapter.getView(i, null, this));
            }
        }
    }

    private LinearLayout createLinearLayout() {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(lp);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        return linearLayout;
    }

    private View createHorizontalDivider() {
        return LayoutInflater.from(getContext())
                .inflate(R.layout.view_divider_horizontal_gray, this, false);
    }

}
