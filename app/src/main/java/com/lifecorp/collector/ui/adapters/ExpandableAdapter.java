package com.lifecorp.collector.ui.adapters;

import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderTitle;
import com.lifecorp.collector.ui.interfaces.OnToggleListener;
import com.lifecorp.collector.ui.view.ExpandableView;

import java.util.List;

/**
 * Created by Alexander Smirnov on 20.04.15.
 *
 * Базовый абстрактный выпадающий адаптер
 */
public abstract class ExpandableAdapter<T> extends BaseAdapter implements OnToggleListener {

    protected List<T> mData;
    protected SparseArray<Boolean> mExpandIsOpen = new SparseArray<>();

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public T getItem(int position) {
        return position < getCount() ? mData.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        final View title = getTitleView(pos, null, parent);
        final View content = getContentView(pos, null, parent);

        ExpandableView view = new ExpandableView(parent.getContext(), title, content, this, pos);

        if (isPositionOpen(pos)) {
            view.open();
        }

        return view;
    }

    protected abstract View getContentView(int position, View convertView, ViewGroup parent);

    protected abstract View getTitleView(final int position, View convertView, ViewGroup parent);

    protected boolean isPositionOpen(int position) {
        Boolean result = mExpandIsOpen.get(position);
        return result == null ? false : mExpandIsOpen.get(position);
    }

    protected void rotateImmediately(View view) {
        if (view != null) {
            Animation animation = getRotateAnimation(true);
            animation.setDuration(0);
            view.startAnimation(animation);
        }
    }

    protected Animation getRotateAnimation(boolean isExpanded) {
        return AnimationUtils.loadAnimation(App.getContext(), isExpanded
                ? R.anim.rotate_90
                : R.anim.rotate_90_back);
    }

    @Override
    public void onToggle(View view, int position, boolean isExpanded) {
        if (view != null) {
            Object object = view.getTag();

            if (object != null && object instanceof ViewHolderTitle) {
                ViewHolderTitle holder = (ViewHolderTitle) object;
                holder.expIcon.startAnimation(getRotateAnimation(isExpanded));
            }

            mExpandIsOpen.append(position, isExpanded);
        }
    }

}
