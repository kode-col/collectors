package com.lifecorp.collector.ui.adapters.holder;

import android.widget.ImageView;

/**
 * Created by Alexander Smirnov on 05.05.15.
 *
 * ViewHolder для маленьких изображений
 */
public class ViewHolderPhotoSmall {

    public ImageView photo;

}
