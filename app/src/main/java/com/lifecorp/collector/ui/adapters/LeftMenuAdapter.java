package com.lifecorp.collector.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.R;

public class LeftMenuAdapter extends BaseAdapter {

    private int[] ivItems = new int[]{
            R.drawable.ico_menu_client,
            R.drawable.ico_menu_plan,
            R.drawable.ico_menu_map
    };

    private int[] tvItems = new int[]{
            R.string.menu_debtors,
            R.string.menu_events,
            R.string.menu_map
    };

    private int mCurrentItem = -1;

    public void setCurrentItem(int selectedItem) {
        mCurrentItem = selectedItem;
    }

    @Override
    public int getCount() {
        return ivItems.length;
    }

    @Override
    public Object getItem(int position) {
        return ivItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_list_item_left, parent, false);
        }

        convertView.setBackgroundResource(mCurrentItem == position
                ? R.drawable.nav_on
                : R.drawable.nav_off);

        TextView tvLeftItem = (TextView) convertView.findViewById(R.id.tvLeftItem);
        ImageView ivLeftItem = (ImageView) convertView.findViewById(R.id.ivLeftItem);

        tvLeftItem.setText(tvItems[position]);
        ivLeftItem.setImageResource(ivItems[position]);

        return convertView;
    }

}
