package com.lifecorp.collector.ui.adapters.collection;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.ui.fragments.collection.CollectionHistoryDefaultFragment;
import com.lifecorp.collector.ui.fragments.collection.CollectionHistoryExpiredFragment;
import com.lifecorp.collector.ui.fragments.collection.CollectionHistoryFragment;
import com.lifecorp.collector.ui.interfaces.OnCollectorFragmentChangeListener;
import com.lifecorp.collector.ui.view.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 17.04.15.
 *
 * Адаптер для {@link com.lifecorp.collector.ui.fragments.collection.CollectionHistoryWrapperFragment}
 */
public class CollectionHistoryPagerAdapter extends FragmentStatePagerAdapter implements
        PagerSlidingTabStrip.CustomTabProvider {

    private int mDebtorId;
    private String mCreditId;
    private String[] mTitles;
    private final OnCollectorFragmentChangeListener mFragmentChangeListener;

    public CollectionHistoryPagerAdapter(FragmentManager fm, int debtorId, String creditId,
                                         OnCollectorFragmentChangeListener fragmentChangeListener) {
        super(fm);

        mDebtorId = debtorId;
        mCreditId = creditId;
        mFragmentChangeListener = fragmentChangeListener;

        mTitles = App.getContext().getResources().getStringArray(R.array.collection_titles);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                CollectionHistoryFragment historyFragment =
                        CollectionHistoryFragment.newInstance(mDebtorId, mCreditId);
                if (mFragmentChangeListener != null) {
                    mFragmentChangeListener.onCollectionHistoryFragmentChanged(historyFragment);
                }
                return historyFragment;
            case 1:
                CollectionHistoryExpiredFragment expiredFragment =
                        CollectionHistoryExpiredFragment.newInstance(mDebtorId);
                if (mFragmentChangeListener != null) {
                    mFragmentChangeListener.onCollectionExpiredFragmentChanged(expiredFragment);
                }
                return expiredFragment;
            default:
                return new CollectionHistoryDefaultFragment();
        }
    }



    @Override
    public CharSequence getPageTitle(int position) {
        String title = mTitles[position];

        if (position == 0) {
            String contract = getContract();

            if (contract != null) {
                title = title + " (" + contract + ")";
            }
        }

        return title;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount(int position) {
        if (position == 1) {
            return countEvents();
        }
        return 0;
    }

    @Override
    public boolean isCustom(int position) {
        return position == 1;
    }

    private int countEvents() {
        List<DBEvent> eventsAll = DBCachePoint.getInstance().getAllEvents();
        long now = System.currentTimeMillis();

        List<DBEvent> eventsTemp = new ArrayList<>();
        if (!eventsAll.isEmpty()) {
            for (DBEvent dbe : eventsAll) {
                if (dbe.getDebtorProfile() != null
                        && dbe.getDebtorProfile().getId() == mDebtorId
                        && !dbe.isCompleted() && dbe.getTime() < now) {
                    eventsTemp.add(dbe);
                }
            }
        }
        return eventsTemp.size();
    }

    @Nullable
    private String getContract() {
        String creditName = null;
        DBDebtorProfile debtorProfile = DBCachePoint.getInstance().findDebtorById(mDebtorId);

        if (debtorProfile != null && debtorProfile.getSections() != null && mCreditId != null) {
            List<DBSection> sections = new ArrayList<>(debtorProfile.getSections());

            mainLoop:
            for (DBSection section : sections) {
                if (section.getType() == DBSection.TYPE_CREDIT_INFO
                        && mCreditId.equals(section.getId())) {
                    List<DBSectionField> fields = new ArrayList<>(section.getFields());

                    for (DBSectionField field : fields) {
                        if (field.getId().equals("productName")) {
                            creditName = field.getValue();
                            break mainLoop;
                        }
                    }
                }
            }
        }

        return creditName;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        // Ничего не делать и не вызывать super.restoreState()
    }

}
