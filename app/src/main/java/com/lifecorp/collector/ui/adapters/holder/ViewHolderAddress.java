package com.lifecorp.collector.ui.adapters.holder;

import android.widget.TextView;

/**
 * Created by Alexander Smirnov on 08.04.15.
 *
 * ViewHolder для {@link com.lifecorp.collector.ui.adapters.debtor.DebtorAddressAdapter}
 */
public class ViewHolderAddress {
    public TextView fio;
    public TextView address;
    public TextView creditInfo;
    public TextView addressDescription;
}
