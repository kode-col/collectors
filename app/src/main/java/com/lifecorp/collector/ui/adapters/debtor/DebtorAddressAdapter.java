package com.lifecorp.collector.ui.adapters.debtor;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.objects.address.AddressContainer;
import com.lifecorp.collector.model.objects.address.AddressGroupContainer;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderAddress;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Alexander Smirnov on 08.04.15.
 *
 * Адаптер выводит адреса должника и поручителей
 */
public class DebtorAddressAdapter extends ArrayAdapter<AddressGroupContainer>
        implements StickyListHeadersAdapter{

    private int mSize;
    private int mSelectedId = -1;
    private final int mRedColor;
    private final int mBlackColor;
    private final int mWhiteColor;
    private final int mBlackBackgoundColor;

    private SparseArray<Integer> mCacheItemsSize = new SparseArray<>();

    public DebtorAddressAdapter(Context context, List<AddressGroupContainer> objects) {
        super(context, 0, objects);

        Resources res = context.getResources();

        mRedColor = res.getColor(R.color.red);
        mWhiteColor = res.getColor(R.color.white);
        mBlackColor = res.getColor(R.color.black);
        mBlackBackgoundColor = res.getColor(R.color.black_5);

        updateSizeCache();
    }

    public void setSelectedId(int selectedId) {
        mSelectedId = selectedId;
    }

    public int getSelectedId() {
        return mSelectedId;
    }

    private void updateSizeCache() {
        mSize = 0;
        mCacheItemsSize.clear();

        final int count = super.getCount();
        for (int position = 0; position < count; position++) {
            AddressGroupContainer addressContainer = getItem(position);
            mSize += addressContainer.getAddress().size();
            mCacheItemsSize.put(position, mSize);
        }
    }

    public void setSelectedIdByAddressContainer(AddressContainer addressContainer) {
        if (addressContainer != null) {
            int i = 0;
            int position = -1;

            for (int j = 0; j < super.getCount(); j++) {
                AddressGroupContainer addressGroupContainer = getItemContainer(j);

                if (addressGroupContainer != null) {
                    for (AddressContainer item : addressGroupContainer.getAddress()) {
                        if (addressContainer.isEqual(item)) {
                            position = i;
                            break;
                        }

                        i++;
                    }
                }
            }

            if (position != -1) {
                setSelectedId(position);
            }
        }
    }

    public int getGroupPosition(int position) {
        int groupPosition = -1;

        if (position >= 0 && position < mSize) {
            final int count = mCacheItemsSize.size();
            for (int cyclePosition = 0; cyclePosition < count; cyclePosition++) {
                Integer size = mCacheItemsSize.valueAt(cyclePosition);

                if (position < size) {
                    groupPosition = mCacheItemsSize.keyAt(cyclePosition);
                    break;
                }
            }
        }

        return groupPosition;
    }

    private AddressGroupContainer getItemContainer(int groupPosition) {
        return getItem(groupPosition);
    }

    @NonNull
    public AddressContainer getItemAddressContainer(int position, int groupPosition) {
        AddressGroupContainer addressContainer = getItemContainer(groupPosition);
        AddressContainer dbAddressContainer;

        if (addressContainer != null) {
            int size = mCacheItemsSize.valueAt(groupPosition);
            int realPosition = position - size + addressContainer.getAddress().size();
            dbAddressContainer = addressContainer.getAddress().get(realPosition);
        } else {
            dbAddressContainer = new AddressContainer();
        }

        return dbAddressContainer;
    }

    @Override
    public void notifyDataSetChanged() {
        updateSizeCache();

        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mSize;
    }

    private ViewHolderAddress createViewHolderAddress(View view) {
        ViewHolderAddress holder = new ViewHolderAddress();
        holder.fio = (TextView) view.findViewById(R.id.address_list_item_fio);
        holder.address = (TextView) view.findViewById(R.id.address_list_item_address);
        holder.creditInfo = (TextView) view.findViewById(R.id.address_list_item_credit_info);
        holder.addressDescription = (TextView)
                view.findViewById(R.id.address_list_item_address_description);

        return holder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderAddress holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_address, parent, false);

            holder = createViewHolderAddress(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderAddress) convertView.getTag();
        }

        int groupPosition = getGroupPosition(position);
        if (groupPosition != -1) {
            AddressContainer item = getItemAddressContainer(position, groupPosition);

            final String fio = item.getGuarantorFio();
            if (fio != null) {
                holder.fio.setVisibility(View.VISIBLE);
                holder.fio.setText(fio);
            } else {
                holder.fio.setVisibility(View.GONE);
            }

            final String creditInfo = item.getCreditInfo();
            if (creditInfo != null) {
                holder.creditInfo.setVisibility(View.VISIBLE);
                holder.creditInfo.setText(creditInfo);
            } else {
                holder.creditInfo.setVisibility(View.GONE);
            }

            if (mSelectedId == position) {
                holder.address.setTextColor(mRedColor);
                convertView.setBackgroundColor(mBlackBackgoundColor);
            } else {
                holder.address.setTextColor(mBlackColor);
                convertView.setBackgroundColor(mWhiteColor);
            }

            holder.address.setText(item.getAddress());
            holder.addressDescription.setText(item.getTitle());
        }

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_event_header, parent, false);
            view.setBackgroundColor(parent.getContext().getResources().getColor(R.color.black_15));
        }

        int groupPosition = getGroupPosition(position);

        if (groupPosition != -1) {
            AddressGroupContainer group = getItemContainer(groupPosition);
            ((TextView) view).setText(group.getTitle());
        }

        return view;
    }

    @Override
    public long getHeaderId(int position) {
        String title = "";
        int groupPosition = getGroupPosition(position);

        if (groupPosition != -1) {
            AddressGroupContainer group = getItemContainer(groupPosition);
            title = group.getTitle();
        }

        return title.hashCode();
    }

}
