package com.lifecorp.collector.ui.fragments.debtor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.debtor.GetDebtors;
import com.lifecorp.collector.ui.adapters.debtor.DebtorsListAdapter;
import com.lifecorp.collector.ui.fragments.BaseFragment;
import com.lifecorp.collector.ui.interfaces.OnSortByListener;
import com.lifecorp.collector.ui.interfaces.PressedDebtorManager;
import com.lifecorp.collector.ui.view.CustomFontTextView;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.LocationHelper;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;


public class DebtorsListFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG_SAVED_SORT_POSITION = "TAG_SAVED_SORT_POSITION";
    private OnSortByListener mOnSortByListener;
    private PressedDebtorManager mManager;

    private int mSaveSortPosition = 1; // default sort by Name

    private View mSortByName;
    private ListView mLvDebtors;
    private Animation mAnimation;
    private TextView mTvNotFound;
    private View mProgressCircle;
    private View mDebtorsProgress;
    private DebtorsListAdapter mDebtorsAdapter;
    private List<DBDebtorProfile> mDebtorsList;
    private boolean mIsCanRefreshListFromServerAnswer = true;

    public static DebtorsListFragment newInstance(PressedDebtorManager manager,
                                                  OnSortByListener onSortByListener) {
        DebtorsListFragment debtorsListFragment = newInstance(manager);
        debtorsListFragment.setOnSortByListener(onSortByListener);
        debtorsListFragment.setIsCanRefreshListFromServerAnswer(false);

        return debtorsListFragment;
    }

    public static DebtorsListFragment newInstance(PressedDebtorManager manager) {
        DebtorsListFragment debtorsListFragment = new DebtorsListFragment();
        debtorsListFragment.setPressedDebtorManager(manager);

        return debtorsListFragment;
    }

    public void setIsCanRefreshListFromServerAnswer(boolean isCanRefreshListFromServerAnwer) {
        mIsCanRefreshListFromServerAnswer = isCanRefreshListFromServerAnwer;
    }

    public void setOnSortByListener(OnSortByListener onSortByListener) {
        mOnSortByListener = onSortByListener;
    }

    public void setPressedDebtorManager(PressedDebtorManager manager) {
        mManager = manager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mSaveSortPosition = savedInstanceState.getInt(TAG_SAVED_SORT_POSITION);
        }

        View view = inflater.inflate(R.layout.fragment_debtors_list, container, false);
        initInterface(view);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(TAG_SAVED_SORT_POSITION, mSaveSortPosition);
    }

    private void getDebtorsServiceResponse() {
        if (mDebtorsAdapter != null) {
            mDebtorsAdapter.invalidateDebtors();
            updateDebtorsCount();
        }
        if (mLvDebtors != null) {
            if (mDebtorsAdapter != null && mDebtorsAdapter.getDebtorsSize() != 0) {
                mLvDebtors.setVisibility(View.VISIBLE);
            } else if(mTvNotFound != null) {
                mTvNotFound.setVisibility(View.VISIBLE);
            }
        }
        if (mDebtorsProgress != null) {
            mDebtorsProgress.setVisibility(View.GONE);
            showAnimation(false);
        }
    }

    public void initInterface(View view) {
        mDebtorsProgress = view.findViewById(R.id.debtors_list_progress_wrapper);
        mProgressCircle = view.findViewById(R.id.debtors_list_progress);
        mAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_infinity);

        mTvNotFound = (CustomFontTextView) view.findViewById(R.id.debtors_list_not_found);
        mDebtorsAdapter = new DebtorsListAdapter(getMainActivity(), mSaveSortPosition,
                mOnSortByListener == null);

        boolean isLoading = getMainActivity().isLoadingDebtors()
                && mDebtorsAdapter.getDebtorsSize() == 0;

        if (mDebtorsList != null) {
            mDebtorsAdapter.setDebtorsList(mDebtorsList);
            mDebtorsList = null;
        } else {
            isLoading = isLoading || mOnSortByListener != null;
        }

        initSortInterface(view);
        initListInterface(view);
        changePageState(isLoading || mOnSortByListener != null);
        showZeroScreen(isLoading);
    }

    private void showZeroScreen(boolean isLoading) {
        if (mDebtorsAdapter.getDebtorsSize() == 0 && !isLoading) {
            mTvNotFound.setText(R.string.tv_stub_nf2);
            mTvNotFound.setVisibility(View.VISIBLE);
            changePageState(false);
        } else {
            mTvNotFound.setVisibility(View.GONE);
        }
    }

    private void initListInterface(View view) {
        mLvDebtors = (ListView) view.findViewById(R.id.debtors_list_debtors);
        mLvDebtors.setAdapter(mDebtorsAdapter);
        updateDebtorsCount();

        int pressedPosition = mDebtorsAdapter.getPressedPosition();
        if (pressedPosition != -1) {
            mLvDebtors.setSelection(pressedPosition);
            goToPressedPosition(pressedPosition);
        }

        mLvDebtors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mDebtorsAdapter.setPressedPosition(i);
                if (mManager != null) {
                    mManager.onDebtorChanged(mDebtorsAdapter.getPressedDeb());
                }
                if (getMainActivity().isSearchActive()) {
                    getMainActivity().collapseSearch();
                } else {
                    mDebtorsAdapter.notifyDataSetChanged();
                }
            }
        });

        mLvDebtors.setDrawingCacheEnabled(true);
        mLvDebtors.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView listView, int scrollState) {
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    ImageLoader.getInstance().pause();
                } else {
                    ImageLoader.getInstance().resume();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
            }
        });
    }

    private void initSortInterface(View view) {
        mSortByName = view.findViewById(R.id.sort_by_name);
        mSortByName.setOnClickListener(this);

        view.findViewById(R.id.sort_by_sum).setOnClickListener(this);
        view.findViewById(R.id.sort_by_distance).setOnClickListener(this);

        int[] sortsBy = {R.id.sort_by_name, R.id.sort_by_sum, R.id.sort_by_distance};
        int saveSortPosition = 0;
        if (mOnSortByListener != null) {
            saveSortPosition = mOnSortByListener.getSaveSortPosition();
            if (saveSortPosition >= sortsBy.length) {
                saveSortPosition = 0;
            }
        }

        view.findViewById(sortsBy[saveSortPosition]).performClick();
    }

    public void setListVisible() {
        if (mLvDebtors != null) {
            changePageState(false);
            mLvDebtors.setVisibility(View.VISIBLE);
        }
    }

    public void sortDebtors() {
        if (mDebtorsAdapter != null) {
            mDebtorsAdapter.sortDebtors();
        }
    }

    public void setDebtorsList(List<DBDebtorProfile> debtors) {
        if (debtors != null) {
            if (mDebtorsAdapter != null) {
                mDebtorsAdapter.setDebtorsList(debtors);
                showZeroScreen(getMainActivity().isLoadingDebtors()
                        && mDebtorsAdapter.getDebtorsSize() == 0);
            } else {
                mDebtorsList = debtors;
            }
        }
    }

    public void notifyDataSetChanged() {
        if (mDebtorsAdapter != null) {
            mDebtorsAdapter.notifyDataSetChanged();
            updateDebtorsCount();

            showZeroScreen(false);
        }
    }

    public void setSearchParams(String searchSubString) {
        if (mDebtorsAdapter != null) {
            mDebtorsAdapter.invalidateCurrentDebtors(searchSubString);
            mDebtorsAdapter.notifyDataSetChanged();
            updateDebtorsCount();
            mTvNotFound.setText(R.string.tv_stub_nf);
            mTvNotFound.setVisibility(mDebtorsAdapter.isEmpty() ? View.VISIBLE : View.GONE);
        }
        goToPressedPosition();
    }

    public void setPressedDebtor(int debtorId) {
        if (mDebtorsAdapter != null) {
            mDebtorsAdapter.setPressedDebtor(debtorId);
            mDebtorsAdapter.notifyDataSetChanged();
        }
    }

    public void goToPressedPosition() {
        if (mDebtorsAdapter != null ) {
            goToPressedPosition(mDebtorsAdapter.getPressedPosition());
        }
    }

    private void goToPressedPosition(final int pressedPosition) {
        if (pressedPosition != -1 && mLvDebtors != null) {
            mLvDebtors.clearFocus();
            mLvDebtors.post(new Runnable() {
                @Override
                public void run() {
                    mLvDebtors.setSelection(pressedPosition);
                }
            });
        }
    }

    private void setSortParams(int position) {
        if (mDebtorsAdapter != null) {
            if (mOnSortByListener != null) {
                mOnSortByListener.setOnSortPositionChanged(position);
            }

            mDebtorsAdapter.sortDebtors(position);
            mDebtorsAdapter.notifyDataSetChanged();
            updateDebtorsCount();
        }
    }

    private void changePageState(boolean isLoading) {
        if (mDebtorsProgress != null && mLvDebtors != null && mTvNotFound != null) {
            if (isLoading) {
                mLvDebtors.setVisibility(View.GONE);
                mTvNotFound.setVisibility(View.GONE);
            }

            mDebtorsProgress.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            showAnimation(isLoading);
        }
    }

    private void showAnimation(boolean showAnimation) {
        if (showAnimation) {
            mProgressCircle.startAnimation(mAnimation);
        } else {
            mAnimation.cancel();
        }
    }

    public void startRefreshAll() {
        changePageState(true);
    }

    public void setCoordinates() {
        if (mDebtorsAdapter != null) {
            mDebtorsAdapter.invalidateDebtors();
            mDebtorsAdapter.notifyDataSetChanged();
        }
    }

    public void onOperationFinished(CollectorOperationResult<? extends ServerResponse> result) {
        if (result != null && result.getOutput() != null) {
            String tag = result.getOutput().getResponseType();

            switch (tag) {
                case Const.TAG_DEBTORS:
                    if (mIsCanRefreshListFromServerAnswer) {
                        GetDebtors.Result response = (GetDebtors.Result) result;

                        if (response.isSuccessful() && !getBaseActivity().checkoutError(response)) {
                            DBCachePoint.getInstance().refreshCacheFields();
                            getDebtorsServiceResponse();
                        } else {
                            getDebtorsServiceResponse();
                        }
                    }
                    break;
            }
        }
    }

    private void removeSelectedFromChild(ViewGroup view) {
        if (view != null) {
            final int size = view.getChildCount();

            for (int i = 0; i < size; i++) {
                view.getChildAt(i).setSelected(false);
            }
        }
    }

    private void updateDebtorsCount() {
        if (mDebtorsAdapter != null) {
            String count = mDebtorsAdapter.getCount() == 0 ? "" : "" + mDebtorsAdapter.getCount();
            getMainActivity().setDebtorsCount(count);
        }
    }

    @Override
    public void onClick(View v) {
        removeSelectedFromChild((ViewGroup) v.getParent());
        v.setSelected(true);

        switch (v.getId()) {
            case R.id.sort_by_name:
                setSortParams(0);
                break;
            case R.id.sort_by_sum:
                setSortParams(1);
                break;
            case R.id.sort_by_distance:
                if (!LocationHelper.getInstance().isLocationServicesEnabled()
                        || !LocationHelper.getInstance().wasFound()) {
                    if (!LocationHelper.getInstance().wasFound()) {
                        getBaseActivity().createLocationSearchProblem();
                    } else {
                        getBaseActivity().createEnableLocationServiceDialog();
                    }

                    if (mSortByName != null) {
                        mSortByName.performClick();
                    }
                } else if (LocationHelper.getInstance().wasFound()) {
                    setSortParams(2);
                }

                break;
        }
    }

}
