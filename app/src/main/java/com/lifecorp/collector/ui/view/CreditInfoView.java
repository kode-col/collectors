package com.lifecorp.collector.ui.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.ui.adapters.debtor.DebtorSectionFieldAdapter;
import com.lifecorp.collector.ui.interfaces.OnCreditCollectionWorkListener;
import com.lifecorp.collector.utils.Utils;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 17.04.15.
 *
 * Вид информации о кредите, он довольно сильно отличается
 */
public class CreditInfoView extends FrameLayout implements View.OnClickListener {

    private String mFio;
    private DBSection mSection;
    private FakeGridView mGuarantorGridView;
    private TextView mLastCreditCollectionText;
    private FakeGridView mMainInfoFakeGridView;
    private ImageView mLastCreditCollectionIcon;
    private List<DBSectionField> mSectionFields;
    private FakeGridView mPaymentHistoryGridView;
    private List<DBCollection> mCreditCollectionList;
    private OnCreditCollectionWorkListener mOnCreditCollectionWorkListener;

    public CreditInfoView(Context context, String fio, DBSection dbSection,
                          List<DBSectionField> fieldList, List<DBCollection> collectionList,
                          OnCreditCollectionWorkListener listener) {
        super(context);

        init(fio, dbSection, fieldList, collectionList, listener);
    }

    public CreditInfoView(Context context) {
        super(context);

        init(null, null, null, null, null);
    }

    public CreditInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(null, null, null, null, null);
    }

    public CreditInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(null, null, null, null, null);
    }

    private void init(String fio, DBSection dbSection, List<DBSectionField> fieldList,
                      List<DBCollection> collectionList, OnCreditCollectionWorkListener listener) {
        mFio = fio;
        mSection = dbSection;
        mCreditCollectionList = collectionList;
        mSectionFields = fieldList != null ? fieldList : new ArrayList<DBSectionField>();
        mOnCreditCollectionWorkListener = listener;

        if (!isInEditMode()) {
            inflate(getContext(), R.layout.listitem_expandable_content_credit_info, this);
            initInterface();
        }
    }

    private void initInterface() {
        mLastCreditCollectionText = (TextView)
                findViewById(R.id.credit_info_last_credit_collection_text);

        mLastCreditCollectionIcon = (ImageView)
                findViewById(R.id.credit_info_last_credit_collection_type);

        findViewById(R.id.credit_info_collection_add).setOnClickListener(this);
        findViewById(R.id.credit_info_collection_history).setOnClickListener(this);

        mGuarantorGridView = (FakeGridView) findViewById(R.id.credit_info_guarantor_grid);
        mMainInfoFakeGridView = (FakeGridView) findViewById(R.id.credit_info_main_info_grid);
        mPaymentHistoryGridView = (FakeGridView)
                findViewById(R.id.credit_info_payment_history_grid);
    }

    public void drawView() {
        setCollectionValues();
        setMainInfo();
        setPaymentHistory();
        setGuarantor();
    }

    private void setMainInfo() {
        List<DBSectionField> sectionFields = filterSectionFields(0);
        if (sectionFields.size() > 0) {
            toggleMainInfo(true);
            showDataForGridView(mMainInfoFakeGridView, sectionFields);
        } else {
            toggleMainInfo(false);
        }
    }

    private void setPaymentHistory() {
        List<DBSectionField> sectionFields  = filterSectionFields(1);
        if (sectionFields.size() > 0) {
            togglePaymentHistory(true);
            showDataForGridView(mPaymentHistoryGridView, sectionFields);
        } else {
            togglePaymentHistory(false);
        }
    }

    private void setGuarantor() {
        int[] exclude = new int[] {0, 1};
        List<DBSectionField> sectionFields  = filterExcludeSectionFieldsByType(asList(exclude));
        if (sectionFields.size() > 0) {
            toggleGuarantor(true);
            showDataForGridView(mGuarantorGridView, sectionFields);
        } else {
            toggleGuarantor(false);
        }
    }

    private List<Integer> asList(final int[] is) {
        return new AbstractList<Integer>() {
            @Override
            public Integer get(int i) {
                return is[i];
            }

            @Override
            public int size() {
                return is.length;
            }
        };
    }

    private void showDataForGridView(FakeGridView gridView, List<DBSectionField> dbSectionFields) {
        if (gridView != null && dbSectionFields != null && mFio != null) {
            gridView.setAdapter(new DebtorSectionFieldAdapter(dbSectionFields, mFio));
            gridView.drawView();
        }
    }

    public void setCollectionValues() {
        if (mLastCreditCollectionText != null && mLastCreditCollectionIcon != null) {

            if (mCreditCollectionList != null && mCreditCollectionList.size() > 0) {
                DBCollection collection = mCreditCollectionList.get(0);

                if (collection != null) {
                    final int imageRes = Utils.getImageForType(collection.getType());
                    String lastRecord = collection.getTitle();
                    if (collection.getComment() != null) {
                        lastRecord += " – " + collection.getComment();
                    }

                    mLastCreditCollectionText.setText(lastRecord);
                    mLastCreditCollectionIcon.setImageResource(imageRes);
                    mLastCreditCollectionIcon.setVisibility(View.VISIBLE);
                } else {
                    setEmptyValues();
                }
            } else {
                setEmptyValues();
            }
        }
    }

    private void setEmptyValues() {
        mLastCreditCollectionText.setText(R.string.no_collection);
        mLastCreditCollectionIcon.setVisibility(View.GONE);
    }

    /** Метод переключения состояния видимости для части MainInfo между видимой и не видимой
      * @param isVisible - видима ли часть
      */
    private void toggleMainInfo(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;

        findViewById(R.id.credit_info_main_info_grid_title).setVisibility(visibility);
        mMainInfoFakeGridView.setVisibility(visibility);
    }

    /** Метод переключения состояния видимости для части PaymentHistory между видимой и не видимой
     * @param isVisible - видима ли часть
     */
    private void togglePaymentHistory(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;

        findViewById(R.id.credit_info_payment_history_grid_title).setVisibility(visibility);
        mPaymentHistoryGridView.setVisibility(visibility);
    }

    /** Метод переключения состояния видимости для части Guarantor между видимой и не видимой
     * @param isVisible - видима ли часть
     */
    private void toggleGuarantor(boolean isVisible) {
        int visibility = isVisible ? View.VISIBLE : View.GONE;

        findViewById(R.id.credit_info_guarantor_grid_title).setVisibility(visibility);
        mGuarantorGridView.setVisibility(visibility);
    }

    @NonNull
    private List<DBSectionField> filterExcludeSectionFieldsByType(List<Integer> types) {
        List<DBSectionField> sectionFieldsTemp = new ArrayList<>();

        if (mSectionFields != null && types != null) {
            for (DBSectionField item: mSectionFields) {
                if (!types.contains(item.getCredit_section_i())) {
                    sectionFieldsTemp.add(item);
                }
            }
        }

        return sectionFieldsTemp;
    }

    @NonNull
    private List<DBSectionField> filterSectionFields(int type) {
        List<DBSectionField> sectionFieldsTemp = new ArrayList<>();

        if (mSectionFields != null) {
            for (DBSectionField item: mSectionFields) {
                if (item.getCredit_section_i() == type) {
                    sectionFieldsTemp.add(item);
                }
            }
        }

        return sectionFieldsTemp;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.credit_info_collection_add:
                if (mOnCreditCollectionWorkListener != null) {
                    mOnCreditCollectionWorkListener.onAddCollection(mSection.getDebtors_id(),
                            mSection.getId());
                }
                break;
            case R.id.credit_info_collection_history:
                if (mOnCreditCollectionWorkListener != null) {
                    mOnCreditCollectionWorkListener.onViewCollectionHistory(
                            mSection.getDebtors_id(), mSection.getId());
                }
                break;
        }
    }

}
