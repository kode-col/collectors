package com.lifecorp.collector.ui.fragments.debtor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.ui.adapters.debtor.DebtorInfoAdapter;
import com.lifecorp.collector.ui.fragments.BaseFragment;
import com.lifecorp.collector.ui.interfaces.OnCreditCollectionWorkListener;
import com.lifecorp.collector.ui.view.FakeListView;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.MyImageLoaderListener;
import com.lifecorp.collector.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

public class DebtorInfoFragment extends BaseFragment {

    private DebtorInfoAdapter mAdapter;
    private FakeListView mDebtorInfoList;
    private TextView mDebtorInfoDistance;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debtor_info, container, false);
        initInterface(view);
        setHasOptionsMenu(true);

        return view;
    }

    public void onOperationFinished(CollectorOperationResult<? extends ServerResponse> result) {
        if (result != null && result.isSuccessful()) {
            if (result.getOutput() != null) {
                String tag = result.getOutput().getResponseType();

                if (!getBaseActivity().checkoutError(result)) {
                    switch (tag) {
                        case Const.TAG_COLLECTIONS:
                            notifyCollectorInfo();
                            break;
                        case Const.TAG_POST_COLLECTION:
                            makeToast(R.string.toast_adding_collection_added);
                            notifyCollectorInfo();
                            break;
                    }
                }
            }
        }
    }

    public void initInterface(View view) {
        DBDebtorProfile pressedDebtor = getMainActivity().getPressedDeb();

        TextView debtorInfoID = (TextView) view.findViewById(R.id.debtor_info_id);
        TextView debtorInfoName = (TextView) view.findViewById(R.id.debtor_info_name);
        TextView debtorInfoDate = (TextView) view.findViewById(R.id.debtor_info_date);
        TextView debtorInfoMoney = (TextView) view.findViewById(R.id.debtor_info_money);
        TextView debtorInfoCoins = (TextView) view.findViewById(R.id.debtor_info_coins);
        ImageView debtorInfoIcon = (ImageView) view.findViewById(R.id.debtor_info_icon);

        mDebtorInfoDistance = (TextView) view.findViewById(R.id.debtor_info_distance);

        if (pressedDebtor != null) {
            debtorInfoName.setText(pressedDebtor.getFio());
            debtorInfoID.setText(Utils.formatIdForOutput(pressedDebtor.getId()));

            if (pressedDebtor.getTotalDebtAmount() != null) {
                String currency = pressedDebtor.getTotalDebtAmount().getOutputCurrency();

                debtorInfoMoney.setText(pressedDebtor.getTotalDebtPaperMoney());
                debtorInfoCoins.setText(pressedDebtor.getTotalDebtCoins() + " " + currency);
            }

            String delay = String.valueOf(pressedDebtor.getTotalDebtDelay());
            int lastDigit = pressedDebtor.getTotalDebtDelay() % 10;
            String pluralDays = getResources().getQuantityString(R.plurals.plurals_days, lastDigit);
            debtorInfoDate.setText(delay + " " + pluralDays);

            setDistanceToDebtor();

            ImageLoader.getInstance().displayImage(pressedDebtor.buildPhotoUrl(), debtorInfoIcon,
                    new MyImageLoaderListener(getMainActivity()));
        }

        OnCreditCollectionWorkListener listener = null;
        if (getMainActivity() instanceof OnCreditCollectionWorkListener) {
            listener = getMainActivity();
        }

        if (mAdapter == null) {
            mAdapter = new DebtorInfoAdapter(pressedDebtor, listener);
        }

        mDebtorInfoList = (FakeListView) view.findViewById(R.id.debtor_info_list);
        mDebtorInfoList.setAdapter(mAdapter);
        mDebtorInfoList.drawView();
    }

    public void setDistanceToDebtor() {
        DBDebtorProfile debtor = getMainActivity().getPressedDeb();

        if (debtor != null && debtor.getDistance() != null && mDebtorInfoDistance != null) {
            mDebtorInfoDistance.setText(debtor.getDistance() + " " + getString(R.string.km));
        }
    }

    private void notifyCollectorInfo() {
        if (mDebtorInfoList != null) {
            mAdapter.updateCollections();
            mDebtorInfoList.drawView();
        }
    }

}
