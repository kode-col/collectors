package com.lifecorp.collector.ui.adapters.debtor;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.ui.activities.MainActivity;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderDebtor;
import com.lifecorp.collector.utils.MyImageLoaderListener;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class DebtorsListAdapter extends BaseAdapter {

    private int PRESSED_ID = -1;

    private int mSavedSortPosition;
    private int colorRed, colorDark;
    private MainActivity mMainActivity;

    private List<DBDebtorProfile> mAllDebtors;
    private List<DBDebtorProfile> mCurrentDebtors;

    public DebtorsListAdapter(MainActivity mainActivity, int savedSortPosition,
                              boolean isDataFromGlobal) {
        mMainActivity = mainActivity;

        if (mainActivity.getPressedDeb() != null) {
            PRESSED_ID = mainActivity.getPressedDeb().getId();
        }

        initColors(mainActivity);
        if (isDataFromGlobal) {
            setDebtorsList(DBCachePoint.getInstance().getAllDebtors());
            sortDebtors(savedSortPosition);
        } else {
            mAllDebtors = new ArrayList<>();
            mCurrentDebtors = new ArrayList<>();
        }
    }

    private void initColors(Context context) {
        colorRed = context.getResources().getColor(R.color.red);
        colorDark = context.getResources().getColor(R.color.black_light);
    }

    public void setDebtorsList(List<DBDebtorProfile> debtors) {
        mAllDebtors = debtors;
        mCurrentDebtors = getCurrentDebtors(mMainActivity.getSavedTextSearchPanel());
        sortDebtors(mSavedSortPosition);
        notifyDataSetChanged();
    }

    public void sortDebtors() {
        sortDebtors(mSavedSortPosition);
    }

    @Override
    public int getCount() {
        return mCurrentDebtors != null ? mCurrentDebtors.size() : 0;
    }

    @Override
    public DBDebtorProfile getItem(int position) {
        return position < getCount() ? mCurrentDebtors.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolderDebtor holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_debtors, parent, false);
            holder = new ViewHolderDebtor();

            holder.name = (TextView) convertView.findViewById(R.id.list_deb_name);
            holder.money = (TextView) convertView.findViewById(R.id.list_deb_money);
            holder.coins = (TextView) convertView.findViewById(R.id.list_deb_coins);
            holder.dateDist = (TextView) convertView.findViewById(R.id.list_deb_date_dist);
            holder.debtorIcon = (ImageView) convertView.findViewById(R.id.list_debtor_icon);
            holder.promisedPay = (ImageView) convertView.findViewById(R.id.list_promised_pay);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderDebtor) convertView.getTag();
        }

        final DBDebtorProfile item = getItem(position); // CurrentDebProfile
        if (item != null) {
            convertView.setBackgroundColor(item.getId() == PRESSED_ID ? colorRed : colorDark);

            String io = String.format("%s %s", item.getFirstName(), item.getMiddleName());
            String fio = String.format(io.length() < 22 ? "%s\n%s %s" : "%s %s\n%s",
                    item.getLastName(), item.getFirstName(), item.getMiddleName());

            holder.name.setText(fio);
            Resources res = App.getContext().getResources();

            if (item.getTotalDebtAmount() != null) {
                String currency = item.getTotalDebtAmount().getOutputCurrency();

                holder.money.setText(item.getTotalDebtPaperMoney());
                holder.coins.setText(", " + item.getTotalDebtCoins() + " " + currency);
            }

            String pluralDays =
                    res.getQuantityString(R.plurals.plurals_days, item.getTotalDebtDelay() % 10);

            String distance = "";
            if (item.getDistance() != null) {
                distance = " / " + item.getDistance() + " " + mMainActivity.getString(R.string.km);
            }

            String delay = String.valueOf(item.getTotalDebtDelay());
            holder.dateDist.setText(delay + " " + pluralDays + distance);
            holder.promisedPay.setVisibility(item.isPromised() ? View.VISIBLE : View.GONE);
            holder.promisedPay.setImageResource(item.getId() == PRESSED_ID
                    ? R.drawable.ico_spisok_promised_payment_white
                    : R.drawable.ico_spisok_promised_payment_red);
            holder.debtorIcon.setImageResource(R.drawable.ico_spisok_no_photo);

            ImageLoader.getInstance().displayImage(item.buildPhotoUrl(), holder.debtorIcon,
                    new MyImageLoaderListener(mMainActivity));
        }

        return convertView;
    }

    public void setPressedPosition(int position) {
        PRESSED_ID = mCurrentDebtors.get(position).getId();
    }

    public void setPressedDebtor(int debtorId) {
        if (mCurrentDebtors != null) {
            int position = -1;
            int i = 0;

            for (DBDebtorProfile item : mCurrentDebtors) {
                if (debtorId == item.getId()) {
                    position = i;
                    break;
                }
                i++;
            }

            if (position != -1) {
                setPressedPosition(position);
            }
        }
    }

    public int getPressedPosition() {
        for (int i = 0; i < mCurrentDebtors.size(); ++i) {
            if (mCurrentDebtors.get(i).getId() == PRESSED_ID) {
                return i;
            }
        }

        return -1;
    }

    public DBDebtorProfile getPressedDeb() {
        if (PRESSED_ID != -1) {
            for (DBDebtorProfile deb : mCurrentDebtors) {
                if (deb.getId() == PRESSED_ID) {
                    return deb;
                }
            }
        }
        return null;
    }

    public void invalidateDebtors() {
        mAllDebtors = DBCachePoint.getInstance().getAllDebtors();
        mCurrentDebtors = getCurrentDebtors(mMainActivity.getSavedTextSearchPanel());
        sortDebtors(mSavedSortPosition);
        notifyDataSetChanged();
    }

    public void invalidateCurrentDebtors(String searchSubString) {
        mCurrentDebtors = getCurrentDebtors(searchSubString);
        sortDebtors(mSavedSortPosition);
    }

    public int getDebtorsSize() {
        return mAllDebtors.size();
    }

    private List<DBDebtorProfile> getCurrentDebtors(String searchSubString) {
        List<DBDebtorProfile> debtors = new ArrayList<>();

        if (searchSubString == null || searchSubString.isEmpty()) {
            return mAllDebtors;
        } else {
            for (DBDebtorProfile deb : mAllDebtors) {
                String fio = (deb.getFirstName() + deb.getMiddleName() + deb.getLastName())
                        .toLowerCase();
                if (fio.contains(searchSubString.toLowerCase())) {
                    debtors.add(deb);
                }
            }
        }

        return debtors;
    }

    public void sortDebtors(int position) {
        if (mCurrentDebtors != null) {
            mSavedSortPosition = position;

            switch (position) {
                case 0:
                    Collections.sort(mCurrentDebtors, new Comparator<DBDebtorProfile>() {
                        @Override
                        public int compare(DBDebtorProfile debtor1, DBDebtorProfile debtor2) {
                            return debtor1.getLastName().compareTo(debtor2.getLastName());
                        }
                    });
                    break;
                case 1:
                    Collections.sort(mCurrentDebtors, new Comparator<DBDebtorProfile>() {
                        @Override
                        public int compare(DBDebtorProfile debtor1, DBDebtorProfile debtor2) {
                            return (int) (debtor2.getTotalDebtAmount().getAmount() * 100) -
                                    (int) (debtor1.getTotalDebtAmount().getAmount() * 100);
                        }
                    });
                    break;
                case 2:
                    Collections.sort(mCurrentDebtors, new Comparator<DBDebtorProfile>() {
                        @Override
                        public int compare(DBDebtorProfile debtor1, DBDebtorProfile debtor2) {
                            int dist1 = debtor1.getDistance() != null
                                    ? (int) (debtor1.getDistance() * 10)
                                    : 0;
                            int dist2 = debtor2.getDistance() != null
                                    ? (int) (debtor2.getDistance() * 10)
                                    : 0;

                            return dist1 - dist2;
                        }
                    });
                    break;
                case 3:
                    Collections.sort(mCurrentDebtors, new Comparator<DBDebtorProfile>() {
                        @Override
                        public int compare(DBDebtorProfile debtor1, DBDebtorProfile debtor2) {
                            return debtor2.getTotalDebtDelay() - debtor1.getTotalDebtDelay();
                        }
                    });
                    break;
            }
        }
    }

}
