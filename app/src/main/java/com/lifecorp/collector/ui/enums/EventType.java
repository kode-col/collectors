package com.lifecorp.collector.ui.enums;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBEvent;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by Alexander Smirnov on 14.04.15.
 *
 * Перечисление возможных типов фильтра отображения карты. Фактически присутствует только три
 * типа мероприятия ("В работе", "Сегодня" и "Просроченное"), но ради удобства были добавлены
 * еще два типа "Все мероприятия" – отображающий все три типа сразу и
 * "Все клиенты" – отображает всех клиентов без привязки к мероприятиям
 */
public enum EventType implements Serializable {
    IN_WORK(R.drawable.ico_map_marker_green, R.drawable.ico_map_marker_green_money),//В работе
    EDGE(R.drawable.ico_map_marker_orange, R.drawable.ico_map_marker_orange_money), //На грани
    EXPIRED(R.drawable.ico_map_marker_red, R.drawable.ico_map_marker_red_money),    //Просрочено

    ALL_CLIENTS(R.drawable.ico_map_marker_dark, R.drawable.ico_map_marker_dark_money),//Все клиенты
    ALL_EVENTS(-1, -1); //Все события (-1 т.к. увидим все типы)

    private final static long MILLIS_IN_DAY = 24 * 60 * 60 * 1000L;
    private final static long MAX_NORMAL_TIME = 3 * MILLIS_IN_DAY;
    private static final int DEFAULT = R.drawable.ico_map_marker_dark;
    private final int mResId, mMoneyResId;

    EventType(int resId, int moneyResId) {
        mResId = resId;
        mMoneyResId = moneyResId;
    }

    // Вернуть ресурс изображения
    private int getResId() {
        return mResId;
    }

    // Вернуть идентификатор ресурса изображения для обещанного платежа
    private int getMoneyResId() {
        return mMoneyResId;
    }

    public static int getMarkerResource(EventType eventType) {
        return eventType != null ? eventType.getResId() : DEFAULT;
    }

    public static int getMarkerMoneyResource(EventType eventType) {
        return eventType != null ? eventType.getMoneyResId() : getMarkerResource(null);
    }

    public static EventType getEventType(DBEvent event, Calendar now) {
        final long distanceInMillis = now.getTimeInMillis() - event.getTime();

        if (distanceInMillis < 0 || event.isCompleted()) {
            return EventType.IN_WORK;
        } else if (distanceInMillis < MAX_NORMAL_TIME) {
            return EventType.EDGE;
        }

        return EventType.EXPIRED;
    }

}