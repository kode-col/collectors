package com.lifecorp.collector.ui.adapters.holder;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Alexander Smirnov on 20.03.15.
 *
 * ViewHolder для {@link com.lifecorp.collector.ui.adapters.event.EventListAdapter}
 */
public class ViewHolderEvent {
    public ImageView buttonOk;
    public ImageView eventType;

    public TextView textId;
    public TextView textDescription;
    public TextView textMixName;
    public TextView textNumName;
}
