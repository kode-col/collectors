package com.lifecorp.collector.ui.adapters.event;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.ui.activities.MainActivity;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderEvent;
import com.lifecorp.collector.utils.DateUtils;
import com.lifecorp.collector.utils.Utils;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Alexander Smirnov on 20.03.15.
 *
 * Адаптер для вывода событий
 */
public class EventListAdapter extends ArrayAdapter<DBEvent> implements StickyListHeadersAdapter {

    private final MainActivity mMainActivity;
    private final boolean mIsSingleDate;

    // номер вкладки во viewPager
    private int mPosition = 0;

    public EventListAdapter(MainActivity mainActivity, List<DBEvent> objects, int position,
                            boolean isSingleDate) {
        super(mainActivity, 0, objects);

        mMainActivity = mainActivity;
        mIsSingleDate = isSingleDate;
        mPosition = position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderEvent holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_event, parent, false);

            holder = new ViewHolderEvent();
            holder.textId = (TextView) convertView.findViewById(R.id.text_id);
            holder.buttonOk = (ImageView) convertView.findViewById(R.id.button_ok);
            holder.eventType = (ImageView) convertView.findViewById(R.id.event_type);
            holder.textMixName = (TextView) convertView.findViewById(R.id.text_mix_name);
            holder.textNumName = (TextView) convertView.findViewById(R.id.text_num_name);
            holder.textDescription = (TextView) convertView.findViewById(R.id.text_description);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ViewHolderEvent holder = (ViewHolderEvent) view.getTag();
                    if (holder != null) {
                        String debtorProfileId = (String) holder.textId.getTag();
                        if (debtorProfileId != null) {
                            mMainActivity.showDebtorsFragment(debtorProfileId);
                        }
                    }
                }
            });

            holder.buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DBEvent item = (DBEvent) v.getTag();
                    if (item != null) {
                        mMainActivity.createCompleteEventFragment(item);
                    }
                }
            });

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderEvent) convertView.getTag();
        }

        DBEvent item = getItem(position);
        DBDebtorProfile debtor = item.getDebtorProfile();


        holder.buttonOk.setTag(item);

        holder.textDescription.setText(item.getDescription());
        holder.eventType.setImageResource(Utils.getImageForType(item.getType()));

        if (debtor != null) {
            holder.textNumName.setText("");
            holder.textMixName.setText(debtor.getFio());
            holder.textId.setTag(String.valueOf(debtor.getId()));
            holder.textId.setText(Utils.formatIdForOutput(debtor.getId()));
        } else {
            holder.textId.setTag(null);
            holder.textId.setText("");
            holder.textNumName.setText("");
            holder.textMixName.setText("");
        }

        holder.buttonOk.setImageResource(mPosition == 2
                ? R.drawable.ico_ok_red
                : R.drawable.ico_ok_black);
        holder.buttonOk.setVisibility(item.isCompleted() ? View.GONE : View.VISIBLE);

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_event_header, parent, false);
        }

        int color = mPosition == 2 ? R.color.red : R.color.dark_gray;
        view.setBackgroundColor(getContext().getResources().getColor(color));

        ((TextView) view).setText(getTitleForTime(getItem(position).getTime()));

        return view;
    }

    private String getTitleForTime(long time) {
        return mIsSingleDate
                ? DateUtils.formatLongCalendar(DateUtils.timestampToCalendar(time))
                : DateUtils.getWeekForDate(time);
    }

    @Override
    public long getHeaderId(int i) {
        return getTitleForTime(getItem(i).getTime()).hashCode();
    }

}
