package com.lifecorp.collector.ui.adapters.debtor;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBCoordinate;
import com.lifecorp.collector.model.database.objects.DBDebtorLink;
import com.lifecorp.collector.model.database.objects.DBPhoneNumber;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.ui.activities.BaseActivity;
import com.lifecorp.collector.ui.activities.MainActivity;
import com.lifecorp.collector.ui.view.FakeGridView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DebtorSectionFieldAdapter extends BaseAdapter {

    private List<DBSectionField> mSectionFields;
    private final String mFio;

    public DebtorSectionFieldAdapter(List<DBSectionField> sectionFields, String fio) {
        setData(refactorSectionFields(sectionFields));
        mFio = fio;
    }

    private void setData(List<DBSectionField> sectionFields) {
        if (sectionFields == null) {
            sectionFields = new ArrayList<>();
        }

        mSectionFields = refactorSectionFields(sectionFields);
    }

    public int getCount() {
        return mSectionFields.size();
    }

    public DBSectionField getItem(int position) {
        return position < getCount() ? mSectionFields.get(position) : null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_expandable_griditem, parent, false);
            holder = new ViewHolder();

            holder.title = (TextView) convertView.findViewById(R.id.grid_item_title);
            holder.content = (TextView) convertView.findViewById(R.id.grid_item_content);
            holder.comment = (TextView) convertView.findViewById(R.id.grid_item_comment);
            holder.phoneWrapper = convertView.findViewById(R.id.grid_item_phone_wrapper);
            holder.addressWrapper = convertView.findViewById(R.id.grid_item_address_wrapper);
            holder.linksWrapper = (FakeGridView)
                    convertView.findViewById(R.id.grid_item_links_wrapper);
            holder.linksWrapper.setColumnNumber(9);

            holder.addressWrapper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Object tag = v.getTag();
                    if (tag != null && tag instanceof Integer) {
                        Integer id = (Integer) tag;
                        ((MainActivity) v.getContext()).showMapFragment(id);
                    }
                }
            });

            holder.phoneWrapper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Object tag = view.getTag();
                    if (tag != null && tag instanceof String) {
                        ((BaseActivity) view.getContext()).createCallDialog(mFio, (String) tag);
                    }
                }
            });

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.comment.setVisibility(View.GONE);
        holder.phoneWrapper.setVisibility(View.GONE);
        holder.addressWrapper.setVisibility(View.GONE);
        holder.linksWrapper.setVisibility(View.GONE);
        holder.linksWrapper.setAdapter(null);
        holder.linksWrapper.drawView();

        String title = "";
        String value = "";

        DBSectionField item = getItem(position);
        if (item.getValue() != null) {
            title = item.getTitle();
            value = item.getValue();

            DBCoordinate dbCoordinate = item.getPoiCoordinate();

            if (dbCoordinate != null) {
                holder.addressWrapper.setTag(item.getId_());
                holder.addressWrapper.setVisibility(View.VISIBLE);
            }
        } else {
            DBPhoneNumber dbPhoneNumber = item.getPhone_number();

            if (dbPhoneNumber != null && dbPhoneNumber.getDisplayString() != null) {
                title = item.getTitle();
                value = item.getPhone_number().getDisplayString();

                holder.comment.setVisibility(View.VISIBLE);
                holder.phoneWrapper.setVisibility(View.VISIBLE);
                holder.phoneWrapper.setTag(item.getPhone_number().getDialNumber());

                if (dbPhoneNumber.getComment() != null) {
                    holder.comment.setText(dbPhoneNumber.getComment());
                }
            }
        }

        Collection<DBDebtorLink> debtorLinks = item.getDebtorLinks();

        if(debtorLinks.size() > 0) {
            title = item.getTitle();

            holder.linksWrapper.setAdapter(new DebtorLinksAdapter(new ArrayList<>(debtorLinks)));
            holder.linksWrapper.drawView();
            holder.linksWrapper.setVisibility(View.VISIBLE);
        }

        holder.title.setText(title);
        holder.content.setText(value);

        holder.content.setVisibility(value.isEmpty() ? View.GONE : View.VISIBLE);

        return convertView;
    }

    class ViewHolder {
        TextView content, comment, title;
        View phoneWrapper, addressWrapper;
        FakeGridView linksWrapper;
    }

    private boolean isDBSectionFieldIsValid(DBSectionField item) {
        return item != null && item.getTitle() != null &&
                (item.getValue() != null || item.getDebtorLinks().size() > 0
                        || (item.getPhone_number() != null
                            && item.getPhone_number().getDisplayString() != null)
                );
    }

    private List<DBSectionField> refactorSectionFields(List<DBSectionField> sectionFields) {
        List<DBSectionField> newSectionFields = new ArrayList<>();

        for (DBSectionField item: sectionFields) {
            if (isDBSectionFieldIsValid(item)) {
                newSectionFields.add(item);
            }
        }

        return newSectionFields;
    }

}
