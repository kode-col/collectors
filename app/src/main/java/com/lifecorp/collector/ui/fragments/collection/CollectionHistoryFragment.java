package com.lifecorp.collector.ui.fragments.collection;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.ui.adapters.collection.CollectionExpandableAdapter;
import com.lifecorp.collector.utils.Const;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CollectionHistoryFragment extends CollectionHistoryBaseFragment<DBCollection> {

    private String mCreditId;

    public static CollectionHistoryFragment newInstance(int debtorId, String creditId) {
        CollectionHistoryFragment fragment = new CollectionHistoryFragment();
        fragment.setDebtorId(debtorId);
        fragment.setCreditId(creditId);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mCreditId = savedInstanceState.getString(Const.TAG_CREDIT_ID);
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(Const.TAG_CREDIT_ID, mCreditId);
    }

    private void setCreditId(String creditId) {
        mCreditId = creditId;
    }

    @Override
    protected BaseAdapter getAdapter(List<DBCollection> dataList) {
        return new CollectionExpandableAdapter(getMainActivity(), dataList);
    }

    @Override
    protected int getEmptyTextId() {
        return R.string.no_history;
    }

    @NonNull
    @Override
    protected List<DBCollection> getFilteredData() {
        List<DBCollection> collectionsAll = DBCachePoint.getInstance().getAllCollections();
        List<DBCollection> collectionsTemp = new ArrayList<>();

        if (mCreditId != null) {

            for (DBCollection dbc: collectionsAll) {
                if (getDebtorId() == dbc.getDebtorId() && mCreditId.equals(dbc.getContractId())) {
                    collectionsTemp.add(dbc);
                }
            }
        }

        // Сортировать по дате
        Collections.sort(collectionsTemp, new Comparator<DBCollection>() {
            @Override
            public int compare(DBCollection col1, DBCollection col2) {
                return compare(col2.getDate(), col1.getDate());
            }

            private int compare(long lhs, long rhs) {
                return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
            }
        });

        return collectionsTemp;
    }

}
