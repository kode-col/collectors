package com.lifecorp.collector.ui.fragments;

import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.ui.dialogfragments.BaseDialog;

/**
 * Created by Alexander Smirnov on 17.03.15.
 *
 * Абстрактный фрагмент для фрагментов обёрток логики различной логики в
 * {@link com.lifecorp.collector.ui.activities.MainActivity}
 */
public abstract class BaseWrapperFragment extends BaseFragment implements
        BaseDialog.OnDialogBtnAcceptClicked {

    // метод вызывается из MainActivity нужен для отображеия статуса загрузки данных
    public void refreshAll() {

    }

    public abstract void onOperationFinished(
            final CollectorOperationResult<? extends ServerResponse> result);

}
