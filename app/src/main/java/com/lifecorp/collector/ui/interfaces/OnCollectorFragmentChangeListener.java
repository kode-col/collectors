package com.lifecorp.collector.ui.interfaces;

import com.lifecorp.collector.ui.fragments.collection.CollectionHistoryExpiredFragment;
import com.lifecorp.collector.ui.fragments.collection.CollectionHistoryFragment;

/**
 * Created by Alexander Smirnov on 17.04.15.
 *
 * Интерфейс для отслеживания изменения Collection фрагментов в
 * {@link com.lifecorp.collector.ui.fragments.collection.CollectionHistoryWrapperFragment}
 */
public interface OnCollectorFragmentChangeListener {

    void onCollectionHistoryFragmentChanged(CollectionHistoryFragment fragment);

    void onCollectionExpiredFragmentChanged(CollectionHistoryExpiredFragment fragment);

}
