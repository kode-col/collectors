package com.lifecorp.collector.ui.interfaces;

import com.lifecorp.collector.model.database.objects.DBDebtorProfile;

/**
 * Created by Alexander Smirnov on 03.04.15.
 *
 * Интерфейс получения и изменения выбраного в списке должника
 */
public interface PressedDebtorManager {

    void onDebtorChanged(DBDebtorProfile dbDebtorProfile);

    DBDebtorProfile getPressedDebtor();

}
