package com.lifecorp.collector.ui.enums;

import com.lifecorp.collector.R;

import java.io.Serializable;

public enum EventFilterType implements Serializable {

    DATE(R.string.button_date),
    CALL(R.string.button_call),
    MEETING(R.string.button_meeting);

    private final int mResId;

    EventFilterType(int resId) {
        mResId = resId;
    }

    public int getResId() {
        return mResId;
    }

    public static EventFilterType getDefault() {
        return DATE;
    }

}
