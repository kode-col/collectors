package com.lifecorp.collector.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.adapters.LeftMenuAdapter;
import com.lifecorp.collector.utils.TabPosition;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;

public class LeftMenuFragment extends BaseFragment {

    private static final String TAG_CURRENT_POSITION = "TAG_CURRENT_POSITION";

    private ListView lvLeft;
    private int mCurrentPosition = -1;
    private LeftMenuAdapter mLeftMenuAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_left, container, false);
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(TAG_CURRENT_POSITION, -1);
        }

        initInterface(view);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(TAG_CURRENT_POSITION, mCurrentPosition);
    }

    public void initInterface(View view) {
        lvLeft = (ListView) view.findViewById(R.id.fragment_left_list);
        mLeftMenuAdapter = new LeftMenuAdapter();
        if (mCurrentPosition != -1) {
            mLeftMenuAdapter.setCurrentItem(mCurrentPosition);
        }

        lvLeft.setAdapter(mLeftMenuAdapter);

        lvLeft.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mLeftMenuAdapter.setCurrentItem(i);
                lvLeft.invalidateViews();
                getMainActivity().closeLeft();
                getMainActivity().openFragment(i);
            }
        });

        View btnExit = view.findViewById(R.id.fragment_left_exit);
        btnExit.setBackgroundResource(R.drawable.nav);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferencesStorage.getInstance().clearLogin();
                PreferencesStorage.getInstance().clearPassword();

                getMainActivity().logout();
                getBaseActivity().goToAuthorization();
            }
        });
    }

    public void setCurrentItem(TabPosition position) {
        if (position != null) {
            mCurrentPosition = position.getCode();

            if (mLeftMenuAdapter != null && lvLeft != null) {
                mLeftMenuAdapter.setCurrentItem(mCurrentPosition);
                lvLeft.invalidateViews();
            }
        }
    }

}
