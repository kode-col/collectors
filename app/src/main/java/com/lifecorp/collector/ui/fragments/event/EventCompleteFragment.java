package com.lifecorp.collector.ui.fragments.event;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.objects.Money;
import com.lifecorp.collector.model.objects.PhotoContainer;
import com.lifecorp.collector.model.objects.debtor.Coordinate;
import com.lifecorp.collector.model.objects.event.EventCompletion;
import com.lifecorp.collector.ui.fragments.BasePostFragment;
import com.lifecorp.collector.utils.DateUtils;
import com.lifecorp.collector.utils.LocationHelper;
import com.lifecorp.collector.utils.spinner.SpinnerSelector;

import java.util.List;

public class EventCompleteFragment extends BasePostFragment {

    private View mPromisedPayWrapper;
    private Spinner mSumCurrencySpinner;
    private EditText mPromisedPaymentSum;

    public static EventCompleteFragment newInstance(DBEvent dbEvent) {
        EventCompleteFragment fragment = new EventCompleteFragment();
        fragment.setEvent(dbEvent);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_event_complete, container, false);
        initInterface(rootView);

        return rootView;
    }

    private void initInterface(View rootView) {
        mPromisedPayWrapper = rootView.findViewById(R.id.complete_event_promised_pay_wrapper);

        initMap();
        initButtons(rootView);

        initType(rootView);
        initGeneralResult(rootView);
        initConcreteResult(rootView);
        initPayDate(rootView);

        initPhotoGrid(rootView);
        initComment(rootView);
        initPromisedSum(rootView);
    }

    private void initPromisedSum(View view) {
        View parent = view.findViewById(R.id.complete_event_promised_sum);
        setTextForTextView(parent, R.id.add_input_sum_title, R.string.tv_add_sum);

        mPromisedPaymentSum = (EditText) parent.findViewById(R.id.add_input_sum_input);
        setDefaultSumHint(mPromisedPaymentSum);

        mSumCurrencySpinner = (Spinner) parent.findViewById(R.id.add_input_sum_currency);
        mSumCurrencySpinner.setAdapter(getArrayAdapter(R.array.spinner_sum));
    }

    private void initComment(View view) {
        initComment(view, R.id.complete_event_comment);
    }

    private void initType(View view) {
        initType(view, R.id.complete_event_type, R.string.event_type);
        mSpinnerType.setEnabled(false);
    }

    private void initGeneralResult(View view) {
        initGeneralResult(view, R.id.complete_event_general_result, R.string.tv_add_status);
    }

    private void initConcreteResult(View view) {
        initConcreteResult(view, R.id.complete_event_concrete_result, R.string.result);
        mSpinnerConcreteResult.setOnItemSelectedListener(new SpinnerSelector() {
            final String pattern = getString(R.string.result_promised_pay);

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = mSpinnerConcreteResult.getSelectedItem().toString();

                showPromisedPay(pattern != null && pattern.equals(selected));
            }
        });
    }

    private void showPromisedPay(boolean isShow) {
        if (mPromisedPayWrapper != null) {
            mPromisedPayWrapper.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    private void initPayDate(View view) {
        initPayDate(view, R.id.complete_event_promised_date);
    }

    @NonNull
    private EventCompletion getEventCompletion() {
        final String eventId = getEvent().getId();
        final String commentId = mComment.getText().toString();
        final int type = getSpinnerValue(mSpinnerType);
        final int summary = getSpinnerValue(mSpinnerGeneralResult);
        final int resultType = getSpinnerValue(mSpinnerConcreteResult);
        final Coordinate poiCoordinate = LocationHelper.getInstance().getCoordinate();

        String amountStr = mPromisedPaymentSum.getText().toString();
        if (amountStr.isEmpty()) {
            amountStr = getString(R.string.zero_sum_hint);
        }

        Money promisedPayment = new Money();
        promisedPayment.setAmount(Float.parseFloat(amountStr));
        promisedPayment.setCurrency(mSumCurrencySpinner.getSelectedItem().toString());

        long tempDate = DateUtils.timestampStringToTimestampLong(mPayDate.getText().toString());
        long promisedPaymentDate = DateUtils.fixDateIfNeed(tempDate);
        final List<PhotoContainer > photoContainerList = mPhotoContainer.getDataList();

        if (mPromisedPaymentSum == null || mPromisedPayWrapper.getVisibility() == View.GONE) {
            promisedPayment = new Money();
            promisedPaymentDate = 0;
        }

        return new EventCompletion(eventId, commentId, type, summary, resultType,
                poiCoordinate, promisedPayment, promisedPaymentDate, photoContainerList);
    }

    @Override
    protected void sendPost() {
        getMainActivity().completeEvent(getEventCompletion());
    }

}
