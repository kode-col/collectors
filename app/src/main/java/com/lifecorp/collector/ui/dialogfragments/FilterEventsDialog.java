package com.lifecorp.collector.ui.dialogfragments;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.adapters.EventFilterAdapter;
import com.lifecorp.collector.ui.enums.EventFilterType;
import com.lifecorp.collector.ui.interfaces.OnEventFilterChanged;
import com.lifecorp.collector.ui.view.CustomFontEditText;
import com.lifecorp.collector.utils.DateUtils;

import java.util.Calendar;

/**
 * Created by Alexander Smirnov on 13.04.15.
 *
 * Диалог фильтрации, вызывается с экрана мероприятий
 * {@link com.lifecorp.collector.ui.fragments.event.EventsPlanFragment}
 */
public class FilterEventsDialog extends BaseDialog implements AdapterView.OnItemClickListener {

    private Long mDateFrom, mDateTo;
    private EventFilterType mEventType;
    private EventFilterAdapter mAdapter;
    private OnEventFilterChanged mOnEventFilterChanged;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FilterDialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.dialog_filter_events, container, false);
        getDialog().getWindow()
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initInterface(rootView);

        return rootView;
    }

    private void initInterface(View rootView) {
        initButtons(rootView);

        mAdapter = new EventFilterAdapter(getActivity(), mEventType);
        ListView listView = (ListView) rootView.findViewById(R.id.dialog_filter_events);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);

        initDateFrom(rootView.findViewById(R.id.layFilterDateFrom));
        initDateTo(rootView.findViewById(R.id.layFilterDateTo));

    }

    private void initDateFrom(View layAddDate) {
        EditText addDateFrom = (CustomFontEditText) layAddDate.findViewById(R.id.etElementAddFrom);
        addDateFrom.setText(mDateFrom == null
                ? getString(R.string.filter_select_date)
                : DateUtils.formatTimestamp(mDateFrom));

        setCalendarDate(addDateFrom, layAddDate.findViewById(R.id.ivElementAddDateFrom), true);
    }

    private void initDateTo(View layAddDate) {
        EditText addDateTo = (CustomFontEditText) layAddDate.findViewById(R.id.etElementAddTo);
        addDateTo.setText(mDateTo == null
                ? getString(R.string.filter_select_date)
                : DateUtils.formatTimestamp(mDateTo));

        setCalendarDate(addDateTo, layAddDate.findViewById(R.id.ivElementAddDateTo), false);
    }

    public void setEventType(EventFilterType eventType) {
        mEventType = eventType;
    }

    public void setDates(Long dateFrom, Long dateTo) {
        mDateFrom = dateFrom;
        mDateTo = dateTo;
    }

    public void setOnEventFilterChanged(OnEventFilterChanged onEventFilterChanged) {
        mOnEventFilterChanged = onEventFilterChanged;
    }



    @Override
    protected void onCancelApproveClick() {
        if (mOnEventFilterChanged != null) {
            mOnEventFilterChanged.onEventFilterChanged(EventFilterType.getDefault(), null, null);
        }

        mOnDialogBtnCancelClicked.onDialogBtnCancelClicked(getTag(), "");
    }

    @Override
    protected void onAcceptApproveClick() {
        if (mOnEventFilterChanged != null) {
            mOnEventFilterChanged.onEventFilterChanged(mAdapter.getFinalEventType(), mDateFrom,
                    mDateTo);
        }

        mOnDialogBtnAcceptClicked.onDialogBtnAcceptClicked(getTag(), "");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mAdapter.setSelectedItem(position);
        mAdapter.notifyDataSetChanged();
    }

    private void setCalendarDate(final EditText editText, View ivAddPayDate, final boolean isFrom) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(editText, calendar);
                if (isFrom) {
                    mDateFrom = calendar.getTime().getTime();
                } else {
                    mDateTo = calendar.getTime().getTime();
                }
            }

        };

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                ).show();
            }
        });
        ivAddPayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                ).show();
            }
        });
    }

    private void updateLabel(EditText editText, Calendar calendar) {
        editText.setText(DateUtils.formatDate(calendar.getTime()));
    }

}
