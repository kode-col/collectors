package com.lifecorp.collector.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.R;

public class StubFragment extends BaseFragment {

    private TextView mText;
    private ImageView mImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stub, container, false);
        view.findViewById(R.id.zero_screen_wrapper).setVisibility(View.VISIBLE);

        mText = (TextView) view.findViewById(R.id.zero_screen_text);
        mImage = (ImageView) view.findViewById(R.id.zero_screen_image);

        setDebtorDefaultData();

        return view;
    }

    private void setDebtorDefaultData() {
        setText(R.string.tv_stub);
        setImage(R.drawable.ico_zeroscreen_attention);
    }

    public void setText(int resId) {
        if (mText != null) {
            mText.setText(resId);
        }
    }

    public void setText(String text) {
        if (mText != null) {
            mText.setText(text);
        }
    }

    public void setImage(int resId) {
        if (mImage != null) {
            mImage.setImageResource(resId);
        }
    }

}
