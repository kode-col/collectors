package com.lifecorp.collector.ui.adapters.collection;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBCollection;
import com.lifecorp.collector.ui.activities.MainActivity;
import com.lifecorp.collector.ui.adapters.CollectionImagesAdapter;
import com.lifecorp.collector.ui.adapters.ExpandableAdapter;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderCollectionContent;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderTitle;
import com.lifecorp.collector.utils.DateUtils;
import com.lifecorp.collector.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CollectionExpandableAdapter extends ExpandableAdapter<DBCollection> {

    private MainActivity mMainActivity;

    public CollectionExpandableAdapter(MainActivity mainActivity, List<DBCollection> collections) {
        mMainActivity = mainActivity;
        mData = collections != null ? collections : new ArrayList<DBCollection>();

        if (mData.size() > 0) {
            mExpandIsOpen.append(0, true);
        }
    }

    @Override
    protected View getTitleView(final int position, View convertView, ViewGroup parent) {
        ViewHolderTitle holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_collection_exp_title, parent, false);
            holder = new ViewHolderTitle();

            holder.expIcon = convertView.findViewById(R.id.collection_exp_triangle);
            holder.date = (TextView) convertView.findViewById(R.id.collection_exp_date);
            holder.icon = (ImageView) convertView.findViewById(R.id.collection_exp_icon);
            holder.title = (TextView) convertView.findViewById(R.id.collection_exp_title);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderTitle) convertView.getTag();
        }

        DBCollection item = getItem(position);

        if (item != null) {
            holder.title.setText(capitalize(item.getTitle()));
            holder.date.setText(DateUtils.formatTimestamp(item.getDate()));
            holder.icon.setImageResource(Utils.getImageForType(item.getType()));
        }

        if (isPositionOpen(position)) {
            rotateImmediately(holder.expIcon);
        }

        return convertView;
    }

    @NonNull
    private String capitalize(final String line) {
        return line != null && line.length() >= 2
                ? Character.toUpperCase(line.charAt(0)) + line.substring(1)
                : "";
    }

    @Override
    protected View getContentView(int position, View convertView, ViewGroup parent) {
        ViewHolderCollectionContent holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_collection_exp_content, parent, false);

            holder = new ViewHolderCollectionContent();
            holder.images = (GridView) convertView.findViewById(R.id.collection_exp_images);
            holder.content = (TextView) convertView.findViewById(R.id.collection_exp_content);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderCollectionContent) convertView.getTag();
        }

        DBCollection item = getItem(position);
        if (item != null) {
            holder.content.setText(item.getComment() != null
                    ? item.getComment()
                    : App.getContext().getString(R.string.no_comment));

            if (item.getImagesExternal() != null && item.getImagesExternal().size() > 0) {
                holder.images.setVisibility(View.VISIBLE);
                holder.images.setAdapter(new CollectionImagesAdapter(mMainActivity,
                        item.getImagesExternal()));
            } else {
                holder.images.setAdapter(null);
                holder.images.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

}