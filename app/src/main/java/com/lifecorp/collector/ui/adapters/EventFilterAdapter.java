package com.lifecorp.collector.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderFilterType;
import com.lifecorp.collector.ui.enums.EventFilterType;
import com.lifecorp.collector.ui.view.CustomFontTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander Smirnov on 14.04.15.
 *
 * Адаптер для списка в диалоге {@link com.lifecorp.collector.ui.dialogfragments.MapFilterDialog}
 * отображает различные варианты фильтрации для
 * {@link com.lifecorp.collector.ui.fragments.map.MapWrapperFragment}
 */
public class EventFilterAdapter extends BaseAdapter {

    private final List<EventFilterType> mFilterItemList = new ArrayList<>();
    private int mSelectedItem = -1;
    private Context mContext;

    public EventFilterAdapter(Context context, EventFilterType selectedEventType) {
        mContext = context;
        fillData();
        eventTypeToPosition(selectedEventType);
    }

    private void fillData() {
        mFilterItemList.add(EventFilterType.DATE);
        mFilterItemList.add(EventFilterType.CALL);
        mFilterItemList.add(EventFilterType.MEETING);
    }

    private void eventTypeToPosition(EventFilterType selectedEventType) {
        if (selectedEventType != null) {
            switch (selectedEventType) {
                case DATE:
                    setSelectedItem(0);
                    break;
                case CALL:
                    setSelectedItem(1);
                    break;
                case MEETING:
                    setSelectedItem(2);
                    break;
            }
        }
    }

    public void setSelectedItem(int position) {
        if (mSelectedItem != position) {
            mSelectedItem = position;
        } else {
            mSelectedItem = -1;
        }
    }

    public EventFilterType getFinalEventType() {
        switch (mSelectedItem) {
            case 0:
                return EventFilterType.DATE;
            case 1:
                return EventFilterType.CALL;
            case 2:
                return EventFilterType.MEETING;

        }
        return null;
    }

    @Override
    public int getCount() {
        return mFilterItemList.size();
    }

    @Override
    public EventFilterType getItem(int position) {
        return position < getCount() ? mFilterItemList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderFilterType holder;

        if (convertView == null) {
            int layoutId = R.layout.view_dialog_filter_list_item;
            convertView = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);

            holder = new ViewHolderFilterType();
            holder.title = (CustomFontTextView)
                    convertView.findViewById(R.id.view_dialog_filter_list_item_title);
            holder.divider = convertView.findViewById(R.id.view_dialog_filter_list_item_divider);
            holder.ivChecked = (ImageView)
                    convertView.findViewById(R.id.view_dialog_filter_list_item_checked);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderFilterType) convertView.getTag();
        }

        final EventFilterType item = getItem(position);
        if (item != null) {
            holder.title.setText(mContext.getString(item.getResId()));
            holder.ivChecked.setVisibility(position == mSelectedItem ? View.VISIBLE : View.GONE);
            holder.divider.setVisibility(View.GONE);
        }

        return convertView;
    }

}