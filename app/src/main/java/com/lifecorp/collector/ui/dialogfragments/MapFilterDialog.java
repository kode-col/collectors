package com.lifecorp.collector.ui.dialogfragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.adapters.MapFilterTypeAdapter;
import com.lifecorp.collector.ui.enums.EventType;
import com.lifecorp.collector.ui.interfaces.OnMapFilterChanged;

/**
 * Created by Alexander Smirnov on 13.04.15.
 *
 * Диалог фильтрации, вызывается с экрана карты
 * {@link com.lifecorp.collector.ui.fragments.map.MapWrapperFragment}
 */
public class MapFilterDialog extends BaseDialog implements AdapterView.OnItemClickListener,
        View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private EventType mEventType;
    private MapFilterTypeAdapter mAdapter;
    private SwitchCompat mPromisedPaySwitch;
    private OnMapFilterChanged mOnMapFilterChanged;
    private boolean mIsPromisedPaymentChecked = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FilterDialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.dialog_filter_map, container, false);
        getDialog().getWindow()
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initInterface(rootView);

        return rootView;
    }

    private void initInterface(View rootView) {
        initButtons(rootView);

        mAdapter = new MapFilterTypeAdapter(mEventType);

        ListView mListView = (ListView) rootView.findViewById(R.id.dialog_filter_map_sort);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);

        rootView.findViewById(R.id.dialog_filter_map_promised_pay).setOnClickListener(this);
        mPromisedPaySwitch = (SwitchCompat)
                rootView.findViewById(R.id.dialog_filter_map_promised_pay_checked);
        mPromisedPaySwitch.setOnCheckedChangeListener(this);

        updatePromisedPayChecked();
    }

    private void updatePromisedPayChecked() {
        if (mPromisedPaySwitch != null) {
            mPromisedPaySwitch.setChecked(mIsPromisedPaymentChecked);
        }
    }

    public void setEventType(EventType eventType) {
        mEventType = eventType;
    }

    public void setPromisedPaymentChecked(boolean isPromisedPaymentChecked) {
        mIsPromisedPaymentChecked = isPromisedPaymentChecked;
    }

    public void setOnMapFilterChanged(OnMapFilterChanged onMapFilterChanged) {
        mOnMapFilterChanged = onMapFilterChanged;
    }

    @Override
    protected void onCancelApproveClick() {
        mOnDialogBtnCancelClicked.onDialogBtnCancelClicked(getTag(), "");
    }

    @Override
    protected void onAcceptApproveClick() {
        if (mOnMapFilterChanged != null) {
            mOnMapFilterChanged.onFilterChanged(mEventType);
            mOnMapFilterChanged.onPromisedPaymentChanged(mIsPromisedPaymentChecked);
        }

        mOnDialogBtnAcceptClicked.onDialogBtnAcceptClicked(getTag(), "");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mAdapter.setSelectedItem(position);
        mAdapter.notifyDataSetChanged();

        mEventType = mAdapter.positionToEventType(position);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_filter_map_promised_pay:
                mIsPromisedPaymentChecked = !mIsPromisedPaymentChecked;
                updatePromisedPayChecked();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mIsPromisedPaymentChecked = isChecked;
    }

}
