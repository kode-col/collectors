package com.lifecorp.collector.ui.adapters.collection;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.objects.PhotoArrayContainer;
import com.lifecorp.collector.ui.interfaces.OnPhotoManageListener;

/**
 * Created by Alexander Smirnov on 21.04.15.
 *
 * Адаптер для грида выводящего фотографии прикрепляемые к записи
 */
public class PhotoGridAdapter extends BaseAdapter implements View.OnClickListener {

    private final OnPhotoManageListener mOnPhotoManageListener;
    private PhotoArrayContainer mPhotoContainer;

    public PhotoGridAdapter(OnPhotoManageListener listener, PhotoArrayContainer photoContainer) {
        mOnPhotoManageListener = listener;
        mPhotoContainer = photoContainer;
    }

    public void addPhoto(String path, double latitude, double longitude) {
        if (mPhotoContainer != null) {
            mPhotoContainer.addPhoto(path, latitude, longitude);
            notifyDataSetChanged();
        }
    }

    public void removePhoto(int position) {
        if (mPhotoContainer != null) {
            mPhotoContainer.removePhoto(position);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return mPhotoContainer.getCount();
    }

    @Override
    public Bitmap getItem(int position) {
        return mPhotoContainer.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderPhoto holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_grid_photo_item, parent, false);

            holder = new ViewHolderPhoto();
            holder.photoWrapper = convertView.findViewById(R.id.grid_photo_item_photo_wrapper);
            holder.imageView = (ImageView) convertView.findViewById(R.id.grid_photo_item_photo);
            holder.removePhoto = convertView.findViewById(R.id.grid_photo_item_remove_photo);
            holder.addPhoto = convertView.findViewById(R.id.grid_photo_item_add_photo);
            holder.removePhoto.setOnClickListener(this);
            holder.addPhoto.setOnClickListener(this);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderPhoto) convertView.getTag();
        }

        Bitmap photo = getItem(position);
        if (photo == null) {
            holder.photoWrapper.setVisibility(View.GONE);
            holder.addPhoto.setVisibility(position == 0 ? View.VISIBLE : View.GONE);
            holder.imageView.setImageBitmap(null);
        } else {
            holder.photoWrapper.setVisibility(View.VISIBLE);
            holder.addPhoto.setVisibility(View.GONE);
            holder.imageView.setImageBitmap(photo);
        }

        holder.addPhoto.setTag(position);
        holder.removePhoto.setTag(position);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.grid_photo_item_add_photo:
                if (mOnPhotoManageListener != null) {
                    mOnPhotoManageListener.makePhoto();
                }
                break;
            case R.id.grid_photo_item_remove_photo:
                removePhoto((Integer) v.getTag());
                break;
        }
    }

    public class ViewHolderPhoto {
        View addPhoto, removePhoto, photoWrapper;
        ImageView imageView;
    }

}
