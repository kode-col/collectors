package com.lifecorp.collector.ui.adapters.event;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.ui.enums.EventFilterType;
import com.lifecorp.collector.ui.fragments.event.EventListFragment;
import com.lifecorp.collector.ui.view.PagerSlidingTabStrip;
import com.lifecorp.collector.utils.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Alexander Smirnov on 23.03.15.
 *
 * Адаптер для {@link com.lifecorp.collector.ui.fragments.event.EventsPlanFragment}
 */
public class EventsPagerAdapter extends FragmentStatePagerAdapter implements
        PagerSlidingTabStrip.CustomTabProvider {

    private int mExpiredCount;
    private final String[] mCollectionTitles;
    private final List<EventFilterType> mFilter;

    private List<DBEvent> mActiveEvents;
    private List<DBEvent> mExpiredEvents;
    private List<DBEvent> mCompletedEvents;

    private EventListFragment mFragment1;
    private EventListFragment mFragment2;
    private EventListFragment mFragment3;

    public EventsPagerAdapter(FragmentManager fm, List<DBEvent> events,
                              List<EventFilterType> filter) {
        super(fm);

        Resources res = App.getContext().getResources();
        mCollectionTitles = res.getStringArray(R.array.collection_titles_events);

        setData(events);
        mFilter = filter;
    }

    private void createNewLists() {
        mActiveEvents = new ArrayList<>();
        mCompletedEvents = new ArrayList<>();
        mExpiredEvents = new ArrayList<>();
    }

    public void setData(List<DBEvent> allEvents){
        createNewLists();

        final long now = Calendar.getInstance().getTime().getTime();
        for (DBEvent event : allEvents) {
            if (!event.isCompleted()) { // если мероприятие не завершенное
                if (event.getTime() < now) { // если мероприятие просроченное
                    mExpiredEvents.add(event);
                } else { // иначе просто мероприятие активное
                    mActiveEvents.add(event);
                }
            } else { // если мероприятие завершенное
                mCompletedEvents.add(event);
            }
        }

        mExpiredCount = mExpiredEvents.size();
    }

    @Nullable
    private EventFilterType getFilter(int position) {
        return mFilter != null && mFilter.size() == 3 ? mFilter.get(position) : null;
    }

    @Override
    public Fragment getItem(int position) {
        EventListFragment fragment = null;
        switch (position) {
            case 0:
                if (mFragment1 == null) {
                    fragment = EventListFragment.newInstance(position, getFilter(0));
                    fragment.setEvents(mActiveEvents);
                    mFragment1 = fragment;
                } else {
                    fragment = mFragment1;
                }
                break;
            case 1:
                if (mFragment2 == null) {
                    fragment = EventListFragment.newInstance(position, getFilter(1));
                    fragment.setEvents(mCompletedEvents);
                    mFragment2 = fragment;
                } else {
                    fragment = mFragment2;
                }
                break;
            case 2:
                if (mFragment3 == null) {
                    fragment = EventListFragment.newInstance(position, getFilter(2));
                    fragment.setEvents(mExpiredEvents);
                    mFragment3 = fragment;
                } else {
                    fragment = mFragment3;
                }
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return mCollectionTitles.length;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mCollectionTitles[position];
    }

    @Override
    public int getCount(int position) {
        return position == 2 ? mExpiredCount : 0;
    }

    @Override
    public boolean isCustom(int position) {
        return position == 2;
    }

    public void refreshFragmentInfo(int position, EventFilterType filterType, Long dateFrom,
                                    Long dateTo) {
        EventListFragment fragment = null;

        switch (position) {
            case 0:
                fragment = mFragment1;
                if (fragment != null) {
                    fragment.setEvents(mActiveEvents);
                }
                break;
            case 1:
                fragment = mFragment2;
                if (fragment != null) {
                    fragment.setEvents(mCompletedEvents);
                }
                break;
            case 2:
                fragment = mFragment3;
                if (fragment != null) {
                    fragment.setEvents(mExpiredEvents);
                }
                break;
        }

        if (fragment != null) {
            boolean isSingleDate = DateUtils.isSingleDate(dateFrom, dateTo);
            fragment.fillList(filterType, dateFrom, dateTo, isSingleDate);
        }
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        // Ничего не делать и не вызывать super.restoreState()
    }
}
