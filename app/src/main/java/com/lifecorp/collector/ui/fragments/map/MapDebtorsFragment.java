package com.lifecorp.collector.ui.fragments.map;

import android.location.Location;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.objects.address.AddressContainer;
import com.lifecorp.collector.ui.adapters.MapInfoWindowAdapter;
import com.lifecorp.collector.ui.interfaces.OnMapUpdatedListener;
import com.lifecorp.collector.utils.LocationHelper;

import java.util.HashMap;
import java.util.Map;

public class MapDebtorsFragment extends MapBaseFragment implements View.OnClickListener,
        GoogleMap.OnInfoWindowClickListener {

    private OnMapUpdatedListener mOnMapUpdatedListener;

    private MapInfoWindowAdapter mMapInfoWindowAdapter;
    private Map<Marker, Object> mMarkerMap = new HashMap<>();

    public static MapDebtorsFragment newInstance(OnMapUpdatedListener onMapLoadedListener) {
        MapDebtorsFragment fragment = new MapDebtorsFragment();
        fragment.setOnMapLoadedListener(onMapLoadedListener);
        return fragment;
    }

    public void showInfoWindowForAddress(AddressContainer addressContainer) {
        Marker marker = null;

        if (addressContainer != null) {
            for (Map.Entry<Marker, Object> item : mMarkerMap.entrySet()) {
                if (item.getValue() instanceof AddressContainer) {
                    if (addressContainer == item.getValue()) {
                        marker = item.getKey();
                        break;
                    }
                }
            }

            showInfoWindowForMarker(marker);
        } else {
            makeToast(R.string.msg_error);
        }
    }

    public void showInfoWindowForDebtor(DBDebtorProfile pressedDeb) {
        Marker marker = null;

        if (pressedDeb != null) {
            for (Map.Entry<Marker, Object> item : mMarkerMap.entrySet()) {
                if (item.getValue() instanceof DBEvent) {
                    if (pressedDeb == ((DBEvent) item.getValue()).getDebtorProfile()) {
                        marker = item.getKey();
                        break;
                    }
                } else if (item.getValue() instanceof DBDebtorProfile) {
                    if (pressedDeb.getId() == ((DBDebtorProfile) item.getValue()).getId()) {
                        marker = item.getKey();
                        break;
                    }
                }
            }

            showInfoWindowForMarker(marker);
        } else {
            makeToast(R.string.msg_error);
        }
    }

    private void showInfoWindowForMarker(Marker marker) {
        if (marker != null && getMap() != null) {
            getMap().animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
            marker.showInfoWindow();
        } else {
            makeToast(R.string.msg_error);
        }
    }

    @Override
    protected void initMapAfterAdding() {
        if (getMap() == null) {
            return;
        }

        mMapInfoWindowAdapter = new MapInfoWindowAdapter(getActivity(), mOnMapUpdatedListener);

        getMap().setInfoWindowAdapter(mMapInfoWindowAdapter);
        getMap().setOnInfoWindowClickListener(this);

        if (mOnMapUpdatedListener != null) {
            mOnMapUpdatedListener.onMapLoaded();
        }
    }

    @Override
    public void onClick(View v) {
        if (getMap() == null) {
            return;
        }

        switch (v.getId()) {
            case R.id.ivMapZoomIn:
                getMap().animateCamera(CameraUpdateFactory.zoomIn());
                break;
            case R.id.ivMapZoomOut:
                getMap().animateCamera(CameraUpdateFactory.zoomOut());
                break;
            case R.id.ivMapMyPos:
                if (getMap().getMyLocation() != null) {
                    Location location = getMap().getMyLocation();
                    LatLng myLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    getMap().animateCamera(CameraUpdateFactory.newLatLng(myLatLng));
                }

                if (!LocationHelper.getInstance().isLocationServicesEnabled()) {
                    getBaseActivity().createEnableLocationServiceDialog();
                }
                break;
        }
    }

    public void clearMarkers() {
        if (getMap() != null) {
            getMap().clear();
            mMarkerMap.clear();

            if (mMapInfoWindowAdapter != null) {
                mMapInfoWindowAdapter.clearCache();
            }
        }
    }

    public void addMarkers(final Map<MarkerOptions, Object> mMarkersOptionsMap) {
        clearMarkers();

        if (mMarkersOptionsMap != null && getMap() != null) {
            for (Map.Entry<MarkerOptions, Object> item : mMarkersOptionsMap.entrySet()) {
                final Marker marker = getMap().addMarker(item.getKey());
                mMarkerMap.put(marker, item.getValue());
            }
        }
    }

    public void setOnMapLoadedListener(OnMapUpdatedListener onMapLoadedListener) {
        mOnMapUpdatedListener = onMapLoadedListener;
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.fragment_map;
    }

    @Override
    protected int getMapReplaceId() {
        return R.id.fragment_map_map;
    }

    @Override
    protected void initButtons(View rootView) {
        if (rootView != null) {
            rootView.findViewById(R.id.ivMapZoomIn).setOnClickListener(this);
            rootView.findViewById(R.id.ivMapZoomOut).setOnClickListener(this);
            rootView.findViewById(R.id.ivMapMyPos).setOnClickListener(this);
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        marker.hideInfoWindow();

        if (mOnMapUpdatedListener != null) {
            mOnMapUpdatedListener.onInfoWindowClicked(marker.getTitle());
        }
    }

}
