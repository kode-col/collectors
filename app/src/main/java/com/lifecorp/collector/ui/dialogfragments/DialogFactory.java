package com.lifecorp.collector.ui.dialogfragments;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.enums.EventFilterType;
import com.lifecorp.collector.ui.enums.EventType;
import com.lifecorp.collector.ui.interfaces.OnEventFilterChanged;
import com.lifecorp.collector.ui.interfaces.OnMapFilterChanged;

public final class DialogFactory {

    private static class SingletonHolder {
        public static final DialogFactory INSTANCE = new DialogFactory();
    }

    public static DialogFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private DialogFactory() {
    }

    public BaseDialog createCallDialog(String fio, String phone) {
        CommonDialog callDialog = new CommonDialog();
        callDialog.setTitle(App.getContext().getString(R.string.call_dialog_title));
        callDialog.setDescription(fio + "\n" + phone);
        callDialog.setBtnCancelText(App.getContext().getString(R.string.cancel_action));
        callDialog.setBtnAcceptText(App.getContext().getString(R.string.call_dialog_accept));
        callDialog.setPhoneNumber(phone);
        callDialog.setCancelable(true);
        return callDialog;
    }

    public BaseDialog createEnableLocationServiceDialog() {
        CommonDialog mapDialog = new CommonDialog();
        mapDialog.setTitle(App.getContext().getString(R.string.map_dialog_title));
        mapDialog.setDescription(App.getContext().getString(R.string.map_dialog_desc));
        mapDialog.setBtnCancelText(App.getContext().getString(R.string.map_dialog_cancel));
        mapDialog.setBtnAcceptText(App.getContext().getString(R.string.map_dialog_accept));
        mapDialog.setCancelable(true);
        return mapDialog;
    }

    public BaseDialog createLocationSearchProblemDialog() {
        CommonDialog dialog = new CommonDialog();
        dialog.setTitle(App.getContext().getString(R.string.map_dialog_title));
        dialog.setDescription(App.getContext().getString(R.string.error_msg_location_not_found));
        dialog.setBtnCancelText(App.getContext().getString(R.string.cancel));
        dialog.setBtnAcceptText(App.getContext().getString(R.string.ok));
        dialog.setCancelable(true);
        return dialog;
    }

    public MapFilterDialog createFilterDialog(EventType eventType, boolean isPromisedPaymentChecked,
                                           OnMapFilterChanged onMapFilterChanged) {
        MapFilterDialog filterDialog = new MapFilterDialog();
        filterDialog.setEventType(eventType);
        filterDialog.setPromisedPaymentChecked(isPromisedPaymentChecked);
        filterDialog.setOnMapFilterChanged(onMapFilterChanged);
        return filterDialog;
    }

    public FilterEventsDialog createFilterEventsDialog(EventFilterType eventType, Long dateFrom,
                                                       Long dateTo,
                                                       OnEventFilterChanged onEventFilterChanged) {
        FilterEventsDialog filterDialog = new FilterEventsDialog();
        filterDialog.setEventType(eventType);
        filterDialog.setDates(dateFrom, dateTo);
        filterDialog.setOnEventFilterChanged(onEventFilterChanged);
        return filterDialog;
    }

}
