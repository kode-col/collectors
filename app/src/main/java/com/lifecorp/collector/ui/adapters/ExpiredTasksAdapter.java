package com.lifecorp.collector.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.ui.activities.BaseActivity;
import com.lifecorp.collector.ui.adapters.holder.ViewHolderExpiredTasks;
import com.lifecorp.collector.utils.DateUtils;
import com.lifecorp.collector.utils.ToastHelper;
import com.lifecorp.collector.utils.Utils;

import java.util.List;

public class ExpiredTasksAdapter extends BaseAdapter {

    private List<DBEvent> mEvents;

    public ExpiredTasksAdapter(List<DBEvent> events) {
        mEvents = events;
    }

    @Override
    public int getCount() {
        return mEvents != null ? mEvents.size() : 0;
    }

    @Override
    public DBEvent getItem(int position) {
        return position >= 0 && position < getCount() ? mEvents.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        ViewHolderExpiredTasks holder;

        if (v == null) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listitem_collection_expired_tasks, parent, false);

            holder = new ViewHolderExpiredTasks();
            holder.tvColExpiredDateNumber = (TextView) v.findViewById(R.id.listitem_expired_date);
            holder.tvColExpiredComment = (TextView) v.findViewById(R.id.listitem_expired_comment);
            holder.ivColExpiredType = (ImageView) v.findViewById(R.id.listitem_expired_type);
            holder.btnExpiredWrite = v.findViewById(R.id.listitem_expired_write);

            holder.btnExpiredWrite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getTag() != null) {
                        DBEvent dbEvent = (DBEvent) v.getTag();

                        if (dbEvent.getDebtorProfile() != null) {
                            ((BaseActivity) v.getContext()).createCompleteEventFragment(dbEvent);
                        } else {
                            ToastHelper.getInstance().makeToast(v.getContext(), R.string.msg_error);
                        }
                    } else {
                        ToastHelper.getInstance().makeToast(v.getContext(), R.string.msg_error);
                    }
                }
            });

            v.setTag(holder);
        } else {
            holder = (ViewHolderExpiredTasks) v.getTag();
        }

        DBEvent item = getItem(position);

        if (item != null) {
            holder.btnExpiredWrite.setTag(item);
            holder.tvColExpiredDateNumber.setText(DateUtils.formatTimestamp(item.getTime()));
            holder.ivColExpiredType.setImageResource(Utils.getExpiredImageForType(item.getType()));

            holder.tvColExpiredComment.setText(item.getDescription() != null
                            ? item.getDescription()
                            : App.getContext().getString(R.string.no_comment)
            );
        }

        return v;
    }

}
