package com.lifecorp.collector.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.lifecorp.collector.App;

import java.util.ArrayList;
import java.util.List;

/**
 * LinearLayout заменяет GridView, необходим для избежания ситуации реализации List в List-е
 **/
public class FakeGridView extends LinearLayout {

    private BaseAdapter mAdapter;

    private int mColumnNumber = 2; // стандартно 2 столбца
    private int mVerticalSpacing = 20; // стандартно 20dp
    private int mHorizontalSpacing = 15; // стандартно 15dp

    private final float mScale = App.getContext().getResources().getDisplayMetrics().density;

    private List<LinearLayout> mLinearLayoutList;

    public FakeGridView(Context context) {
        super(context);

        setDefaultSpacing();
    }

    public FakeGridView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDefaultSpacing();
    }

    public FakeGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setDefaultSpacing();
    }

    private void setDefaultSpacing() {
        setColumnNumber(mColumnNumber);
        setVerticalSpacing(mVerticalSpacing);
        setHorizontalSpacing(mHorizontalSpacing);
    }

    private int dpToPixels(int dp) {
        return (int) (dp * mScale + 0.5f);
    }

    public void setVerticalSpacing(int dp) {
        mVerticalSpacing = dpToPixels(dp);
    }

    public void setHorizontalSpacing(int dp) {
        mHorizontalSpacing = dpToPixels(dp);
    }

    public int getVerticalSpacing() {
        return mVerticalSpacing;
    }

    public int getHorizontalSpacing() {
        return mHorizontalSpacing;
    }

    public void setAdapter(BaseAdapter adapter) {
        mAdapter = adapter;
    }

    public int getColumnNumber() {
        return mColumnNumber;
    }

    public void setColumnNumber(int column) {
        mColumnNumber = column;
    }

    public void drawView() {
        removeAllViews();
        setOrientation(VERTICAL);

        mLinearLayoutList = new ArrayList<>();

        if (mAdapter != null) {
            final int size = mAdapter.getCount();

            int row = -1;
            for (int i = 0; i < size; i++) {
                if (i % getColumnNumber() == 0) {
                    addNewRow();
                    row++;
                }
                addViewToRow(mLinearLayoutList.get(row), mAdapter.getView(i, null, this));
            }

            checkRowForColumnsNumber(row, size);
        }
    }

    private void checkRowForColumnsNumber(int row, int size) {
        if (mLinearLayoutList.size() > row && size % getColumnNumber() != 0) {
            int elementsForAdd = getColumnNumber() - (size % getColumnNumber());

            for (;elementsForAdd > 0; elementsForAdd--) {
                addViewToRow(mLinearLayoutList.get(row), getFakeView());
            }
        }
    }

    private View getFakeView() {
        LayoutParams lp = new LayoutParams(0, LayoutParams.MATCH_PARENT, 1f);
        View view = new View(getContext());
        view.setLayoutParams(lp);
        return view;
    }

    private void addViewToRow(LinearLayout row, View view) {
        if (row != null && view != null) {
            if (row.getChildCount() != 0) {
                row.addView(getNewHorizontalSpacing());
            }

            row.addView(view);
        }
    }

    private View getNewVerticalSpacingView() {
        return createView(new LayoutParams(getHorizontalSpacing(), 1));
    }

    private View getNewHorizontalSpacing() {
        return createView(new LayoutParams(1, getVerticalSpacing()));
    }

    private View createView(LayoutParams layoutParams) {
        View view = new View(getContext());
        view.setLayoutParams(layoutParams);
        return view;
    }

    private void addNewRow() {
        if (getChildCount() != 0) {
            addView(getNewVerticalSpacingView());
        }

        LinearLayout linearLayout = getNewRow();
        mLinearLayoutList.add(linearLayout);
        addView(linearLayout);
    }

    private LinearLayout getNewRow() {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setLayoutParams(lp);
        return linearLayout;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);

            getLayoutParams().height = getMeasuredHeight();
    }

}
