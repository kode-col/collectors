package com.lifecorp.collector.ui.dialogfragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lifecorp.collector.R;

public class CommonDialog extends BaseDialog {

    private final String DESC_TEXT = "mDescText";
    private final String TITLE_TEXT = "mTitleText";
    private final String CANCEL_TEXT = "mCancelText";
    private final String ACCEPT_TEXT = "mAcceptText";
    private final String NUMBER_TEXT = "phoneNumber";

    private String mDescText = "";
    private String mTitleText = "";
    private String mCancelText = "";
    private String mAcceptText = "";
    private String phoneNumber = "";

    private TextView tvTitle, tvDesc, tvCancel, tvAccept;
    private View layTitle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.dialog_base, container, false);

        if (savedInstanceState != null) {
            mTitleText = savedInstanceState.getString(TITLE_TEXT);
            mDescText = savedInstanceState.getString(DESC_TEXT);
            mCancelText = savedInstanceState.getString(CANCEL_TEXT);
            mAcceptText = savedInstanceState.getString(ACCEPT_TEXT);
            phoneNumber = savedInstanceState.getString(NUMBER_TEXT);
        }

        initInterface(rootView);

        return rootView;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TITLE_TEXT, mTitleText);
        outState.putString(DESC_TEXT, mDescText);
        outState.putString(CANCEL_TEXT, mCancelText);
        outState.putString(ACCEPT_TEXT, mAcceptText);
        outState.putString(NUMBER_TEXT, phoneNumber);
    }

    public void setTitle(String titleText) {
        mTitleText = titleText;
        if (tvTitle != null) {
            tvTitle.setText(titleText);
            layTitle.setVisibility(View.VISIBLE);
        }
    }

    public void setDescription(String descText) {
        mDescText = descText;
        if (tvDesc != null) {
            tvDesc.setText(descText);
        }
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setBtnCancelText(String cancelText) {
        mCancelText = cancelText;

        if (tvCancel != null) {
            tvCancel.setText(cancelText);
            tvCancel.setVisibility(View.VISIBLE);
        }
    }

    public void setBtnAcceptText(String acceptText) {
        mAcceptText = acceptText;

        if (tvAccept != null) {
            tvAccept.setText(acceptText);
            tvAccept.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onCancelApproveClick() {
        mOnDialogBtnCancelClicked.onDialogBtnCancelClicked(getTag(), phoneNumber);
    }

    @Override
    protected void onAcceptApproveClick() {
        mOnDialogBtnAcceptClicked.onDialogBtnAcceptClicked(getTag(), phoneNumber);
    }

    private void initInterface(View rootView) {
        layTitle = rootView.findViewById(R.id.layDialogTitle);
        tvDesc = (TextView) rootView.findViewById(R.id.tvDialogDescription);
        tvTitle = (TextView) rootView.findViewById(R.id.tvDialogTitle);
        tvCancel = (TextView) rootView.findViewById(R.id.tvDialogCancel);
        tvAccept = (TextView) rootView.findViewById(R.id.tvDialogAccept);

        if (mTitleText != null) {
            setTitle(mTitleText);
        }
        if (mDescText != null) {
            setDescription(mDescText);
        }
        if (mCancelText != null) {
            setBtnCancelText(mCancelText);
        }
        if (mAcceptText != null) {
            setBtnAcceptText(mAcceptText);
        }

        tvCancel.setOnClickListener(getOnCancelClickListener());
        tvAccept.setOnClickListener(getOnAcceptClickListener());
    }

}
