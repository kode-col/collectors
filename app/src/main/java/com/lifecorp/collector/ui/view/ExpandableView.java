package com.lifecorp.collector.ui.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.lifecorp.collector.R;
import com.lifecorp.collector.ui.interfaces.OnToggleListener;

/**
 * Created by Alexander Smirnov on 02.04.15.
 *
 * Вид содержащий два вида - заголовок и контент, изначально отображается только заголовок
 * при клике на заголовок используя анимацию отображается контент
 */
public class ExpandableView extends LinearLayout implements View.OnClickListener {

    private int mPosition;
    private View mTitleView;
    private View mContentView;
    private FrameLayout mTitleContainer;
    private FrameLayout mContentContainer;
    private OnToggleListener mOnToggleListener;

    public ExpandableView(Context context, View titleView, View contentView,
                          OnToggleListener onToggleListener, int position) {
        super(context);

        mPosition = position;
        mTitleView = titleView;
        mContentView = contentView;
        mOnToggleListener = onToggleListener;

        init();
    }

    public ExpandableView(Context context) {
        super(context);

        init();
    }

    public ExpandableView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public ExpandableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        if (!isInEditMode()) {
            inflate(getContext(), R.layout.list_item_expandable_card, this);

            mTitleContainer = (FrameLayout) findViewById(R.id.list_item_expandable_card_title);
            mContentContainer = (FrameLayout) findViewById(R.id.list_item_expandable_card_content);

            mTitleContainer.setOnClickListener(this);
            setData();
        }
    }

    public boolean isExpanded() {
        return mContentContainer != null && mContentContainer.getVisibility() != View.GONE;
    }

    private void setData() {
        if (mTitleView != null) {
            mTitleContainer.addView(mTitleView);
        }
        if (mContentView != null) {
            mContentContainer.addView(mContentView);
        }
    }

    private void startExpand() {
        if (mContentContainer != null) {
            mContentContainer.setVisibility(View.VISIBLE);

            final int widthSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
            final int heightSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
            mContentContainer.measure(widthSpec, heightSpec);

            ValueAnimator mAnimator = slideAnimator(0, mContentContainer.getMeasuredHeight());
            mAnimator.addListener(new AnimatorListener() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    ViewGroup.LayoutParams layoutParams = mContentContainer.getLayoutParams();
                    layoutParams.height = LayoutParams.WRAP_CONTENT;
                    mContentContainer.setLayoutParams(layoutParams);
                }

            });

            mAnimator.start();
        }
    }

    private void startCollapse() {
        if (mContentContainer != null) {
            int finalHeight = mContentContainer.getHeight();

            ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

            mAnimator.addListener(new AnimatorListener() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    mContentContainer.setVisibility(View.GONE);
                }

            });

            mAnimator.start();
        }
    }

    private ValueAnimator slideAnimator(int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (Integer) animation.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = mContentContainer.getLayoutParams();
                layoutParams.height = value;
                mContentContainer.setLayoutParams(layoutParams);
            }
        });

        return animator;
    }

    public void open() {
        if (mContentContainer != null) {
            ViewGroup.LayoutParams layoutParams = mContentContainer.getLayoutParams();
            layoutParams.height = LayoutParams.WRAP_CONTENT;
            mContentContainer.setLayoutParams(layoutParams);
            mContentContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        boolean isExpanded = isExpanded();
        if (isExpanded) {
            startCollapse();
        } else {
            startExpand();
        }

        if (mOnToggleListener != null) {
            mOnToggleListener.onToggle(mTitleView, mPosition, !isExpanded);
        }
    }

    private abstract class AnimatorListener implements Animator.AnimatorListener {

        @Override
        public void onAnimationStart(Animator animation) {
            // ничего
        }

        @Override
        public void onAnimationCancel(Animator animation) {
            // ничего
        }

        @Override
        public void onAnimationRepeat(Animator animation) {
            // ничего
        }
    }

}
