package com.lifecorp.collector.ui.interfaces;

/**
 * Created by Alexander Smirnov on 22.04.15.
 *
 * Интерфейс управления фотографиями для прикрепления к коллекции
 */
public interface OnPhotoManageListener {

    void makePhoto();

}
