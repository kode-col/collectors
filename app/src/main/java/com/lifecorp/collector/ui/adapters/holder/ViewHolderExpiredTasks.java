package com.lifecorp.collector.ui.adapters.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Alexander Smirnov on 24.03.15.
 *
 * ViewHolder для {@link com.lifecorp.collector.ui.adapters.ExpiredTasksAdapter}
 */
public class ViewHolderExpiredTasks {
    public View btnExpiredWrite;
    public ImageView ivColExpiredType;
    public TextView tvColExpiredComment;
    public TextView tvColExpiredDateNumber;
}
