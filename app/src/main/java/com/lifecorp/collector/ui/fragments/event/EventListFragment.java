package com.lifecorp.collector.ui.fragments.event;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.objects.event.Event;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.ui.adapters.event.EventListAdapter;
import com.lifecorp.collector.ui.enums.EventFilterType;
import com.lifecorp.collector.ui.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class EventListFragment extends BaseFragment {

    private static final String TAG_POSITION = "TAG_POSITION";
    private static final int TAB_EXECUTED_POSITION = 1;

    private int mPosition = 0;
    private View mZeroScreen;
    private List<DBEvent> mEvents;
    private EventFilterType mFilter;
    private TextView mZeroScreenText;
    private ImageView mZeroScreenImage;
    private boolean mIsSingleDate = false;
    private StickyListHeadersListView mListView;

    public static EventListFragment newInstance(final int position, EventFilterType filterType) {
        EventListFragment fragment = new EventListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TAG_POSITION, position);
        fragment.setArguments(bundle);
        fragment.setFilter(filterType);
        return fragment;
    }

    public void setEvents(List<DBEvent> events) {
        mEvents = events;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        if (getArguments() != null) {
            mPosition = getArguments().getInt(TAG_POSITION);
        }

        View view = inflater.inflate(R.layout.fragment_event_list, container, false);
        mListView = (StickyListHeadersListView) view.findViewById(R.id.list_events);

        setZeroScreen(view);

        fillList(mFilter, null, null, false);

        return view;
    }

    private void setZeroScreen(View view) {
        mZeroScreen = view.findViewById(R.id.zero_screen_wrapper);
        mZeroScreenImage = (ImageView) mZeroScreen.findViewById(R.id.zero_screen_image);
        mZeroScreenText = (TextView) mZeroScreen.findViewById(R.id.zero_screen_text);
    }

    public void fillList(EventFilterType type, Long dateFrom, Long dateTo, boolean isSingleDate) {
        mIsSingleDate = isSingleDate;

        if (mEvents != null && mListView != null) {
            if (type == null || type == EventFilterType.MEETING || type == EventFilterType.CALL) {
                fillListType(type, dateFrom, dateTo);
            } else if (type == EventFilterType.DATE) {
                fillListDate(dateFrom, dateTo);
            }

            updateCalendar();
        }
    }

    // если показываем мероприятия для выбранного дня
    private boolean isSelectDate(Long dateFrom, Long dateTo) {
        return dateFrom != null && dateFrom.equals(dateTo);
    }

    private void fillListType(EventFilterType type, Long dateFrom, Long dateTo) {
        List<DBEvent> sortedEventsList = sortByDate(filterEventsByType(mEvents, type));
        setAdapterWithData(filterByDates(sortedEventsList, dateFrom, dateTo), dateFrom, dateTo);
    }

    private void fillListDate(Long dateFrom, Long dateTo) {
        setAdapterWithData(filterByDates(sortByDate(mEvents), dateFrom, dateTo), dateFrom, dateTo);
    }

    private void setAdapterWithData(List<DBEvent> events, Long dateFrom, Long dateTo) {
        if (events != null) {
            mListView.setAdapter(new EventListAdapter(getMainActivity(), events, mPosition,
                    mIsSingleDate));
            showZeroScreen(events.size() == 0, isSelectDate(dateFrom, dateTo));
        }
    }

    private void showZeroScreen(boolean isEmpty, boolean isDay) {
        mZeroScreen.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
        mListView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        setZeroScreenData(isDay);
    }

    private void setZeroScreenData(boolean isDay) {
        boolean isSynchronized = DBCachePoint.getInstance().isSynchronized();

        mZeroScreenImage.setImageResource(getZeroScreenImage(isSynchronized));
        mZeroScreenText.setText(getZeroScreenText(isSynchronized, isDay));
    }

    private int getZeroScreenImage(boolean isSynchronized) {
        return isSynchronized
                ? R.drawable.ico_zeroscreen_no_tasks
                : R.drawable.ico_zeroscreen_attention;
    }

    private String getZeroScreenText(boolean isSynchronized, boolean isDay) {
        return !isSynchronized
                ? getString(R.string.msg_need_synchronized)
                : (isDay ? getString(R.string.msg_no_data_at_day) : getZeroScreenEmptyText());
    }

    @NonNull
    private String getZeroScreenEmptyText() {
        String[] msg = getResources().getStringArray(R.array.msg_collection_no_data);

        return msg != null && msg.length > mPosition
                ? msg[mPosition]
                : getString(R.string.msg_error);
    }

    public void updateCalendar() {
        if (mEvents != null) {
            final Collection<Date> highlightedDates = new ArrayList<>(mEvents.size());
            for (DBEvent event : mEvents) {
                highlightedDates.add(new Date(event.getTime()));
            }

            try {
                EventsPlanFragment.highlightDatesAndSelect(highlightedDates, mPosition);
            } catch (IllegalStateException e) {
                ///ignore, exception is caused by highlighting date out of range
            }
        }
    }

    private List<DBEvent> filterByDates(List<DBEvent> events, Long dateFrom, Long dateTo) {
        if (dateFrom == null && dateTo == null) {
            return events;
        }

        final List<DBEvent> filteredEvents = new ArrayList<>();
        final Calendar eventCalendar = Calendar.getInstance();
        Calendar calendarFrom = null;
        Calendar calendarTo = null;

        if (dateFrom != null) {
            calendarFrom = Calendar.getInstance();
            calendarFrom.setTimeInMillis(dateFrom);
        }
        if (dateTo != null) {
            calendarTo = Calendar.getInstance();
            calendarTo.setTimeInMillis(dateTo);
        }

        if (calendarFrom != null && calendarTo != null && isCurrentDay(calendarFrom, calendarTo)) {
            for (DBEvent event : events) {
                eventCalendar.setTimeInMillis(event.getTime());

                if (isCurrentDay(calendarFrom, eventCalendar)) {
                    filteredEvents.add(event);
                }
            }
        } else {
            for (DBEvent event : events) {
                eventCalendar.setTimeInMillis(event.getTime());

                if ((calendarTo == null || isOverDay(calendarTo, eventCalendar))
                        && (calendarFrom == null || isUnderDay(calendarFrom, eventCalendar))) {
                    filteredEvents.add(event);
                }
            }
        }

        return filteredEvents;
    }

    private List<DBEvent> filterEventsByType(Collection<DBEvent> events, EventFilterType type) {
        List<DBEvent> filteredEvents;
        if (type == null) {
            filteredEvents = new ArrayList<>(events);
        } else {
            filteredEvents = new ArrayList<>();
            int intType = 0;

            switch (type) {
                case CALL:
                    intType = Event.TYPE_CALL;
                    break;
                case MEETING:
                    intType = Event.TYPE_MEETING;
                    break;
                default:
                    break;
            }

            for (DBEvent event : events) {
                if (event.getType() == intType) {
                    filteredEvents.add(event);
                }
            }
        }

        return filteredEvents;
    }

    private List<DBEvent> sortByDate(Collection<DBEvent> events) {
        final List<DBEvent> filteredEvents = new ArrayList<>(events);
        Collections.sort(filteredEvents, new Comparator<DBEvent>() {
            public int compare(DBEvent o1, DBEvent o2) {
                long lhs = TAB_EXECUTED_POSITION != mPosition ? o1.getTime() : o2.getTime();
                long rhs = TAB_EXECUTED_POSITION != mPosition ? o2.getTime() : o1.getTime();

                return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
            }
        });
        return filteredEvents;
    }

    private boolean isCurrentDay(Calendar first, Calendar second) {
        return first.get(Calendar.YEAR) == second.get(Calendar.YEAR)
                && first.get(Calendar.DAY_OF_YEAR) == second.get(Calendar.DAY_OF_YEAR);
    }

    private boolean isUnderDay(Calendar first, Calendar second) {
        return first.get(Calendar.YEAR) == second.get(Calendar.YEAR)
                && first.get(Calendar.DAY_OF_YEAR) <= second.get(Calendar.DAY_OF_YEAR);
    }

    private boolean isOverDay(Calendar first, Calendar second) {
        return first.get(Calendar.YEAR) == second.get(Calendar.YEAR)
                && first.get(Calendar.DAY_OF_YEAR) >= second.get(Calendar.DAY_OF_YEAR);
    }

    public void setFilter(EventFilterType filter) {
        mFilter = filter;
    }

}
