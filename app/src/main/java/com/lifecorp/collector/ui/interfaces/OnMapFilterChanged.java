package com.lifecorp.collector.ui.interfaces;

import com.lifecorp.collector.ui.enums.EventType;

/**
 * Created by Alexander Smirnov on 14.04.15.
 *
 * Интерфейс изменения состояния фильтра
 */
public interface OnMapFilterChanged {

    void onFilterChanged(EventType eventType);

    void onPromisedPaymentChanged(boolean state);

}
