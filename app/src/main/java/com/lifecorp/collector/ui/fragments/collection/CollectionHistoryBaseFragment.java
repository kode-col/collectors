package com.lifecorp.collector.ui.fragments.collection;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.ui.fragments.BaseFragment;
import com.lifecorp.collector.utils.Const;

import java.util.List;

/**
 * Created by Alexander Smirnov on 28.04.15.
 *
 * Базовый класс для фрагментов истории коллекций
 */
public abstract class CollectionHistoryBaseFragment<T> extends BaseFragment {

    private View mEmptyWrapper;
    private ListView mListView;

    private int mDebtorId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_collection_history, container, false);

        if (savedInstanceState != null) {
            mDebtorId = savedInstanceState.getInt(Const.TAG_DEBTOR_ID);
        }

        initInterface(rootView);

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Const.TAG_DEBTOR_ID, mDebtorId);
    }

    private void initInterface(View rootView) {
        mEmptyWrapper = rootView.findViewById(R.id.zero_screen_wrapper);
        mListView = (ListView) rootView.findViewById(R.id.collection_history_list_view);
        TextView empty = (TextView) rootView.findViewById(R.id.zero_screen_text);
        empty.setText(getEmptyTextId());

        mEmptyWrapper.setVisibility(View.GONE);
        setExpandableAdapter();
    }

    private void setExpandableAdapter() {
        List<T> dataList = getFilteredData();

        if (dataList.size() > 0) {
            mListView.setAdapter(getAdapter(dataList));
            mEmptyWrapper.setVisibility(View.GONE);
        } else {
            mListView.setVisibility(View.GONE);
            mEmptyWrapper.setVisibility(View.VISIBLE);
        }
    }

    public void onOperationFinished(CollectorOperationResult<? extends ServerResponse> result) {
        if (result.isSuccessful()) {
            setExpandableAdapter();
        }
    }

    protected abstract BaseAdapter getAdapter(List<T> dataList);

    protected abstract int getEmptyTextId();

    @NonNull
    protected abstract List<T> getFilteredData();

    protected void setDebtorId(int debtorId) {
        mDebtorId = debtorId;
    }

    public int getDebtorId() {
        return mDebtorId;
    }

}
