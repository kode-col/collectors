package com.lifecorp.collector.ui.interfaces;

import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.objects.address.AddressContainer;

/**
 * Created by Alexander Smirnov on 07.04.15.
 *
 * Принимает событие сигнализирующее что карта была загруженна
 */
public interface OnMapUpdatedListener {

    void onMapLoaded();

    void onInfoWindowClicked(String debtorId);

    void onInfoWindowOpened(DBDebtorProfile debtor);

    void onInfoWindowOpened(AddressContainer addressContainer);

}
