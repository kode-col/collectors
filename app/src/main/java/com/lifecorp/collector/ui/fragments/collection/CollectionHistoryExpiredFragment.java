package com.lifecorp.collector.ui.fragments.collection;

import android.support.annotation.NonNull;
import android.widget.BaseAdapter;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.ui.adapters.ExpiredTasksAdapter;

import java.util.ArrayList;
import java.util.List;

public class CollectionHistoryExpiredFragment extends CollectionHistoryBaseFragment<DBEvent> {

    private final static long NOW = System.currentTimeMillis();

    public static CollectionHistoryExpiredFragment newInstance(int debtorId) {
        CollectionHistoryExpiredFragment fragment = new CollectionHistoryExpiredFragment();
        fragment.setDebtorId(debtorId);
        return fragment;
    }

    @Override
    protected BaseAdapter getAdapter(List<DBEvent> dataList) {
        return new ExpiredTasksAdapter(dataList);
    }

    @Override
    protected int getEmptyTextId() {
        return R.string.no_expired_tasks;
    }

    @NonNull
    @Override
    protected List<DBEvent> getFilteredData() {
        List<DBEvent> eventsAll = DBCachePoint.getInstance().getAllEvents();
        List<DBEvent> eventsTemp = new ArrayList<>();

        for (DBEvent item : eventsAll) {
            if (isEventExpired(item)) {
                eventsTemp.add(item);
            }
        }

        return eventsTemp;
    }


    private boolean isEventExpired(DBEvent item) {
        return item.getDebtorProfile() != null && getDebtorId() == item.getDebtorProfile().getId()
                && !item.isCompleted() && item.getTime() < NOW;
    }

}
