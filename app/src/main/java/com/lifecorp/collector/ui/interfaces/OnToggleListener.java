package com.lifecorp.collector.ui.interfaces;

import android.view.View;

/**
 * Created by Alexander Smirnov on 16.04.15.
 *
 * Интерфейс использяется для оповещения о расширении(expand)/сжатии(collapse)
 */
public interface OnToggleListener {

    void onToggle(View view, int position, boolean isExpanded);
}
