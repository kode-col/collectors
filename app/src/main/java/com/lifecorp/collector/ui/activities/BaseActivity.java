package com.lifecorp.collector.ui.activities;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;

import com.flurry.android.FlurryAgent;
import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.helpers.DBSaver;
import com.lifecorp.collector.model.database.objects.DBCollectionImageUpload;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.model.database.objects.DBEventImageUpload;
import com.lifecorp.collector.model.objects.collection.Collection;
import com.lifecorp.collector.model.objects.event.EventCompletion;
import com.lifecorp.collector.model.objects.event.SingleEventCompletion;
import com.lifecorp.collector.network.ImageQueueManager;
import com.lifecorp.collector.network.NetworkConstants;
import com.lifecorp.collector.network.QueueManager;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.api.response.collection.CollectionsListResponse;
import com.lifecorp.collector.network.operations.CollectorOperationFactory;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.ErrorCode;
import com.lifecorp.collector.network.operations.api.auth.Login;
import com.lifecorp.collector.network.operations.api.auth.Logout;
import com.lifecorp.collector.network.operations.api.collection.LoadImageOperation;
import com.lifecorp.collector.network.operations.api.collection.PostCollection;
import com.lifecorp.collector.network.operations.api.event.EventCompleteOperation;
import com.lifecorp.collector.ui.dialogfragments.DialogFactory;
import com.lifecorp.collector.ui.enums.EventFilterType;
import com.lifecorp.collector.ui.enums.EventType;
import com.lifecorp.collector.ui.fragments.BaseWrapperFragment;
import com.lifecorp.collector.ui.fragments.collection.CollectionAddFragment;
import com.lifecorp.collector.ui.fragments.event.EventCompleteFragment;
import com.lifecorp.collector.ui.interfaces.OnEventFilterChanged;
import com.lifecorp.collector.ui.interfaces.OnMapFilterChanged;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.LocationHelper;
import com.lifecorp.collector.utils.Logger;
import com.lifecorp.collector.utils.ToastHelper;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;
import com.redmadrobot.library.async.gui.ServiceActivity;
import com.redmadrobot.library.async.service.operation.OperationError;

import java.io.Serializable;
import java.util.Arrays;

public abstract class BaseActivity extends ServiceActivity {

    public static final String TAG_ADD_DIALOG = "TAG_ADD_DIALOG";
    public static final String TAG_MAP_DIALOG = "TAG_MAP_DIALOG";
    public static final String TAG_CALL_DIALOG = "TAG_CALL_DIALOG";
    public static final String TAG_IS_TRY_LOGIN = "TAG_IS_TRY_LOGIN";
    public static final String TAG_COMPLETE_EVENT = "TAG_COMPLETE_EVENT";
    public static final String TAG_OBJECT_TO_SEND = "TAG_OBJECT_TO_SEND";
    public static final String TAG_MAP_FILTER_DIALOG = "TAG_MAP_FILTER_DIALOG";
    public static final String TAG_IS_LOADING_DEBTORS = "TAG_IS_LOADING_DEBTORS";
    public static final String TAG_EVENT_FILTER_DIALOG = "TAG_EVENT_FILTER_DIALOG";
    public static final String TAG_PHOTO_OBJECT_TO_SEND = "TAG_PHOTO_OBJECT_TO_SEND";
    public static final String TAG_LOCATION_SEARCH_PROBLEM = "TAG_LOCATION_SEARCH_PROBLEM";

    protected boolean mIsTryLogin;
    private Serializable mObjectToSend;
    private Serializable mPhotoObjectForSend;
    protected boolean mIsLoadingDebtors = false;

    private BroadcastReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mIsTryLogin = savedInstanceState.getBoolean(TAG_IS_TRY_LOGIN);
            mIsLoadingDebtors = savedInstanceState.getBoolean(TAG_IS_LOADING_DEBTORS);
            mObjectToSend = savedInstanceState.getSerializable(TAG_OBJECT_TO_SEND);
            mPhotoObjectForSend = savedInstanceState.getSerializable(TAG_PHOTO_OBJECT_TO_SEND);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(TAG_IS_TRY_LOGIN, mIsTryLogin);
        outState.putBoolean(TAG_IS_LOADING_DEBTORS, mIsLoadingDebtors);
        outState.putSerializable(TAG_OBJECT_TO_SEND, mObjectToSend);
        outState.putSerializable(TAG_PHOTO_OBJECT_TO_SEND, mPhotoObjectForSend);
    }

    public void callPhone(String phone) {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int SIMState = tm.getSimState();
        if (SIMState == TelephonyManager.SIM_STATE_READY) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
            startActivity(intent);
        } else if (SIMState == TelephonyManager.SIM_STATE_ABSENT) {
            makeToast(R.string.sim_state_absent);
        }
    }

    private boolean ifFragmentNotExist(String TAG) {
        return getFragmentManager().findFragmentByTag(TAG) == null;
    }

    public void createFilterDialog(EventType eventType, boolean isPromisedPayChecked,
                                   OnMapFilterChanged onMapFilterChanged) {
        if (ifFragmentNotExist(TAG_MAP_FILTER_DIALOG)) {
            DialogFactory.getInstance()
                    .createFilterDialog(eventType, isPromisedPayChecked, onMapFilterChanged)
                    .show(getFragmentManager(), TAG_MAP_FILTER_DIALOG);
        }
    }

    public void createEventFilterDialog(EventFilterType eventType, Long dateFrom, Long dateTo,
                                        OnEventFilterChanged onEventFilterChanged) {
        if (ifFragmentNotExist(TAG_EVENT_FILTER_DIALOG)) {
            DialogFactory.getInstance()
                    .createFilterEventsDialog(eventType, dateFrom, dateTo, onEventFilterChanged)
                    .show(getFragmentManager(), TAG_EVENT_FILTER_DIALOG);
        }
    }

    public void createEnableLocationServiceDialog() {
        if (ifFragmentNotExist(TAG_MAP_DIALOG)) {
            DialogFactory.getInstance().createEnableLocationServiceDialog()
                    .show(getFragmentManager(), TAG_MAP_DIALOG);
        }
    }

    public void createLocationSearchProblem() {
        if (ifFragmentNotExist(TAG_LOCATION_SEARCH_PROBLEM)) {
            DialogFactory.getInstance().createLocationSearchProblemDialog()
                    .show(getFragmentManager(), TAG_LOCATION_SEARCH_PROBLEM);
        }
    }

    public void createCallDialog(String fio, String phone) {
        if (ifFragmentNotExist(TAG_CALL_DIALOG) && fio != null && phone != null) {
            DialogFactory.getInstance().createCallDialog(fio, phone)
                    .show(getFragmentManager(), TAG_CALL_DIALOG);
        }
    }

    public void createCompleteEventFragment(DBEvent dbEvent) {
        if (ifFragmentNotExist(TAG_COMPLETE_EVENT)) {
            showCompleteEventFragment(EventCompleteFragment.newInstance(dbEvent));
        }
    }

    public void createAddCollectionFragment(DBDebtorProfile dbDebtorProfile, String creditId) {
        if (ifFragmentNotExist(TAG_ADD_DIALOG)) {
            showCollectionAddFragment(CollectionAddFragment.newInstance(dbDebtorProfile, creditId));
        }
    }

    protected void showCompleteEventFragment(Fragment fragment) {

    }

    protected void showCollectionAddFragment(Fragment fragment) {
        // реализуется у производнных классов
    }

    protected void makeToast(String msg) {
        if (msg != null && !msg.isEmpty()) {
            ToastHelper.getInstance().makeToast(this, msg);
        }
    }

    protected void makeToast(int msgId) {
        ToastHelper.getInstance().makeToast(this, msgId);
    }

    // Метод обновления всех данных
    public void refreshAll() {
        refreshAll(true);
    }

    protected void refreshAll(boolean showMessage) {
        if (!mIsLoadingDebtors) {
            mIsLoadingDebtors = true;

            if (getCurrentFragment() != null) {
                getCurrentFragment().refreshAll();
            }
            if (showMessage) {
                makeToast(R.string.toast_refresh);
            }

            loadActionTypes();
            loadActionResultTypes();
            loadDebtors();
            loadCollections();
        } else {
            if (showMessage) {
                makeToast(R.string.toast_no_refresh);
            }
        }
    }

    protected BaseWrapperFragment getCurrentFragment() {
        return null;
    }


    // Загрузить список должников
    public void loadDebtors() {
        mIsLoadingDebtors = true;

        runServiceRequest(CollectorOperationFactory.getInstance().getDebtors(true),
                Const.TAG_DEBTORS);
    }

    // Загрузить список коллекций
    public void loadCollections() {
        runServiceRequest(CollectorOperationFactory.getInstance().getCollections(),
                Const.TAG_COLLECTIONS);
    }

    // Загрузить список событий
    public void loadEvents() {
        runServiceRequest(CollectorOperationFactory.getInstance().getEvents(), Const.TAG_EVENTS);
    }

    // Загрузить список типов действий
    protected void loadActionTypes() {
        runServiceRequest(CollectorOperationFactory.getInstance().getActionTypes(),
                Const.TAG_ACTION_TYPES);
    }

    // Загрузить список типов результатов действий
    protected void loadActionResultTypes() {
        runServiceRequest(CollectorOperationFactory.getInstance().getActionResultTypes(),
                Const.TAG_ACTION_RESULT_TYPES);
    }

    // Получение токена, вход в аккаунт
    public void login() {
        if (PreferencesStorage.getInstance().getToken().isEmpty() && !mIsTryLogin) {
            String login = PreferencesStorage.getInstance().getLogin();
            String password = PreferencesStorage.getInstance().getPassword();
            mIsTryLogin = true;

            runServiceRequest(CollectorOperationFactory.getInstance().login(login, password),
                    Const.TAG_LOGIN);
        }
    }

    // Удаление токена, выход из аккаунта
    public void logout() {
        runServiceRequest(CollectorOperationFactory.getInstance().logout(),
                Const.TAG_LOGOUT);
    }

    // Выполнение события, отправка на сервер выполненного события
    public void completeEvent(Serializable object) {
        if (object != null && object instanceof EventCompletion) {
            mObjectToSend = object;

            EventCompletion eventCompletion = (EventCompletion) object;
            if (eventCompletion.getPoiCoordinate() == null) {
                eventCompletion.setPoiCoordinate(LocationHelper.getInstance().getCoordinate());
            }

            final SingleEventCompletion completion = new SingleEventCompletion(eventCompletion);

            runServiceRequest(
                    CollectorOperationFactory.getInstance().completeEvent(completion)
            );
        } else {
            makeToast(R.string.msg_error);
        }
    }

    public void loadImage(Serializable object) {
        if (object != null) {
            mPhotoObjectForSend = object;
            if (object instanceof DBEventImageUpload) {
                DBEventImageUpload image = (DBEventImageUpload) object;
                runServiceRequest(CollectorOperationFactory.getInstance().loadImage(image));
            } else if (object instanceof DBCollectionImageUpload) {
                DBCollectionImageUpload image = (DBCollectionImageUpload) object;
                runServiceRequest(CollectorOperationFactory.getInstance().loadImage(image));
            } else {
                makeToast(R.string.msg_error);
            }
        } else {
            makeToast(R.string.msg_error);
        }
    }

    // Отправление новой коллекции на сервер
    public void postCollection(Collection collection) {
        if (collection != null) {
            collection.setPoiCoordinate(LocationHelper.getInstance().getCoordinate());
            CollectionsListResponse collections =
                    new CollectionsListResponse(Arrays.asList(collection));

            mObjectToSend = collections;

            runServiceRequest(CollectorOperationFactory.getInstance().postCollection(collections));
        } else {
            makeToast(R.string.msg_error);
        }
    }

    protected void tryLaunchOperationQueue() {
        if (App.IS_NETWORK_AVAILABLE) {
            launchQuery();
            launchImageQuery();
        }
    }

    public void onOperationFinished(Login.Result result) {
        mIsTryLogin = false;
    }

    public final void onOperationFinished(
            final CollectorOperationResult<? extends ServerResponse> response) {
        final ErrorCode returnCode = response.getRedirectionCode();

        Logger.e("-| className " + response.getClass().getName());

        if (!response.isSuccessful()) {
            addResponseToQueue(response);
        } else {
            final String tag = response.getOutput().getResponseType();

            if (tag != null) {
                switch (tag) {
                    case Const.TAG_LOAD_IMAGE:
                        if (ImageQueueManager.getInstance().getImageQuerySize() > 0) {
                            launchImageQuery();
                        } else {
                            refreshAll();
                        }
                        break;
                    case Const.TAG_COMPLETE_EVENT:
                    case Const.TAG_POST_COLLECTION:
                        tryLaunchOperationQueue();
                        break;
                    case Const.TAG_EVENTS:
                        DBCachePoint.getInstance().setIsSynchronized(true);
                        break;
                    default:
                        break;
                }
            }
        }

        if (returnCode != null) {
            switch (returnCode) {
                case UNAUTHORIZED:
                    handleUnauthorized(response.getErrorMessage());
                    break;
                case FAILED_DEPENDENCY: //fall through
                case I_AM_A_TEAPOT:
                    goToAuthorization();
                    break;
                default:
                    // do nothing
                    break;
            }
        }

        sendOperationForFragment(response);
    }

    private void addResponseToQueue(CollectorOperationResult<? extends ServerResponse> response) {
        if (response == null || response instanceof Login.Result) {
            return;
        }

        if (response instanceof PostCollection.Result) {
            if (mObjectToSend != null && mObjectToSend instanceof CollectionsListResponse) {
                QueueManager.getInstance().add(QueueManager.QUEUE_POST_COLLECTION,
                        (CollectionsListResponse) mObjectToSend);

                mObjectToSend = null;
            }
        } else if (response instanceof EventCompleteOperation.Result) {
            if (mObjectToSend != null && mObjectToSend instanceof EventCompletion) {
                QueueManager.getInstance().add(QueueManager.QUEUE_POST_EVENT,
                        (EventCompletion) mObjectToSend);

                mObjectToSend = null;
            }
        } else if (response instanceof LoadImageOperation.Result) {
            if (mPhotoObjectForSend != null) {
                if (mPhotoObjectForSend instanceof DBCollectionImageUpload) {
                    DBCollectionImageUpload item = (DBCollectionImageUpload) mPhotoObjectForSend;
                    DBSaver.getInstance().saveCollectionImageUpload(item);
                } else if (mPhotoObjectForSend instanceof DBEventImageUpload) {
                    DBEventImageUpload item = (DBEventImageUpload) mPhotoObjectForSend;
                    DBSaver.getInstance().saveEventImageUpload(item);
                }

                mPhotoObjectForSend = null;
            }
        } else if (response instanceof Logout.Result) {
            // ничего не добавлять т.к. выходим
        } else {
            DBCachePoint.getInstance().setIsSynchronized(false);
            QueueManager.getInstance().addRefreshAll();

            addRefreshAllToQuery();
        }
    }

    protected void addRefreshAllToQuery() {
        // ничего не делаем, реализация в наследниках
    }

    public final boolean checkoutError(final CollectorOperationResult operationResult) {
        if (operationResult.isSuccessful()) {
            return false;
        } else {
            if (operationResult.hasOnlyCommonError()
                    && !handleError(operationResult.getOperationError())) {
                Logger.e("Some error: " + operationResult.getOperationError().getMessage());
            }
            return true;
        }
    }

    @SuppressWarnings("UnusedParameters")
    protected boolean handleError(final OperationError error) {
        return false;
    }

    @SuppressWarnings("UnusedParameters")
    protected void handleUnauthorized(final String errorMessage) {
        Logger.e("Unauthorized");
        goToAuthorization();
    }

    protected abstract void sendOperationForFragment(
            CollectorOperationResult<? extends ServerResponse> result);

    public void showHideProgress(boolean state) {

    }

    public void goToAuthorization() {
        Intent intent = new Intent(this, AuthActivity.class);
        startActivity(intent);
        this.finish();
    }

    protected void launchQuery() {
        QueueManager.getInstance().launchLoading(this);
    }

    protected void launchImageQuery() {
        ImageQueueManager.getInstance().launchLoading(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    tryLaunchOperationQueue();
                }
            };
        }

        registerReceiver(mReceiver, new IntentFilter(NetworkConstants.NETWORK_STATE_CHANGED));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "MNC96K26QSW5V6ZJXW6C");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

}
