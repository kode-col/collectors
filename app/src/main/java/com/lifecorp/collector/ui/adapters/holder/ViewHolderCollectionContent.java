package com.lifecorp.collector.ui.adapters.holder;

import android.widget.GridView;
import android.widget.TextView;

/**
 * Created by Alexander Smirnov on 05.05.15.
 *
 * Холдер контента выпадающего списка коллекции
 */
public class ViewHolderCollectionContent {

    public GridView images;
    public TextView content;

}
