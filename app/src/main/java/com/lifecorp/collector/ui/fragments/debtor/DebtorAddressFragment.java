package com.lifecorp.collector.ui.fragments.debtor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.objects.address.AddressContainer;
import com.lifecorp.collector.model.objects.address.AddressGroupContainer;
import com.lifecorp.collector.ui.adapters.debtor.DebtorAddressAdapter;
import com.lifecorp.collector.ui.fragments.BaseFragment;
import com.lifecorp.collector.ui.interfaces.OnAddressSelectedListener;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.MyImageLoaderListener;
import com.lifecorp.collector.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by Alexander Smirnov on 07.04.15.
 *
 * Фрагмент выводит список адресов клиента и поручителей
 */
public class DebtorAddressFragment extends BaseFragment implements AdapterView.OnItemClickListener,
        View.OnClickListener {

    private static final String TAG_FROM_MENU = "TAG_FROM_MENU";

    private DBDebtorProfile mDebtor;
    private List<AddressGroupContainer> mGroupList;

    private ImageView mIvPhoto;
    private boolean mIsFromMenu;
    private View mProgressCircle;
    private Animation mAnimation;
    private View mDebtorsProgress;
    private View mPromisedPayWrapper;
    private StickyListHeadersListView mListView;
    private DebtorAddressAdapter mDebtorAddressAdapter;
    private OnAddressSelectedListener mOnAddressSelectedListener;
    private TextView mTvId, mTvTitle, mTvDate, mTvCoins, mTvMoney;

    public static DebtorAddressFragment newInstance(DBDebtorProfile debtor, boolean fromMenu,
                                                    OnAddressSelectedListener listener,
                                                    List<AddressGroupContainer> dataList) {
        DebtorAddressFragment debtorAddressFragment = new DebtorAddressFragment();
        debtorAddressFragment.setDebtor(debtor);
        debtorAddressFragment.setFromMenu(fromMenu);
        debtorAddressFragment.setAllAddressFromDebtor(dataList);
        debtorAddressFragment.setOnAddressSelectedListener(listener);

        return debtorAddressFragment;
    }

    private void setFromMenu(boolean fromMenu) {
        mIsFromMenu = fromMenu;
    }

    public void setOnAddressSelectedListener(OnAddressSelectedListener listener) {
        mOnAddressSelectedListener = listener;
    }

    public OnAddressSelectedListener getOnAddressSelectedListener() {
        return mOnAddressSelectedListener;
    }

    public void setAllAddressFromDebtor(List<AddressGroupContainer> dataList) {
        mGroupList = dataList;
    }

    public void setDebtor(DBDebtorProfile debtor) {
        mDebtor = debtor;
    }

    public DBDebtorProfile getDebtor() {
        return mDebtor;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem menuItem = menu.findItem(R.id.action_filter);
        if (menuItem != null) {
            menuItem.setVisible(false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debtors_address_list, container, false);
        if (savedInstanceState != null) {
            mIsFromMenu = savedInstanceState.getBoolean(TAG_FROM_MENU);
            mDebtor = (DBDebtorProfile) savedInstanceState.getSerializable(Const.TAG_DEBTOR);
        }

        initInterface(view);
        setHasOptionsMenu(true);
        setData();

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(TAG_FROM_MENU, mIsFromMenu);
        outState.putSerializable(Const.TAG_DEBTOR, mDebtor);
    }

    private void initInterface(View rootView) {
        mTvId = (TextView) rootView.findViewById(R.id.address_list_debtor_id);
        mTvTitle = (TextView) rootView.findViewById(R.id.address_list_debtor_title);
        mIvPhoto = (ImageView) rootView.findViewById(R.id.address_list_debtor_photo);
        mDebtorsProgress = rootView.findViewById(R.id.debtors_address_list_progress_wrapper);
        mProgressCircle = rootView.findViewById(R.id.debtors_address_list_progress);
        mAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_infinity);

        // Обещанный платёж
        mTvDate = (TextView) rootView.findViewById(R.id.address_list_promised_date);
        mTvCoins = (TextView) rootView.findViewById(R.id.address_list_promised_coins);
        mTvMoney = (TextView) rootView.findViewById(R.id.address_list_promised_money);
        mPromisedPayWrapper = rootView.findViewById(R.id.address_list_promised_pay_wrapper);
        mListView = (StickyListHeadersListView) rootView.findViewById(R.id.address_list_list);

        rootView.findViewById(R.id.address_list_debtor_wrapper).setOnClickListener(this);
    }

    private void setData() {
        if (getDebtor() != null) {
            ImageLoader.getInstance().displayImage(getDebtor().buildPhotoUrl(), mIvPhoto,
                    new MyImageLoaderListener(getMainActivity()));

            mTvTitle.setText(getDebtor().getFio());
            mTvId.setText(Utils.formatIdForOutput(getDebtor().getId()));
            mPromisedPayWrapper.setVisibility(getDebtor().isPromised() ? View.VISIBLE : View.GONE);

            if (getDebtor().isPromised()) {
                mTvDate.setText(getDebtor().getPromisedDate());
                mTvCoins.setText(", " + getDebtor().getPromisedCoins());
                mTvMoney.setText(getDebtor().getPromisedPaperMoney());
            }

            setAdapter();
        }
    }

    private void setAdapter() {
        if (mGroupList != null) {
            mDebtorAddressAdapter = new DebtorAddressAdapter(getMainActivity(), mGroupList);
            mListView.setAdapter(mDebtorAddressAdapter);
            mListView.setOnItemClickListener(this);
            showAnimation(false);
        } else {
            showAnimation(true);
        }
    }

    public void notifyDataSetChanged() {
        setAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mIsFromMenu) {
            getMainActivity().setDebtorAddressTitleAndIcon();
        } else {
            getMainActivity().setDebtorFormTitleAndIcon();
        }
    }

    public void setPressedAddress(AddressContainer addressContainer) {
        if (mDebtorAddressAdapter != null) {
            mDebtorAddressAdapter.setSelectedIdByAddressContainer(addressContainer);
            mDebtorAddressAdapter.notifyDataSetChanged();
        }
    }

    public void goToPressedPosition() {
        if (mDebtorAddressAdapter != null) {
            updateListSelection(mDebtorAddressAdapter.getSelectedId());
        }
    }

    private void updateListSelection(final int position) {
        if (mListView != null) {
            mListView.clearFocus();
            mListView.post(new Runnable() {
                @Override
                public void run() {
                    mListView.smoothScrollToPosition(position);
                }
            });
        }
    }

    private void updateSelectedPosition(int position) {
        updateListSelection(position);

        if (mDebtorAddressAdapter != null) {
            mDebtorAddressAdapter.setSelectedId(position);
            mDebtorAddressAdapter.notifyDataSetChanged();
        }
    }


    private void showAnimation(boolean showAnimation) {
        if (showAnimation) {
            mListView.setVisibility(View.GONE);
            mDebtorsProgress.setVisibility(View.VISIBLE);
            mProgressCircle.startAnimation(mAnimation);
        } else {
            mListView.setVisibility(View.VISIBLE);
            mDebtorsProgress.setVisibility(View.GONE);
            mAnimation.cancel();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (getOnAddressSelectedListener() != null) {
            int groupPosition = mDebtorAddressAdapter.getGroupPosition(position);

            if (groupPosition != -1) {
                updateSelectedPosition(position);
                AddressContainer addressContainer =
                        mDebtorAddressAdapter.getItemAddressContainer(position, groupPosition);

                mOnAddressSelectedListener.onAddressSelected(addressContainer);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (mDebtor != null) {
            getMainActivity().showDebtorsFragment("" + mDebtor.getId());
        }
    }

}
