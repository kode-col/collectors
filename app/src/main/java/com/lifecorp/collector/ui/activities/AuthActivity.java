package com.lifecorp.collector.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.lifecorp.collector.R;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.auth.Login;
import com.lifecorp.collector.ui.fragments.auth.AuthFragment;
import com.lifecorp.collector.utils.preferences.PreferencesStorage;

/**
 * Авторизационное активити
 */
public class AuthActivity extends BaseActivity {

    private View mContentView;
    private View mLoadingView;
    private View mProgressCircle;
    private AuthFragment mAuthFragment;
    private Animation mAnimationRotate;
    private static final String FRAGMENT_TAG = "auth_fragment_tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        if (!PreferencesStorage.getInstance().getToken().isEmpty()) {
            goToNextActivity();
        }

        if (savedInstanceState == null) {
            mAuthFragment = new AuthFragment();
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.transparent_in, R.anim.transparent_out)
                    .replace(R.id.auth_content, mAuthFragment, FRAGMENT_TAG)
                    .commit();
        } else {
            mAuthFragment = (AuthFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        }

        mAnimationRotate = AnimationUtils.loadAnimation(this, R.anim.rotate_infinity);

        mContentView = findViewById(R.id.auth_content);
        mLoadingView = findViewById(R.id.auth_loading);
        mProgressCircle = findViewById(R.id.auth_loading_progress);
        showHideProgress(false);
    }

    @Override
    protected void sendOperationForFragment(CollectorOperationResult result) {
        if (mAuthFragment != null && result instanceof Login.Result) {
            mAuthFragment.onOperationFinished((Login.Result) result);
        }
    }

    @Override
    protected void launchQuery() {
        // ничего не делать
    }

    @Override
    public void goToAuthorization() {
        // не позволяем пересоздать активити, попадаем в этот метод только при проблемах
        // с авторизацией

        if (mAuthFragment != null) {
            mAuthFragment.setIsUnauthorized(true);
        }
    }

    public void goToNextActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void showHideProgress(boolean state) {
        startStopAnimation(state);
    }

    private void setFadeInAnimation(final View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
            view.startAnimation(animation);
        }
    }

    private void setFadeOutAnimation(final View view) {
        if (view != null) {
            Animation animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
            animation.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {
                    // ничего
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // ничего
                }
            });
            view.startAnimation(animation);
        }
    }

    private void startStopAnimation(boolean flag) {
        if (flag) {
            setFadeInAnimation(mLoadingView);
            setFadeOutAnimation(mContentView);
            mProgressCircle.post(new Runnable() {
                @Override
                public void run() {
                    mProgressCircle.startAnimation(mAnimationRotate);
                }
            });
        } else {
            setFadeInAnimation(mContentView);
            setFadeOutAnimation(mLoadingView);
            mProgressCircle.post(new Runnable() {
                @Override
                public void run() {
                    mProgressCircle.clearAnimation();
                }
            });
        }
    }

}
