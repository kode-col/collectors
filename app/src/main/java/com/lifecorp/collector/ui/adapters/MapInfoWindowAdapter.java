package com.lifecorp.collector.ui.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorProfile;
import com.lifecorp.collector.model.database.objects.DBSection;
import com.lifecorp.collector.model.database.objects.DBSectionField;
import com.lifecorp.collector.model.objects.address.AddressContainer;
import com.lifecorp.collector.model.objects.address.AddressGroupContainer;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.ui.interfaces.OnMapUpdatedListener;
import com.lifecorp.collector.utils.AddressGroupBuilder;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MapInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private TextView mTvId;
    private TextView mTvName;
    private TextView mTvAddress;
    private TextView mTvAddressDescription;

    private LayoutInflater inflater;
    private int mLastLoadAddressGroupDebtorId = -1;
    private List<AddressGroupContainer> mCacheAddressGroup;
    private final OnMapUpdatedListener mOnMapUpdatedListener;

    public MapInfoWindowAdapter(Context context, OnMapUpdatedListener onMapUpdatedListener) {
        inflater = LayoutInflater.from(context);
        mOnMapUpdatedListener = onMapUpdatedListener;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    /*
     * Инициализирует интерфейс информационного окна, заполняя данными из контейнера адресов
     * вызывается при отображении детальной информации о должнике
    */
    private void initAddressInterface(DBDebtorProfile debtor, int positionId) {
        AddressContainer addressContainer = findAddressContainer(debtor, positionId);

        if (addressContainer != null) {
            if (addressContainer.getGuarantorFio() == null) {
                mTvId.setVisibility(View.VISIBLE);
            } else {
                mTvId.setVisibility(View.GONE);
                mTvName.setText(addressContainer.getGuarantorFio());
            }

            mTvAddress.setText(addressContainer.getAddress());
            mTvAddressDescription.setText(addressContainer.getTitle());

            onAddressInfoWindowSelected(addressContainer);
        }
    }

    @Nullable
    private AddressContainer findAddressContainer(DBDebtorProfile debtor, int positionId) {
        AddressContainer found = null;

        if (debtor == null) {
            return null;
        }

        if (debtor.getId() != mLastLoadAddressGroupDebtorId) {
            mLastLoadAddressGroupDebtorId = debtor.getId();
            mCacheAddressGroup = AddressGroupBuilder.getInstance().getAllAddressFromDebtor(debtor);
        }

        if (mCacheAddressGroup != null) {
            int i = 0;

            mainLoop:
            for (AddressGroupContainer group : mCacheAddressGroup) {
                for (AddressContainer item : group.getAddress()) {
                    if (positionId == i) {
                        found = item;
                        break mainLoop;
                    }
                    i++;
                }
            }
        }

        return found;
    }

    public void clearCache() {
        mCacheAddressGroup = null;
        mLastLoadAddressGroupDebtorId = -1;
    }

    /*
     * Инициализирует интерфейс информационного окна, заполняя данными из собития
     * вызывается при отображении списка событий
    */
    private void initEventInterface(DBDebtorProfile debtorProfile, Marker marker) {
        if (debtorProfile != null && debtorProfile.getSections() != null) {
            onDebtorInfoWindowSelected(debtorProfile);

            List<DBSection> sections = debtorProfile.getSections();

            if (sections != null) {
                main_loop:
                for (DBSection section : sections) {
                    if (section.getType() == DBSection.TYPE_ADDRESS) {
                        List<DBSectionField> sectionFields = new ArrayList<>(section.getFields());

                        for (DBSectionField fieldItem : sectionFields) {
                            if (String.valueOf(fieldItem.getId_()).equals(marker.getSnippet())) {
                                mTvAddress.setText(fieldItem.getValue());
                                break main_loop;
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * Инициализирует интерфейс информационного окна, заполняя данными из информации о должнике
     * вызывается при отображении списка должников
    */
    private void initDebtorInterface(DBDebtorProfile debtorProfile) {
        if (debtorProfile != null && debtorProfile.getSections() != null) {
            onDebtorInfoWindowSelected(debtorProfile);

            mTvAddress.setText(getAddressFromSections(debtorProfile.getSections()));
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View view = inflater.inflate(R.layout.element_map_balloon, null);

        mTvId = (TextView) view.findViewById(R.id.map_balloon_id);
        mTvName = (TextView) view.findViewById(R.id.map_balloon_name);
        mTvAddress = (TextView) view.findViewById(R.id.map_balloon_address);
        mTvAddressDescription = (TextView) view.findViewById(R.id.map_balloon_address_description);

        if(marker != null) {
            int debId = Integer.parseInt(marker.getTitle());
            DBDebtorProfile debtor = DBCachePoint.getInstance().findDebtorById(debId);

            if (debtor != null) {
                if (debtor.isPromised()) {
                    view.findViewById(R.id.map_balloon_promised_pay).setVisibility(View.VISIBLE);

                    TextView promisedPaySum = (TextView)
                            view.findViewById(R.id.map_balloon_promised_pay_sum);
                    TextView promisedPayDate = (TextView)
                            view.findViewById(R.id.map_balloon_promised_pay_end_date);

                    promisedPaySum.setText(debtor.getPromisedSum());
                    promisedPayDate.setText(debtor.getPromisedDate());
                }

                mTvId.setText(Utils.formatIdForOutput(debId));
                mTvName.setText(debtor.getFio());

                if (marker.getSnippet() == null) {
                    initDebtorInterface(debtor);
                } else {
                    if (marker.getSnippet().contains(Const.TAG_MARKER_FOR_ADDRESS)) {
                        String position = marker.getSnippet();
                        position = position.replace(Const.TAG_MARKER_FOR_ADDRESS, "");
                        int positionId = Integer.parseInt(position);
                        initAddressInterface(debtor, positionId);
                    } else {
                        initEventInterface(debtor, marker);
                    }
                }
            }
        }

        return view;
    }

    // обновить выбранного должника в списке должников
    private void onDebtorInfoWindowSelected(DBDebtorProfile debtor) {
        if (mOnMapUpdatedListener != null) {
            mOnMapUpdatedListener.onInfoWindowOpened(debtor);
        }
    }

    // обновить выбранный адрес в списке адресов
    private void onAddressInfoWindowSelected(AddressContainer addressContainer) {
        if (mOnMapUpdatedListener != null) {
            mOnMapUpdatedListener.onInfoWindowOpened(addressContainer);
        }
    }

    @NonNull
    private String getAddressFromSections(List<DBSection> sections) {
        String address = "";

        if (sections != null) {
            for (DBSection section : sections) {
                if (section.getType() == DBSection.TYPE_ADDRESS) {
                    address = getAddressFromFields(section);
                    break;
                }
            }
        }

        return address != null ? address : "";
    }

    private String getAddressFromFields(DBSection section) {
        List<DBSectionField> fields = new ArrayList<>(section.getFields());
        String address = "";

        for (DBSectionField item : fields) {
            address = item.getValue();

            if (address != null && !address.isEmpty()) {
                break;
            }
        }

        return address;
    }

}
