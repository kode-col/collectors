package com.lifecorp.collector.ui.adapters.debtor;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.objects.DBDebtorLink;
import com.lifecorp.collector.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Alexander Smirnov on 14.05.15.
 *
 * Адаптер выводи изображения ссылок для перехода на внешние ресурсы через браузер
 */
public class DebtorLinksAdapter extends BaseAdapter implements View.OnClickListener {

    private final int DEFAULT_RES_ID = R.drawable.ico_main_no_social_icon;

    private final List<DBDebtorLink> mDebtorLinkList;
    private final List<String> mTypedArrayList;
    private final TypedArray mImagesArray;

    public DebtorLinksAdapter(List<DBDebtorLink> list) {
        mDebtorLinkList = list;

        Resources res = App.getContext().getResources();
        String[] typedArray = res.getStringArray(R.array.images_types);
        mTypedArrayList = new ArrayList<>(Arrays.asList(typedArray));

        mImagesArray = res.obtainTypedArray(R.array.images_types_res_id);
    }

    @Override
    public int getCount() {
        return mDebtorLinkList != null ? mDebtorLinkList.size() : 0;
    }

    @Override
    public DBDebtorLink getItem(int position) {
        return position < getCount() ? mDebtorLinkList.get(position) : new DBDebtorLink();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_debtor_link, parent, false);
            convertView.setOnClickListener(this);
        }

        DBDebtorLink item = getItem(position);
        if (item != null) {
            convertView.setTag(item.getLink());
            ((ImageView) convertView).setImageResource(getImageResource(item.getType()));
        }

        return convertView;
    }

    private int getImageResource(String type) {
        return mImagesArray.getResourceId(mTypedArrayList.indexOf(type), DEFAULT_RES_ID);
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();

        if (tag != null) {
            Utils.showLinkInBrowser(v.getContext(), (String) tag);
        }
    }

}
