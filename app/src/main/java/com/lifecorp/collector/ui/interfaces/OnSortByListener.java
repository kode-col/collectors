package com.lifecorp.collector.ui.interfaces;

/**
 * Created by Alexander Smirnov on 08.04.15.
 *
 * Интерфейс для сохранения состояния выбранной сортировки, необходим
 * для востановление выбранной сортироки, после изменения
 */
public interface OnSortByListener {

    void setOnSortPositionChanged(int sortType);

    int getSaveSortPosition();

}
