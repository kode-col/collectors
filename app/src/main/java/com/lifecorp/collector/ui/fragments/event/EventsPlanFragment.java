package com.lifecorp.collector.ui.fragments.event;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.lifecorp.collector.App;
import com.lifecorp.collector.R;
import com.lifecorp.collector.model.database.helpers.DBFront;
import com.lifecorp.collector.model.database.objects.DBEvent;
import com.lifecorp.collector.network.ImageQueueManager;
import com.lifecorp.collector.network.api.DBCachePoint;
import com.lifecorp.collector.network.api.response.ServerResponse;
import com.lifecorp.collector.network.operations.CollectorOperationResult;
import com.lifecorp.collector.network.operations.api.auth.Logout;
import com.lifecorp.collector.ui.adapters.event.EventsPagerAdapter;
import com.lifecorp.collector.ui.enums.EventFilterType;
import com.lifecorp.collector.ui.fragments.BaseWrapperFragment;
import com.lifecorp.collector.ui.interfaces.OnEventFilterChanged;
import com.lifecorp.collector.ui.view.PagerSlidingTabStrip;
import com.lifecorp.collector.utils.Const;
import com.lifecorp.collector.utils.SetAdapterForViewPagerWithTitleTask;
import com.squareup.timessquare.CalendarPickerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class EventsPlanFragment extends BaseWrapperFragment implements OnEventFilterChanged {

    private final static String TAG_EVENTS_LOADED = "TAG_EVENTS_LOADED";
    private final static String TAG_IS_DATA_LOADED = "TAG_IS_DATA_LOADED";
    private final static String TAG_DEBTORS_LOADED = "TAG_DEBTORS_LOADED";
    private final static String TAG_SELECTED_POSITION = "TAG_SELECTED_POSITION";
    private final static String TAG_DATES_FROM = "TAG_DATES_FROM";
    private final static String TAG_DATE_TO = "TAG_DATE_TO";
    private final static String TAG_FILTERS = "TAG_FILTERS";

    private View mRootView;
    private View mContentView;
    private View mLoadingView;
    private View mProgressCircle;

    private Animation mAnimationRotate;
    private EventsPagerAdapter mAdapter;

    private boolean mIsDataLoaded = false;
    private boolean mEventsLoaded = false;
    private boolean mDebtorsLoaded = false;

    private static int mSelectedPosition = 0;

    private List<Long> mDatesTo = new ArrayList<>(3);
    private List<Long> mDatesFrom = new ArrayList<>(3);
    private List<EventFilterType> mFilters = new ArrayList<>(3);

    private static boolean mIsNeedUpdateHighlightedDates = true;
    private static CalendarPickerView mCalendarPickerView;

    public static EventsPlanFragment newInstance() {
        return new EventsPlanFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_events, container, false);
        mAnimationRotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_infinity);

        mContentView = mRootView.findViewById(R.id.layout_events_content);
        mLoadingView = mRootView.findViewById(R.id.layout_events_loading);
        // кастомный progressBar
        mProgressCircle = mRootView.findViewById(R.id.events_list_progress);

        mIsNeedUpdateHighlightedDates = true;

        initCalendar();

        if (savedInstanceState == null) {
            // заполняем массивы стандартными значениями
            fillDefaultsValues();

            startStopAnimation(true);

            tryToSetData();

            if (!App.IS_NETWORK_AVAILABLE) {
                tryToLoadDataFromDB();
            }
        } else {
            mEventsLoaded = savedInstanceState.getBoolean(TAG_EVENTS_LOADED);
            mIsDataLoaded = savedInstanceState.getBoolean(TAG_IS_DATA_LOADED);
            mDebtorsLoaded = savedInstanceState.getBoolean(TAG_DEBTORS_LOADED);
            mSelectedPosition = savedInstanceState.getInt(TAG_SELECTED_POSITION, 0);
            mDatesTo = (List<Long>) savedInstanceState.getSerializable(TAG_DATE_TO);
            mDatesFrom = (List<Long>) savedInstanceState.getSerializable(TAG_DATES_FROM);
            mFilters = (List<EventFilterType>) savedInstanceState.getSerializable(TAG_FILTERS);
            tryToSetData();
        }

        setHasOptionsMenu(true);

        return mRootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(TAG_EVENTS_LOADED, mEventsLoaded);
        outState.putBoolean(TAG_IS_DATA_LOADED, mIsDataLoaded);
        outState.putBoolean(TAG_DEBTORS_LOADED, mDebtorsLoaded);
        outState.putInt(TAG_SELECTED_POSITION, mSelectedPosition);
        outState.putSerializable(TAG_DATE_TO, (Serializable) mDatesTo);
        outState.putSerializable(TAG_DATES_FROM, (Serializable) mDatesFrom);
        outState.putSerializable(TAG_FILTERS, (Serializable) mFilters);
    }

    private void fillDefaultsValues() {
        mFilters.add(EventFilterType.getDefault());
        mFilters.add(EventFilterType.getDefault());
        mFilters.add(EventFilterType.getDefault());
        mDatesFrom.add(null);
        mDatesFrom.add(null);
        mDatesFrom.add(null);
        mDatesTo.add(null);
        mDatesTo.add(null);
        mDatesTo.add(null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_map, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                getBaseActivity().createEventFilterDialog(getCurrentTabFilter(),
                        getCurrentTabDateFrom(), getCurrentTabDateTo(), this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPrepareOptionsMenu (Menu menu) {
        MenuItem item = menu.findItem(R.id.action_filter);
        item.setVisible(mIsDataLoaded);
    }

    private void tryToLoadDataFromDB() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!DBCachePoint.getInstance().isSynchronized()
                        && DBCachePoint.getInstance().getActionTypes().isEmpty()) {
                    DBCachePoint.getInstance().setActionTypes(
                                DBFront.getInstance().getActionTypes());
                    DBCachePoint.getInstance().setActionResultTypes(
                            DBFront.getInstance().getActionResultTypes());

                    DBCachePoint.getInstance().setAllDebtors(DBFront.getInstance().getAllDebtors());
                    DBCachePoint.getInstance().setAllCollections(
                            DBFront.getInstance().getAllCollections());
                    DBCachePoint.getInstance().setAllSections(
                            DBFront.getInstance().getAllSections());
                    DBCachePoint.getInstance().setAllEvents(DBFront.getInstance().getAllEvents());

                    if (getView() != null) {
                        getView().post(new Runnable() {
                            @Override
                            public void run() {
                                tryToSetData();
                            }
                        });
                    }
                }
            }
        }).start();
    }

    private void tryToSetData() {
        if (!DBCachePoint.getInstance().getAllEvents().isEmpty()
                || DBCachePoint.getInstance().isSynchronized()) {
            mEventsLoaded = true;
            mDebtorsLoaded = true;
            showData();
        } else if (!DBCachePoint.getInstance().getAllDebtors().isEmpty()) {
            mDebtorsLoaded = true;
            startLoadingFromNetwork();
        }
    }

    private void showLoading(boolean status) {
        // показываем кнопку фильтрации
        mIsDataLoaded = !status;
        getActivity().invalidateOptionsMenu();

        mLoadingView.setVisibility(status ? View.VISIBLE : View.GONE);
        startStopAnimation(status);
        mContentView.setVisibility(status ? View.GONE : View.VISIBLE);
    }

    private void showData() {
        showLoading(false);
        initTabs(DBCachePoint.getInstance().getAllEvents(), mRootView);
    }

    private void startLoadingFromNetwork() {
        showLoading(true);
        getMainActivity().loadEvents();
    }

    private void initTabs(List<DBEvent> events, View parent) {
        final ViewPager viewPager = (ViewPager) parent.findViewById(R.id.collectionPager);
        viewPager.setOffscreenPageLimit(3);

        boolean isUpdate = false;
        int currentId = viewPager.getCurrentItem();
        if (mAdapter == null) {
            mAdapter = new EventsPagerAdapter(getFragmentManager(), events, mFilters);
        } else {
            isUpdate = true;
            mAdapter.setData(events);
            mAdapter.notifyDataSetChanged();
        }

        PagerSlidingTabStrip pagerTitleStrip = (PagerSlidingTabStrip)
                parent.findViewById(R.id.collectionTabsStrip);
        pagerTitleStrip.setPageSelector(new PagerSlidingTabStrip.PageSelectListener() {
            @Override
            public void onPageSelected(int position) {
                onChangePage(position);
            }
        });

        if (!isUpdate) {
            new SetAdapterForViewPagerWithTitleTask(viewPager, mAdapter, pagerTitleStrip).execute();
        } else {
            onChangePage(currentId);
        }
    }

    private void onChangePage(int position) {
        mSelectedPosition = position;
        mIsNeedUpdateHighlightedDates = true;
        Long dateFrom = getCurrentTabDateFrom();
        Long dateTo = getCurrentTabDateTo();

        if (dateFrom != null && dateFrom.equals(dateTo)) {
            setCalendarPickerSelectedDate(dateFrom);
        } else {
            setCalendarPickerSelectedDateAsToday();
        }

        refreshInfo(mSelectedPosition, getCurrentTabFilter(), dateFrom, dateTo);
    }

    private void refreshInfo(int position, EventFilterType filterType, Long dateFrom, Long dateTo) {
        if (mAdapter != null) {
            if (mIsNeedUpdateHighlightedDates) {
                mCalendarPickerView.clearHighlightedDates();
            }

            mFilters.set(position, filterType);
            mDatesFrom.set(position, dateFrom);
            mDatesTo.set(position, dateTo);


            mAdapter.refreshFragmentInfo(position, filterType, dateFrom, dateTo);
        }
    }

    private void initCalendar() {
        mCalendarPickerView = (CalendarPickerView) mRootView.findViewById(R.id.calendar_view);

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.YEAR, -5);

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.YEAR, 5);

        Date today = new Date();

        mCalendarPickerView.init(startDate.getTime(), endDate.getTime()).withSelectedDate(today);

        mCalendarPickerView.setOnDateSelectedListener(
                new CalendarPickerView.OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(Date date) {
                        final Long day = date.getTime();
                        refreshInfo(mSelectedPosition, getCurrentTabFilter(), day, day);
                    }

                    @Override
                    public void onDateUnselected(Date date) {
                        //
                    }

                    @Override
                    public void onDateUnselectedCustom(Date date) {
                        setCalendarPickerSelectedDateAsToday();
                        refreshInfo(mSelectedPosition, getCurrentTabFilter(), null, null);
                    }

                }
        );
    }

    private EventFilterType getCurrentTabFilter() {
        return mFilters.get(mSelectedPosition);
    }

    private Long getCurrentTabDateFrom() {
        return mDatesFrom.get(mSelectedPosition);
    }

    private Long getCurrentTabDateTo() {
        return mDatesTo.get(mSelectedPosition);
    }

    @Override
    public void onDialogBtnAcceptClicked(String tag, Object object) {
    }

    // TODO: реализовать негативный сценарий
    @Override
    public void onOperationFinished(CollectorOperationResult<? extends ServerResponse> result) {
        if (result.getOutput() != null) {
            String tag = result.getOutput().getResponseType();

            switch (tag) {
                case Const.TAG_EVENTS: {
                    if (result.isSuccessful()) {
                        mEventsLoaded = true;
                    } else {
                        showData();
                    }
                    break;
                }
                case Const.TAG_DEBTORS: {
                    if (result.isSuccessful()) {
                        mDebtorsLoaded = true;
                    } else {
                        makeToast(R.string.msg_error_load_data_from_server);
                        tryToLoadDataFromDB();
                    }
                    break;
                }
                case Const.TAG_COMPLETE_EVENT:
                    makeToast(R.string.toast_complete_event_complete);

                    if (ImageQueueManager.getInstance().getImageQuerySize() == 0) {
                        startLoadingFromNetwork();
                    }

                    break;
            }

            if (mEventsLoaded && mDebtorsLoaded) {
                showData();
            }
        } else if (!(result instanceof Logout.Result)) {
            makeToast(R.string.msg_error_load_data_from_server);
            tryToLoadDataFromDB();
        }
    }

    @Override
    public void refreshAll() {
        mDebtorsLoaded = false;
        mEventsLoaded = false;
    }

    public static void highlightDatesAndSelect(Collection<Date> highlightedDates, int position) {
        if (mCalendarPickerView != null
                && mSelectedPosition == position
                && mIsNeedUpdateHighlightedDates) {
            mCalendarPickerView.highlightDates(highlightedDates, true);
            mCalendarPickerView.selectDate(mCalendarPickerView.getSelectedDate());
            mIsNeedUpdateHighlightedDates = false;
        }
    }

    @Override
    public void onEventFilterChanged(EventFilterType eventType, Long dateFrom, Long dateTo) {
        setCalendarPickerSelectedDateAsToday();

        refreshInfo(mSelectedPosition, eventType, dateFrom, dateTo);
    }

    private void startStopAnimation(boolean flag) {
        if (flag) {
            mProgressCircle.startAnimation(mAnimationRotate);
        } else {
            mProgressCircle.clearAnimation();
        }
    }

    private void setCalendarPickerSelectedDateAsToday() {
        setCalendarPickerSelectedDate(new Date());
    }

    private void setCalendarPickerSelectedDate(Long date) {
        if (date != null) {
            setCalendarPickerSelectedDate(new Date(date));
        }
    }

    private void setCalendarPickerSelectedDate(Date date) {
        if (mCalendarPickerView != null) {
            mCalendarPickerView.selectDate(date);
        }
    }

}
