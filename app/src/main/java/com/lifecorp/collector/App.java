package com.lifecorp.collector;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.lifecorp.collector.model.database.helpers.DBHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

import io.fabric.sdk.android.Fabric;

public class App extends Application {

    private static App sInstance;

    // network const
    public static boolean IS_NETWORK_AVAILABLE;
    // const is able google services
    public static boolean GOOGLE_SERVICE_IS_ENABLE;

    public App() {
        sInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }

        DBHelper.init(this);

        IS_NETWORK_AVAILABLE = getNetworkState();
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(sInstance);
        App.GOOGLE_SERVICE_IS_ENABLE = (status == ConnectionResult.SUCCESS);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ico_spisok_no_photo)
                .showImageOnFail(R.drawable.ico_spisok_no_photo)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .preProcessor(new BitmapProcessor() {
                    @Override
                    public Bitmap process(Bitmap source) {
                        int size = Math.min(source.getWidth(), source.getHeight());
                        int x = (source.getWidth() - size) / 2;
                        int y = (source.getHeight() - size) / 2;
                        Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
                        if (result != source) {
                            source.recycle();
                        }
                        return result;
                    }
                })
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(options)
                .diskCacheFileCount(300)
                .build();
        ImageLoader.getInstance().init(config);
    }

    private boolean getNetworkState() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static Context getContext() {
        return sInstance;
    }

}
